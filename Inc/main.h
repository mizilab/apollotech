/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_dma.h"

#include "stm32f4xx_ll_exti.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DET_ENET_ACT_Pin GPIO_PIN_2
#define DET_ENET_ACT_GPIO_Port GPIOE
#define EXTI_AP_HV_OUT_Pin GPIO_PIN_3
#define EXTI_AP_HV_OUT_GPIO_Port GPIOE
#define EXTI_AP_HV_OUT_EXTI_IRQn EXTI3_IRQn
#define EXTI_AP_ALM_RESET_Pin GPIO_PIN_4
#define EXTI_AP_ALM_RESET_GPIO_Port GPIOE
#define EXTI_AP_ALM_RESET_EXTI_IRQn EXTI4_IRQn
#define nAP_ALM_STATUS_Pin GPIO_PIN_5
#define nAP_ALM_STATUS_GPIO_Port GPIOE
#define nAP_BOARD_DET_Pin GPIO_PIN_6
#define nAP_BOARD_DET_GPIO_Port GPIOE
#define DAC_HV_PWR_CONTROL_Pin GPIO_PIN_13
#define DAC_HV_PWR_CONTROL_GPIO_Port GPIOC
#define nDET_MODULE_CHARGER_Pin GPIO_PIN_14
#define nDET_MODULE_CHARGER_GPIO_Port GPIOC
#define nDET_BATTERY_Pin GPIO_PIN_15
#define nDET_BATTERY_GPIO_Port GPIOC
#define nSW_DOWN_Pin GPIO_PIN_6
#define nSW_DOWN_GPIO_Port GPIOF
#define nSW_LEFT_Pin GPIO_PIN_7
#define nSW_LEFT_GPIO_Port GPIOF
#define nSW_RIGHT_Pin GPIO_PIN_8
#define nSW_RIGHT_GPIO_Port GPIOF
#define nSW_HV_PWR_Pin GPIO_PIN_9
#define nSW_HV_PWR_GPIO_Port GPIOF
#define nSW_TOGGLE_Pin GPIO_PIN_10
#define nSW_TOGGLE_GPIO_Port GPIOF
#define nRELAY_HV_A_POS_Pin GPIO_PIN_0
#define nRELAY_HV_A_POS_GPIO_Port GPIOC
#define nRELAY_HV_B_POS_Pin GPIO_PIN_1
#define nRELAY_HV_B_POS_GPIO_Port GPIOC
#define cLCD_OE_Pin GPIO_PIN_2
#define cLCD_OE_GPIO_Port GPIOC
#define cLCD_DIR_Pin GPIO_PIN_3
#define cLCD_DIR_GPIO_Port GPIOC
#define ADC_BAT_V_Pin GPIO_PIN_0
#define ADC_BAT_V_GPIO_Port GPIOA
#define ADC_BAT_I_Pin GPIO_PIN_1
#define ADC_BAT_I_GPIO_Port GPIOA
#define ADC_HV_A_V_Pin GPIO_PIN_2
#define ADC_HV_A_V_GPIO_Port GPIOA
#define ADC_HV_A_I_Pin GPIO_PIN_3
#define ADC_HV_A_I_GPIO_Port GPIOA
#define ADC_HV_B_V_Pin GPIO_PIN_4
#define ADC_HV_B_V_GPIO_Port GPIOA
#define ADC_HV_B_I_Pin GPIO_PIN_5
#define ADC_HV_B_I_GPIO_Port GPIOA
#define ADC_AP_HV_V_Pin GPIO_PIN_6
#define ADC_AP_HV_V_GPIO_Port GPIOA
#define nDET_MODULE_ENET_Pin GPIO_PIN_7
#define nDET_MODULE_ENET_GPIO_Port GPIOA
#define nDAC_HV_SHUTDOWN_Pin GPIO_PIN_4
#define nDAC_HV_SHUTDOWN_GPIO_Port GPIOC
#define nDET_MODULE_LORA_Pin GPIO_PIN_5
#define nDET_MODULE_LORA_GPIO_Port GPIOC
#define LAN_INT_Pin GPIO_PIN_0
#define LAN_INT_GPIO_Port GPIOB
#define LAN_RESET_Pin GPIO_PIN_1
#define LAN_RESET_GPIO_Port GPIOB
#define nSW_SELECT_Pin GPIO_PIN_11
#define nSW_SELECT_GPIO_Port GPIOF
#define nSW_CANCEL_Pin GPIO_PIN_15
#define nSW_CANCEL_GPIO_Port GPIOF
#define nLED_RUN_Pin GPIO_PIN_0
#define nLED_RUN_GPIO_Port GPIOG
#define nLED_CHARGED_Pin GPIO_PIN_1
#define nLED_CHARGED_GPIO_Port GPIOG
#define COMM_SCL_Pin GPIO_PIN_10
#define COMM_SCL_GPIO_Port GPIOB
#define COMM_SDA_Pin GPIO_PIN_11
#define COMM_SDA_GPIO_Port GPIOB
#define BUZZER_Pin GPIO_PIN_12
#define BUZZER_GPIO_Port GPIOB
#define nDAC_LDAC_Pin GPIO_PIN_13
#define nDAC_LDAC_GPIO_Port GPIOB
#define nDAC_CLR_Pin GPIO_PIN_14
#define nDAC_CLR_GPIO_Port GPIOB
#define nSW_UP_Pin GPIO_PIN_15
#define nSW_UP_GPIO_Port GPIOB
#define cLCD_RST_Pin GPIO_PIN_11
#define cLCD_RST_GPIO_Port GPIOD
#define cLCD_RS_Pin GPIO_PIN_12
#define cLCD_RS_GPIO_Port GPIOD
#define cLCD_RW_Pin GPIO_PIN_13
#define cLCD_RW_GPIO_Port GPIOD
#define cLCD_EN_Pin GPIO_PIN_2
#define cLCD_EN_GPIO_Port GPIOG
#define nLED_ALM_BAT_Pin GPIO_PIN_3
#define nLED_ALM_BAT_GPIO_Port GPIOG
#define nLED_ALM_OCP_Pin GPIO_PIN_4
#define nLED_ALM_OCP_GPIO_Port GPIOG
#define nLED_ALM_ARC_Pin GPIO_PIN_5
#define nLED_ALM_ARC_GPIO_Port GPIOG
#define cLCD_DB0_Pin GPIO_PIN_6
#define cLCD_DB0_GPIO_Port GPIOG
#define PRESS_INT1_Pin GPIO_PIN_7
#define PRESS_INT1_GPIO_Port GPIOG
#define PRESS_INT2_Pin GPIO_PIN_8
#define PRESS_INT2_GPIO_Port GPIOG
#define LORA_TX_Pin GPIO_PIN_6
#define LORA_TX_GPIO_Port GPIOC
#define LORA_RX_Pin GPIO_PIN_7
#define LORA_RX_GPIO_Port GPIOC
#define LORA_WAKE_Pin GPIO_PIN_8
#define LORA_WAKE_GPIO_Port GPIOC
#define LORA_STAT_Pin GPIO_PIN_9
#define LORA_STAT_GPIO_Port GPIOC
#define LORA_MODE_Pin GPIO_PIN_8
#define LORA_MODE_GPIO_Port GPIOA
#define LORA_BUSY_Pin GPIO_PIN_9
#define LORA_BUSY_GPIO_Port GPIOA
#define nLORA_RESET_Pin GPIO_PIN_10
#define nLORA_RESET_GPIO_Port GPIOA
#define cLCD_DB1_Pin GPIO_PIN_11
#define cLCD_DB1_GPIO_Port GPIOA
#define cLCD_DB2_Pin GPIO_PIN_12
#define cLCD_DB2_GPIO_Port GPIOA
#define cLCD_DB3_Pin GPIO_PIN_10
#define cLCD_DB3_GPIO_Port GPIOC
#define cLCD_DB4_Pin GPIO_PIN_11
#define cLCD_DB4_GPIO_Port GPIOC
#define PLC_TX_Pin GPIO_PIN_12
#define PLC_TX_GPIO_Port GPIOC
#define PLC_RX_Pin GPIO_PIN_2
#define PLC_RX_GPIO_Port GPIOD
#define cLCD_DB5_Pin GPIO_PIN_3
#define cLCD_DB5_GPIO_Port GPIOD
#define cLCD_DB6_Pin GPIO_PIN_6
#define cLCD_DB6_GPIO_Port GPIOD
#define cLCD_DB7_Pin GPIO_PIN_7
#define cLCD_DB7_GPIO_Port GPIOD
#define LAN_CS_Pin GPIO_PIN_9
#define LAN_CS_GPIO_Port GPIOG
#define nTEMP_CS1_Pin GPIO_PIN_10
#define nTEMP_CS1_GPIO_Port GPIOG
#define nTEMP_CS2_Pin GPIO_PIN_11
#define nTEMP_CS2_GPIO_Port GPIOG
#define SPI_MISO_Pin GPIO_PIN_12
#define SPI_MISO_GPIO_Port GPIOG
#define SPI_SCK_Pin GPIO_PIN_13
#define SPI_SCK_GPIO_Port GPIOG
#define SPI_MOSI_Pin GPIO_PIN_14
#define SPI_MOSI_GPIO_Port GPIOG
#define nLED_WLINK_Pin GPIO_PIN_15
#define nLED_WLINK_GPIO_Port GPIOG
#define TEST_Pin GPIO_PIN_5
#define TEST_GPIO_Port GPIOB
#define CONSOLE_TX_Pin GPIO_PIN_6
#define CONSOLE_TX_GPIO_Port GPIOB
#define CONSOLE_RX_Pin GPIO_PIN_7
#define CONSOLE_RX_GPIO_Port GPIOB
#define DAC_SCL_Pin GPIO_PIN_8
#define DAC_SCL_GPIO_Port GPIOB
#define DAC_SDA_Pin GPIO_PIN_9
#define DAC_SDA_GPIO_Port GPIOB
#define GLCD_RX_Pin GPIO_PIN_0
#define GLCD_RX_GPIO_Port GPIOE
#define GLCD_TX_Pin GPIO_PIN_1
#define GLCD_TX_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
