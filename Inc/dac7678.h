#ifdef __cplusplus
extern "C" {
#endif
#include "main.h"

void dac7678Init(I2C_HandleTypeDef *hi2c);
void dac7678Reset(void);
void dac7678SetVREF(uint8_t _refstate);
void dac7678LDAC(uint8_t _state);
void dac7678LDACChannel(unsigned char _channel, uint8_t _state); 
void dac7678OffMode(unsigned char mode);
void dac7678OffModeChannel(unsigned char channel, unsigned char mode);
void dac7678Enable(void);
void dac7678Disable(void);
void dac7678EnableChannel(unsigned char channel);
void dac7678DisableChannel(unsigned char channel);
void dac7678Set(unsigned char channel, int _value);
void dac7678Select(unsigned char _channel);
void dac7678Update(unsigned char _channel, int _value);
void dac7678ClrMode(int _value);

unsigned int dac7678ReadChan(unsigned char _command);

void dac7678Transmit(unsigned char _command, unsigned char _msdb, unsigned char _lsdb);

#define DAC7678_VREF_MODE_STATIC_CMD          0x80
#define DAC7678_VREF_MODE_FLEXIBLE_CMD        0x90
#define DAC7678_VREF_MODE_STATIC_ON           0x10
#define DAC7678_VREF_MODE_STATIC_OFF          0x00
#define DAC7678_VREF_MODE_FLEXIBLE_ON         (0x40 << 8)
#define DAC7678_VREF_MODE_FLEXIBLE_ALWAYS_ON  (0x50 << 8)
#define DAC7678_VREF_MODE_FLEXIBLE_ALWAYS_OFF (0x60 << 8)
#define DAC7678_VREF_MODE_SWITCH              0x00

extern int dac7678Setup (const int pinBase, const int i2cAddress, const unsigned short vrefMode) ;

//#define ENABLE 1
//#define DISABLE 0

// VREF options (Flexible Mode)
#define DAC7678_EXT_REF 0 // External VREF
#define DAC7678_INT_REF 1 // Internal VREF

// DAC power mode
#define L100K 0x40
#define L1K 0x20
#define HIGHZ 0x60
#define ON	1
#define OFF 0

// Clear pin Modes
#define DAC7678_CLR_NO 		3
#define DAC7678_CLR_MID 	1
#define DAC7678_CLR_FULL 	2
#define DAC7678_CLR_ZERO 	0

// Channel # to Pin name numbering
#define PIN_A 0
#define PIN_B 1
#define PIN_C 2
#define PIN_D 3
#define PIN_E 4
#define PIN_F 5
#define PIN_G 6
#define PIN_H 7

#define CMD_WRITE_BASE_ADDR     0x00
#define CMD_SELECT              0x10
#define CMD_IS_LDAC_BASE_ADDR   0x30
#define CMD_POWER_DOWN          0x40
#define CMD_CLEAR_CODE          0x50
#define CMD_LDAC                0x60
#define CMD_SOFT_RESET          0x70
#define CMD_INTREF_RS           0x80
#ifdef __cplusplus
}
#endif
