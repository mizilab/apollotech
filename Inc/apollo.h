#ifndef APOLLO_H
#define APOLLO_H

#include "stdint.h"

#include "../Wiznet/Ethernet/wizchip_conf.h"
#include "../Wiznet/Application/loopback/loopback.h"
#include "../Wiznet/Internet/DHCP/dhcp.h"
#include "../Wiznet/Ethernet/socket.h"
#include "dataformat.h"

/****************************************/
// Defines
#define FW_VER	102 // 100~250

#define BOARD_REV2	1

// 아폴로 요청: Alaram Control (OCP, ARC, Battery)은 출력(HV) 제어에만
#define ALARM_CONTROL_HV_ONLY	1

#define	N_MIN_ALARAM_DURATION	20	

#define ACK_FOR_JOIN		0 //60000

// ARC limit 수치
#define LIMIT_ARC	2530
#define LIMIT_ARC_MINMAX	500
// ARC 발생 시 최대치로 설정: 존재할 수 없는 값
#define N_ARC_MAX	3400
#define N_MAX_EDGE_COUNT	5
#define N_EDGE_JITTER	50

#define CLEAR_ALARMS_ANY_EVENT	1

#define USE_BATT_PERCENT_TABLE	1

#define USE_ADC_DMA		1
#define ADC_INTERRUPT	1

#define IS_HV_ON	( (pInfo->volatile_stat.hvOutputStatus & 0x01)==0x01 )

#define	BUZZER_MUTE	pInfo->setting.mute	//1
#define	REPEAT_KEY	1

#define BEEP_IN_IO_TASK	1
#define	BUZ_DELAY	2

#define SWITCH_MODE_DET	0

#define DET_LORA		!HAL_GPIO_ReadPin(nDET_MODULE_LORA_GPIO_Port, nDET_MODULE_LORA_Pin)
#define DET_LAN			1	//!HAL_GPIO_ReadPin(nDET_MODULE_ENET_GPIO_Port, nDET_MODULE_ENET_Pin)
#define DET_AP 			!HAL_GPIO_ReadPin(nAP_BOARD_DET_GPIO_Port, nAP_BOARD_DET_Pin)

#define DET_LAN_LINK	1 //HAL_GPIO_ReadPin(DET_ENET_ACT_GPIO_Port, DET_ENET_ACT_Pin)

#define DET_BATTERY		!HAL_GPIO_ReadPin(nDET_BATTERY_GPIO_Port, nDET_BATTERY_Pin)
#define DET_CHARGER		HAL_GPIO_ReadPin(nDET_MODULE_CHARGER_GPIO_Port, nDET_MODULE_CHARGER_Pin)

#define N_ADC_CHANNELS	8

#define PING_ENABLE	1

#define LAN_LORA_DUAL	0

#define SN_LE_TX	1
#define SN_SERVER_TX	2
#define SN_SERVER_RX	3

#define SN_MONITOR_RX	4
#define SN_MONITOR_TX	5

#define TEST_W5300_OFF	0
#define TEST_NO_LE_TX	0
#define TEST_NO_LAN_TX	0
#define TEST_NO_LAN_RX	0


#define STR_GREET_0	"SOLUSON Co.,LTD"
#define STR_GREET_1	"SPR-3.0K"

#define	USE_CLCD_4BIT_DB		0
#define USE_4_LINE_RELAY_CONTROL	0

#define ARC_CONDITION_CURR	98

#define	AP_MODE_ON	1

// Set 300 mSec for change volt
#define HV_CHANGE_TIME	300

#define CLCD_USE_MACRO	1
#define CLCD_NO_AUTO_REFRESH	1

#define PLC_FROM_WHERE	plcFromWhere	//GetEscInfo()->setting.lanLora

#define BATT_WARN_TIME	50
#define BATT_COMPLETE_TIME	50

#define	USE_LAN_AND_SERIAL	1

#define BATT_CHG_CHK_COUNT	2

#define BATT_COMPLETE_PERCENT	98

#define	BATT_COEFITIENT	143 //141
#define BATT_MIN	7.6
#define BATT_MAX	12.2

#define BLINK_WLINK	1

#define DISP_CURR_ZERO	0

#define CURSOR_TYPE_BLANK	0
#if CURSOR_TYPE_BLANK
#define CURSOR_SET	0x0F
#else
#define CURSOR_SET	0x0E
#endif

#define HVPWR_ALWAYS_ON		0

#define	TEST_TEMPERATURE	0

#define LORA_USE_ABP	0

#define	LORA_USE_DIRECT_BUFFER		0

#define LORA_TX_CONFIRM 	0

#define LORA_TX_DIFF_TASK	1

#define LORA_N_MAX_BUF_COUNT	5

#define	RELAY_CON_TYPE	1

#define	KEY_EXTI	0

#define USE_HV_VOLT_FOMULA	1
#define USE_HV_VOLT_TABLE	!USE_HV_VOLT_FOMULA

#define ADC_V_REF	(3300)

#define CHAR_CODE_DEGREE	0xB0	//0xDF

#define N_AVERAGE_COUNT	(20)
#define	SKIP_MIN_MAX	0

#define N_MAX_HV_CURR_MA	100	//표시되는 최대값 1.00 mA
#define CURR_HV_ADC_MAX	(992)  // ADC에서 최대값 0.992mA


#define N_MAX_HV_VOLT	30	//표시되는 최대값 3.0 KV
#define VOLT_HV_ADC_MAX	(2450)  // ADC에서 최대값 2.5V

#define CURR_HV_DAC_MAX_mA	2300
#define N_MAX_HV_CURR	1000 //2500	// 2.5mA, 2500uA -> 1.0mA
#define CURR_HV_DAC_MAX	(CURR_HV_DAC_MAX_mA/DAC_STEP/DAC_VREF) //(1850.0)	//	2800 // 2.8V or 2.0V -> 2.8V

#define	DAC_STEP	4095

#define	ADC_STEP	4095
#define	VOLT_STEP	3000
#define DAC_VREF	3000
#define VOLT_HV_DAC_MAX_KV		2500 //2700
#define VOLT_HV_DAC_MAX_STEP	3509
#define CURR_MAX	CURR_HV_DAC_MAX
#define	CURR_STEP	1000

#define MAX_ADC_CHANNEL	8
#define MAX_GPIO_CHANNEL 8

#define	INPUT_TASK_DELAY	10
#define	INPUT_KEY_TIMEOUT	50	//100

#define	OUTPUT_TASK_DELAY	10
#define	OUTPUT_LCD_REFRESH_TIME	(1000/20) // 25 Hz
#define	OUTPUT_LCD_REFRESH_CURSOR_TIME	(1000/4) // 2Hz // org 200

#define	DAC_DELAY	2

#define USE_TX_IT_PLC	1
#define N_MAX_PARSE_TIME	500

#define N_PLC_RX_BUFFER_SIZE N_MAX_DATA_BUFFER_SIZE
#define N_PLC_TX_BUFFER_SIZE N_MAX_DATA_BUFFER_SIZE

#define MIN_VOLT_SET	0
#define MAX_VOLT_SET	30

//#define MIN_AUTO_DISCHARGE_STEP_COUNT	0 // zero is Not comparable
#define MAX_AUTO_DISCHARGE_STEP_COUNT	3

#define MIN_AUTO_DISCHARGE_VOLT_STEP	5
#define MAX_AUTO_DISCHARGE_VOLT_STEP	30

//#define MIN_AUTO_DISCHARGE_KEEP_SETP	0
#define MAX_AUTO_DISCHARGE_KEEP_SETP	240

#define MIN_RAMP_TIME					3
#define MAX_RAMP_TIME					100

//#define MIN_CONTROL_ONOFF				0
#define MAX_CONTROL_ONOFF				1

#define MAX_BATT_ALARM_LIMIT			100
#define MAX_BATT_CHARGE_STATUS			2

#define MIN_OCP_ALARM_LIMIT				10
#define MAX_OCP_ALARM_LIMIT				100	//250

#define DAC_OCP_CONTROL_MAX_UA			100
//#define DAC_OCP_CONTROL_MAX			150

#define	MIN_ARC_ALARM_LIMIT				0
#define	MAX_ARC_ALARM_LIMIT			65000

#define MIN_WAVE_MODE				1
#define MAX_WAVE_MODE				32
#define MAX_USER_MODE				3

#define MIN_AUTO_QUERY_INTERVAL		1
#define MAX_AUTO_QUERY_INTERVAL		3600

#define BAT_LOW_DELTA		1

#define USE_PLC_QUEUE	1

/****************************************/	
/// Macro



/****************************************/	
/// type defines

struct IOMessage
{
	uint8_t commandID;
	uint8_t argument;
};

struct	OutputMessage
{
	uint8_t commandID;
	uint8_t channel;
	int16_t value;
	int16_t value2;
};

struct LoraMessage
{
	uint8_t length;
	uint8_t payload[20];
};

enum {
	CHECK_TASK_DEFAULT = 0x01,
	CHECK_TASK_INPUT = 0x02,
	CHECK_TASK_OUTPUT = 0x04,
	CHECK_TASK_LORA = 0x08,
	CHECK_TASK_COMM = 0x10,
	CHECK_TASK_LAN = 0x20,
	CHECK_TASK_LANRX = 0x40,
	
	CHECK_TASK_STARTED = 0x7f // Sum of all task bit
};

enum {
	CMD_GET_GPIO  = 0,
	CMD_SET_GPIO,
	CMD_CLEAR_GPIO,
	CMD_TEST,
	CMD_BUZ,
	CMD_KEY,
	CMD_HVOUT_BOOST,
	CMD_TOGGLE,
	
	CMD_CLCD_UPDATE,

	CMD_QLIGHT,
	CMD_QLIGHT_TEST,
	
	CMD_ALARM_BATT_LOW,
	CMD_ALARM_ARC,
	CMD_ALARM_OCP,
	
	OUTPUT_DAC,
	OUTPUT_RELAY,
	OUTPUT_SHUTDOWN,
	OUTPUT_KEY,
	OUTPUT_KEY_PRESSING,
	
	OUTPUT_SYST_LOC,
	OUTPUT_SYST_REM,
	
	OUTPUT_MEAS_ARC,
	OUTPUT_MEAS_CURR,
	OUTPUT_MEAS_VOLT,
	
	OUTPUT_TOGGLE,
	
	OUTPUT_SOUR_VOLT,
	
	OUTPUT_CONF_ARC_STAT,
	OUTPUT_CONF_ARC_LEV,
	OUTPUT_CONF_VOLT,
	OUTPUT_CONF_VOLT2,
	OUTPUT_CONF_CURR,
	
	OUTPUT_AT_COUN,
	OUTPUT_AT_VOLT,
	OUTPUT_AT_STAT,
	
	OUTPUT_BUZ,
	
	
//	OUTPUT_HVOUT,
	OUTPUT_HV_OUT_ONOFF,	
	
	
	OUTPUT_CMID_HV_SET_VOLT_CH1,
	OUTPUT_CMID_HV_SET_VOLT_CH2,
};

enum {
	VALUE_UP = 0,
	VALUE_DOWN
};
enum {
	VALUE_MODE_VOLT = 0,
	VALUE_MODE_AMP,	
	VALUE_MODE_CONTROL,	
};


/*
Red: HV-output alarm
Orange; Battery Low
Green: HV Output status(하나 이상 ON)
Blue:PLC connected
White: Lora coonnected
*/
enum {
	QLIGHT_RED_ON =  0x01,
	QLIGHT_RED_OFF = 0x10,
	QLIGHT_ORANGE_ON =  0x02,
	QLIGHT_ORANGE_OFF = 0x20,
	QLIGHT_GREEN_ON =  0x04,
	QLIGHT_GREEN_OFF = 0x40,
	QLIGHT_BLUE_ON =  0x08,
	QLIGHT_BLUE_OFF = 0x80,
	QLIGHT_ALL_OFF = 0xff,
};


enum {
	CLCD_CMD_CLEAR = 0x9F,
};

enum {
	HV_STATE_READY = 0,
	
	HV_STATE_RAMPUP_AT2_UP_START,
	HV_STATE_RAMPUP_AT2_UP,
	HV_STATE_RAMPUP_AT2_UP_DONE,
	HV_STATE_RAMPUP_AT2_DOWN,
	
	HV_STATE_ON_START,
	HV_STATE_RAMPUP,
	HV_STATE_RAMPUP_DONE,
	HV_STATE_OFF_START,
	HV_STATE_STEPDOWN1,
	HV_STATE_STEPUP1, // 10
	HV_STATE_KEEP_1, 
	HV_STATE_STEPDOWN2,
	HV_STATE_STEPUP2,
	HV_STATE_KEEP_2,
	HV_STATE_STEPDOWN3, //15,
	HV_STATE_STEPUP3, // 0x10
	HV_STATE_KEEP_3,
	HV_STATE_RAMPDOWN_START,
	HV_STATE_RAMPDOWN_DONE,
	
	HV_STATE_ALARM = 0xff
};

/**
adc 0: Batt volt
adc 1: Batt curr

adc 2: Hv A volt
adc 3: Hv A curr
adc 4: Hv B Volt
adc 5: Hv B Curr

adc 6: Hv V Set
adc 7: Charge curr
  */
enum {
	ESC_ADC_CH_BAT_V = 0,
	ESC_ADC_CH_BAT_I ,
	ESC_ADC_CH_HV_A_OUT_V , //2
	ESC_ADC_CH_HV_A_OUT_I ,
	ESC_ADC_CH_HV_B_OUT_V , //4
	ESC_ADC_CH_HV_B_OUT_I ,
	ESC_ADC_CH_AP_HV_V ,
	ESC_ADC_CH_INTERNAL_TEMP ,
};

enum {
	PROTOCOL_SCPI = 0,
	PROTOCOL_CUSTOM,
};




/****************************************/
/// Constants...



/****************************************/
/// extern vars...

/****************************************/
/// extern FN declaration

extern void vOutputString( const char * const pcMessage );
extern uint8_t g_checkTaskStart;

/****************************************/
/// FN declaration
#endif

