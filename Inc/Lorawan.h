#ifndef __LORAWAN_H
#define __LORAWAN_H

#include "main.h"
#include "cmsis_os.h"
#include "apollo.h"

extern UART_HandleTypeDef huart6;

typedef enum {
	MODE_INT,
	MODE_CMD,		//mode pin high
	MODE_TRANSPARENT,
} LoraNode_Mode_T;

typedef enum {
	NET_ABP,
	NET_OTAA,
} LoraNode_NetMode_T;

typedef enum {
	MODE_WAKEUP,
	MODE_SLEEP,	
} LoraNode_SleepMode_T;

typedef enum {
	CLASS_A = 0,
	CLASS_C = 2
} LoraNode_Class_T;

typedef struct {
	uint8_t DevEUI[8];	
	uint8_t AppEUI[8];	
	uint8_t DevADDR[4];	
	uint8_t AppKEY[16];	
	uint8_t AppSKEY[16];	
	uint8_t NwkSKEY[16];

	uint8_t ver;

	uint8_t Join;
	LoraNode_Mode_T MODE;
	uint8_t ONline;
	uint8_t BUSY;
	uint8_t NET_Mode;
	uint8_t Confirm;
	uint8_t ADR;
	uint8_t SF;
	uint8_t NBT;
	uint8_t BAND;
	uint8_t CLASS;
	uint8_t POWER_code;
	uint8_t POWER;

	uint8_t P2P_Mode;
	uint8_t P2P_SF;
	uint16_t P2P_FRQ;
	uint8_t P2P_FRQ_step;
	uint8_t P2P_PWR;
	uint8_t P2P_PWR_code;
	uint8_t P2P_TIME;
	uint8_t P2P_RSSI[5];
	uint8_t P2P_SNR[5];
} Node_Info;

typedef struct {
	uint8_t Up_Result;	
	uint8_t Up_CH;	
	uint8_t Up_RATE;	
	uint8_t Up_DB;	
	uint8_t Dn_CH;	
	uint8_t Dn_RATE;
	uint8_t RFU0;
	uint8_t Rx_Slot;
	uint8_t Port;
	uint8_t RFU1;
	uint8_t RFU2;
	uint8_t SNR;
	uint8_t RFU3;
	uint16_t RSSI;
	uint32_t Dn_COUNT;
	uint16_t Remain;
	uint16_t Ret;
	uint32_t RFU4;
	uint8_t RFU5;
	uint32_t RFU6;
	uint32_t RFU7;
	uint32_t Up_COUNT;
} Send_Info;


/* stop the wfi sleep */	
#define LORANODE_WAKE_HIGH()      HAL_GPIO_WritePin(LORA_WAKE_GPIO_Port, LORA_WAKE_Pin, GPIO_PIN_SET)
#define LORANODE_WAKE_LOW()     HAL_GPIO_WritePin(LORA_WAKE_GPIO_Port, LORA_WAKE_Pin, GPIO_PIN_RESET)													
													
#define LORANODE_MODE_HIGH()      HAL_GPIO_WritePin(LORA_MODE_GPIO_Port, LORA_MODE_Pin, GPIO_PIN_SET)
#define LORANODE_MODE_LOW()     HAL_GPIO_WritePin(LORA_MODE_GPIO_Port, LORA_MODE_Pin, GPIO_PIN_RESET) 

#define LORANODE_NRST_HIGH()      HAL_GPIO_WritePin(nLORA_RESET_GPIO_Port, nLORA_RESET_Pin, GPIO_PIN_SET)
#define LORANODE_NRST_LOW()     HAL_GPIO_WritePin(nLORA_RESET_GPIO_Port, nLORA_RESET_Pin, GPIO_PIN_RESET)

#define LORANODE_STAT_STATUS	HAL_GPIO_ReadPin(LORA_STAT_GPIO_Port, LORA_STAT_Pin)
#define LORANODE_BUSY_STATUS	HAL_GPIO_ReadPin(LORA_BUSY_GPIO_Port, LORA_BUSY_Pin)
#define LORANODE_MODE_STATUS	HAL_GPIO_ReadPin(LORA_MODE_GPIO_Port, LORA_MODE_Pin)
#define LORANODE_WAKE_STATUS	HAL_GPIO_ReadPin(LORA_WAKE_GPIO_Port, LORA_WAKE_Pin)

void LoraNode_GPIO_Init(void);
void LoraNode_Init(void);
void LoraNode_Mode_Set(LoraNode_Mode_T mode);
void LoraNode_Wake_Sleep(LoraNode_SleepMode_T mode);
static void LoraNode_Reset(void);
char *LoraNode_GetVer(void);
void LoraNode_GetDevEUI(Node_Info *LoRa_temp);
void LoraNode_GetAppEUI(Node_Info *LoRa_temp);
void LoraNode_GetAppKey(Node_Info *LoRa_temp);
void LoraNode_GetDevAddr(Node_Info *LoRa_temp);
void LoraNode_GetAppSKey(Node_Info *LoRa_temp);
void LoraNode_GetNwkSKey(Node_Info *LoRa_temp);
void LoraNode_GetNetMode(Node_Info *LoRa_temp);
void LoraNode_GetConfirm(Node_Info *LoRa_temp);
void LoraNode_GetADR(Node_Info *LoRa_temp);
void LoraNode_GetVER(Node_Info *LoRa_temp);
void LoraNode_GetSF(Node_Info *LoRa_temp);
void LoraNode_GetNBT(Node_Info *LoRa_temp);
void LoraNode_GetBAND(Node_Info *LoRa_temp);
void LoraNode_GetCLASS(Node_Info *LoRa_temp);
void LoraNode_GetSNR(Node_Info *LoRa_temp);
void LoraNode_GetPOWER(Node_Info *LoRa_temp);
void LoraNode_GetState(Send_Info *LoRa_temp);

int32_t LoraNode_SetGPIO(uint32_t pin, uint32_t state);
int32_t LoraNode_SetNetMode(LoraNode_NetMode_T mode);
int32_t LoraNode_SetCFM(uint8_t nu);
int32_t LoraNode_SetADR(uint8_t nu);
int32_t LoraNode_SetSF(uint8_t nu);
int32_t LoraNode_SetNBT(uint8_t nu);
int32_t LoraNode_SetBAND(uint8_t nu);
int32_t LoraNode_SetCLASS(uint8_t nu);
int32_t LoraNode_SetPOWER(uint8_t nu);
int32_t LoraNode_SetDebug(uint32_t value);
int32_t LoraNode_SetRADIO(uint32_t f);
int32_t LoraNode_SetP2P(uint32_t f,uint8_t a,uint8_t b,uint8_t c,uint8_t d,uint8_t e,uint8_t ff,uint8_t g,uint16_t h);
int32_t LoraNode_SetMINI_RF(uint32_t value);
int32_t LoraNode_SetAppEUI(uint8_t *dev);
int32_t LoraNode_SetAppKey(uint8_t *key);
int32_t LoraNode_SetAppSKey(uint8_t *skey);
int32_t LoraNode_SetNwkSKey(uint8_t *nwk_skey);

int32_t LoraNode_Save(void);

void LoraNode_OTAA_Config(void);
void LoraNode_ABP_Config(void);

void GET_LoraNode_Info(Node_Info *lora_temp);
void LoraNode_STA_Check(Node_Info *LoRa_temp);

void LoraNode_Send_AT(uint8_t *at_buf);
void LoraNode_Read(uint8_t *str);

int8_t LoRaNode_LowPower_Send(uint8_t *buffer, int32_t len,uint8_t CONFIRM);
int32_t LoraNode_Write_Receive_Bytes(uint8_t *buffer, int32_t len,uint8_t CONFIRM);
int32_t loraReqUplink(uint8_t *buffer, int32_t len, uint8_t CONFIRM);

uint32_t LoraNode_IO_State(uint32_t pin);
void LoraNode_Delay(uint32_t Delay);
char *StringStr(char *str, char *dest);


void Printf_LoRaNode_Info(Node_Info LoRa_temp);
uint8_t StrToHex(uint8_t temp);

void TaskLora(void *argument);

#endif 

