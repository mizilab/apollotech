#ifndef __QUEUE_H__

#define __QUEUE_H__


#define MAX_QUEUE_SIZE  256

void enQueue(char data)	;
char deQueue(void);
int nQueueSize(void)	;
char isQueueFull(void);
char isQueueEmpty(void);	





#endif
