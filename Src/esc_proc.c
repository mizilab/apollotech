/**
  ******************************************************************************
  * @file    esc_proc.c
  * @author  jw Lee
  * @version 0.0.0.1
  * @date    03.20.2019
  * @brief   ESC, status/data informations
  ******************************************************************************
  * @contact
  * <h2><center>&copy; COPYRIGHT 2019 jw@mizilab.com </center></h2>
  ******************************************************************************
  */
  
#include "esc_proc.h"
#include "Lorawan.h"
#include "io_tasks.h"

#include <ctype.h>

#define dbg_proc debug_console

uint8_t plcRxData;
#if USE_PLC_QUEUE==0
extern uint8_t plcRxBuffer[N_PLC_RX_BUFFER_SIZE];
extern uint32_t plcRxIndex;
extern uint32_t plcRxSize;		
#endif

uint8_t RxDataRemocon;

uint8_t OnRemoteCLcd(uint8_t state, StructEscInfo *pInfo);

extern U08 plcFromWhere;
extern UART_HandleTypeDef huart8;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
   if (huart->Instance == UART5)
   {
	  PLC_FROM_WHERE = COMM_MODE_SERIAL;

		enQueue(plcRxData);

	   
      HAL_UART_Receive_IT(&huart5, &plcRxData, 1);
   }
	 else if (huart->Instance == USART6)
	 {

#if LORA_USE_DIRECT_BUFFER
		extern uint8_t g_LoraConnected;
		if(g_LoraConnected) {
			PLC_FROM_WHERE = COMM_MODE_LORA;
			// Send rx bytes to plcRx buffer
			enQueue(RxData);
		}
#endif		 
		AT_Data_buf[RxIdx++] = RxData;
		if (RxIdx == RXLEN) {
	     	RxIdx = 0;
			EndOfTrans = 1;
		}

		HAL_UART_Receive_IT(&huart6, &RxData, 1);		
	 }
	 else if (huart->Instance == UART8)
	 {
		  PLC_FROM_WHERE = COMM_MODE_REMOCON;

	   enQueue(RxDataRemocon);

		 HAL_UART_Receive_IT(&huart8, &RxDataRemocon, 1);			
	 }
}

//static uint8_t buff[512]; //org=2048
static StructApolloPacket CustomPacket;
/*
Qlight control: 0=off, 1=on, 2=toggle, 0x64=as is
byte order:	cmd, dummy, Red, Orange, Green, Blue, White, Sound...
*/
U08 g_CustomState = 0;
void TaskComm(void *argument)
{
	uint16_t tick = 0;
	uint32_t tickLastParse = 0;
	uint32_t tickNow = 0;
	
	uint8_t rxData = 0;

	char plcBuffer[64] = "";
//	uint8_t netState=0;

	
	StructApolloPacket *pCustomPacket = &CustomPacket;
	StructScpi scpiPacket;

	U08 alarmStatus = 0;
	U08 alarmControl = 0;
	U08 alarmEvent = 0;	
	U08 alarmStatusPrev = 0;
	
	U08 waitRxCount = 0;
	
	char *ptr = NULL;

	U08 i, j;
//	U08 bIsStrParam; // disabled by jwaani: to fix warnning
	
	StructEscInfo *pInfo;
	int16_t param;
	int16_t param2;
	U08 nParam;
	
#if USE_PLC_QUEUE==0
	plcRxSize = 0;
	plcRxIndex = 0;
#endif
	
	pInfo = GetEscInfo();
	
	
	osDelayUntil(1000);
	
	// Must start HAL_UART_Receive_IT() first to Get Rx IT
	HAL_UART_Receive_IT(&huart8, &RxDataRemocon, 1);
	

	g_checkTaskStart |= CHECK_TASK_COMM;

	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(1000);
		dbg_proc(9, "Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}
  /* Infinite loop */
	for(;;)
	{
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
		
		tick++;
	
		tickNow = HAL_GetTick();
		if(tickLastParse==0) {
			tickLastParse = tickNow;
		}
		
		if(abs((int)tickNow - (int)tickLastParse)>N_MAX_PARSE_TIME) {
			//dbg_proc(1, "Parsing time is too long! %d %d %d\r\n",tickNow-tickLastParse, tickNow, tickLastParse);
			if(g_CustomState != STATE_READY) {
				//g_CustomState = STATE_READY;
				//dbg_proc(1, "Custom State is not changed too long!, Reset to READY\r\n");
			}
			tickLastParse = tickNow;
		}
#if 0		
		if(waitRxCount>0) {
			waitRxCount--;
		}	
		else 
#endif

		if( nQueueSize()> 0 ) {

#if BLINK_WLINK			 // LED On
//	HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_RESET);
#endif
			
			
			rxData = deQueue();			
			
			extern osMessageQueueId_t queueRxCommHandle;			
			//osMessageQueuePut(queueRxCommHandle, &rxData, NULL, NULL);	

			//if(rxData != 0 || state!=0) {  // void NULL data when state is READY: wait for ST or valid data
			if(1) {  // parse direct!
				

				if(pInfo->setting.protocol==PROTOCOL_SCPI) { // SCPI protocol
					if(rxData==0) {
						continue;
					}
					rxData = toupper(rxData);
					g_CustomState = ParseSCPI(g_CustomState, rxData, &scpiPacket);
					
					if(g_CustomState == SCPI_STATE_PARSE_DONE) {
						//waitRxCount = 50;
						uint8_t cmdIndex, subCmdIndex, bCmdFound;
						g_CustomState = SCPI_STATE_READY;					
						osDelay(2);
						
						
						if( Strlen(g_dataBuf) <2 ) { // Too short command!
	//						goto ERR_CMD;
							continue;
						}
						
						cmdIndex=0;
						subCmdIndex=0;
						bCmdFound = 0;
						for(i=0;i<N_MAX_SCPI_CMD_COUNT;i++) {
							for(j=0;j<3;j++) {
								char *compCmd = (char*)StrScpiCommands[i][j];
								if(compCmd != NULL && Strlen(compCmd)!=0) {
									if(Strncmp(compCmd, g_dataBuf, Strlen(compCmd)) == 0)
									{
										cmdIndex=i;
										subCmdIndex=j;	
										bCmdFound = 1;
										break;
									}
								}								
							}
							if(bCmdFound) {
								break;
							}
						}
						
						ptr = strstr((char*)g_dataBuf, "?");
						if( ptr!=NULL) {
							scpiPacket.bGet = 1;
						}
						else {
							scpiPacket.bGet = 0;
						}					
						//StrScpiCommands[N_MAX_SCPI_CMD_COUNT]
						if(bCmdFound) {
							nParam = 0;
							
							if(scpiPacket.bGet != 1) 
							{
								uint8_t offset = Strlen(StrScpiCommands[cmdIndex][subCmdIndex]);
								ptr = Trim((char*)g_dataBuf+offset);
								Strcpy(scpiPacket.param, ptr);
								if(isStringDouble((char*)scpiPacket.param)) 
								{
									//bIsStrParam = 0; // disabled by jwaani: to fix warnning
									
									//param = atoi(((char*)scpiPacket.param));
									
									if((char*)scpiPacket.param!=NULL) {								
										if(isalpha(scpiPacket.param[0])) {
											if( strcmp("ON", (char*)scpiPacket.param) == 0
												|| strcmp("on", (char*)scpiPacket.param) == 0
											) {
												param = 1;
											}
											else {
												param = 0;
											}
										}
										else {
											param = atoi((char*)scpiPacket.param);
										}
										nParam = 1;
									}
									else {
										param = 0;
									}
								}
								else {
									dbg_proc(1, "Param is NOT String or number: %s\r\n", scpiPacket.param);
									ptr = strstr((char*)scpiPacket.param, ",");
									if( ptr!=NULL) { // multi parameters...
										ptr = strtok((char*)scpiPacket.param, ","); // Find first
										if(ptr!=NULL) {
											param = atoi(ptr);
											dbg_proc(1, "param %d\r\n", param);
											ptr = strtok(NULL, ",");  // Find second param...
											if(ptr!=NULL) {
												param2 = atoi(ptr);
												dbg_proc(1, "param2 %d\r\n", param2);
												nParam = 2;
											}
											else {
												dbg_proc(1, "Parse Error param2: %s\r\n", scpiPacket.param);
												param2 = 0;
												nParam = 1;
											}
										}
										else {
											param = 0;
											param2 = 0;
											dbg_proc(1, "Parse Error param: %s\r\n", scpiPacket.param);
										}
									}
									else {
										
									}	
									//bIsStrParam = 1; // disabled by jwaani: to fix warnning
								}
							}
									
							switch(cmdIndex)
							{
								case SCPI_CMD_CLS:	//	{ "CLS" }	 // clear error queue	
									//TODO
									break;
								case SCPI_CMD_IDN:	//,{ "IDN?" }	// ESC Controller 식별 코드를 반환, Get ONLY ex> ESC Controller SOA 2.5K 0 V2.0.016
								//	sprintf(plcBuffer, "%s\n", GetEscInfo()->stat.id);
									//sprintf(plcBuffer, "%s %x \n%s %x\n", "ESC Controller SOA ", GetEscInfo()->setting.hwVer, "V", GetEscInfo()->setting.fwVer);
								sprintf(plcBuffer, "%s %x %s %x\n", GetEscInfo()->setting.serialNumber, GetEscInfo()->setting.hwVer, "V", GetEscInfo()->setting.fwVer);
								
								
									break;
								case SCPI_CMD_RST: //2	,{ "RST" } // reset ESC Controller
									NVIC_SystemReset();
									break;
								case SCPI_CMD_SYST_ERR: //3	,{ "SYST:ERR?" }	// SYSTem command +0, No Error
									sprintf(plcBuffer, "%s\n", "+0, No Error");
									break;
								case SCPI_CMD_SYST_VERS: //4	,{ "SYST:VERS?" } -> ex> 2017.1  
									sprintf(plcBuffer, "%x %x\n", GetEscInfo()->setting.hwVer, GetEscInfo()->setting.fwVer);
								//	sprintf(plcBuffer, "%s\n", GetEscInfo()->stat.ver);
								
									break;
								case SCPI_CMD_SYST_BEEP: //5	,{ "SYST:BEEP" , "BEEP" }
									SetOutput(OUTPUT_BUZ, 0);
									
									break;
								case SCPI_CMD_SYST_LOC:  //6 ,{ "SYST:LOC", "LOC" }
									SetOutput(OUTPUT_SYST_LOC, param);
									break;
								case SCPI_CMD_SYST_REM: //7	,{ "SYST:REM", "REM" }
									SetOutput(OUTPUT_SYST_REM, param);												
									break;
								case SCPI_CMD_OUTP_Q: //8	,{ "OUTP?" , "OUTP:STAT?" }
									sprintf(plcBuffer, "%d\n", GetEscInfo()->volatile_stat.hvOutputStatus);
									break;
								case SCPI_CMD_OUTP_STAT: //9	,{ "OUTP" , "OUTP:STAT" }
									//info.stat.hvOut = (U08)param;		SetEscInfo(&info);								
									SetOutput(OUTPUT_HV_OUT_ONOFF, param);
									break;
								case SCPI_CMD_TOGG_Q: //10	,{ "TOGG?" }
									sprintf(plcBuffer, "%d\n", IS_TOGGLE);
									break;
								case SCPI_CMD_TOGG: //11	,{ "TOGG" }
									//info.stat.toggle = (U08)param;		SetEscInfo(&info);								
									SetOutput(OUTPUT_TOGGLE, param);
									break;
								
								case SCPI_CMD_SOUR_VOLT_PROT_TRIP_Q:	//,{ "SOUR:VOLT:PROT:TRIP?", "VOLT:PROT:TRIP?" }//12
									sprintf(plcBuffer, "%d\n", GetEscInfo()->stat.arcAlarmControl);
									break;
								case SCPI_CMD_SOUR_VOLT_Q: //13	,{ "SOUR:VOLT?", "VOLT?" }		ex> V+2500
									//sprintf(plcBuffer, "V+%d\n", GetEscInfo()->stat.sourVolt);								
									sprintf(plcBuffer, "V+%d\n", GetEscInfo()->stat.hvSetVolt1*100);
									break;
								case SCPI_CMD_SOUR_VOLT: //14	,{ "SOUR:VOLT", "VOLT" }
									//info.stat.voltA = param;
									SetOutput(OUTPUT_SOUR_VOLT, param);								
									break;
								
								case SCPI_CMD_SOUR_CURR_PROT_TRIP_Q: //,{ "SOUR:CURR:PROT:TRIP?", "CURR:PROT:TRIP?" }//15
									sprintf(plcBuffer, "%d\n", GetEscInfo()->stat.ocpAlarmControl);
									break;
								case SCPI_CMD_SOUR_CURR_PROT_TRIP: //,{ "SOUR:CURR:PROT:TRIP?", "CURR:PROT:TRIP?" }//15
									GetEscInfo()->stat.ocpAlarmControl = param;
									break;
								case SCPI_CMD_MEAS_VOLT_Q: //,{ "MEAS:VOLT?", "MEAS?" }//16	ex> V+0000;V-0000
									//sprintf(plcBuffer, "V+%d;V-%d\n", GetEscInfo()->measure.voltA, GetEscInfo()->measure.voltB);
									strcpy(plcBuffer, GetEscMeasVolt());
									break;
								case SCPI_CMD_MEAS_CURR_Q: //,{ "MEAS:CURR?"  }//17	ex> A+0000;A+0000
									//sprintf(plcBuffer, "A+%d;A+%d\n", GetEscInfo()->measure.currA, GetEscInfo()->measure.currB);
									strcpy(plcBuffer, GetEscMeasCurr());
									break;
								case SCPI_CMD_MEAS_ARC_Q: //,{ "MEAS:ARC?", "ARC?"  }//18
									sprintf(plcBuffer, "%d\n", GetEscInfo()->measure.arc);
									break;
								
								case SCPI_CMD_ALM_ARC: //,{ "ALM:ARC" }//19	ex> ??, 아크 Alarm모드가 2(auto)일 경우 P/S -> PLC로 자동 전송, "ALM:ARC"
									//sprintf(plcBuffer, "%d\n", GetEscInfo()->measure.arcAlarmStatus);
									//TODO
									break;
								
								case SCPI_CMD_MEAS_ARC_TRIP_Q: //,{ "MEAS:ARC:TRIP?", "ARC:TRIP?"  }//20			ex> 0 or 1					
									sprintf(plcBuffer, "%d\n", GetEscInfo()->measure.arcTrip);
									break;
								
								case SCPI_CMD_CONF_ARC_LEV_Q: //,{ "CONF:ARC:LEV?", }//21	ex> 120
									sprintf(plcBuffer, "%d\n", GetEscInfo()->stat.arcAlarmLimit);
									break;
								case SCPI_CMD_CONF_ARC_LEV: //,{ "CONF:ARC:LEV", }//22
									SetOutput(OUTPUT_CONF_ARC_LEV, param);								
									break;
								case SCPI_CMD_CONF_ARC_STAT_Q: //,{ "CONF:ARC:STAT?", "CONF:ARC?" }//23	ex> 0 or 1
									sprintf(plcBuffer, "%d\n", GetEscInfo()->stat.confArcStat);
									break;
								case SCPI_CMD_CONF_ARC_STAT: //,{ "CONF:ARC:STAT", "CONF:ARC" }//24
									//info.stat.arcState = param;
									SetOutput(OUTPUT_CONF_ARC_STAT, param);
									break;
								
								case SCPI_CMD_CONF_VOLT_Q: //,{ "CONF:VOLT?", }//25		ex> V+2500
									sprintf(plcBuffer, "V%+4.4d;V%+4.4d\n", ByteToVolt(GetEscInfo()->stat.hvSetVolt1),
												ByteToVolt(GetEscInfo()->stat.hvSetVolt2));
													//, GetEscInfo()->stat.voltB);
									break;
								case SCPI_CMD_CONF_VOLT: //,{ "CONF:VOLT", }//26
									//info.stat.voltA = param;
									if(nParam==1) {
										SetOutput(OUTPUT_CONF_VOLT, param);
									}
									else if(nParam==2) {// A/B multi parameter proc....
										SetOutput2(OUTPUT_CONF_VOLT2, param, param2);
									}
								
									break;
								
								case SCPI_CMD_CONF_CURR_Q: //,{ "CONF:CURR?", }//27	ex> 0500
									sprintf(plcBuffer, "%4.4d\n", GetEscInfo()->stat.ocpAlarmLimit*10);
									break;
								case SCPI_CMD_CONF_CURR: //,{ "CONF:CURR", }//28
									SetOutput(OUTPUT_CONF_CURR, param);
									break;
								
								case SCPI_CMD_CONF_RAMPDOWN_Q: //,{ "CONF:RAMPDOWN?", }//29	ex> 0300;0300
									sprintf(plcBuffer, "%4.4d\n", GetEscInfo()->stat.rampDownTime*100);
									break;
								case SCPI_CMD_CONF_RAMPDOWN: //,{ "CONF:RAMPDOWN", }//30
									GetEscInfo()->stat.rampDownTime = param/100;
									break;
								
								case SCPI_CMD_CONF_RAMPUP_Q: //,{ "CONF:RAMPUP?", }//29	ex> 0300;0300
									sprintf(plcBuffer, "%4.4d\n", GetEscInfo()->stat.rampUpTime*100);
									break;
								case SCPI_CMD_CONF_RAMPUP: //,{ "CONF:RAMPUP", }//30
									GetEscInfo()->stat.rampUpTime = param/100;
									break;
								case SCPI_CMD_CONF_RAMP_Q: //,{ "CONF:RAMP?", }//29	ex> 0300;0300
									sprintf(plcBuffer, "%4.4d;%4.4d\n", GetEscInfo()->stat.rampUpTime*100, GetEscInfo()->stat.rampDownTime*100);
									break;
								case SCPI_CMD_CONF_RAMP: //,{ "CONF:RAMP", }//30
									GetEscInfo()->stat.rampUpTime = param/100;
									GetEscInfo()->stat.rampDownTime = param/100;
									break;

								case SCPI_CMD_STAT: //,{ "STAT?" }//31
									// ex> V+2500;V-2500;A+1000;1;0;0 -> chA전압;ChB전압;ChA전류;ChB전류; 출력상태;토글상태;모드상태(Remote/local)
									//sprintf(plcBuffer, "V%d;V%d;A%d;A%d;%d;%d;%d\n", GetEscInfo()->stat.voltA, GetEscInfo()->stat.voltB ,GetEscInfo()->stat.currA ,GetEscInfo()->stat.currB, GetEscInfo()->stat.hvOut, GetEscInfo()->stat.toggle, GetEscInfo()->stat.mode); 
									strcpy( plcBuffer, GetSTAT());
									break;
								
								case SCPI_CMD_AT_COUN_Q: //,{ "AT:COUN?" }//32 ex> 3
									sprintf(plcBuffer, "%d\n", GetEscInfo()->volatile_stat.toggleCount);
									break;
								case SCPI_CMD_AT_COUN: //,{ "AT:COUN" }//33
									SetOutput(OUTPUT_AT_COUN, param);				
									
									break;
								case SCPI_CMD_AT_VOLT_Q: //,{ "AT:VOLT?" }//34 ex> 0900									
									//sprintf(plcBuffer, "%4.4d\n", GetEscInfo()->stat.atVolt);
									sprintf(plcBuffer, "%4.4d\n", GetEscInfo()->measure.voltA);								
									break;
								case SCPI_CMD_AT_VOLT: //,{ "AT:VOLT", "AT:VOLT:LEV" }//35
									SetOutput(OUTPUT_AT_VOLT, param);
									break;
								
								case SCPI_CMD_AT_Q: //,{ "AT?" }//36
									sprintf(plcBuffer, "%d\n", GetEscInfo()->stat.hvAutoToggle);
									break;
								case SCPI_CMD_AT: //,{ "AT", "AT:STAT" }//37
									SetOutput(OUTPUT_AT_STAT, param);								
									break;
								
								default:									
									//sprintf(plcBuffer, "NA!\n");
									break;
							}
						}
						else {
							//sprintf(lcdBuffer, "NA! ");
							dbg_proc(1, "SCPI Cmd Not found !\r\n\r>");
						}
						
						
						if(Strlen(plcBuffer) > 0) {
							HAL_UART_Transmit(&huart5, (uint8_t*)plcBuffer, strlen(plcBuffer), 64);
							
							if(PLC_FROM_WHERE==COMM_MODE_LAN) {
								//HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_RESET);
								extern uint8_t g_lanBuf[N_PLC_TX_BUFFER_SIZE];
								extern uint8_t g_lanBufCount;
								extern  void TakeUplink();
								extern void GiveUplink();
								memcpy(g_lanBuf, plcBuffer, strlen(plcBuffer));
								TakeUplink();
								g_lanBufCount = strlen(plcBuffer);		
								GiveUplink();
								dbg_proc(3, "Send to LAN\r\n");
							}
							
							dbg_proc(1, "SCPI res %d:%s cmd=%s\r\n\r>", cmdIndex, plcBuffer, g_dataBuf); // Show Response on console
						}
						else { // TEST: No Response at here in normal mode...
							//sprintf(plcBuffer, "\n");
							//HAL_UART_Transmit(&huart5, (uint8_t*)plcBuffer, strlen(plcBuffer), 20);
							dbg_proc(1, "SCPI cmd %d:%s\r\n\r>",cmdIndex, g_dataBuf); // Show data on console
						}
						
						
						// clear buffers...						
						memset(plcBuffer, 0, sizeof(plcBuffer));
						memset(&scpiPacket, 0, sizeof(scpiPacket));
						memset(g_dataBuf, 0, sizeof(g_dataBuf));
					}
					else { // parshing...
						osDelay(1);
						//continue; // If there was rx data, do not call odDelay to fast comm & parsing...
					}
				}
				else if(pInfo->setting.protocol==PROTOCOL_CUSTOM) {  // Custom protocol
					// test for Apollo Custom protocol
					g_CustomState = ParsePacket(g_CustomState, rxData, pCustomPacket);	
					dbg_proc(10, "State %x %d\r\n", rxData, g_CustomState);
					
					tickLastParse = tickNow;
					if(g_CustomState == STATE_PARSE_DONE) {
						g_CustomState = STATE_READY;
						
						waitRxCount = 50;
						U16 targetID = pCustomPacket->targetIdH<<8 | pCustomPacket->targetIdL;
						if( targetID == pInfo->setting.cid || 
							targetID == 0 ||
							((pCustomPacket->targetIdH&0x80) == 0x80) ) 
						{
							//static uint8_t prevResult = 0, prevProcCount=0;
							static uint32_t prevTick = 0;
							if(pCustomPacket->result <= 0xFF && pCustomPacket->result >= 0xFD) {
								dbg_proc(2, "Device ID match! %d %x\r\n", pCustomPacket->targetIdL, pCustomPacket->result);
								//prevResult = pCustomPacket->result;
								//prevProcCount++;
								
								if( abs((int)(prevTick-HAL_GetTick())) > 100 ) {
									DoCustomProc(pCustomPacket);
									
#if CLEAR_ALARMS_ANY_EVENT
									#if 1
									if( (pCustomPacket->cmd==CCMD_GET_ALL) ||
										((pCustomPacket->cmd==CCMD_GET_MID) &&
										(pCustomPacket->mid==CMID_ARC_ALARM_STATUS || pCustomPacket->mid==CMID_OCP_ALARM_STATUS
											|| pCustomPacket->mid==CMID_BATT_LOW_ALARM_STATUS)) ) {
									}
									else 
									#endif
									{
										// Clear all Alarms on Cmd
										ClearAlarm(pInfo);
										dbg_proc(3, "COMM CLR ALM! %x\r\n", pCustomPacket->cmd);
										UpdateAlarmLED(pInfo, 1);
									}

									//OnRemoteCLcd(0, pInfo);		
#endif
								
								}
								else {
									dbg_proc(1, "Too fast command RX %d %d\r\n", prevTick, HAL_GetTick());
								}
								prevTick = HAL_GetTick();
							}
							else {
								
								dbg_proc(1, "Result is NOT 0xff %d\r\n", pCustomPacket->result);
							}
						}
						else {
							dbg_proc(1, "Device ID NOT match! rx=%d cid=%d\r\n",targetID, pInfo->setting.cid);
						}
						
					}
					else if(g_CustomState==STATE_ERR_CRC) {
						g_CustomState = STATE_READY;
						dbg_proc(1, "ERROR CRC! %x %x %d\r\n", pCustomPacket->CRCH, pCustomPacket->CRCL, pCustomPacket->length);
						memset(g_dataBuf, 0, sizeof(g_dataBuf));
						//Nack(CRESULT_ABNORMAL);
					}
				}
				else {
					osDelay(5);
				}
			}
		}
		else { // No rx data...
			osDelay(10);
		}
		osDelay(10);
	}
}


U08 g_loraTxBufIndex=0;
U08 g_loraTxSet[LORA_N_MAX_BUF_COUNT];
U08 g_loraTxSizes[LORA_N_MAX_BUF_COUNT];
U08 g_loraTxBufs[LORA_N_MAX_BUF_COUNT][N_PLC_TX_BUFFER_SIZE+12];

U08 bUplinkBusy;
StructApolloPacket ackPacket;
U08 g_ackDataBuf[N_PLC_TX_BUFFER_SIZE];
U08 g_ackBuf[N_PLC_TX_BUFFER_SIZE+12];
void SendCustom(StructApolloPacket* packet)
{
	U08 crc[2];
	int nData =  N_DATA_POSITION;	
	int nTotal = 0;
//	int32_t ret = 0;

	// generate CRC
	MakeCRC(packet, packet->length);
	
	dbg_proc(8, "%s len? %x crc %x %x\r\n",__FUNCTION__, packet->length, packet->CRCH, packet->CRCL);
	crc[0] = packet->CRCH;
	crc[1] = packet->CRCL;

	memcpy(g_ackBuf, (uint8_t*)packet, nData);
	nTotal+=nData;
	
#if PACKET_LENGTH_INCLUDING_HEADER	
	memcpy(g_ackBuf+nTotal, (uint8_t*)packet->data, packet->length - N_PACKET_LENGTH_HEADER);
	nTotal+=packet->length - N_PACKET_LENGTH_HEADER;
#else
	memcpy(g_ackBuf+nTotal, (uint8_t*)packet->data, packet->length);
	nTotal+=packet->length;
#endif
	memcpy(g_ackBuf+nTotal, (uint8_t*)crc, sizeof(crc));
	nTotal+= sizeof(crc);
	
	bUplinkBusy = 1;
	
	//switch(GetEscInfo()->setting.lanLora) {
	switch(PLC_FROM_WHERE) {
		case COMM_MODE_LORA:
		{
			HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_RESET);

			memcpy(g_loraTxBufs[g_loraTxBufIndex], g_ackBuf, nTotal);			
			g_loraTxSizes[g_loraTxBufIndex] = nTotal;		
			
			dbg_proc(3, "Copy to LoRa Buffer %d, %d\r\n", g_loraTxBufIndex, g_loraTxSizes[g_loraTxBufIndex]);
			g_loraTxBufIndex++;
			if(g_loraTxBufIndex>=LORA_N_MAX_BUF_COUNT) {
				dbg_proc(3, "LoRa Buffer Full %d\r\n", g_loraTxBufIndex);
				g_loraTxBufIndex = 0;				
			}
	
			
		}
		break;
		case COMM_MODE_LAN:
		{
			//HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_RESET);
			extern uint8_t g_lanBuf[N_PLC_TX_BUFFER_SIZE];
			extern uint8_t g_lanBufCount;
			extern  void TakeUplink();
			extern void GiveUplink();
			memcpy(g_lanBuf, g_ackBuf, nTotal);
			TakeUplink();
			g_lanBufCount = nTotal;		
			GiveUplink();
			dbg_proc(3, "Send to LAN\r\n");
		}
		break;

		case COMM_MODE_SERIAL:
		{
			// Send data to UART
#if USE_TX_IT_PLC
			HAL_UART_Transmit_IT(&huart5, (uint8_t*)g_ackBuf, nTotal); // 64
#else
			HAL_UART_Transmit(&huart5, (uint8_t*)g_ackBuf, nTotal, 16*nTotal); // 64
#endif

			dbg_proc(3, "Send to Serial\r\n");
		}
		break;
		case COMM_MODE_REMOCON:
		{
			// Send data to UART
#if USE_TX_IT_PLC
			HAL_UART_Transmit_IT(&huart8, (uint8_t*)g_ackBuf, nTotal);
#else			
			HAL_UART_Transmit(&huart8, (uint8_t*)g_ackBuf, nTotal, 64);
#endif
			dbg_proc(3, "Send to Remocon\r\n");
		}
		break;
	}
	
	bUplinkBusy = 0;
	dbg_proc(10, "Response data %d %d\r\n", nTotal, nData);	

	{
		int i;
		for(i=0;i<nTotal;i++) {
			dbg_proc(10, "%x ", g_ackBuf[i]);
		}
	}

}

void ResponsCmdByte(U08 cmd, U08 mid, U08 data)
{
	StructApolloPacket *pAck = &ackPacket;
	
	// Set data format...
	pAck->ST = CODE_ST;
	pAck->ST2 = CODE_ST2;
	pAck->data = g_ackDataBuf;

	pAck->cmd = cmd;
	pAck->result = CRESULT_OK;
	pAck->mid = mid;
	
	pAck->targetIdH = 0x00;
	pAck->targetIdL = 0x00;
	
#if 1
	pAck->cmd = CCMD_RESPONSE_ESC;
	SendStatusAutoProc(pAck);
#else	
	// Set data...
	pAck->length = 1; 
	#if PACKET_LENGTH_INCLUDING_HEADER
	// Include header too
	pAck->length += N_PACKET_LENGTH_HEADER;  
	#endif
	pAck->data[0] = data;
	
	SendCustom(pAck);	
#endif
}


void AckForCmd(U08 cmd, U08 mid, U08 *data, U08 len)
{
	StructApolloPacket *pAck = &ackPacket;
	U08 i;
	
	//memcpy(pAck, &CustomPacket, sizeof(CustomPacket));
	//memcpy(pAck->data, &CustomPacket.data, CustomPacket.length);
	// Set data format...
	pAck->ST = CODE_ST;
	pAck->ST2 = CODE_ST2;
	pAck->data = g_ackDataBuf;
	
	pAck->cmd = cmd;
	pAck->mid = mid;

	pAck->result = CRESULT_OK;
	
	pAck->targetIdH = 0x00;
	pAck->targetIdL = 0x00;
	
	// Set data...
	pAck->length = len; 
	#if PACKET_LENGTH_INCLUDING_HEADER
	// Include header too
	pAck->length += N_PACKET_LENGTH_HEADER;  
	#endif
	
	for(i=0;i<len;i++) {
		pAck->data[i] = data[i];
	}
	dbg_proc(1, "ack %d %d %d\r\n", cmd, mid, pAck->data[0]);
	SendCustom(pAck);
}

void Ack()
{
	StructApolloPacket *pAck = &ackPacket;
	
	// Set data format...
	pAck->ST = CODE_ST;
	pAck->ST2 = CODE_ST2;
	pAck->data = g_ackDataBuf;

	pAck->cmd = CCMD_ACK;
	pAck->result = CRESULT_OK;
	pAck->mid = 0x00;
	
	pAck->targetIdH = (GetEscInfo()->setting.cid)>>8;
	pAck->targetIdL = GetEscInfo()->setting.cid;
	
	// Set data...
	pAck->length = 1; 
	#if PACKET_LENGTH_INCLUDING_HEADER
	// Include header too
	pAck->length += N_PACKET_LENGTH_HEADER;  
	#endif
	pAck->data[0] = 0x00;
	
	SendCustom(pAck);
}

void Nack(U08 result)
{
	StructApolloPacket *pPacket = &ackPacket;
	
	// Set data format...
	pPacket->ST = CODE_ST;
	pPacket->ST2 = CODE_ST2;
	pPacket->data = g_ackDataBuf;

	pPacket->cmd = CCMD_NACK;
	pPacket->result = result; // CRESULT_CMD_NOT_ACCEPTED;
	pPacket->mid = 0x00;
	
	pPacket->targetIdH = 0x00;
	pPacket->targetIdL = GetEscInfo()->setting.cid;
	
	// Set data...
	pPacket->length = 1; 
	pPacket->data[0] = 0x00;
	
	SendCustom(pPacket);
}

/// USE g_ackDataBuf in this function..
void ResponseCustomProtocol(U08 cmd, U08 mid, U08 result, U08 size)
{
	StructApolloPacket *pAck = &ackPacket;
	
	// Set data format...
	pAck->ST = CODE_ST;
	pAck->ST2 = CODE_ST2;
	pAck->data = g_ackDataBuf;

	pAck->cmd = cmd;
	pAck->result = result;
	pAck->mid = mid;
	
	pAck->targetIdH = (GetEscInfo()->setting.cid)>>8;
	pAck->targetIdL = GetEscInfo()->setting.cid;
	
	// Set data...
	pAck->length = size;  	
	#if PACKET_LENGTH_INCLUDING_HEADER
	// Include header too
	pAck->length += N_PACKET_LENGTH_HEADER;  
	#endif
	
	SendCustom(pAck);
}

void RespEventReport() //CMID_OCP_ALARM_STATUS
{
	StructApolloPacket *pPacket = &ackPacket;
	StructEscInfo* pInfo = GetEscInfo();
	U16 id;
	U08 nBufPos=0;
	id = pInfo->setting.cid;
	
	// Set data format...
	pPacket->ST = CODE_ST;
	pPacket->ST2 = CODE_ST2;
	pPacket->data = g_ackDataBuf;

	pPacket->cmd = CCMD_EVENT_REPORT_ALARM;
	pPacket->result = CRESULT_OK;

	pPacket->mid = 0;
	
	pPacket->targetIdH = id<<8;
	pPacket->targetIdL = id&0x0ff;
	
	// Set data...
	
	g_ackDataBuf[nBufPos] = pInfo->volatile_stat.hvOutputStatus; // No 1
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = ( pInfo->measure.voltA ); //2
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.currA; //3
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = (pInfo->measure.voltB); //4
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.currB; //5
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = pInfo->measure.ocpAlarmStatus; // 6
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.ocpAlarmCount; // 7
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.arcAlarmStatus;	//8
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = (pInfo->stat.arcAlarmCount)>>8; // 9
	g_ackDataBuf[nBufPos+1] = (pInfo->stat.arcAlarmCount)&0x0ff;
	nBufPos += 2;

	g_ackDataBuf[nBufPos] = pInfo->measure.battLowAlarmStatus;	//11
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.battPercent;	//12
	nBufPos += 1;

	
#if PACKET_LENGTH_INCLUDING_HEADER	
	pPacket->length = nBufPos + N_PACKET_LENGTH_HEADER; 
#else
	pPacket->length = nBufPos; 
#endif
	
	dbg_proc(3, "Eventreport ocp %d %d arc %d %d bat %d %d\r\n",
		pInfo->measure.ocpAlarmStatus, pInfo->stat.ocpAlarmCount,
		pInfo->measure.arcAlarmStatus, pInfo->stat.arcAlarmCount,
		pInfo->measure.battLowAlarmStatus, pInfo->measure.battPercent
		);
		
	if(pInfo->setting.protocol == PROTOCOL_SCPI) {
		dbg_proc(3, "No Event report on SCPI\r\n");
	}
	else {
		SendCustom(pPacket);
	}
}

void SendStatusAutoProc(StructApolloPacket* packet)
{
	U08 mid = packet->mid;
	U08 nResponseLength = 0;
	U08 ret = CRESULT_OK;
	U08 nBufPos=0;
	
	StructEscInfo *pInfo = GetEscInfo();
	
	g_ackDataBuf[nBufPos] = pInfo->volatile_stat.hvOutputStatus; // No 1
	nBufPos += 1;

	if(IS_HV_ON) {
		g_ackDataBuf[nBufPos] = ( pInfo->measure.voltA ); //2
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = pInfo->measure.currA; //3
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = (pInfo->measure.voltB); //4
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = pInfo->measure.currB; //5
		nBufPos += 1;
	}
	else {
		g_ackDataBuf[nBufPos] = 0;
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = 0;
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = 0;
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = 0;
		nBufPos += 1;
	}

	g_ackDataBuf[nBufPos] = pInfo->measure.battPercent; // 6
	nBufPos += 1;


	g_ackDataBuf[nBufPos] = (pInfo->measure.temp1)>>8; // 7
	g_ackDataBuf[nBufPos+1] = (pInfo->measure.temp1)&0x0ff;
	nBufPos += 2;
	g_ackDataBuf[nBufPos] = (pInfo->measure.temp2)>>8; // No 8
	g_ackDataBuf[nBufPos+1] = (pInfo->measure.temp2)&0x0ff;
	nBufPos += 2;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.battStatusVolt; // 9
	nBufPos += 1;	

	nResponseLength = nBufPos;		
	ResponseCustomProtocol(packet->cmd, mid, ret, nResponseLength);

	dbg_proc(1, "SendAuto OutpOn:%d Bat:%d V=%x[%d]%x[%d] I=%d,%d\r\n", pInfo->volatile_stat.hvOutputStatus, pInfo->measure.battStatusVolt
		,pInfo->measure.voltA, ByteToVolt(pInfo->measure.voltA)
			,pInfo->measure.voltB, ByteToVolt(pInfo->measure.voltB)
			,pInfo->measure.currA, pInfo->measure.currB);
}

static U08 procReady = 1;
void GetAllProc(StructApolloPacket* packet)
{
	//U08 bIsGet = packet->cmd==CCMD_GET_MID?1:0; // get->1, set ->0
	U08 mid = packet->mid;
	U08 nResponseLength = 0;
	U08 ret = CRESULT_OK;
	U08 nBufPos=0;
	
	StructEscInfo *pInfo = GetEscInfo();
	
	if(procReady) {
		procReady = 0;
	}
	else {
		dbg_proc(1, "GetAllProc is busy!");
		return;
	}
	
	/*
1	"HV Output Status
(고전압 출력 ON/OFF상태)"	1	0x08	0/1	OFF / ON	0	1	1	0			R	
2	HV Output Status ch-1 Voltage	1	0x0B	"Variable
(MSB)bit 0/1"	"Value
(MSB) (+)극성 / (-)극성"	0	30	1	0	V x 100		R	ch-1 출력 전압 측정 치 (고전압 OFF 시 Ram Down Time이후 Value=0)
3	HV Output Status ch-1 Current	1	0x0C	Variable	Value	0	250		0	uA x 10		R	ch-1 출력 전류 측정 치 (고전압 OFF 시 Ram Down Time이후 Value=0)
4	HV Output Status ch-2 Voltage	1	0x0D	"Variable
(MSB)bit 0/1"	"Value
(MSB) (+)극성 / (-)극성"	0	30	1	0	V x 100		R	ch-2 출력 전압 측정 치 (고전압 OFF 시 Ram Down Time이후 Value=0)
5	HV Output Status ch-2 Current	1	0x0E	Variable	Value	0	250		0	uA x 10		R	ch-2 출력 전류 측정 치 (고전압 OFF 시 Ram Down Time이후 Value=0)
	
	*/
	
	/// Add No 1~5
	g_ackDataBuf[nBufPos] = pInfo->volatile_stat.hvOutputStatus; // No 1, 
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = ( pInfo->measure.voltA ); //2
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.currA; //3
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = (pInfo->measure.voltB); //4
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.currB; //5
	nBufPos += 1;


	g_ackDataBuf[nBufPos] = pInfo->stat.rampUpTime; // No 6, mid 0x0f
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->stat.hvSetVolt1; // No 7, mid 0x10
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.hvSetVolt2; // No 8, mid 0x11
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeStepCount; // No 9, mid 0x12
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeVoltStep1; // No 10, mid 0x13
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeVoltStep2; // No 11, mid 0x14
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeVoltStep3; // No 12, mid 0x15
	nBufPos += 1;	
	
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeKeepTimeStep1; // No 13, mid 0x16
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeKeepTimeStep2; // No 14, mid 0x17
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.autoDischargeKeepTimeStep3; // No 15, mid 0x18
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = pInfo->stat.rampDownTime; // No 16, mid 0x19
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.ocpAlarmStatus; // No 17, mid 0x2A
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.ocpAlarmControl; // No 18, mid 0x2B
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->stat.ocpAlarmEvent; // No 19, mid 0x2C
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.ocpAlarmLimit; // No 20, mid 0x2E
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.ocpAlarmCount; // No 21, mid 0x2F
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = pInfo->measure.arcAlarmStatus; // No 22, mid 0x30
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.arcAlarmControl; // No 23, mid 0x31
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.arcAlarmEvent; // No 24, mid 0x32
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = (pInfo->stat.arcAlarmLimit)>>8; // No 25, mid 0x34
	g_ackDataBuf[nBufPos+1] = (pInfo->stat.arcAlarmLimit)&0x0ff; // No 25, mid 0x34
	nBufPos += 2;
	g_ackDataBuf[nBufPos] = (pInfo->stat.arcAlarmCount)>>8; // No 26, mid 0x35
	g_ackDataBuf[nBufPos+1] = (pInfo->stat.arcAlarmCount)&0x0ff; // No 26, mid 0x35
	nBufPos += 2;
	
	if(DET_BATTERY) {
		g_ackDataBuf[nBufPos] = pInfo->measure.battStatusVolt; // No 27, mid 0x37
		nBufPos += 1;
		g_ackDataBuf[nBufPos] = pInfo->measure.battPercent; // No 38, mid 0x38, Battery Percent
		nBufPos += 1;
		g_ackDataBuf[nBufPos] = pInfo->measure.battCurr; // No 29, mid 0x39 A/10
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = pInfo->measure.battLowAlarmStatus; // No 30, mid 0x3A
		nBufPos += 1;
	}
	else { // No info, No battery
		g_ackDataBuf[nBufPos] = 0; // No 27, mid 0x37
		nBufPos += 1;
		g_ackDataBuf[nBufPos] = 0; // No 38, mid 0x38, Battery Percent
		nBufPos += 1;
		g_ackDataBuf[nBufPos] = 0; // No 29, mid 0x39 A/10
		nBufPos += 1;
		
		g_ackDataBuf[nBufPos] = 0; // No 30, mid 0x3A
		nBufPos += 1;
	}
	
	g_ackDataBuf[nBufPos] = pInfo->stat.battAlarmControl; // No 31, mid 0x3B
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.battAlarmLimit; // No 32, mid 0x3C
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.battEventControl; // No 33, mid 0x3D
	nBufPos += 1;
	

	g_ackDataBuf[nBufPos] = pInfo->measure.battChargeConnectStatus; // No 34, mid 0x3E
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = pInfo->stat.userMode; // No 35, mid 0x40
	nBufPos += 1;
	g_ackDataBuf[nBufPos] = pInfo->stat.waveMode; // No 36, mid 0x41
	nBufPos += 1;

	g_ackDataBuf[nBufPos] = (pInfo->measure.temp1)>>8; // No 37, mid 0x42
	g_ackDataBuf[nBufPos+1] = (pInfo->measure.temp1)&0x0ff;
	nBufPos += 2;
	g_ackDataBuf[nBufPos] = (pInfo->measure.temp2)>>8; // No 38, mid 0x43
	g_ackDataBuf[nBufPos+1] = (pInfo->measure.temp2)&0x0ff;
	nBufPos += 2;

	g_ackDataBuf[nBufPos] = (pInfo->measure.press)>>8; // No 39
	g_ackDataBuf[nBufPos+1] = (pInfo->measure.press)&0x0ff;
	nBufPos += 2;

//	dbg_proc(3, "temp %d %d %f %d %d %f\r\n",pInfo->measure.temp1>>8, pInfo->measure.temp1&0x0ff, pInfo->measure.temp1/10.0, 
	//		pInfo->measure.temp2>>8, pInfo->measure.temp2&0x0ff, pInfo->measure.temp2/10.0);
	

	g_ackDataBuf[nBufPos] = pInfo->stat.autoQueryControl; // No 40, mid 0x48
	nBufPos += 1;
	
	g_ackDataBuf[nBufPos] = (pInfo->stat.autQueryIntervalTime)>>8; // No 41, mid 0x49
	g_ackDataBuf[nBufPos+1] = (pInfo->stat.autQueryIntervalTime)&0x0ff; 
	nBufPos += 2;	
	
	g_ackDataBuf[nBufPos] = pInfo->stat.hvAutoToggle; // No 42, mid 0x4A
	nBufPos += 1;

	nResponseLength = nBufPos;		
	//dbg_proc(1, "size = %d %d, data[0] = %x %x %x\r\n", nBufPos, nResponseLength, packet->data[0], g_ackDataBuf[0], pInfo->stat.ocpAlarmControl);
	ResponseCustomProtocol(packet->cmd, mid, ret, nResponseLength);
	
	procReady = 1;

dbg_proc(3, "GETALL Outp V=%x[%d]%x[%d] I=%d,%d T %d %d Bat I:%d %d AT:%x\r\n",pInfo->measure.voltA, ByteToVolt(pInfo->measure.voltA)
			,pInfo->measure.voltB, ByteToVolt(pInfo->measure.voltB)
			,pInfo->measure.currA, pInfo->measure.currB
			,pInfo->measure.temp1, pInfo->measure.temp2
			,pInfo->measure.battCurr ,pInfo->measure.chargeCurr, pInfo->stat.hvAutoToggle
			);
	//dbg_proc(1, "GETALL Outp %d,%d %d OA %d AA %d\r\n",pInfo->stat.hvSetVolt1,pInfo->stat.hvSetVolt2, pInfo->volatile_stat.hvOutputStatus, pInfo->measure.ocpAlarmStatus, pInfo->measure.arcAlarmStatus);
}

void MidProc(StructApolloPacket* packet)
{
	U08 bIsGet = packet->cmd==CCMD_GET_MID?1:0; // get->1, set ->0
	U08 mid = packet->mid;
	U08 nResponseLength = 0;
	U08 ret = CRESULT_OK;
	U08 *data = packet->data;	
	U08 value;
	int value16;
	StructEscInfo *pInfo = GetEscInfo();
	
//	struct OutputMessage outputMsg;
	
	dbg_proc(2, "GET? %d Mid 0x%x data %x size %d\r\n",bIsGet, mid, data[0], packet->length);
	value = data[0];
	switch(mid)
	{
		case CMID_CID:
			if(bIsGet) { 
				memcpy(g_ackDataBuf, &pInfo->setting.cid, sizeof(pInfo->setting.cid));
				nResponseLength = sizeof(pInfo->setting.cid);
				dbg_proc(3, "GET CID %x %x %d\r\n",g_ackDataBuf[0], g_ackDataBuf[1], nResponseLength);
				break;
			}
			else {
				pInfo->setting.cid = value;				
				//g_ackDataBuf[0] = pInfo->setting.cid;
				memcpy(&pInfo->setting.cid,(char*)packet->data, sizeof(pInfo->setting.cid));
				nResponseLength = sizeof(pInfo->setting.cid);
				dbg_proc(3, "CID changed %d\r\n", pInfo->setting.cid);
				Ack();
			}
			break;
			
		case CMID_SERIAL_NUMBER:
			if(bIsGet) { 
				memcpy(g_ackDataBuf, pInfo->setting.serialNumber, N_MAX_SN_LENGTH);
				nResponseLength = N_MAX_SN_LENGTH;
				dbg_proc(3, "GET SN %s %d\r\n",pInfo->setting.serialNumber , nResponseLength);
				break;
			}
			else {									
				memcpy(pInfo->setting.serialNumber,(char*)packet->data, N_MAX_SN_LENGTH);
				nResponseLength = 0; //sizeof(pInfo->setting.serialNumber);
				dbg_proc(3, "SN changed %s\r\n", pInfo->setting.serialNumber);
				Ack();
			}
			break;
			
		
		case CMID_AID:
			if(bIsGet) { 
				memcpy(g_ackDataBuf,&pInfo->setting.aid, sizeof(pInfo->setting.aid));
				nResponseLength = sizeof(pInfo->setting.aid);
				break;
			}
			else {
				memcpy((char*)&pInfo->setting.aid, (char*)packet->data, sizeof(pInfo->setting.aid));
				dbg_proc(2, "AID changed %s\r\n", pInfo->setting.aid);
				nResponseLength = sizeof(pInfo->setting.aid);
				Ack();
			}
			break;
		case CMID_HW_VER:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->setting.hwVer;
				nResponseLength = 1;
				break;
			}
			else {
				pInfo->setting.hwVer = (U08)(value);
				dbg_proc(3, "CMID_HW_VER %x %d %x\r\n", value, pInfo->setting.hwVer, value);
				Ack();
			}
			break;
		case CMID_FW_VER:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->setting.fwVer;
				nResponseLength = 1;
				dbg_proc(1, "FW Ver: %d\r\n", pInfo->setting.fwVer);
				break;
			}
			else { // CMID_FW_VER is Read Only
				Nack(CRESULT_CMD_NOT_ACCEPTED); 
			}
			break;
	
		case CMID_HV_OUT_STATUS:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->volatile_stat.hvOutputStatus;
				nResponseLength = 1;
				break;
			}
			else { // CMID_HV_OUT_STATUS is Read Only
				//Nack();						
				SetHvOnOFF(data[0] & 0x01);
				
				U08 toggleChanged=0;
				dbg_proc(2, "[REMOTE] AT? %x\r\n",data[0]);
				if(data[0] & (AUTO_TOGGLE1 | AUTO_TOGGLE2)) {					
					toggleChanged = hvToggleMode(pInfo, data[0] & (AUTO_TOGGLE1 | AUTO_TOGGLE2));
				}
				else {
					toggleChanged = hvToggleMode(pInfo, TOGGLE_OFF);
				}
				
				static U08 hvOut=0;
				g_ackDataBuf[0] = pInfo->volatile_stat.hvOutputStatus;
//				nResponseLength = 1;
				
				// Disabled by jwaani @2020-0604: Error with local/lan mixed action
				//if(toggleChanged || hvOut != pInfo->volatile_stat.hvOutputStatus) 
				{
					OnRemoteCLcd(0,pInfo);					
					hvOut = pInfo->volatile_stat.hvOutputStatus;
				}				
				//ResponsCmdByte(CCMD_GET_HV_OUT_STATUS,0, pInfo->volatile_stat.hvOutputStatus);
				
				//ResponsCmdByte(CCMD_GET_MID, CMID_HV_OUT_STATUS, pInfo->volatile_stat.hvOutputStatus);
				g_ackDataBuf[0] = pInfo->volatile_stat.hvOutputStatus;
				g_ackDataBuf[1] = pInfo->stat.hvSetVolt1;
				g_ackDataBuf[2] = pInfo->stat.hvSetVolt2;
				nResponseLength = 3;
				//AckForCmd(CCMD_GET_MID, CMID_HV_OUT_STATUS, g_ackDataBuf, nResponseLength);
				
				Ack();
			
				dbg_proc(2, "CMID_HV_OUT_STATUS %d %x %d[V] T:%d\r\n",pInfo->volatile_stat.hvOutputStatus, (data[0]),ByteToVolt(pInfo->stat.hvSetVolt1) ,IS_TOGGLE);
			
				
			}
			
			break;
		case CMID_HV_OUT_STATUS_CH1_VOLT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.voltA;
				//g_ackDataBuf[1] = pInfo->measure.voltA&0xff;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_HV_OUT_STATUS_CH1_CURR:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.currA;
				//g_ackDataBuf[1] = pInfo->measure.currA&0xff;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_HV_OUT_STATUS_CH2_VOLT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.voltB; //>>8;
				//g_ackDataBuf[1] = pInfo->measure.voltB&0xff;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_HV_OUT_STATUS_CH2_CURR:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.currB; //>>8;
				//g_ackDataBuf[1] = pInfo->measure.currB&0xff;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		
		case CMID_RAMP_UP_TIME:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.rampUpTime;
				nResponseLength = 1;				
				break;
			}
			value = data[0];
			if(value<MIN_RAMP_TIME || value>MAX_RAMP_TIME) {
				dbg_proc(1, "CMID_RAMP_UP_TIME Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.rampUpTime = (U08)(value);
				dbg_proc(1, "CMID_RAMP_UP_TIME %x %d[msec] %x\r\n", value, pInfo->stat.rampUpTime, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
			
		case CMID_RAMP_DOWN_TIME:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.rampDownTime;
				nResponseLength = 1;
				break;
			}
			
			value = data[0];
			if(value<MIN_RAMP_TIME || value>MAX_RAMP_TIME) {
				dbg_proc(1, "CMID_RAMP_DOWN_TIME Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.rampDownTime = (U08)(value);
				dbg_proc(1, "CMID_RAMP_DOWN_TIME %x %d[msec] %x\r\n", value, pInfo->stat.rampDownTime, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
			
		
		case CMID_HV_SET_VOLT_CH1: // 0x10
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.hvSetVolt1;
				nResponseLength = 1;
				break;
			}
			
			value = data[0] & 0x007F;			
			if(value<MIN_VOLT_SET && value>MAX_VOLT_SET) { // volt min=0 100V, max=30, 3kV
				dbg_proc(1, "CMID_HV_SET_VOLT_CH1 is Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				SetHvVolt(1, data[0]);
				
				dbg_proc(1, "CMID_HV_SET_VOLT_CH1 %x %d[V] %x\r\n", value, ByteToVolt(pInfo->stat.hvSetVolt1), value);
				//AckForCmd(packet->cmd, mid, data, 1);  //data[0]);
				Ack();
			}			
		break;
		case CMID_HV_SET_VOLT_CH2:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.hvSetVolt2;
				nResponseLength = 1;
				break;
			}
			
			value = data[0] & 0x007F;			
			if(value<MIN_VOLT_SET && value>MAX_VOLT_SET) { // volt min=1 100V, max=30, 3kV
				dbg_proc(1, "CMID_HV_SET_VOLT_CH2 is Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.hvSetVolt2 = data[0];				
						
				dbg_proc(1, "CMID_HV_SET_VOLT_CH2 %x %d[V] %x\r\n", value, ByteToVolt(pInfo->stat.hvSetVolt2), value);
				//AckForCmd(packet->cmd, mid, data, 1); //AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}										
		break;
		
		case CMID_AUTO_DISCHARGE_STEP_COUNT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeStepCount;
				nResponseLength = 1;
				break;
			}
			
			value = data[0];
			if(value>MAX_AUTO_DISCHARGE_STEP_COUNT) {
				dbg_proc(1, "CMID_AUTO_DISCHARGE_STEP_COUNT is Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeStepCount = (U08)(value);
				dbg_proc(1, "CMID_AUTO_DISCHARGE_STEP_COUNT setted %x %d[step] %x\r\n", value, pInfo->stat.autoDischargeStepCount, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_AUTO_DISCHARGE_VOLT_STEP1:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeVoltStep1;
				nResponseLength = 1;
				break;
			}
			
			value = data[0] & 0x007F;			
			if(value<MIN_AUTO_DISCHARGE_VOLT_STEP || value>MAX_AUTO_DISCHARGE_VOLT_STEP) { // volt min=1 100V, max=30, 3kV
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP1 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeVoltStep1 = data[0];
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP1 %x %d[V] %x\r\n", value, ByteToVolt(pInfo->stat.autoDischargeVoltStep1), value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}										

		break;
		case CMID_AUTO_DISCHARGE_VOLT_STEP2:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeVoltStep2;
				nResponseLength = 1;
				break;
			}
			value = data[0] & 0x007F;			
			if(value<MIN_AUTO_DISCHARGE_VOLT_STEP || value>MAX_AUTO_DISCHARGE_VOLT_STEP) { // volt min=1 100V, max=30, 3kV
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP2 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeVoltStep2 = data[0];
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP2 %x %d[V] %x\r\n", value, ByteToVolt(pInfo->stat.autoDischargeVoltStep2), value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}										
		break;
		case CMID_AUTO_DISCHARGE_VOLT_STEP3:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeVoltStep3;
				nResponseLength = 1;
				break;
			}
			value = data[0] & 0x007F;			
			if(value<MIN_AUTO_DISCHARGE_VOLT_STEP || value>MAX_AUTO_DISCHARGE_VOLT_STEP) { // volt min=1 100V, max=30, 3kV
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP3 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeVoltStep3 = data[0];
				dbg_proc(1, "CMID_AUTO_DISCHARGE_VOLT_STEP3 %x %d[V] %x\r\n", value, ByteToVolt(pInfo->stat.autoDischargeVoltStep3), value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}										
		break;
		case CMID_AUTO_DISCHARGE_KEEPTIME_STEP1:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeKeepTimeStep1;
				nResponseLength = 1;
				break;
			}
			
			value = data[0];
			if(value>MAX_AUTO_DISCHARGE_KEEP_SETP) {
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP1 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeKeepTimeStep1 = (U08)(value);
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP1 %x %d[sec] %x\r\n", value, pInfo->stat.autoDischargeKeepTimeStep1, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_AUTO_DISCHARGE_KEEPTIME_STEP2:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeKeepTimeStep2;
				nResponseLength = 1;
				break;
			}
			
			value = data[0];
			if(value>MAX_AUTO_DISCHARGE_KEEP_SETP) {
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP2 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeKeepTimeStep2 = (U08)(value);
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP2 %x %d[sec] %x\r\n", value, pInfo->stat.autoDischargeKeepTimeStep2, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_AUTO_DISCHARGE_KEEPTIME_STEP3:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoDischargeKeepTimeStep3;
				nResponseLength = 1;
				break;
			}
			
			value = data[0];
			if(value>MAX_AUTO_DISCHARGE_KEEP_SETP) {
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP3 Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoDischargeKeepTimeStep3 = (U08)(value);
				dbg_proc(1, "CMID_AUTO_DISCHARGE_KEEPTIME_STEP3 %x %d[sec] %x\r\n", value, pInfo->stat.autoDischargeKeepTimeStep3, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		
		
		case CMID_OCP_ALARM_STATUS:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.ocpAlarmStatus;
				nResponseLength = 1;
				break;
			}
			else { // CMID_OCP_ALARM_STATUS is Read Only				
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
		break;
		case CMID_OCP_ALARM_CONTROL:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.ocpAlarmControl;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_OCP_ALARM_CONTROL Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.ocpAlarmControl = (U08)(value);
				dbg_proc(1, "CMID_OCP_ALARM_CONTROL %x %d %x\r\n", value, pInfo->stat.ocpAlarmControl, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_OCP_ALARM_EVENT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.ocpAlarmEvent;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_OCP_ALARM_EVENT Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.ocpAlarmEvent = (U08)(value);
				dbg_proc(1, "CMID_OCP_ALARM_EVENT %x %d %x\r\n", value, pInfo->stat.ocpAlarmEvent, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_OCP_ALARM_LIMIT: // *10 [uA]
			if(bIsGet) { 
				g_ackDataBuf[0] = (pInfo->stat.ocpAlarmLimit);
				nResponseLength = 1;
				break;
			}
			value = data[0];
			
			if(value<MIN_OCP_ALARM_LIMIT || value > MAX_OCP_ALARM_LIMIT)
			{
				dbg_proc(1, "CMID_OCP_ALARM_LIMIT Out of Range! %d, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {
				pInfo->stat.ocpAlarmLimit = value;
				dbg_proc(1, "CMID_OCP_ALARM_LIMIT setted %d [uA] %x\r\n", ((U08)pInfo->stat.ocpAlarmLimit)*10, data[0]);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}			
		break;
		case CMID_OCP_ALARM_COUNT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.ocpAlarmCount;
				nResponseLength = 1;
				break;
			}
			else { // CMID_OCP_ALARM_COUNT is Read Only
				Nack(CRESULT_CMD_NOT_ACCEPTED); 
			}
		break;

		case CMID_ARC_ALARM_STATUS:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.arcAlarmStatus;
				nResponseLength = 1;
				break;
			}
			else { // CMID_ARC_ALARM_STATUS is Read Only
				Nack(CRESULT_CMD_NOT_ACCEPTED); 
			}
		break;
		case CMID_ARC_ALARM_CONTROL:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.arcAlarmControl;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_ARC_ALARM_CONTROL Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.arcAlarmControl = (U08)(value);
				dbg_proc(1, "CMID_ARC_ALARM_CONTROL %x %d %x\r\n", value, pInfo->stat.arcAlarmControl, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_ARC_ALARM_EVENT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.arcAlarmEvent;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_ARC_ALARM_EVENT Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.arcAlarmEvent = (U08)(value);
				dbg_proc(1, "CMID_ARC_ALARM_EVENT %x %d %x\r\n", value, pInfo->stat.arcAlarmEvent, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_ARC_ALARM_LIMIT:
			if(bIsGet) { 
				g_ackDataBuf[0] = (pInfo->stat.arcAlarmLimit)>>8;
				g_ackDataBuf[1] = (pInfo->stat.arcAlarmLimit)&0x00ff;
				nResponseLength = 2;
				dbg_proc(1, "CMID_ARC_ALARM_LIMIT ack %d 0x%x%x\r\n", pInfo->stat.arcAlarmLimit, g_ackDataBuf[0], g_ackDataBuf[1]);
				break;
			}
			value16 = data[0]<<8 | data[1]; 
			
			if(value16<MIN_ARC_ALARM_LIMIT || value16 > MAX_ARC_ALARM_LIMIT)
			{
				dbg_proc(1, "Arc Limit is Out of Range! %d, %x%x\r\n", value16, data[0], data[1]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {
				pInfo->stat.arcAlarmLimit = value16;
				dbg_proc(1, "CMID_ARC_ALARM_LIMIT setted %d %x\r\n", pInfo->stat.arcAlarmLimit, value16);
				//AckForCmd(packet->cmd, mid, data, 2);
				Ack();
			}
		break;
		case CMID_ARC_ALARM_COUNT:
			if(bIsGet) { 
				g_ackDataBuf[0] = (pInfo->stat.arcAlarmCount)>>8;
				g_ackDataBuf[1] = (pInfo->stat.arcAlarmCount)&0x00ff;
				nResponseLength = 2;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
		break;
		
		case CMID_BATT_STATUS_VOLT:
			if(bIsGet) {  // Read only
				g_ackDataBuf[0] = pInfo->measure.battStatusVolt;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
		break;
		case CMID_BATT_PERCENT:
			if(bIsGet) {  // Read only
				g_ackDataBuf[0] = pInfo->measure.battPercent;
				nResponseLength = 1;
				dbg_proc(1, "BATT %d %%\r\n", g_ackDataBuf[0]);
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
		break;
		case CMID_BATT_STATUS_CURR:
			if(bIsGet) {  // Read only
				g_ackDataBuf[0] = pInfo->measure.battCurr;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_BATT_LOW_ALARM_STATUS:
			if(bIsGet) {  // Read only
				g_ackDataBuf[0] = pInfo->measure.battLowAlarmStatus;
				nResponseLength = 1;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_BATT_ALARM_CONTROL:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.battAlarmControl;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_BATT_ALARM_CONTROL Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.battAlarmControl = (U08)(value);
				dbg_proc(1, "CMID_BATT_ALARM_CONTROL %x %d %x\r\n", value, pInfo->stat.battAlarmControl, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_BATT_ALARM_LIMIT:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.battAlarmLimit;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_BATT_ALARM_LIMIT) {
				dbg_proc(1, "CMID_BATT_ALARM_LIMIT Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.battAlarmLimit = (U08)(value);
				dbg_proc(1, "CMID_BATT_ALARM_LIMIT %x %d %x\r\n", value, pInfo->stat.battAlarmLimit, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		case CMID_BATT_EVENT_CONTROL:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.battEventControl;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_BATT_EVENT_CONTROL Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.battEventControl = (U08)(value);
				dbg_proc(1, "CMID_BATT_EVENT_CONTROL %x %d %x\r\n", value, pInfo->stat.battEventControl, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;

		case CMID_CHARGE_CONNECT_STATUS:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->measure.battChargeConnectStatus;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_BATT_CHARGE_STATUS) { // 0/1/2, NC, Charing, Complete
				dbg_proc(1, "CMID_CHARGE_CONNECT_STATUS Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->measure.battChargeConnectStatus = (U08)(value);
				dbg_proc(1, "CMID_CHARGE_CONNECT_STATUS %x %d %x\r\n", value, pInfo->measure.battChargeConnectStatus, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}						
		break;
		
		case CMID_USER_MODE_SAVE_LOAD:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.userMode;
				nResponseLength = 1;
				break;
			}
			value = data[0] & 0x7f;
			if( (data[0] & 0x80)==0x80 ) { // MSB=1 -> save
				pInfo->stat.userMode = (U08)(value);
				data[0] = pInfo->stat.userMode;
				dbg_proc(1, "CMID_USER_MODE_SAVE_LOAD %x %d %x\r\n", value, pInfo->stat.userMode, value);
				//AckForCmd(packet->cmd, mid, data, 1);;						
				Ack();
				SaveUserMode(pInfo);				
			}
			else {
				if(value>MAX_USER_MODE) {
					dbg_proc(1, "CMID_USER_MODE_SAVE_LOAD Out of Range!! %x, %x\r\n", value, data[0]);
					Nack(CRESULT_RANGE_ERR);
				}
				else {		
					pInfo->stat.userMode = (U08)(value);
					data[0] = pInfo->stat.userMode;
					dbg_proc(1, "CMID_USER_MODE_SAVE_LOAD %x %d %x\r\n", value, pInfo->stat.userMode, value);
					//AckForCmd(packet->cmd, mid, data, 1);;					
					Ack();
					LoadUserMode(pInfo);
				}				
			}
		break;

		case CMID_WAVE_MODE:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.waveMode;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value<MIN_WAVE_MODE || value>MAX_WAVE_MODE) {
				dbg_proc(1, "CMID_WAVE_MODE Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.waveMode = (U08)(value);
				dbg_proc(1, "CMID_WAVE_MODE %x %d %x\r\n", value, pInfo->stat.waveMode, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
				LoadWaveMode(pInfo);
			}				
		break;
		
		case CMID_TEMP1_STATUS:
			if(bIsGet) {
				g_ackDataBuf[0] = (pInfo->measure.temp1)>>8; // 7
				g_ackDataBuf[1] = (pInfo->measure.temp1)&0x0ff;
				nResponseLength = 2;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_TEMP2_STATUS:
			if(bIsGet) {
				g_ackDataBuf[0] = (pInfo->measure.temp2)>>8; 
				g_ackDataBuf[1] = (pInfo->measure.temp2)&0x0ff;
				nResponseLength = 2;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
				break;
		case CMID_PRESS_STATUS:
			if(bIsGet) {
				g_ackDataBuf[0] = (pInfo->measure.press)>>8; 
				g_ackDataBuf[1] = (pInfo->measure.press)&0x0ff;
				nResponseLength = 2;
				break;
			}
			else {
				Nack(CRESULT_CMD_NOT_ACCEPTED);
			}
			break;

		case CMID_AUTO_QUERY_CONTROL:
			if(bIsGet) { 
				g_ackDataBuf[0] = pInfo->stat.autoQueryControl;
				nResponseLength = 1;
				break;
			}
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_AUTO_QUERY_CONTROL Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				pInfo->stat.autoQueryControl = (U08)(value);
				dbg_proc(1, "CMID_AUTO_QUERY_CONTROL %x %d %x\r\n", value, pInfo->stat.autoQueryControl, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}				
		break;
		case CMID_AUTO_QUERY_INVERVAL_TIME:
			if(bIsGet) { 
				g_ackDataBuf[0] = (pInfo->stat.autQueryIntervalTime)>>8;
				g_ackDataBuf[1] = (pInfo->stat.autQueryIntervalTime)&0x00ff;
				nResponseLength = 2;
				break;
			}
			value16 = data[0]<<8 | data[1]; 
			
			if(value16<MIN_AUTO_QUERY_INTERVAL || value16 > MAX_AUTO_QUERY_INTERVAL)
			{
				dbg_proc(1, "CMID_AUTO_QUERY_INVERVAL_TIME is Out of Range! %d, %x%x\r\n", value16, data[0], data[1]);
				
				Nack(CRESULT_RANGE_ERR);
			
			}
			else {
				pInfo->stat.autQueryIntervalTime = value16;
				dbg_proc(1, "CMID_AUTO_QUERY_INVERVAL_TIME setted %d %x\r\n", pInfo->stat.autQueryIntervalTime, value16);
				Ack();
			}
			
		break;
		
		case CMID_HV_OUT_AUTO_TOGGLE:
			if(bIsGet) { 
				g_ackDataBuf[0] = IS_TOGGLE; // pInfo->stat.hvAutoToggle;
				nResponseLength = 1;
				break;
			}
			swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
			dbg_proc(1, "CMID_HV_OUT_AUTO_TOGGLE %x %d %x\r\n", IS_TOGGLE, pInfo->stat.hvAutoToggle, value);
			//AckForCmd(packet->cmd, mid, data, 1);
			Ack();

			/*
			value = data[0];
			if(value>MAX_CONTROL_ONOFF) {
				dbg_proc(1, "CMID_HV_OUT_AUTO_TOGGLE Out of Range!! %x, %x\r\n", value, data[0]);
				Nack(CRESULT_RANGE_ERR);
			}
			else {				
				//pInfo->stat.hvAutoToggle = (U08)(value);
				swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
				dbg_proc(1, "CMID_HV_OUT_AUTO_TOGGLE %x %d %x\r\n", value, pInfo->stat.hvAutoToggle, value);
				//AckForCmd(packet->cmd, mid, data, 1);
				Ack();
			}*/				
		break;
			
		default:
			dbg_proc(1, "Unkown MID!\r\n");
			Nack(CRESULT_CMD_NOT_ACCEPTED);
			break;
	}
	
	if(bIsGet) {
//		ret = CRESULT_OK;			
		dbg_proc(1, "data[0] = %x %x %x\r\n", packet->data[0], g_ackDataBuf[0], pInfo->stat.ocpAlarmControl);
		ResponseCustomProtocol(packet->cmd, mid, ret, nResponseLength);
	}
}

void DoCustomProc(StructApolloPacket* packet)
{
	StructEscInfo* pInfo = GetEscInfo();
	U08 cmd = packet->cmd;
	U08 data = packet->data[0];
	U08 mid = packet->mid;
	//TODO: make action for each CMDs...
	//dbg_proc(1, "RX DONE! CMD = %x %x %x\r\n" ,packet->cmd, packet->CRCH, packet->CRCL );
	
	U08 noReact = 0;
	U08 alarmStatus = 0;
	alarmStatus = pInfo->measure.ocpAlarmStatus<<2 | pInfo->measure.arcAlarmStatus<<1; // | pInfo->measure.battLowAlarmStatus;
	U08 alarmControl = ((pInfo->stat.ocpAlarmControl)<<2) 
			| ((pInfo->stat.arcAlarmControl) <<1 ) ;
			//| (pInfo->stat.battAlarmControl) );
	U08 i = 0;
	
#if ALARM_CONTROL_HV_ONLY
	noReact = 0;
#else
	if(alarmStatus & alarmControl) {
		noReact = 1;
		switch(cmd)
		{		
			case CCMD_SET_OCP_ALARM_COUNT_CLEAR:
			case CCMD_SET_ARC_ALARM_COUNT_CLEAR:
			case CCMD_SET_ALL_ALARM_COUNT_CLEAR:
			case CCMD_GET_ALL:
				noReact = 0;				
				break;
			case CCMD_GET_MID:
				switch(mid) {
					case CMID_HV_OUT_STATUS:
						if(data==0) {
							noReact = 0;
						}
						break;
					}
				break;
		}
	}	
#endif	
	if(noReact) {
		dbg_proc(1, "No React on Alarm %x\r\n", alarmStatus);
		Nack(CRESULT_OPERATION_CMD_ERR);
	}
	else {
		switch(cmd)
		{
			case CCMD_ACK:			
				Ack();
			break;
			
			//case CCMD_NACK:
			//break;
			case CCMD_EVENT_REPORT_ALARM:
				//RespEventReport();
				dbg_proc(1, "event report!\r\n");
			break;
			/*
			case CCMD_EVENT_REPORT_AUTO_QUERY:
			break;
			case CCMD_REQUEST_ALIVE:
			break;
		*/
			case CCMD_GET_MID:
				MidProc(packet);
			break;
			case CCMD_GET_HV_OUT_STATUS:
				packet->length = 1;
#if 0			
				g_ackDataBuf[0] = pInfo->volatile_stat.hvOutputStatus & 0x01;				
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
#else			
				i = 0;
				g_ackDataBuf[i++] = pInfo->volatile_stat.hvOutputStatus;
				g_ackDataBuf[i++] = pInfo->measure.voltA;
				g_ackDataBuf[i++] = pInfo->measure.voltB;
				g_ackDataBuf[i++] = pInfo->measure.currA;
				g_ackDataBuf[i++] = pInfo->measure.currB;				
				packet->length = i;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
#endif
			break;
			case CCMD_GET_RAMP_UPDOWN_TIME:
				packet->length = 2;
				g_ackDataBuf[0] = pInfo->stat.rampUpTime;
				g_ackDataBuf[1] = pInfo->stat.rampDownTime;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			case CCMD_GET_HV_SET_VOLT:
				packet->length = 2;
				g_ackDataBuf[0] = pInfo->stat.hvSetVolt1;
				g_ackDataBuf[1] = pInfo->stat.hvSetVolt2;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			case CCMD_GET_AUTO_DISCHARGE:
				packet->length = 7;
				g_ackDataBuf[0] = pInfo->stat.autoDischargeStepCount;
				g_ackDataBuf[1] = pInfo->stat.autoDischargeVoltStep1;
				g_ackDataBuf[2] = pInfo->stat.autoDischargeVoltStep2;
				g_ackDataBuf[3] = pInfo->stat.autoDischargeVoltStep3;
				g_ackDataBuf[4] = pInfo->stat.autoDischargeKeepTimeStep1;
				g_ackDataBuf[5] = pInfo->stat.autoDischargeKeepTimeStep2;
				g_ackDataBuf[6] = pInfo->stat.autoDischargeKeepTimeStep3;
					
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			case CCMD_GET_OCP:
				packet->length = 5;
				g_ackDataBuf[0] = pInfo->stat.ocpAlarmControl;
				g_ackDataBuf[1] = pInfo->stat.ocpAlarmEvent;
				g_ackDataBuf[2] = pInfo->stat.ocpAlarmLimit;
				g_ackDataBuf[3] = pInfo->measure.ocpAlarmStatus;
				g_ackDataBuf[4] = pInfo->stat.ocpAlarmCount;
					
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			case CCMD_GET_ARC:
				packet->length = 5;
				g_ackDataBuf[0] = pInfo->stat.arcAlarmControl;
				g_ackDataBuf[1] = pInfo->stat.arcAlarmEvent;
				g_ackDataBuf[2] = pInfo->stat.arcAlarmLimit;
				g_ackDataBuf[3] = pInfo->measure.arcAlarmStatus;
				g_ackDataBuf[4] = pInfo->stat.arcAlarmCount;
					
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			case CCMD_GET_BATTERY:
				packet->length = 5;
				g_ackDataBuf[0] = pInfo->stat.battAlarmControl;
				g_ackDataBuf[1] = pInfo->stat.battAlarmLimit;
				g_ackDataBuf[2] = pInfo->stat.battEventControl;
				g_ackDataBuf[3] = pInfo->measure.battLowAlarmStatus;
				g_ackDataBuf[4] = pInfo->measure.battStatusVolt;
					
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	

			break;
			case CCMD_GET_TEMP:
				packet->length = 4;
				g_ackDataBuf[0] = (pInfo->measure.temp1)>>8; 
				g_ackDataBuf[1] = (pInfo->measure.temp1)&0x0ff;
				g_ackDataBuf[2] = (pInfo->measure.temp2)>>8; 
				g_ackDataBuf[3] = (pInfo->measure.temp2)&0x0ff;				
					
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			
			case CCMD_GET_AUTO_QUERY:
				packet->length = 2;
				g_ackDataBuf[0] = pInfo->stat.autoQueryControl;
				g_ackDataBuf[1] = pInfo->stat.autQueryIntervalTime;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	
			break;
			
			case CCMD_GET_ALL:
				GetAllProc(packet);
			break;
			
			case CCMD_GET_REALTIME_GROUP1:
				//"Spec" sheet : No. 1~5 / 17 / 21 / 22 / 26 / 30
				i = 0;
				g_ackDataBuf[i++] = pInfo->volatile_stat.hvOutputStatus;
				g_ackDataBuf[i++] = pInfo->measure.voltA;
				g_ackDataBuf[i++] = pInfo->measure.voltB;
				g_ackDataBuf[i++] = pInfo->measure.currA;
				g_ackDataBuf[i++] = pInfo->measure.currB;
			
				g_ackDataBuf[i++] = pInfo->measure.ocpAlarmStatus; // 17
				g_ackDataBuf[i++] = pInfo->stat.ocpAlarmCount; // 21
			
				g_ackDataBuf[i++] = pInfo->measure.arcAlarmStatus; // 22
				g_ackDataBuf[i++] = pInfo->stat.arcAlarmCount; // 26
			
				g_ackDataBuf[i++] = pInfo->measure.battLowAlarmStatus; // 30
								
				packet->length = i;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	 // 10 bytes
			break;
			case CCMD_GET_REALTIME_GROUP2:
				//"Spec" sheet : No. 1~5 / 17 / 21 / 22 / 26 / 30 / 37 / 38
			i = 0;
				g_ackDataBuf[i++] = pInfo->volatile_stat.hvOutputStatus;
				g_ackDataBuf[i++] = pInfo->measure.voltA;
				g_ackDataBuf[i++] = pInfo->measure.voltB;
				g_ackDataBuf[i++] = pInfo->measure.currA;
				g_ackDataBuf[i++] = pInfo->measure.currB;
			
				g_ackDataBuf[i++] = pInfo->measure.ocpAlarmStatus; // 17
				g_ackDataBuf[i++] = pInfo->stat.ocpAlarmCount; // 21
			
				g_ackDataBuf[i++] = pInfo->measure.arcAlarmStatus; // 22
				g_ackDataBuf[i++] = pInfo->stat.arcAlarmCount; // 26
			
				g_ackDataBuf[i++] = pInfo->measure.battLowAlarmStatus; // 30
			
				g_ackDataBuf[i++] = (pInfo->measure.temp1)>>8;  //37
				g_ackDataBuf[i++] = (pInfo->measure.temp1)&0x0ff;
				g_ackDataBuf[i++] = (pInfo->measure.temp2)>>8; 
				g_ackDataBuf[i++] = (pInfo->measure.temp2)&0x0ff;	// 38											
				packet->length = i;
				ResponseCustomProtocol(cmd, 0, 0, packet->length);	// 14bytes
			break;
		
			case CCMD_SET_MID:
				MidProc(packet);
			break;
			case CCMD_SET_RAMP_UPDOWN_TIME:
				if(packet->length != 2) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.rampUpTime = packet->data[0];
					pInfo->stat.rampDownTime = packet->data[1];
					Ack();
				}
			break;
					
			case CCMD_SET_HV_SET_VOLT:
				SetHvVolt(1, data);
				if(packet->length>1) {
					SetHvVolt(2, packet->data[1]);
				//	pInfo->stat.hvSetVolt2 = packet->data[1];
				}
				/*
				if(data&0x80) {
					hvToggle(pInfo, AUTO_TOGGLE1);
				}
				else {
					hvToggle(pInfo, 0);
				}
				*/
				//ResponseCustomProtocol(cmd, 0, 0, packet->length);	
				//ResponsCmdByte(CCMD_SET_HV_SET_VOLT, CCMD_SET_HV_SET_VOLT, pInfo->volatile_stat.hvOutputStatus);
				Ack();
				
				dbg_proc(3, "CCMD_SET_HV_SET_VOLT %x %d[V] T:%d\r\n", (data),ByteToVolt(data) ,IS_TOGGLE);
				
				OnRemoteCLcd(0,pInfo);
				
			break;
			
			case CCMD_SET_AUTO_DISCHARGE:
				if(packet->length != 7) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.autoDischargeStepCount = packet->data[0];
					pInfo->stat.autoDischargeVoltStep1 = packet->data[1];
					pInfo->stat.autoDischargeVoltStep2 = packet->data[2];
					pInfo->stat.autoDischargeVoltStep3 = packet->data[3];
					pInfo->stat.autoDischargeKeepTimeStep1 = packet->data[4];
					pInfo->stat.autoDischargeKeepTimeStep2 = packet->data[5];
					pInfo->stat.autoDischargeKeepTimeStep3 = packet->data[6];
					
					Ack();
				}
			break;
			case CCMD_SET_OCP:
				if(packet->length != 3) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.ocpAlarmControl = packet->data[0];
					pInfo->stat.ocpAlarmLimit = packet->data[1];
					pInfo->stat.ocpAlarmEvent = packet->data[2];
					Ack();
				}
			break;
			case CCMD_SET_ARC:
				if(packet->length != 3) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.arcAlarmControl = packet->data[0];
					pInfo->stat.arcAlarmLimit = packet->data[1];
					pInfo->stat.arcAlarmEvent = packet->data[2];
					Ack();
				}
			break;
			case CCMD_SET_BATTERY:
				if(packet->length != 3) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.battAlarmControl = packet->data[0];
					pInfo->stat.battAlarmLimit = packet->data[1];
					pInfo->stat.battEventControl = packet->data[2];
					Ack();
				}
				
			break;
		
			case CCMD_SET_AUTO_QUERY:
				if(packet->length != 2) {
					Nack(CRESULT_RANGE_ERR);
				}
				else {
					pInfo->stat.autoQueryControl = packet->data[0];
					pInfo->stat.autQueryIntervalTime = packet->data[1];					
					Ack();
				}
			break;
			
			case CCMD_SET_ALL:
				dbg_proc(1, "Set All\r\n");
				GetAllWriteToInfo(packet, pInfo);
				Ack();
			break;

			case CCMD_SET_OCP_ALARM_COUNT_CLEAR:
				dbg_proc(1, "CCMD_SET_OCP_ALARM_COUNT_CLEAR\r\n");
				pInfo->measure.ocpAlarmStatus = 0;
				
				pInfo->stat.ocpAlarmCount=data;				
				
				UpdateAlarmLED(pInfo, 1);
				OnRemoteCLcd(0, pInfo);
				GetAllProc(packet);				
			break;
			
			case CCMD_SET_ARC_ALARM_COUNT_CLEAR:
				dbg_proc(1, "CCMD_SET_ARC_ALARM_COUNT_CLEAR\r\n");
				pInfo->measure.arcAlarmStatus = 0;
				pInfo->stat.arcAlarmCount=data;
				
				UpdateAlarmLED(pInfo, 1);

				OnRemoteCLcd(0, pInfo);
				GetAllProc(packet);
			break;
			case CCMD_SET_ALL_ALARM_COUNT_CLEAR:
				dbg_proc(1, "CCMD_SET_ALL_ALARM_COUNT_CLEAR\r\n");
				pInfo->measure.ocpAlarmStatus = 0;
				pInfo->measure.arcAlarmStatus = 0;
				pInfo->stat.ocpAlarmCount=0;
				pInfo->stat.arcAlarmCount=0;
				
				UpdateAlarmLED(pInfo, 1);

				OnRemoteCLcd(0, pInfo);		
				GetAllProc(packet);		
			break;
			
			case CCMD_READ_INFO_ALL:
				packet->length = N_SETTING_INFO_SIZE;
				memcpy(g_ackDataBuf, (uint8_t*)(&pInfo->setting), packet->length);			
				ResponseCustomProtocol(CCMD_READ_INFO_ALL, 0, 0, packet->length);			
				dbg_proc(1, "CCMD_READ_INFO_ALL %d %x %x\r\n", packet->length, packet->data[0], packet->data[0]);
				//memset(g_ackDataBuf, 0x00, packet->length);
				break;
			case CCMD_WRITE_INFO_ALL:
				if(packet->length > sizeof(pInfo->setting.netInfo)) {
					StructSetting *pSetting = (StructSetting *)g_dataBuf;
					
					//printf("IP %d.%d.%d.%d %d.%d.%d.%d\r\n" \
						, pSetting->netInfo.ip[0], pSetting->netInfo.ip[1], pSetting->netInfo.ip[2], pSetting->netInfo.ip[3] \
						, pSetting->netInfo.gw[0], pSetting->netInfo.gw[1], pSetting->netInfo.gw[2], pSetting->netInfo.gw[3]);
					
					memcpy((uint8_t*)(&pInfo->setting.netInfo), (uint8_t*)(&pSetting->netInfo), 6+4*3);		
					memcpy((uint8_t*)(&pInfo->setting.leQlight.ip), (uint8_t*)(&pSetting->leQlight.ip), 4);
					pInfo->setting.leQlight.port = pSetting->leQlight.port;
					pInfo->setting.nPort = pSetting->nPort;
					
					pInfo->setting.serialSpeed = pSetting->serialSpeed;
					pInfo->setting.serialDatabit = pSetting->serialDatabit;
					pInfo->setting.serialParity = pSetting->serialParity;
					pInfo->setting.serialStopbit = pSetting->serialStopbit;
					
					Ack(); // Ack First: Baud rate or IP will be changed
					extern StructEscInfo infoPrev;
					if(CompareInfo(pInfo, &infoPrev, N_VALID_INFO_SIZE) != 0) {
						dbg_proc(1, "Setting has been changed!\r\n");
						SaveEscInfo();
						BaudRateChange(pInfo->setting.serialSpeed);
						memcpy((void*)&infoPrev, pInfo, sizeof(infoPrev));
					}					
				
				
					//ResponseCustomProtocol(CCMD_READ_INFO_ALL, 0, 0, packet->length);	
					dbg_proc(1, "CCMD_WRITE_INFO_ALL Done %d %x %x\r\n", packet->length, packet->data[0], packet->data[0]);					
					
				}
				else {
					dbg_proc(1, "CCMD_WRITE_INFO_ALL Failed %d %x %x\r\n", packet->length, packet->data[0], packet->data[0]);
					Nack(CRESULT_ABNORMAL);
				}
				
				break;
		}
	}

	//U08 testack[] = {0xc5, 0xc5, 0x01, 0x03, 0x07, 0x0f, 0x01, 0x02, 0x05, 0xf0, 0x29, 0xce};
	//HAL_UART_Transmit(&huart5, (uint8_t*)testack, sizeof(testack), 64);
}
  

