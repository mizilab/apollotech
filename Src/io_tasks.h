#ifndef _IO_TASKS_H
#define _IO_TASKS_H


#include "esc_proc.h"

	
/****************************************/	
/// define

	
/****************************************/	
/// type

	
	/*
	* LED
	1. LED_RUN: PG0
	2. LED_CHARGED: PG1
	3. LED_CHARGING: PG2
	4. LED_ALM_BAT : PG3
	5. LED_ALM_OCP : PG4
	6. LED_ALM_ARC : PG5
	7. LED_ALM_PRESSURE: PG6
	*/
	enum _LED_BIT {
		LED_RUN = 0,
		LED_CHARGED,
		LED_CHARGING,
		LED_ALM_BAT,
		LED_ALM_OCP,
		LED_ALM_ARC,
		LED_ALM_PRESSURE
	};
	
	enum  {
		DAC_CH_VOLT_1 = 0,
		DAC_CH_VOLT_2 = 1,
		DAC_CH_CURR_1 = 2,
		DAC_CH_CURR_2 = 3,
	};
	
	
/****************************************/	
/// Macro
	
#define BEEP()	BeepN(1)
#define ERR_BEEP()	BeepN(2);
	
#if CLCD_USE_MACRO	
#define	LcdPuts(x) do { uint16_t __index_;  for(__index_=0;__index_<strlen(x);__index_++) clcdWriteChar(x[__index_]); }while(0);
#define	UpdateLcdData(str, line)	do {uint8_t ret = 0;char i;	uint8_t nSpace = 0;	 \
	clcdSetCursor(line,0); LcdPuts(str);	nSpace = sizeof(str) - strlen(str)-1; \
	if(nSpace!=0) {	ret += nSpace;	for(i=0;i<nSpace;i++)LcdPuts(" ");} }while(0);
#endif

#define	UpdateLcdDataAt(str, line, at)	do {uint8_t ret = 0;char i;	uint8_t nSpace = 0;	 \
	clcdSetCursor(line,at); LcdPuts(str);	nSpace = sizeof(str) - strlen(str)-1; \
	if(nSpace!=0) {	ret += nSpace;	for(i=0;i<nSpace;i++)LcdPuts(" ");} }while(0);

#define LCD_EN(x)	HAL_GPIO_WritePin(cLCD_EN_GPIO_Port, cLCD_EN_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);
#define LCD_RS(x)	HAL_GPIO_WritePin(cLCD_RS_GPIO_Port, cLCD_RS_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);
#define LCD_RW(x)	HAL_GPIO_WritePin(cLCD_RW_GPIO_Port, cLCD_RW_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);

#define LCD_OE(x)	HAL_GPIO_WritePin(cLCD_OE_GPIO_Port, cLCD_OE_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);
#define LCD_DIR(x)	HAL_GPIO_WritePin(cLCD_DIR_GPIO_Port, cLCD_DIR_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);
#define LCD_RST(x)	HAL_GPIO_WritePin(cLCD_RST_GPIO_Port, cLCD_RST_Pin, x==1?GPIO_PIN_SET:GPIO_PIN_RESET);

/****************************************/
/// extern vars...
extern osMessageQueueId_t outputQueueHandle;
extern osMessageQueueId_t lanTxQueueHandle;
/****************************************/
/// FN declaration
void TaskInput(void *argument);
void TaskOutput(void *argument);


void RelayControl(uint8_t value) ;	
	
void BeepN(U08 n) ;
	
uint8_t UpdateCLcd(uint8_t state, StructEscInfo *pInfo, uint8_t key, uint8_t force);
	
void CLcdSetPos(uint8_t row, uint8_t col);	
void CLcdSetCursorPos(uint8_t row, uint8_t col);	
	
void clcdWriteCmd(uint8_t cmd);
void clcdWriteChar(uint8_t c);
void clcdSetCursor(uint8_t line, uint8_t pos);
void clcdInit(void);

uint8_t OnKeyCLcd(uint8_t state, StructEscInfo *pInfo, uint8_t key);
uint8_t OnRefreshCLcd(uint8_t state, StructEscInfo *pInfo);
uint8_t OnRemoteCLcd(uint8_t state, StructEscInfo *pInfo);
void OnRefreshCLcdCursor(void) ;

uint8_t StateToMenu(uint8_t state);
U08 UpdateAlarmLED(StructEscInfo *pInfo, U08 ledOnly);
U08 hvOnOffRamp(U08 onoff, int rampCount);
void ShowHvVolt(StructEscInfo *pInfo);

int SetDacVolt(U08 ch, int voltA);

void hvOnOff(U08 onoff);

U08 SetHvVolt(U08 ch, U08 value) ;
void swap(U08* a, U08*b) ;

U08 hvToggleMode(StructEscInfo *pInfo, U08 onoff);
void UpdateDac(void);
void BaudRateChange(U08 index) ;
void TaskMonitorLan(void);

void TaskCheck(char *buff) ;
void AdcCheck(char* buff);
void MinmaxCheck(char *buff);

void SetHvOnOFF(U08 onoff);
void ClearAlarm(StructEscInfo *pInfo);
void EventQlight(U08 alarmType);

uint8_t MPL3115A_Init(void);
float MPL3115A_Read_Press(void);
float MPL3115A_Read_Temp(void);
uint8_t MPL3115A_Read_Byte (uint16_t eep_address);
HAL_StatusTypeDef MPL3115A_Write_One (uint16_t eep_address, uint8_t byte_data);

#if CLCD_USE_MACRO	==0
void UpdateLcdData(char *str,uint8_t line);	
void	LcdPuts(char *x);
#endif
#endif // _IO_TASKS_H

