#include "lorawan.h"
//#include "app.h"
//#include "usart.h"
#include <stdarg.h>
#include <string.h>


#define RXLEN	512
uint8_t AT_Data_buf[RXLEN];
uint8_t RxData, EndOfTrans;
uint16_t RxIdx;
uint8_t TimeOut_Sign = 0;
Node_Info nodeInfo;

/* OTAA mode default setting */
//uint8_t devEui[] = "00 4A 77 00 66 FF FE CA";

uint8_t appEui[] = "00 00 00 00 00 00 00 01";

uint8_t appKey[] = "2B 7E 15 16 28 AE D2 A6 AB F7 15 88 09 CF 4F 3C";
//uint8_t appKey[] = "00 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF";

/* ABP mode default setting */
uint8_t appSKey[] = "2B 7E 15 16 28 AE D2 A6 AB F7 15 88 09 CF 4F 3C";

uint8_t nwkSKey[] = "2B 7E 15 16 28 AE D2 A6 AB F7 15 88 09 CF 4F 3C";

uint8_t devAddr[] = "FF FF FE A6";


char *StringStr(char *str, char *dest)
{
#define STR_BUFF_LEN	0x100
	int i = STR_BUFF_LEN;
	char *cp = str;
	char *s1, *s2;
	
	if (*dest == 0)
	{
		return str;
	}
	
//	while(*str)
	while(i--)
	{		
		s1 = cp;
		s2 = dest;
		
		while((*s1 == *s2) && *s1 && *s2)
		{
			s1++;
			s2++;
		}
		if(!*s2)
			return cp;
		cp++;
	}
	
	return NULL;
}

uint8_t StrToHex(uint8_t temp)
{
	uint8_t ret=0;
	if(temp>=48 && temp<=57)
	{
		ret = temp - 48;
		return ret;
	}

	if(temp>=65 && temp<=70)
	{
		ret = temp - 55;
		return ret;
	}

	if(temp>=97 && temp<=102)
	{
		ret = temp - 87;
		return ret;
	}
	
	return ret;
}

void Int2Str(uint8_t* str, int32_t intnum)
{
  uint32_t i, Div = 1000000000, j = 0, Status = 0;
 
  if(intnum < 0)
  {
	intnum = intnum*(-1);
	str[j++] = '-';
  }
  
  for (i = 0; i < 10; i++)
  {
    str[j++] = (intnum / Div) + 48;	/* '0' */

    intnum = intnum % Div;
    Div /= 10;
    if ((str[j-1] == '0') & (Status == 0))
    {
      j = 0;
    }
    else
    {
      Status++;
    }
  }
}

uint8_t *StringCat(uint8_t *str, const uint8_t *string)
{
	uint8_t *s = str;
	
	while(*s)
	{
		s++;
	}
	
	while(*string)
	{
		*s++ = *string++;
	}
	
	*s++ = '\r';
	*s++ = '\n';
	*s = '\0';
			
	return str;		
}

uint8_t *StringCat2(uint8_t *str, const uint8_t *string)
{
	uint8_t *s = str;
	
	while(*s)
	{
		s++;
	}
	
	while(*string)
	{
		*s++ = *string++;
	}
			
	return str;		
}

void LoraNode_Delay(uint32_t Delay)
{
  osDelay(Delay);
}

uint8_t Time_Out_Break(uint16_t MAX_time)
{
	static uint32_t time_start = 0;
	static uint32_t time_new = 0;
	uint16_t temp=0;

	if(TimeOut_Sign == 0)
	{
		TimeOut_Sign = 1;
		time_start = HAL_GetTick();
	}
	
	if(TimeOut_Sign == 1)
	{
		time_new = HAL_GetTick();
		temp = time_new - time_start;
		if(temp > MAX_time)
		{
			TimeOut_Sign = 0;
			return 1;
		}
	}

	return 0;
}

static void LoraNode_Reset(void)
{
	LORANODE_NRST_LOW();
	
	LoraNode_Delay(15);	//15ms
	
	LORANODE_NRST_HIGH();
}

void LoraNode_Mode_Set(LoraNode_Mode_T mode)
{
	if (mode == MODE_CMD )
		LORANODE_MODE_HIGH();
	if (mode == MODE_TRANSPARENT)
		LORANODE_MODE_LOW();
}

void LoraNode_Wake_Sleep(LoraNode_SleepMode_T mode)
{
	if (mode == MODE_WAKEUP)
	{
		/* wake signal, high for module wakeup, low for module sleep */	
		if (LORANODE_WAKE_STATUS != GPIO_PIN_SET)
		{
			/* wake module first, and wait 10 ms */
			LORANODE_WAKE_HIGH();
			
			LoraNode_Delay(20);				
		}
	}
	
	if (mode == MODE_SLEEP)
	{
		if (LORANODE_WAKE_STATUS != GPIO_PIN_RESET)
		{
			LORANODE_WAKE_LOW();		
		}
	}
}

void LoraNode_Send_AT(uint8_t *at_buf)
{
	RxIdx = 0;
	HAL_UART_Transmit_IT(&huart6, at_buf, strlen((char *)at_buf));
}

void LoraNode_Read(uint8_t *str)
{
	uint32_t i = 0;
	uint32_t startTime;
	
	startTime = HAL_GetTick();
	while(1)
	{
		if (RxIdx > 4) {
			if (AT_Data_buf[RxIdx - 2] == '\r' && AT_Data_buf[RxIdx - 1] == '\n') break;
		}
		
		if (HAL_GetTick() - startTime > 1000) break;
	}
	
	for(i=0 ; i < RxIdx; i++)
	{
		str[i] = AT_Data_buf[i];
	}
}
/*
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == USART6)
	{
		AT_Data_buf[RxIdx++] = RxData;
		if (RxIdx == RXLEN) {
			RxIdx = 0;
			EndOfTrans = 1;
		}
			
		HAL_UART_Receive_IT(&huart6, &RxData, 1);		
	}
}
*/
void LoraNode_Init(void)
{
	RxIdx = 0;
	HAL_UART_Receive_IT(&huart6, &RxData, 1);		
}

char *LoraNode_GetVer(void)
{
	uint8_t ASK_VER[] = "AT+VER?\r\n";
	char *temp = "+VER:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(ASK_VER);
	
	LoraNode_Read(AT_Data_buf);	/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		return ptr;	
	}

	return ptr;
}

char DEVEUI[17] = {0};
void LoraNode_GetDevEUI(Node_Info *LoRa_temp)
{
	uint8_t GetDevEUI[] = "AT+DEVEUI?\r\n";
	char *temp = "+DEVEUI:";
	char *ptr = NULL;

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetDevEUI);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */	
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<8;i++)
		{
			DEVEUI[2*i] = *((ptr + 8+(3*i))+1);
			DEVEUI[2*i+1] = *((ptr + 8+(3*i))+2);
		}
		for(uint8_t i=0;i<8;i++)
		{
			temp1 = *((ptr + 8+(3*i))+1);
			temp2 = *((ptr + 8+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->DevEUI[i] = temp1*16 + temp2;
		}
	}
	
}

void LoraNode_GetAppEUI(Node_Info *LoRa_temp)
{
	uint8_t GetAPPEUI[] = "AT+APPEUI?\r\n";
	char *temp = "+APPEUI:";
	char *ptr = NULL;

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetAPPEUI);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<8;i++)
		{
			temp1 = *((ptr + 8+(3*i))+1);
			temp2 = *((ptr + 8+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->AppEUI[i] = temp1*16 + temp2;
		}
	}
	
}

void LoraNode_GetAppKey(Node_Info *LoRa_temp)
{
	uint8_t GetAPPKEY[] = "AT+APPKEY?\r\n";	
	char *temp = "+APPKEY:";
	char *ptr = NULL;	

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetAPPKEY);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<16;i++)
		{
			temp1 = *((ptr + 8+(3*i))+1);
			temp2 = *((ptr + 8+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->AppKEY[i] = temp1*16 + temp2;
		}
	}
	
}

void LoraNode_GetDevAddr(Node_Info *LoRa_temp)
{
	uint8_t GetDEVAddr[] = "AT+DEVADDR?\r\n";
	char *temp = "+DEVADDR:";
	char *ptr = NULL;

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetDEVAddr);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<4;i++)
		{
			temp1 = *((ptr + 9+(3*i))+1);
			temp2 = *((ptr + 9+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->DevADDR[i] = temp1*16 + temp2;
		}
	}
	
}

void LoraNode_GetAppSKey(Node_Info *LoRa_temp)
{
	uint8_t GetAppSKey[] = "AT+APPSKEY?\r\n";
	char *temp = "+APPSKEY:";
	char *ptr = NULL;

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetAppSKey);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<16;i++)
		{
			temp1 = *((ptr + 9+(3*i))+1);
			temp2 = *((ptr + 9+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->AppSKEY[i] = temp1*16 + temp2;
		}
	}
}

void LoraNode_GetNwkSKey(Node_Info *LoRa_temp)
{
	uint8_t GetNwkSKey[] = "AT+NWKSKEY?\r\n";	
	char *temp = "+NWKSKEY:";
	char *ptr = NULL;	

	uint8_t temp1=0,temp2=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetNwkSKey);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i=0;i<16;i++)
		{
			temp1 = *((ptr + 9+(3*i))+1);
			temp2 = *((ptr + 9+(3*i))+2);
			if(temp1 > 0x40)
			{
				temp1 = temp1 - 55;
			}else{
					temp1 = temp1 - 48;
				}
			if(temp2 > 0x40)
			{
				temp2 = temp2 - 55;
			}else{
					temp2 = temp2 - 48;
				}
			LoRa_temp->NwkSKEY[i] = temp1*16 + temp2;
		}
	}

}

void LoraNode_GetNetMode(Node_Info *LoRa_temp)
{
	uint8_t GetNetMode[] = "AT+OTAA?\r\n";
	char *temp = "+OTAA:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetNetMode);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		if( *(ptr + 7) == 0x30)
		{
			LoRa_temp->NET_Mode = 0;
		}
		if(*(ptr + 7) == 0x31)
		{
			LoRa_temp->NET_Mode = 1;
		}
	}
	
}

void LoraNode_GetConfirm(Node_Info *LoRa_temp)
{
	uint8_t GetConfirm[] = "AT+CONFIRM?\r\n";
	char *temp = "+CONFIRM:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetConfirm);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	printf("AT+CONFIRM? %s\r\n", AT_Data_buf);
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		if(*(ptr + 10) == 0x30)
		{
			LoRa_temp->Confirm= 0;
		}
		if(*(ptr + 10) == 0x31)
		{
			LoRa_temp->Confirm= 1;
		}
	}
}

void LoraNode_GetFREQ(Node_Info *LoRa_temp)
{
	uint8_t FREQ[] = "AT+FREQ?\r\n";
	char *temp = "+FREQ:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(FREQ);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */

}


void LoraNode_GetRX2()
{
	uint8_t RX2[] = "AT+RX2?\r\n";
	char *temp = "+RX2:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(RX2);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
}

void LoraNode_GetJOIN()
{
	uint8_t JOIN[] = "AT+JOIN?\r\n";
	char *temp = "+JOIN:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(JOIN);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
}

void LoraNode_GetADR(Node_Info *LoRa_temp)
{
	uint8_t ADR[] = "AT+ADR?\r\n";
	char *temp = "+ADR:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(ADR);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		if(*(ptr + 6) == 0x30)
		{
			LoRa_temp->ADR= 0;
		}
		if(*(ptr + 6) == 0x31)
		{
			LoRa_temp->ADR= 1;
		}
	}
	
}

void LoraNode_GetVER(Node_Info *LoRa_temp)
{
	uint8_t VER[] = "AT+VER?\r\n";
	char *temp = "AS923";
	char *temp1 = "CLAA";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(VER);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->ver = 2; // cn470
	}

	if((ptr = StringStr((char *)AT_Data_buf, temp1)) != NULL)
	{
		LoRa_temp->ver = 1; // claa
	}
	
}


void LoraNode_GetSF(Node_Info *LoRa_temp)
{
	uint8_t SF[] = "AT+DATARATE?\r\n";
	char *temp = "+DATARATE:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SF);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->SF = (*(ptr + 11)) - 0x30;	
	}
	
}

void LoraNode_GetNBT(Node_Info *LoRa_temp)
{
	uint8_t GetNBTRIALS[] = "AT+NBTRIALS?\r\n";
	char *temp = "+NBTRIALS:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetNBTRIALS);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->NBT = (*(ptr + 11)) - 0x30;	
	}
	
}

void LoraNode_GetBAND(Node_Info *LoRa_temp)
{
	uint8_t GetBAND[] = "AT+BAND?\r\n";
	char *temp = "+BAND:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetBAND);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->BAND = (*(ptr + 7)) - 0x30;	
	}
}

void LoraNode_GetCLASS(Node_Info *LoRa_temp)
{
	uint8_t GetCLASS[] = "AT+CLASS?\r\n";
	char *temp = "+CLASS:";
	char *ptr = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetCLASS);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->CLASS = (*(ptr + 8)) - 0x30;	
	}
}

void LoraNode_GetSNR(Node_Info *LoRa_temp)
{
	uint8_t GetSNR[] = "AT+SNR?\r\n";
	char *temp = ",";
	char *temp1 = "]";
	char *ptr = NULL;
	char *ptr1 = NULL;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetSNR);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr1 = StringStr((char *)AT_Data_buf, temp1)) != NULL)
	{
		for(uint8_t i = 0;i<5;i++)
		{
			if(*(ptr1+i+3) == 0x2c)
			{
				break;
			}else
				{
					LoRa_temp->P2P_RSSI[i] = *(ptr1+i+3);
				}
		}
	}

	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		for(uint8_t i = 0;i<5;i++)
		{
			if(*(ptr+i+2) == 0x0d)
			{
				break;
			}else
				{
					LoRa_temp->P2P_SNR[i] = *(ptr+i+2);
				}
		}
		
	}
	
}

void LoraNode_GetPOWER(Node_Info *LoRa_temp)
{
	uint8_t GetPOWER[] = "AT+POWER?\r\n";
	char *temp = "+POWER:";
	char *ptr = NULL;
	uint8_t code = 0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetPOWER);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		LoRa_temp->POWER_code = (*(ptr + 8)) - 0x30;	
		code = LoRa_temp->POWER_code;
	}

	if(code == 0)
	{
		LoRa_temp->POWER = 20;
	}

	if(code == 1)
	{
		LoRa_temp->POWER = 17;
	}

	if(code == 2)
	{
		LoRa_temp->POWER = 16;
	}

	if(code == 3)
	{
		LoRa_temp->POWER = 14;
	}

	if(code == 4)
	{
		LoRa_temp->POWER = 12;
	}

	if(code == 5)
	{
		LoRa_temp->POWER = 10;
	}

	if(code == 6)
	{
		LoRa_temp->POWER = 7;
	}

	if(code == 7)
	{
		LoRa_temp->POWER = 5;
	}

	if(code == 8)
	{
		LoRa_temp->POWER = 2;
	}
	
}

void LoraNode_GetState(Send_Info *LoRa_temp)
{
	uint8_t GetConfirm[] = "AT+STATUS?\r\n";
	char *temp = "+STATUS:";
	char *ptr = NULL;
	uint8_t dec=0,dec1=0,dec2=0,dec3=0,dec4=0,dec5=0,dec6=0,dec7=0;
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(GetConfirm);
	HAL_Delay(200);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		dec =StrToHex( *(ptr + 9));
		dec1 = StrToHex( *(ptr + 10));

		LoRa_temp->Up_Result = dec*16 + dec1;

		dec = StrToHex( *(ptr + 12));
		dec1 = StrToHex( *(ptr + 13));

		LoRa_temp->Up_CH = dec*16 + dec1;

		dec = StrToHex( *(ptr + 15));
		dec1 = StrToHex( *(ptr + 16));

		LoRa_temp->Up_RATE = dec*16 + dec1;

		dec = StrToHex( *(ptr + 18));
		dec1 = StrToHex( *(ptr + 19));

		LoRa_temp->Up_DB = dec*16 + dec1;

		dec = StrToHex( *(ptr + 21));
		dec1 = StrToHex( *(ptr + 22));

		LoRa_temp->Dn_CH = dec*16 + dec1;

		dec = StrToHex( *(ptr + 24));
		dec1 = StrToHex( *(ptr + 25));

		LoRa_temp->Dn_RATE = dec*16 + dec1;

		dec = StrToHex( *(ptr + 30));
		dec1 = StrToHex( *(ptr + 31));

		LoRa_temp->Rx_Slot = dec*16 + dec1;

		dec = StrToHex( *(ptr + 33));
		dec1 = StrToHex( *(ptr + 34));

		LoRa_temp->Port = dec*16 + dec1;

		dec = StrToHex( *(ptr + 42));
		dec1 = StrToHex( *(ptr + 43));

		LoRa_temp->SNR = dec*16 + dec1;

		dec = StrToHex( *(ptr + 48));
		dec1 = StrToHex( *(ptr + 49));
		dec2 = StrToHex( *(ptr + 50));
		dec3 = StrToHex( *(ptr + 51));

		LoRa_temp->RSSI = (dec*16+ dec1) +(dec2*16 + dec3)*256 ;

		dec = StrToHex( *(ptr + 53));
		dec1 = StrToHex( *(ptr + 54));
		dec2 = StrToHex( *(ptr + 55));
		dec3 = StrToHex( *(ptr + 56));
		dec4 = StrToHex( *(ptr + 57));
		dec5 = StrToHex( *(ptr + 58));
		dec6 = StrToHex( *(ptr + 59));
		dec7 = StrToHex( *(ptr + 60));

		LoRa_temp->Dn_COUNT = (dec*16+ dec1) +(dec2*16 + dec3)*256 + (dec4*16+ dec5)*65536 +(dec6*16 + dec7)*256*65536 ;

		dec = StrToHex( *(ptr + 62));
		dec1 = StrToHex( *(ptr + 63));
		dec2 = StrToHex( *(ptr + 64));
		dec3 = StrToHex( *(ptr + 65));

		LoRa_temp->Remain = (dec*16+ dec1) +(dec2*16 + dec3)*256 ;
		
		dec = StrToHex( *(ptr + 67));
		dec1 = StrToHex( *(ptr + 68));
		dec2 = StrToHex( *(ptr + 69));
		dec3 = StrToHex( *(ptr + 70));

		LoRa_temp->Ret = (dec*16+ dec1) +(dec2*16 + dec3)*256 ;
		
		dec = StrToHex( *(ptr + 111));
		dec1 = StrToHex( *(ptr + 112));
		dec2 = StrToHex( *(ptr + 113));
		dec3 = StrToHex( *(ptr + 114));
		dec4 = StrToHex( *(ptr + 115));
		dec5 = StrToHex( *(ptr + 116));
		dec6 = StrToHex( *(ptr + 117));
		dec7 = StrToHex( *(ptr + 118));

		LoRa_temp->Up_COUNT = (dec*16+ dec1) +(dec2*16 + dec3)*256 + (dec4*16+ dec5)*65536 +(dec6*16 + dec7)*256*65536 ;

	}
}

int32_t LoraNode_SetGPIO(uint32_t pin, uint32_t state)
{	
	uint8_t GPIO[20] = "AT+GPIO=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	char *ptr = (char*)GPIO;
	
	Int2Str(buf, pin);
	strcat((char *)GPIO, (char *)buf);
	
	while(*++ptr);	//get the end
	*ptr++ = ',';

	memset(buf,0,5);
	Int2Str(buf, state);
	StringCat(GPIO, buf);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;

	LoraNode_Send_AT(GPIO);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if((ptr = StringStr((char *)AT_Data_buf, temp)) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetNetMode(LoraNode_NetMode_T mode)
{	
	uint8_t NetMode[20] = "AT+OTAA=";
	uint8_t buf[10] = {0};
	char *temp = "OK";
	
	Int2Str(buf, mode);
	StringCat(NetMode, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(NetMode);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetCFM(uint8_t nu)
{	
	uint8_t CFM[20] = "AT+CONFIRM=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(CFM, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(CFM);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetFREQ()
{	
	uint8_t FREQ[40] = "AT+FREQ=1,7,922100000\r\n";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	//Int2Str(buf, nu);
	//StringCat(ADR, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(FREQ);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetRX2()
{	
	//uint8_t RX2[40] = "AT+RX2=0,921900000\r\n";
	uint8_t RX2[40] = "AT+RX2=5,921900000\r\n";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	//Int2Str(buf, nu);
	//StringCat(ADR, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(RX2);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}


int32_t LoraNode_SetSTATUS()
{	
	uint8_t STATUS[40] = "AT+STATUS=1,1\r\n";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	//Int2Str(buf, nu);
	//StringCat(ADR, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(STATUS);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}


int32_t LoraNode_SetADR(uint8_t nu)
{	
	uint8_t ADR[20] = "AT+ADR=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(ADR, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(ADR);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetJOIN()
{	
	uint8_t JOIN[20] = "AT+JOIN=-1,1,8\r\n";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	//Int2Str(buf, nu);
	//StringCat(ADR, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(JOIN);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetSF(uint8_t nu)
{	
	uint8_t SF[20] = "AT+DATARATE=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(SF, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SF);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	printf("SF? %s\r\n", AT_Data_buf);
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetNBT(uint8_t nu)
{	
	uint8_t NBT[20] = "AT+NBTRIALS=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(NBT, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(NBT);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetBAND(uint8_t nu)
{	
	uint8_t BAND[20] = "AT+band=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(BAND, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(BAND);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoRaNode_SetCLASS(uint8_t nu)
{
	uint8_t CLASS[20] = "AT+CLASS=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(CLASS, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(CLASS);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetPOWER(uint8_t nu)
{	
	uint8_t POWER[20] = "AT+POWER=";
	uint8_t buf[5] = {0};
	char *temp = "OK";
	
	Int2Str(buf, nu);
	StringCat(POWER, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(POWER);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetDebug(uint32_t value)
{
	uint8_t SetDebug[20] = "AT+DEBUG=";
	uint8_t buf[10] = {0};
	char *temp = "OK";
	
	Int2Str(buf, value);
	StringCat(SetDebug, buf);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetDebug);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}


int32_t LoraNode_SetRADIO(uint32_t f)
{
	static uint8_t SetRADIO[40] = "AT+RADIO=";
	uint8_t buf[10] = {0}; 
	
	char *temp = "OK";
	
	Int2Str(buf, f);
	StringCat(SetRADIO, buf);

	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetRADIO);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetP2P(uint32_t f,uint8_t a,uint8_t b,uint8_t c,uint8_t d,uint8_t e,uint8_t ff,uint8_t g,uint16_t h)
{
	static uint8_t SetDebug[50] = "AT+RADIO=";
	uint8_t buf[10] = {0}; 
	uint8_t buf1[10] = {0}; 
	uint8_t buf2[10] = {0}; 
	uint8_t buf3[10] = {0}; 
	uint8_t buf4[10] = {0}; 
	uint8_t buf5[10] = {0}; 
	uint8_t buf6[10] = {0}; 
	uint8_t buf7[10] = {0}; 
	uint8_t buf8[10] = {0}; 
	
	uint8_t dou[2] = ",";
	char *temp = "OK";
	
	Int2Str(buf, f);
	StringCat2(SetDebug, buf);
	StringCat2(SetDebug, dou);

	Int2Str(buf1, a);
	StringCat2(SetDebug, buf1);
	StringCat2(SetDebug, dou);

	Int2Str(buf2, b);
	StringCat2(SetDebug, buf2);
	StringCat2(SetDebug, dou);

	Int2Str(buf3, c);
	StringCat2(SetDebug, buf3);
	StringCat2(SetDebug, dou);

	Int2Str(buf4, d);
	StringCat2(SetDebug, buf4);
	StringCat2(SetDebug, dou);

	Int2Str(buf5, e);
	StringCat2(SetDebug, buf5);
	StringCat2(SetDebug, dou);

	Int2Str(buf6, ff);
	StringCat2(SetDebug, buf6);
	StringCat2(SetDebug, dou);

	Int2Str(buf7, g);
	StringCat2(SetDebug, buf7);
	StringCat2(SetDebug, dou);

	Int2Str(buf8, h);
	StringCat(SetDebug, buf8);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetDebug);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetMINI_RF(uint32_t value)
{
	uint8_t MINI[20] = "AT+MINIRF=";
	uint8_t buf[10] = {0}; 
	char *temp = "OK";
	
	Int2Str(buf, value);
	StringCat(MINI, buf);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(MINI);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetAppEUI(uint8_t *dev)
{
	uint8_t SetAppEUI[50] = "AT+APPEUI=";
	char *temp = "OK";
	
	StringCat(SetAppEUI, dev);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetAppEUI);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;

}

int32_t LoraNode_SetAppKey(uint8_t *key)
{
	uint8_t SetAppKey[80] = "AT+APPKEY="; 
	char *temp = "OK";
	
	StringCat(SetAppKey, key);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetAppKey);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetAppSKey(uint8_t *skey)
{
	uint8_t SetAppSKey[80] = "AT+APPSKEY=";
	char *temp = "OK";
	
	StringCat(SetAppSKey, skey);
		
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetAppSKey);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_SetNwkSKey(uint8_t *nwk_skey)
{
	uint8_t SetNwkSKey[80] = "AT+NWKSKEY=";
	char *temp = "OK";
	
	StringCat(SetNwkSKey, nwk_skey);
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SetNwkSKey);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{
		return 0;
	}
	
	return -1;
}

int32_t LoraNode_Save(void)
{
	uint8_t SAVE[] = "AT+SAVE\n\r";
	char *temp = "OK";
	
	memset(AT_Data_buf,0,RXLEN);	       /* clear the buffer and wPoint */
	RxIdx = 0;
	
	LoraNode_Send_AT(SAVE);
	
	LoraNode_Read(AT_Data_buf);				/* receive the response of this command */
	
	if(StringStr((char *)AT_Data_buf, temp) != NULL)
	{	
		return 0;
	}
	
	return -1;
}

int32_t loraReqUplink(uint8_t *buffer, int32_t len, uint8_t CONFIRM)
{
	int32_t ret = -99;
	uint32_t startTime;

	if (!nodeInfo.Join) return -99;
	
	startTime = HAL_GetTick();
	while(LORANODE_BUSY_STATUS == GPIO_PIN_RESET)
	{
		if (HAL_GetTick() - startTime > 2000) {
			return -98;
		}
		//osDelay(1);
	}
	
	ret = LoraNode_Write_Receive_Bytes(buffer, len, CONFIRM);
  return ret;
}

int32_t LoraNode_Write_Receive_Bytes(uint8_t *buffer, int32_t len,uint8_t CONFIRM)
{
	int32_t ret = 0;
	TimeOut_Sign = 0;
	uint32_t startTime;
	
	LoraNode_Wake_Sleep(MODE_WAKEUP);
	LoraNode_Delay(10);

	if(LORANODE_MODE_STATUS == 0)	
	{
		startTime = HAL_GetTick();
		while(LORANODE_BUSY_STATUS == GPIO_PIN_RESET)
		{
			if (HAL_GetTick() - startTime > 2000)
			{
				ret = -1;            //timeout on ready
				return ret;
			}
		}
		HAL_UART_Transmit_IT(&huart6, buffer, len);

		while(LORANODE_BUSY_STATUS == GPIO_PIN_SET)
		{
			startTime = HAL_GetTick();
			if (HAL_GetTick() - startTime > 2000)
			{
				ret = -2;             //timeout on start to transmit
				return ret;
			}
		}
/*
		while(LORANODE_BUSY_STATUS == GPIO_PIN_RESET)
		{
			startTime = HAL_GetTick();
			if (HAL_GetTick() - startTime > 6000)
			{
				ret = -3;             //timeout by transmit
				return ret;
			}
		}
*/
		if(CONFIRM == 1)
		{
			while(LORANODE_STAT_STATUS == GPIO_PIN_RESET)
			{
				startTime = HAL_GetTick();
				if (HAL_GetTick() - startTime > 2000)
				{
					ret = -4;             //timeout by ack
					return ret;
				}
			}
		}
	}

	ret = 1;
	return ret;
}


int32_t LoraNode_GetJoined()
{
	uint8_t ADR[25] = "AT+LINK CHECK?\r\n"; 	//"AT+NJS\r\n";
	char *temp = "1";
	
	memset(AT_Data_buf,0,RXLEN);          /* clear the buffer and wPoint */
   RxIdx = 0;
   
   LoraNode_Send_AT(ADR);
   
   LoraNode_Read(AT_Data_buf);            /* receive the response of this command */
   
   printf("RET %s\r\n", AT_Data_buf);
   if(StringStr((char *)AT_Data_buf, temp) != NULL)
   {
      return 0;
   }
   
   return -1;
}

U32 g_devAddr = 0;
int32_t LoraNode_SetDevAddr(U32 devAddr)
{   
   uint8_t ADR[25] = "AT+DEVADDR=FF FF FE A6\r\n";
   uint8_t buf[5] = {0};
   char *temp = "OK";
   
   g_devAddr = devAddr;
   sprintf(ADR, "AT+DEVADDR=%x %x %x %x\r\n", ((devAddr&0xff000000)>>24), ((devAddr&0x00ff0000)>>16), ((devAddr&0x0000ff00)>>8), ((devAddr&0x000000ff)>>0) );
//   sprintf(ADR, "AT+DEVADDR=%2.2x %2.2x %2.2x %2.2x\r\n", ((devAddr&0xff000000)>>24), ((devAddr&0x00ff0000)>>16), ((devAddr&0x0000ff00)>>8), ((devAddr&0x000000ff)>>0) );
   printf("Set devAddr: %s \r\n", ADR);

   //Int2Str(buf, nu);
   //StringCat(ADR, buf);

   memset(AT_Data_buf,0,RXLEN);          /* clear the buffer and wPoint */
   RxIdx = 0;
   
   LoraNode_Send_AT(ADR);
   
   LoraNode_Read(AT_Data_buf);            /* receive the response of this command */
   //printf("RET %s\r\n", AT_Data_buf);
   if(StringStr((char *)AT_Data_buf, temp) != NULL)
   {	   
      return 0;
   }
   
   return -1;
}

/* Over-the-Air Activation */
void LoraNode_OTAA_Config(void)
{
	Node_Info *lora_temp;
	Node_Info lora_temp2;
	lora_temp = &lora_temp2;

	LoraNode_Reset();
	LoraNode_Wake_Sleep(MODE_WAKEUP);
	LoraNode_Mode_Set(MODE_CMD);	
	LoraNode_Delay(500);
		
	
	LoraNode_SetRX2();
	LoraNode_SetFREQ();
	LoraNode_SetSTATUS();
	LoraNode_SetSF(5); // 5 -> SF7
	LoraNode_SetADR(0);
	LoraNode_SetNetMode(NET_OTAA);
	LoRaNode_SetCLASS(CLASS_C);
	LoraNode_SetAppEUI(appEui);
	LoraNode_SetAppKey(appKey);
	LoraNode_SetJOIN();

	LoraNode_SetCFM(1);   LoraNode_GetConfirm(lora_temp);   printf("Confirm? %d\r\n", lora_temp->Confirm);

	LoraNode_Save();
	LoraNode_Mode_Set(MODE_TRANSPARENT);
}


void LoraNode_ABP_Config(void)
{   
   Node_Info *lora_temp;
	Node_Info lora_temp2;
	lora_temp = &lora_temp2;
	U32 devAddr = 0;
	

   LoraNode_Reset();
   LoraNode_Wake_Sleep(MODE_WAKEUP);
   LoraNode_Mode_Set(MODE_CMD);   
   LoraNode_Delay(500);
	LoraNode_SetNetMode(NET_ABP);
   
	LoraNode_GetDevEUI(lora_temp);
	devAddr = xcrc32((U08*)DEVEUI, 16, 0);
	LoraNode_SetDevAddr(devAddr);
   LoraNode_GetDevAddr(lora_temp);  
	printf("Lora GetDevADDR %8.8x\r\n", devAddr);
   LoraNode_SetRX2();
   LoraNode_SetFREQ();
   LoraNode_SetSTATUS();
   LoraNode_SetSF(5); // 5 -> SF7
   LoraNode_SetADR(0);
   
   //LoraNode_SetNetMode(NET_ABP);
   
   LoraNode_SetCFM(0);   LoraNode_GetConfirm(lora_temp);   printf("Confirm? %d\r\n", lora_temp->Confirm);
   //LoraNode_SetPOWER(8);
   //LoraNode_GetPOWER(lora_temp); printf("Power? %d\r\n", lora_temp->POWER);
   
   LoRaNode_SetCLASS(CLASS_C);
   
   LoraNode_SetAppSKey(appSKey);   
   LoraNode_SetNwkSKey(nwkSKey);
   LoraNode_Save();
    //LoraNode_Delay(500);
    //LoraNode_Reset();
    //LoraNode_Delay(500);
	//GET_LoraNode_Info(lora_temp);	printf("EUI %s SF %d NetMode %d\r\n", DEVEUI, lora_temp->SF, lora_temp->NET_Mode);
   LoraNode_Mode_Set(MODE_TRANSPARENT);
   //LoraNode_Delay(500);
}

void GET_LoraNode_Info(Node_Info *lora_temp)
{
	LoraNode_Mode_Set(MODE_CMD);	
	LoraNode_Wake_Sleep(MODE_WAKEUP);
	LoraNode_Delay(100);
	
	/* read some info after lora module initial */	
	LoraNode_GetDevEUI(lora_temp);
	LoraNode_Delay(10);
	
	LoraNode_GetAppEUI(lora_temp);	
	LoraNode_Delay(10);
	
	LoraNode_GetAppKey(lora_temp);	
	LoraNode_Delay(10);
	
	LoraNode_GetDevAddr(lora_temp);	
	LoraNode_Delay(10);	
	
	LoraNode_GetAppSKey(lora_temp);
	LoraNode_Delay(10);	
	
	LoraNode_GetNwkSKey(lora_temp);
	LoraNode_Delay(10);	
	
	LoraNode_GetCLASS(lora_temp);
	LoraNode_Delay(10);	
	
	LoraNode_GetRX2(lora_temp);
	LoraNode_Delay(10);
	
	LoraNode_GetBAND(lora_temp);
	LoraNode_Delay(10);	
	
	LoraNode_GetNetMode(lora_temp);
	LoraNode_Delay(10);	
}

void LoraNode_STA_Check(Node_Info *LoRa_temp)
{
	static uint8_t online_log = 0;
	if(LORANODE_STAT_STATUS == 0)
	{
		LoRa_temp->ONline = 0;
	}else
		{
			LoRa_temp->ONline = 1;
		}

	if(LORANODE_BUSY_STATUS == 0)
	{
		LoRa_temp->BUSY = 0;
	}else
		{
			LoRa_temp->BUSY = 1;
		}

	if(LoRa_temp->NET_Mode == 1)
	{
		if(LoRa_temp->ONline == 1)
		{
			if(LoRa_temp->BUSY == 1)
			{
				LoRa_temp->Join = 1;
			}
		}else{
				LoRa_temp->Join = 0;
			 }
	}else if(LoRa_temp->NET_Mode == 0)
		{
			if(LoRa_temp->BUSY == 1)
			{
				LoRa_temp->Join = 1;
			}else{
				LoRa_temp->Join = 0;
			 }
		}

	if(LoRa_temp->Join != online_log )
	{
		if(LoRa_temp->Join == 1)
		{
			//DEBUG_Printf("\r\n--> 网络已连接\r\n");
		}else{
				//Uart_Printf("\r\n 网关已断开连接 \r\n");
			}
	}
			
		online_log = LoRa_temp->Join;
}





////////////////// MQTT Downlink sample message //////////////////
/*
*
topic = application/1/device/d896e0ff000131aa/tx

message
{
  "confirmed": true,
  "fPort": 10,
  "data":"AQ=="
}
*/

//--------------------------------------------------------------------------------------------


