/*
    FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.

    ***************************************************************************
    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<
    ***************************************************************************

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
    the FAQ page "My application does not run, what could be wrong?".  Have you
    defined configASSERT()?

    http://www.FreeRTOS.org/support - In return for receiving this top quality
    embedded software for free we request you assist our global community by
    participating in the support forum.

    http://www.FreeRTOS.org/training - Investing in training allows your team to
    be as productive as possible as early as possible.  Now you can receive
    FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
    Ltd, and the world's leading authority on the world's leading RTOS.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/


 /******************************************************************************
 *
 * http://www.FreeRTOS.org/cli
 *
 ******************************************************************************/


/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
//#include "gpio.h"
/* Standard includes. */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/* FreeRTOS+CLI includes. */
#include "FreeRTOS_CLI.h"
#include "main.h"
#include "cmsis_os.h"

#include "apollo.h"
#include "esc_info.h"

#include "ad779x.h"

#ifndef  configINCLUDE_TRACE_RELATED_CLI_COMMANDS
	#define configINCLUDE_TRACE_RELATED_CLI_COMMANDS 0
#endif

#ifndef configINCLUDE_QUERY_HEAP_COMMAND
	#define configINCLUDE_QUERY_HEAP_COMMAND 1
#endif

extern osMessageQueueId_t lanTxQueueHandle;

extern osMessageQueueId_t lanTxQueueHandle;
extern osMessageQueueId_t outputQueueHandle;
extern osMessageQueueId_t inputQueueHandle;

/*
 * The function that registers the commands that are defined within this file.
 */
void vRegisterCLICommands( void );

/*
 * Implements the task-stats command.
 */
static BaseType_t prvTaskStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
 * Implements the run-time-stats command.
 */
#if( configGENERATE_RUN_TIME_STATS == 1 )
	static BaseType_t prvRunTimeStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
#endif /* configGENERATE_RUN_TIME_STATS */

static BaseType_t prvGPIOCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

static BaseType_t prvCmdCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvDacCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvMeasCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvScpiCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvNetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvEnvCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
static BaseType_t prvSetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );

/*
 * Implements the "query heap" command.
 */
#if( configINCLUDE_QUERY_HEAP_COMMAND == 1 )
static BaseType_t prvQueryHeapCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
#endif

/*
 * Implements the "trace start" and "trace stop" commands;
 */
#if( configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1 )
	static BaseType_t prvStartStopTraceCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString );
#endif

        
        
static const CLI_Command_Definition_t xTaskStats =
{
	"task-stats", /* The command string to type. */
	"\r\ntask-stats:\r\n Displays a table showing the state of each FreeRTOS task\r\n",
	prvTaskStatsCommand, /* The function to run. */
	0 /* No parameters are expected. */
};

static const CLI_Command_Definition_t xGPIO =
{
	"gpio",
	"\r\ngpio [read | set | clear]:\r\n Read current state or set/clear GPIO\r\n",
	prvGPIOCommand, /* The function to run. */
	2 /* The user can enter any number of commands. */
};


// Add by jwaani @2019-0904
static const CLI_Command_Definition_t xTEST =
{
	"test",
	"\r\nTEST [cmd]:\r\n Test commands. \r\nCmds: buz, at?, at1, at2, det, chk, task, repeat, redon, redoff, blueon, blueoff, greenon, greenoff,orangeon, orangeoff, dlevel, ver, mute, scpi, custom, mode, lora, lan, minmax, adc\r\n",
	prvCmdCommand,
	1 
};
// Add by jwaani @2019-0918
static const CLI_Command_Definition_t xDAC =
{
	"dac",
	"\r\nDAC [channel value]:\r\n Set DAC output. \r\n",
	prvDacCommand,
	2
};
// Add by jwaani @2019-0920
static const CLI_Command_Definition_t xMEAS =
{
	"meas",
	"\r\nMeasure [value]:\r\n Get values. \r\n",
	prvMeasCommand,
	1
};

static const CLI_Command_Definition_t xSCPI =
{
	"scpi",
	"\r\nSCPI CMDs:\r\n Send SCPI Command for test. \r\n",
	prvScpiCommand,
	1
};

static const CLI_Command_Definition_t xNET =
{
	"net",
	"\r\nNetwork CMDs: show all| ip | sn | gw |mac \r\n Cmds: show, ip, sm, gw, mac serverport, leip, lesm, legw, leport\r\na. show: net show all\r\nb. ip : Change ip\r\n ex> net ip 192.168.0.130\r\n c. sm : Change subnet mask\r\n d. gw : Change gateway\r\n e. mac : change MAC\r\n ex> net mac 4b:cb:7b:f7:bb:25\r\n f. serverport: change server's port\r\n ex> net serverport 41010\r\n g. leip, lesm, legw, leport: change LE's ip/sm/gw/port",
	prvNetCommand,
	2
};

static const CLI_Command_Definition_t xENV =
{
	"env",
	"\r\nEnvironment CMDs: info | load | reset | save  \r\nCmds: info, load, save, reset",
	prvEnvCommand,
	1
};

static const CLI_Command_Definition_t xSET =
{
	"set",
	"\r\nSet [property] [value]:\r\n Set value to ESC. \r\n Cmds: repeatdac, temp1, temp2, offsetvolt1, offsetvolt2, offsetcurr1, offsetcurr2, cid\r\n",
	prvSetCommand,
	2
};

#if( configGENERATE_RUN_TIME_STATS == 1 )
	/* Structure that defines the "run-time-stats" command line command.   This
	generates a table that shows how much run time each task has */
	static const CLI_Command_Definition_t xRunTimeStats =
	{
		"run-time-stats", /* The command string to type. */
		"\r\nrun-time-stats:\r\n Displays a table showing how much processing time each FreeRTOS task has used\r\n",
		prvRunTimeStatsCommand, /* The function to run. */
		0 /* No parameters are expected. */
	};
#endif /* configGENERATE_RUN_TIME_STATS */

#if( configINCLUDE_QUERY_HEAP_COMMAND == 1 )
	/* Structure that defines the "query_heap" command line command. */
	static const CLI_Command_Definition_t xQueryHeap =
	{
		"query-heap",
		"\r\nquery-heap:\r\n Displays the free heap space, and minimum ever free heap space.\r\n",
		prvQueryHeapCommand, /* The function to run. */
		0 /* The user can enter any number of commands. */
	};
#endif /* configQUERY_HEAP_COMMAND */

#if configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1
	/* Structure that defines the "trace" command line command.  This takes a single
	parameter, which can be either "start" or "stop". */
	static const CLI_Command_Definition_t xStartStopTrace =
	{
		"trace",
		"\r\ntrace [start | stop]:\r\n Starts or stops a trace recording for viewing in FreeRTOS+Trace\r\n",
		prvStartStopTraceCommand, /* The function to run. */
		1 /* One parameter is expected.  Valid values are "start" and "stop". */
	};
#endif /* configINCLUDE_TRACE_RELATED_CLI_COMMANDS */

/*-----------------------------------------------------------*/

void vRegisterCLICommands( void )
{
	/* Register all the command line commands defined immediately above. */
	FreeRTOS_CLIRegisterCommand( &xTaskStats );	
	FreeRTOS_CLIRegisterCommand( &xGPIO );

	FreeRTOS_CLIRegisterCommand( &xTEST );
	FreeRTOS_CLIRegisterCommand( &xDAC );
	FreeRTOS_CLIRegisterCommand( &xMEAS );
	FreeRTOS_CLIRegisterCommand( &xSCPI );
	FreeRTOS_CLIRegisterCommand( &xNET );
	FreeRTOS_CLIRegisterCommand( &xENV );
	FreeRTOS_CLIRegisterCommand( &xSET );
	
	#if( configGENERATE_RUN_TIME_STATS == 1 )
	{
		FreeRTOS_CLIRegisterCommand( &xRunTimeStats );
	}
	#endif
	
	#if( configINCLUDE_QUERY_HEAP_COMMAND == 1 )
	{
		FreeRTOS_CLIRegisterCommand( &xQueryHeap );
	}
	#endif

	#if( configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1 )
	{
		FreeRTOS_CLIRegisterCommand( &xStartStopTrace );
	}
	#endif
}
/*-----------------------------------------------------------*/
static BaseType_t prvMeasCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[1];
	BaseType_t lParameterStringLength1;

	struct OutputMessage outputMsg;

	char strCmp[][16] = { 
		"arc",
		"curr",
		"volt",
	};
	
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
	
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {
		if( strncmp(strCmp[0], pcParameter[0], strlen(strCmp[0]))==0) {
			outputMsg.commandID = OUTPUT_MEAS_ARC;
			osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
			
			sprintf( pcWriteBuffer, "Read ARC?\r\n");
		}
		else if( strncmp(strCmp[1], pcParameter[0], strlen(strCmp[1]))==0) {
			outputMsg.commandID = OUTPUT_MEAS_CURR;
			osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
			
			sprintf( pcWriteBuffer, "Read CURR?\r\n");
		}
		else if( strncmp(strCmp[2], pcParameter[0], strlen(strCmp[2]))==0) {
			outputMsg.commandID = OUTPUT_MEAS_VOLT;
			osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
			
			sprintf( pcWriteBuffer, "Read VOLT?\r\n");
		}
		else {
			sprintf( pcWriteBuffer, "Invalid parameters..Channel should be 0 or 1 \r\n" );
		}
	}		
		

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}

static BaseType_t prvEnvCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[1];
	BaseType_t lParameterStringLength1;


	const char strCmp[][16] = { 
		"info",
		"load",
		"save",
		"reset"
	};
	
	wiz_NetInfo *wiznet_info = GetNetInfo();
	StructEscInfo *pInfo = GetEscInfo();
	
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {
		if( strncmp(strCmp[0], pcParameter[0], strlen(strCmp[0]))==0) {
			sprintf( pcWriteBuffer, "Network\r\nMAC %x:%x:%x:%x:%x:%x\r\nip %d.%d.%d.%d\r\n sm %d.%d.%d.%d\r\n gw %d.%d.%d.%d\r\n\r\npInfo->setting.server\r\nip %d.%d.%d.%d\r\nport %d\r\n\r\nLE\r\nip %d.%d.%d.%d\r\n port %d\r\n"
			, wiznet_info->mac[0], wiznet_info->mac[1], wiznet_info->mac[2], wiznet_info->mac[3], wiznet_info->mac[4], wiznet_info->mac[5]
			, wiznet_info->ip[0], wiznet_info->ip[1], wiznet_info->ip[2], wiznet_info->ip[3]
			, wiznet_info->sn[0], wiznet_info->sn[1], wiznet_info->sn[2], wiznet_info->sn[3]
			, wiznet_info->gw[0], wiznet_info->gw[1], wiznet_info->gw[2], wiznet_info->gw[3]
			,pInfo->setting.serveIp[0],pInfo->setting.serveIp[1],pInfo->setting.serveIp[2],pInfo->setting.serveIp[3]
			,pInfo->setting.nPort
			
			,pInfo->setting.leQlight.ip[0],pInfo->setting.leQlight.ip[1],pInfo->setting.leQlight.ip[2],pInfo->setting.leQlight.ip[3]
			,pInfo->setting.leQlight.port

			);
			extern char DEVEUI[17];
			extern U32 g_devAddr;
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "\r\nEUI %s DevADDR %8.8x \r\nShow set values... \r\n" , DEVEUI, g_devAddr);
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Temp1/2 tune setted %2.1f , %2.1f \r\n", pInfo->setting.temp1Tune/(float)10.0 ,  pInfo->setting.temp2Tune/(float)10.0);
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset volt1/2 setted %dmV  %dmV\r\n", pInfo->setting.offsetVoltA , pInfo->setting.offsetVoltB);
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset curr1/2 setted %dmV %dmV \r\n", pInfo->setting.offsetCurrA, pInfo->setting.offsetCurrB );
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "cutOff1/2 setted %d, %d \r\n", pInfo->setting.cutOffA , pInfo->setting.cutOffB );
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Controller ID(CID) setted %d \r\n", pInfo->setting.cid );
		}
		else if( strncmp(strCmp[1], pcParameter[0], strlen(strCmp[1]))==0) {
			if(LoadEscInfo()<0) {
				sprintf( pcWriteBuffer, "FAIL to load informations from EEPROM\r\n");
			}
			else {
				sprintf( pcWriteBuffer, "Load informations from EEPROM done\r\n");				
			}
		}
		else if( strncmp(strCmp[2], pcParameter[0], strlen(strCmp[2]))==0) {
			if(SaveEscInfo()<0) {
				sprintf( pcWriteBuffer, "FAIL to save informations to EEPROM\r\n");
			}
			else {
				sprintf( pcWriteBuffer, "Save informations to EEPROM done\r\n");
			}
		}
		else if( strncmp(strCmp[3], pcParameter[0], strlen(strCmp[3]))==0) {
			ResetEscInfo();			
			sprintf( pcWriteBuffer, "Reset informations to default\r\n");
			
		}
		else {
			sprintf( pcWriteBuffer, "Invalid parameters...\r\n" );
		}
	}			

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}

static BaseType_t prvNetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[2];
	BaseType_t lParameterStringLength1;
	BaseType_t lParameterStringLength2;

	int value[6];
	int ret = 0;
	
	wiz_NetInfo *wiznet_info = GetNetInfo();
	StructEscInfo *pInfo = GetEscInfo();
	
	
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
	
	pcParameter[1] = FreeRTOS_CLIGetParameter(pcCommandString, 2, &lParameterStringLength2);
	sprintf( pcWriteBuffer, "[2]%s", pcParameter[1] );
	
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {
		if( strncmp("show", pcParameter[0], strlen("show"))==0) { // show
			//outputMsg.commandID = OUTPUT_MEAS_ARC;
			//osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
//			sprintf( pcWriteBuffer, "Network\r\nMAC %x:%x:%x:%x:%x:%x\r\nip %d.%d.%d.%d\r\n sm %d.%d.%d.%d\r\n gw %d.%d.%d.%d\r\n\r\npInfo->setting.server\r\nip %d.%d.%d.%d\r\nport %d\r\n\r\nLE\r\nip %d.%d.%d.%d\r\n sm %d.%d.%d.%d\r\n gw %d.%d.%d.%d\r\nport %d\r\n"
		
			sprintf( pcWriteBuffer, "Network\r\nMAC %x:%x:%x:%x:%x:%x\r\nip %d.%d.%d.%d\r\n sm %d.%d.%d.%d\r\n gw %d.%d.%d.%d\r\n\r\npInfo->setting.server\r\nip %d.%d.%d.%d\r\nport %d\r\n\r\nLE\r\nip %d.%d.%d.%d\r\n port %d\r\n"
			, wiznet_info->mac[0], wiznet_info->mac[1], wiznet_info->mac[2], wiznet_info->mac[3], wiznet_info->mac[4], wiznet_info->mac[5]
			, wiznet_info->ip[0], wiznet_info->ip[1], wiznet_info->ip[2], wiznet_info->ip[3]
			, wiznet_info->sn[0], wiznet_info->sn[1], wiznet_info->sn[2], wiznet_info->sn[3]
			, wiznet_info->gw[0], wiznet_info->gw[1], wiznet_info->gw[2], wiznet_info->gw[3]
			,pInfo->setting.serveIp[0],pInfo->setting.serveIp[1],pInfo->setting.serveIp[2],pInfo->setting.serveIp[3]
//			,pInfo->setting.server.sm[0],pInfo->setting.server.sm[1],pInfo->setting.server.sm[2],pInfo->setting.server.sm[3]
//			,pInfo->setting.server.gw[0],pInfo->setting.server.gw[1],pInfo->setting.server.gw[2],pInfo->setting.server.gw[3]
			,pInfo->setting.nPort
			
			,pInfo->setting.leQlight.ip[0],pInfo->setting.leQlight.ip[1],pInfo->setting.leQlight.ip[2],pInfo->setting.leQlight.ip[3]
//			,pInfo->setting.leQlight.sm[0],pInfo->setting.leQlight.sm[1],pInfo->setting.leQlight.sm[2],pInfo->setting.leQlight.sm[3]
//			,pInfo->setting.leQlight.gw[0],pInfo->setting.leQlight.gw[1],pInfo->setting.leQlight.gw[2],pInfo->setting.leQlight.gw[3]
			,pInfo->setting.leQlight.port

			);
		}
		else if( strncmp("ip", pcParameter[0], strlen("ip"))==0) { // ip
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {
				wiznet_info->ip[0] = value[0];
				wiznet_info->ip[1] = value[1];
				wiznet_info->ip[3] = value[2];
				wiznet_info->ip[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nip %d.%d.%d.%d\r\n"
				, wiznet_info->ip[0], wiznet_info->ip[1], wiznet_info->ip[2], wiznet_info->ip[3]
				);
				
				ctlnetwork(CN_SET_NETINFO, (void*) wiznet_info);
				ctlnetwork(CN_GET_NETINFO, (void*) wiznet_info);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		else if( strncmp("sm", pcParameter[0], strlen("sm"))==0) { // sub net
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				wiznet_info->sn[0] = value[0];
				wiznet_info->sn[1] = value[1];
				wiznet_info->sn[3] = value[2];
				wiznet_info->sn[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nsn %d.%d.%d.%d\r\n"
				, wiznet_info->sn[0], wiznet_info->sn[1], wiznet_info->sn[2], wiznet_info->sn[3]
				);
				
				ctlnetwork(CN_SET_NETINFO, (void*) wiznet_info);
				ctlnetwork(CN_GET_NETINFO, (void*) wiznet_info);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		else if( strncmp("gw", pcParameter[0], strlen("gw"))==0) { // gateway
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				wiznet_info->gw[0] = value[0];
				wiznet_info->gw[1] = value[1];
				wiznet_info->gw[3] = value[2];
				wiznet_info->gw[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\ngw %d.%d.%d.%d\r\n"
				, wiznet_info->gw[0], wiznet_info->gw[1], wiznet_info->gw[2], wiznet_info->gw[3]
				);
				
				ctlnetwork(CN_SET_NETINFO, (void*) wiznet_info);
				ctlnetwork(CN_GET_NETINFO, (void*) wiznet_info);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		else if( strncmp("mac", pcParameter[0], strlen("mac"))==0) { // mac
			ret = sscanf(pcParameter[1], "%x:%x:%x:%x:%x:%x", &value[0], &value[1], &value[2], &value[3], &value[4], &value[5]);
			
			if(ret==4) {				
				wiznet_info->mac[0] = value[0];
				wiznet_info->mac[1] = value[1];
				wiznet_info->mac[2] = value[2];
				wiznet_info->mac[3] = value[3];
				wiznet_info->mac[4] = value[4];
				wiznet_info->mac[5] = value[5];
				
				sprintf( pcWriteBuffer, "Set \r\nMAC %x:%x:%x:%x:%x:%x"
				, wiznet_info->mac[0], wiznet_info->mac[1], wiznet_info->mac[2], wiznet_info->mac[3], wiznet_info->mac[4], wiznet_info->mac[5]
				);
				
				ctlnetwork(CN_SET_NETINFO, (void*) wiznet_info);
				ctlnetwork(CN_GET_NETINFO, (void*) wiznet_info);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
/*
		else if( strncmp("serverip", pcParameter[0], strlen("serverip"))==0) { // pInfo->setting.server ip
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {
				serverip[0] = value[0];
				serverip[1] = value[1];
				serverip[3] = value[2];
				serverip[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nip %d.%d.%d.%d\r\n"
				, serverip[0], serverip[1], serverip[2], serverip[3]
				);
				
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		else if( strncmp("serversm", pcParameter[0], strlen("serversm"))==0) { // sub net
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				pInfo->setting.server.sm[0] = value[0];
				pInfo->setting.server.sm[1] = value[1];
				pInfo->setting.server.sm[3] = value[2];
				pInfo->setting.server.sm[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nsn %d.%d.%d.%d\r\n"
				, pInfo->setting.server.sm[0], pInfo->setting.server.sm[1], pInfo->setting.server.sm[2], pInfo->setting.server.sm[3]
				);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		else if( strncmp("servergw", pcParameter[0], strlen("servergw"))==0) { // gateway
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				pInfo->setting.server.gw[0] = value[0];
				pInfo->setting.server.gw[1] = value[1];
				pInfo->setting.server.gw[3] = value[2];
				pInfo->setting.server.gw[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\ngw %d.%d.%d.%d\r\n"
				, pInfo->setting.server.gw[0], pInfo->setting.server.gw[1], pInfo->setting.server.gw[2], pInfo->setting.server.gw[3]
				);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing ip");
			}
		}
		*/
		else if( strncmp("serverport", pcParameter[0], strlen("serverport"))==0) { // pInfo->setting.server port
			ret = sscanf(pcParameter[1], "%d", &value[0]);
			
			if(ret==1) {				
				pInfo->setting.nPort = value[0];
				
				sprintf( pcWriteBuffer, "Set \r\nServer port %d\r\n", pInfo->setting.nPort);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing serverport");
			}
		}
		else if( strncmp("leip", pcParameter[0], strlen("leip"))==0) { // LE ip
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {
				pInfo->setting.leQlight.ip[0] = value[0];
				pInfo->setting.leQlight.ip[1] = value[1];
				pInfo->setting.leQlight.ip[3] = value[2];
				pInfo->setting.leQlight.ip[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nip %d.%d.%d.%d\r\n"
				, pInfo->setting.leQlight.ip[0], pInfo->setting.leQlight.ip[1], pInfo->setting.leQlight.ip[2], pInfo->setting.leQlight.ip[3]
				);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing leip");
			}
		}
		else if( strncmp("lesm", pcParameter[0], strlen("lesm"))==0) { // sub net
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				pInfo->setting.leQlight.sm[0] = value[0];
				pInfo->setting.leQlight.sm[1] = value[1];
				pInfo->setting.leQlight.sm[3] = value[2];
				pInfo->setting.leQlight.sm[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\nsn %d.%d.%d.%d\r\n"
				, pInfo->setting.leQlight.sm[0], pInfo->setting.leQlight.sm[1], pInfo->setting.leQlight.sm[2], pInfo->setting.leQlight.sm[3]
				);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing lesm");
			}
		}
		else if( strncmp("legw", pcParameter[0], strlen("legw"))==0) { // gateway
			ret = sscanf(pcParameter[1], "%d.%d.%d.%d", &value[0], &value[1], &value[2], &value[3]);
			
			if(ret==4) {				
				pInfo->setting.leQlight.gw[0] = value[0];
				pInfo->setting.leQlight.gw[1] = value[1];
				pInfo->setting.leQlight.gw[3] = value[2];
				pInfo->setting.leQlight.gw[3] = value[3];		
				
				sprintf( pcWriteBuffer, "Set \r\ngw %d.%d.%d.%d\r\n"
				, pInfo->setting.leQlight.gw[0], pInfo->setting.leQlight.gw[1], pInfo->setting.leQlight.gw[2], pInfo->setting.leQlight.gw[3]
				);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing legw");
			}
		}
		else if( strncmp("leport", pcParameter[0], strlen("leport"))==0) { // le port
			ret = sscanf(pcParameter[1], "%d", &value[0]);
			
			if(ret==1) {				
				pInfo->setting.leQlight.port = value[0];
				
				sprintf( pcWriteBuffer, "Set \r\nLE port %d\r\n", pInfo->setting.leQlight.port);
			}
			else {
				sprintf( pcWriteBuffer, "Error parshing leport");
			}
		}
		else {
			sprintf( pcWriteBuffer, "Invalid parameters...\r\n" );
		}
	}			

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}

static BaseType_t prvScpiCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[2];
	BaseType_t lParameterStringLength1;

	

		uint8_t i;
	
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
		
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {			
		for(i=0;i<strlen(pcParameter[0]);i++) {
			if(pcParameter[0][i] == '_') {
				enQueue(' ');
			}
			else {
				// make upper case for SCPI
				
				enQueue(toupper(pcParameter[0][i]));

			}
		}
		
		enQueue('\n');
		
			
		//sprintf( pcWriteBuffer, "cmd = %s\r\n", pcParameter[0]);
		
	}
	else {
		sprintf( pcWriteBuffer, "Invalid parameters.. \r\n" );
	}		
		

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}


static BaseType_t prvDacCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[2];
	BaseType_t lParameterStringLength1;
	BaseType_t lParameterStringLength2;
	struct OutputMessage outputMsg;
		
	int channel = 0;
	int value;
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
	pcParameter[1] = FreeRTOS_CLIGetParameter(pcCommandString, 2, &lParameterStringLength2);
	sprintf( pcWriteBuffer, "[2]%s", pcParameter[1] );
	
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {
		value = atoi(pcParameter[1]);
		
		if( strncmp("relay", pcParameter[0], strlen("relay"))==0) {
			outputMsg.commandID = OUTPUT_RELAY;
			outputMsg.value = value;
			osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
		}
		else if( strncmp("shutdown", pcParameter[0], strlen("shutdown"))==0
			|| strncmp("shutd", pcParameter[0], strlen("shutd"))==0
			) {
			outputMsg.commandID = OUTPUT_SHUTDOWN;
			outputMsg.value = value;
			osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
		}
		else {
			sprintf( pcWriteBuffer, "Invalid parameters.. Unknown parameters \r\n" );
		}
	}
	else {
		channel = atoi(pcParameter[0]);
		value = atoi(pcParameter[1]);
		
		
		if ( channel >= 0 && channel <=1 ) 
		{
			if ( value >= 0 && value <= DAC_VREF ) 
			{
				outputMsg.commandID = OUTPUT_DAC;
				outputMsg.channel = channel;
				outputMsg.value = (value * ADC_STEP) / DAC_VREF;
				osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
				
				sprintf( pcWriteBuffer, "DAC ch %d is set to %d(%d mV)\r\n", channel, outputMsg.value, value );
			}
			else {
				sprintf( pcWriteBuffer, "Invalid parameters.. vaule should be 0~2700[mV] \r\n" );
			}
		}
		else if ( channel >= 2 && channel <=3 ) // dac for CURR 
		{
			if ( value >= 0 && value <= DAC_VREF ) 
			{
				outputMsg.commandID = OUTPUT_DAC;
				outputMsg.channel = channel;
				outputMsg.value = (value * ADC_STEP) / DAC_VREF;
				osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
				
				sprintf( pcWriteBuffer, "DAC ch %d (Current) is set to %d(%d mV)\r\n", channel, outputMsg.value, value );
			}
			else {
				sprintf( pcWriteBuffer, "Invalid parameters.. vaule should be 0~2800[mV] \r\n" );
			}
		}
		else {
			sprintf( pcWriteBuffer, "Invalid parameters..Channel should be 0 or 1 \r\n" );
		}
	}		
		

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}

static BaseType_t prvSetCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[2];
	BaseType_t lParameterStringLength1;
	BaseType_t lParameterStringLength2;
	struct OutputMessage outputMsg;
	StructEscInfo *pInfo = GetEscInfo();
		
//	int channel = 0;
	int value;
	
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength1);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
	pcParameter[1] = FreeRTOS_CLIGetParameter(pcCommandString, 2, &lParameterStringLength2);
	sprintf( pcWriteBuffer, "[2]%s", pcParameter[1] );
	
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	if( lParameterStringLength1 > 1) {
		value = atoi(pcParameter[1]);
		
		if( strncmp("temp1", pcParameter[0], strlen("temp1"))==0) {
			pInfo->setting.temp1Tune = value;
			sprintf( pcWriteBuffer, "Temp1 tune setted %2.1f (%d)\r\n", pInfo->setting.temp1Tune/(float)10.0 , value);
		}
		else if( strncmp("temp2", pcParameter[0], strlen("temp2"))==0) {
			pInfo->setting.temp2Tune = value;
			sprintf( pcWriteBuffer, "Temp2 tune setted %2.1f (%d)\r\n", pInfo->setting.temp2Tune/(float)10.0 , value);
		}		
		else if( strncmp("repeatdac", pcParameter[0], strlen("repeatdac"))==0) {
			pInfo->setting.noRepeatDac = value==0?1:0;
			sprintf( pcWriteBuffer, "Repeat DAC? %d \r\n", pInfo->setting.noRepeatDac );
		}		
		else if( strncmp("offsetvolt1", pcParameter[0], strlen("offsetvolt1"))==0) {
			pInfo->setting.offsetVoltA = value;
			sprintf( pcWriteBuffer, "Offset volt1 setted %dmV \r\n", pInfo->setting.offsetVoltA );
		}		
		else if( strncmp("offsetvolt2", pcParameter[0], strlen("offsetvolt2"))==0) {
			pInfo->setting.offsetVoltB = value;
			sprintf( pcWriteBuffer, "Offset volt2 setted %dmV \r\n", pInfo->setting.offsetVoltB );
		}		
		else if( strncmp("offsetcurr1", pcParameter[0], strlen("offsetcurr1"))==0) {
			pInfo->setting.offsetCurrA = value;
			sprintf( pcWriteBuffer, "Offset curr1 setted %dmV \r\n", pInfo->setting.offsetCurrA );
		}		
		else if( strncmp("offsetcurr2", pcParameter[0], strlen("offsetcurr2"))==0) {
			pInfo->setting.offsetCurrB = value;
			sprintf( pcWriteBuffer, "Offset curr2 setted %d \r\n", pInfo->setting.offsetCurrB );
		}		
		else if( strncmp("cutoff1", pcParameter[0], strlen("cutoff1"))==0) {
			pInfo->setting.cutOffA = value;
			sprintf( pcWriteBuffer, "cutOff1 setted %d \r\n", pInfo->setting.cutOffA );
		}	
		else if( strncmp("cutoff2", pcParameter[0], strlen("cutoff1"))==0) {
			pInfo->setting.cutOffB = value;
			sprintf( pcWriteBuffer, "cutOff2 setted %dmV \r\n", pInfo->setting.cutOffB );
		}
		else if( strncmp("cid", pcParameter[0], strlen("cid"))==0 || strncmp("did", pcParameter[0], strlen("did"))==0) {
			pInfo->setting.cid = value;
			sprintf( pcWriteBuffer, "Controller ID(CID) setted %d \r\n", pInfo->setting.cid );
		}		
		else if( strncmp("chk", pcParameter[0], strlen("chk"))==0) {
			extern uint32_t g_nSystemCheckCount;
			g_nSystemCheckCount = value;
			sprintf( pcWriteBuffer, "System Check time = %d mSec\r\n", g_nSystemCheckCount*10);
		}				
		else {
			sprintf( pcWriteBuffer, "Show set values... \r\n" );
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Temp1/2 tune setted %2.1f , %2.1f \r\n", pInfo->setting.temp1Tune/(float)10.0 ,  pInfo->setting.temp2Tune/(float)10.0);
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset volt1/2 setted %dmV  %dmV\r\n", pInfo->setting.offsetVoltA , pInfo->setting.offsetVoltB);
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset curr1/2 setted %dmV %dmV \r\n", pInfo->setting.offsetCurrA, pInfo->setting.offsetCurrB );
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "cutOff1/2 setted %d, %d \r\n", pInfo->setting.cutOffA , pInfo->setting.cutOffB );
			sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Controller ID(CID) setted %d \r\n", pInfo->setting.cid );
			//sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Repeat DAC? %d \r\n", pInfo->setting.noRepeatDac );
		}
	}
	else {
		//sprintf( pcWriteBuffer, "Invalid parameters..Channel should be 0 or 1 \r\n" );		
		sprintf( pcWriteBuffer, "Show set values... \r\n" );
		sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Temp1/2 tune setted %2.1f , %2.1f \r\n", pInfo->setting.temp1Tune/(float)10.0 ,  pInfo->setting.temp2Tune/(float)10.0);
		sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset volt1/2 setted %dmV  %dmV\r\n", pInfo->setting.offsetVoltA , pInfo->setting.offsetVoltB);
		sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Offset curr1/2 setted %dmV %dmV \r\n", pInfo->setting.offsetCurrA, pInfo->setting.offsetCurrB );
		sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "cutOff1/2 setted %d, %d \r\n", pInfo->setting.cutOffA , pInfo->setting.cutOffB );
		sprintf( pcWriteBuffer + strlen(pcWriteBuffer), "Controller ID(CID) setted %d \r\n", pInfo->setting.cid );
		
	}		
		

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}

static U08 boostOn = 0;
static BaseType_t prvCmdCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
	const char *pcParameter[1];
	BaseType_t lParameterStringLength;
	struct IOMessage txMessage;
	const char strModeSel[][7] = {"Local", "Lora", "Lan", "Unkown"};
		
	int arg;
	
	extern uint8_t OnRemoteCLcd(uint8_t state, StructEscInfo *pInfo);
	
	extern void ad7793CS2Control(unsigned char State);
	extern void ad7793CS1Control(unsigned char State);
	extern unsigned char ad7793RDYState(void);
	extern void ad7793TxByte(unsigned char Data);
	extern unsigned char ad7793RxByte(void);
	tAD779X_Device pTEMP2;
	pTEMP2.CSControl 	= &ad7793CS2Control;
	pTEMP2.RDYState 	= &ad7793RDYState;
	pTEMP2.TxByte		= &ad7793TxByte;
	pTEMP2.RxByte		= &ad7793RxByte;


	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );
	
	extern uint32_t g_adc_result[8];
	
	extern uint8_t g_ConnectedServer;
		StructEscInfo *pInfo = GetEscInfo();
	

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1,	&lParameterStringLength);
	/* Sanity check something was returned. */
	configASSERT( pcParameter );
	
	txMessage.commandID = CMD_TEST;
	if ( strncmp( pcParameter[0], "buz", strlen( "buz" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_BUZ;
		txMessage.argument = 2;
		osMessageQueuePut(inputQueueHandle, &txMessage, NULL, NULL);

	}
	else if ( strncmp( pcParameter[0], "reset", strlen( "reset" ) ) == 0 ) {
		NVIC_SystemReset();
	}
	else if ( strncmp( pcParameter[0], "hvout", strlen( "hvout" ) ) == 0 ) {
		GetEscInfo()->volatile_stat.hvOutputStatus ^= 1;
		OnRemoteCLcd(0,GetEscInfo());
	}
	else if ( strncmp( pcParameter[0], "at?", strlen( "at?" ) ) == 0 ) {		
		sprintf( pcWriteBuffer, "Auto Toggle? %x\r\n", GetEscInfo()->stat.hvAutoToggle);
	}
	else if ( strncmp( pcParameter[0], "at1", strlen( "at1" ) ) == 0 ) {
		GetEscInfo()->stat.hvAutoToggle ^= AUTO_TOGGLE1;
		sprintf( pcWriteBuffer, "Auto Toggle? %x\r\n", GetEscInfo()->stat.hvAutoToggle);
	}
	else if ( strncmp( pcParameter[0], "at2", strlen( "at2" ) ) == 0 ) {
		GetEscInfo()->stat.hvAutoToggle ^= AUTO_TOGGLE2;
		sprintf( pcWriteBuffer, "Auto Toggle? %x\r\n", GetEscInfo()->stat.hvAutoToggle);
	}
	else if ( strncmp( pcParameter[0], "toggle", strlen( "toggle" ) ) == 0 ) {
		swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
		sprintf( pcWriteBuffer, "Toggle? %d\r\n", IS_TOGGLE);
	}
	else if ( strncmp( pcParameter[0], "volt1", strlen( "volt1" ) ) == 0 ) {
		int value;
		int ret = sscanf(pcParameter[0], "volt1=%d", &value);
		U08 bValue = value;
		if(value<0) {
			bValue = -value;
			bValue |= 0x80;
		}
		SetHvVolt(1, bValue);
		sprintf( pcWriteBuffer, "Volt1 Set to %d %d %d\r\n", GetEscInfo()->stat.hvSetVolt1, value, bValue);
	}
	else if ( strncmp( pcParameter[0], "volt2", strlen( "volt2" ) ) == 0 ) {
		int value;
		int ret = sscanf(pcParameter[0], "volt2=%d", &value);
		U08 bValue = value;
		if(value<0) {
			bValue = -value;
			bValue |= 0x80;
		}
		SetHvVolt(2, bValue);
		sprintf( pcWriteBuffer, "Volt2 Set to %d %d %d\r\n", GetEscInfo()->stat.hvSetVolt2, value, bValue);
	}
	else if ( strncmp( pcParameter[0], "volts", strlen( "volts" ) ) == 0 ) {
		int value, value2;
		int ret = sscanf(pcParameter[0], "volts=%d,%d", &value, &value2);
		
		if(ret==2) {
			GetEscInfo()->stat.hvSetVolt1 = value;
			GetEscInfo()->stat.hvSetVolt2 = value2;
			
			if(value<0) {
				value = -value | 0x80;
			}
			else {
				value2 = -value2 | 0x80;
			}
			
			SetHvVolt(1, value);
			SetHvVolt(2, value2);			

			sprintf( pcWriteBuffer, "Volts Set to %d %d\r\n", GetEscInfo()->stat.hvSetVolt1, GetEscInfo()->stat.hvSetVolt2);
		}
		else {
			sprintf( pcWriteBuffer, "Volts parshing error! %s\r\n", pcParameter[0]);
		}
	}
	
	else if ( strncmp( pcParameter[0], "det", strlen( "det" ) ) == 0 ) {
		sprintf( pcWriteBuffer,"Det Batt:%d Charger:%d Eth:%d Link %d Lora:%d AP:%d\r\n"
			,DET_BATTERY
			,DET_CHARGER
			,DET_LAN
			,DET_LAN_LINK
			,DET_LORA
			,DET_AP
		);
	}
	else if ( strncmp( pcParameter[0], "chk", strlen( "chk" ) ) == 0 ) {
		extern uint8_t g_systemCheck;
		g_systemCheck ^= 1;
		sprintf( pcWriteBuffer, "System Check %d \r\n",g_systemCheck);
	}
	else if( strncmp("comm", pcParameter[0], strlen("comm"))==0) {
			extern U08 g_CustomState;
			extern long g_nUseQCount;
			
			sprintf( pcWriteBuffer, "Custom Comm State %d used %ld \r\n", g_CustomState, g_nUseQCount);
	}	
	else if ( strncmp( pcParameter[0], "task", strlen( "task" ) ) == 0 ) {
		TaskCheck(pcWriteBuffer);
	}
	else if ( strncmp( pcParameter[0], "uart8", strlen( "uart8" ) ) == 0 ) {
		char str[]="test!\r\n";
		extern UART_HandleTypeDef huart8;

		//HAL_UART_Transmit(&huart8, (uint8_t*)str, 7, 64);
		HAL_UART_Transmit_IT(&huart8, (uint8_t*)str, 7);
		sprintf( pcWriteBuffer, "Tx Test to GLCD \r\n");
	}
			
	else if ( strncmp( pcParameter[0], "repeat", strlen( "repeat" ) ) == 0 ) 
	{
		extern uint8_t g_sendAlive;
		g_sendAlive ^= 1;
		sprintf( pcWriteBuffer, "Send info repeat? %d \r\n", g_sendAlive);
	}
	else if ( strncmp( pcParameter[0], "temp", strlen( "temp" ) ) == 0 )  {

		AD779X_Init(&pTEMP2);
	}
	else if ( strncmp( pcParameter[0], "hvout", strlen( "hvout" ) ) == 0 ) {		
		boostOn ^=1;
		txMessage.commandID = CMD_HVOUT_BOOST;
		txMessage.argument = boostOn; // DAC_HV_PWR_CONTROL_Pin
		osMessageQueuePut(outputQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "HVOUT Boost (12V) %s \r\n", boostOn?"ON":"OFF");
	}	
	else if ( strncmp( pcParameter[0], "lcd", strlen( "lcd" ) ) == 0 ) {		
		extern void clcdInit();
		clcdInit();
		sprintf( pcWriteBuffer, "Init CLCD\r\n");
	}	
	else if ( strncmp( pcParameter[0], "battlow", strlen( "battlow" ) ) == 0 ) {
		extern U08 g_testBattLow;
		if(g_testBattLow==0) g_testBattLow = 0x01;
		else g_testBattLow = 0;
		txMessage.commandID = CMD_ALARM_BATT_LOW;
		txMessage.argument = GetEscInfo()->measure.battLowAlarmStatus;
		osMessageQueuePut(inputQueueHandle, &txMessage, NULL, NULL);	
		sprintf( pcWriteBuffer, "test Battery low alarm %x %d\r\n", g_testBattLow, UpdateAlarmLED(GetEscInfo()));		
	}
	else if ( strncmp( pcParameter[0], "battfull", strlen( "battfull" ) ) == 0 ) {
		extern U08 g_testBattLow;
		if(g_testBattLow==0) g_testBattLow = 0x10;
		else g_testBattLow = 0;
		sprintf( pcWriteBuffer, "test Battery Full %x \r\n", g_testBattLow);		
	}
	else if ( strncmp( pcParameter[0], "arc", strlen( "arc" ) ) == 0 ) {
		/*
		if(GetEscInfo()->stat.arcAlarmControl) {
			
			GetEscInfo()->stat.arcAlarmCount++;
			if(GetEscInfo()->stat.arcAlarmCount >= GetEscInfo()->stat.arcAlarmLimit) {
				GetEscInfo()->measure.arcAlarmStatus =1;
			}
		}
		txMessage.commandID = CMD_ALARM_ARC;
		txMessage.argument = GetEscInfo()->measure.arcAlarmStatus;
		osMessageQueuePut(inputQueueHandle, &txMessage, NULL, NULL);		
		sprintf( pcWriteBuffer, "test ARC alarm %d\r\n", UpdateAlarmLED(GetEscInfo()));		
		*/
		extern U08 g_testArc;
		g_testArc ^= 1;
		sprintf( pcWriteBuffer, "test ARC alarm %d \r\n", g_testArc);				
		
	}
	else if ( strncmp( pcParameter[0], "ocp", strlen( "ocp" ) ) == 0 ) {
		/*
		if(GetEscInfo()->stat.ocpAlarmControl) {
			GetEscInfo()->measure.ocpAlarmStatus =1;
			GetEscInfo()->stat.ocpAlarmCount++;
		}		
		txMessage.commandID = CMD_ALARM_OCP;
		txMessage.argument = GetEscInfo()->measure.ocpAlarmStatus;
		osMessageQueuePut(inputQueueHandle, &txMessage, NULL, NULL);	
		sprintf( pcWriteBuffer, "test OCP alarm %d \r\n", UpdateAlarmLED(GetEscInfo()));				
		*/
		extern U08 g_testOcp;
		g_testOcp ^= 1;
		sprintf( pcWriteBuffer, "test OCP alarm %d \r\n", g_testOcp);				
	}
	else if ( strncmp( pcParameter[0], "ack", strlen( "ack" ) ) == 0 ) {
		extern U08 plcFromWhere;
		plcFromWhere = COMM_MODE_LORA;
		Ack();
	}
	else if ( strncmp( pcParameter[0], "redon", strlen( "redon" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_RED_ON;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Red on \r\n");		
	}
	else if ( strncmp( pcParameter[0], "redoff", strlen( "redoff" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_RED_OFF;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Red off \r\n");		
	}
	else if ( strncmp( pcParameter[0], "orangeon", strlen( "orangeon" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_ORANGE_ON;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Orange on \r\n");		
	}
	else if ( strncmp( pcParameter[0], "orangeoff", strlen( "orangeoff" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_ORANGE_OFF;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Orange off \r\n");		
	}
	else if ( strncmp( pcParameter[0], "greenon", strlen( "greenon" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_GREEN_ON;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Green on \r\n");		
	}
	else if ( strncmp( pcParameter[0], "greenoff", strlen( "greenoff" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_GREEN_OFF;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Green off \r\n");		
	}
	else if ( strncmp( pcParameter[0], "blueon", strlen( "blueon" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_BLUE_ON;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Blue on \r\n");		
	}
	else if ( strncmp( pcParameter[0], "blueoff", strlen( "blueoff" ) ) == 0 ) 
	{
		txMessage.commandID = CMD_QLIGHT_TEST;
		txMessage.argument = QLIGHT_BLUE_OFF;
		osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
		sprintf( pcWriteBuffer, "test LE Blue off \r\n");		
	}
	else if ( strncmp( pcParameter[0], "dlevel", strlen( "dlevel" ) ) == 0 ) 
	{
		
		int value;
		int ret = sscanf(pcParameter[0], "dlevel=%d", &value);
		pInfo->setting.debugModeOnOff = value;
		
		if(N_MAX_DEBUG_LEVEL<pInfo->setting.debugModeOnOff) {
			pInfo->setting.debugModeOnOff = 0;
		}
		
		sprintf( pcWriteBuffer, "Debug level %d\r\n", pInfo->setting.debugModeOnOff);
	}
	else if ( strncmp( pcParameter[0], "ver", strlen( "ver" ) ) == 0 ) {
		extern const char flash_compiletime[9];
		extern const char flash_compiledate[12];
		sprintf( pcWriteBuffer, "Ver %d Compiled at %s %s\r\n>",pInfo->setting.fwVer, flash_compiledate, flash_compiletime);
		//sprintf( pcWriteBuffer, "Ver %d Compiled at %s %s\r\n>",pInfo->setting.fwVer, __DATE__, __TIME__);
	}
	else if ( strncmp( pcParameter[0], "dbg", strlen( "dbg" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		//pInfo->setting.debugModeOnOff++; // toggle dbg mode
		if(N_MAX_DEBUG_LEVEL<pInfo->setting.debugModeOnOff) {
			pInfo->setting.debugModeOnOff = 0;
		}
		
		sprintf( pcWriteBuffer, "Debug level %d\r\n", pInfo->setting.debugModeOnOff);
	}
	
	else if ( strncmp( pcParameter[0], "mute", strlen( "mute" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		BUZZER_MUTE ^= 1;
		
		sprintf( pcWriteBuffer, "Mute? %d\r\n", BUZZER_MUTE);
	}
	else if ( strncmp( pcParameter[0], "scpi", strlen( "scpi" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		pInfo->setting.protocol = 0;
		
		sprintf( pcWriteBuffer, "Protocol = %s\r\n", pInfo->setting.protocol==0?"SCPI":"Custom");
	}	
	else if ( strncmp( pcParameter[0], "custom", strlen( "custom" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		pInfo->setting.protocol = 1;
		
		sprintf( pcWriteBuffer, "Protocol = %s\r\n", pInfo->setting.protocol==0?"SCPI":"Custom");
	}	
	else if ( strncmp( pcParameter[0], "mode", strlen( "mode" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		
		sprintf( pcWriteBuffer, "Net mode = %s, Protocol %s\r\n", strModeSel[pInfo->setting.lanLora], pInfo->setting.protocol==0?"SCPI":"Custom");
	}
	else if ( strncmp( pcParameter[0], "lora", strlen( "lora" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		pInfo->setting.lanLora = COMM_MODE_LORA;
		g_ConnectedServer = 0;
		
		sprintf( pcWriteBuffer, "Net = %s\r\n", strModeSel[pInfo->setting.lanLora]);
	}
	else if ( strncmp( pcParameter[0], "lan", strlen( "lan" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		pInfo->setting.lanLora = COMM_MODE_LAN;		
		g_ConnectedServer = 0;
		
		sprintf( pcWriteBuffer, "Net = %s\r\n", strModeSel[pInfo->setting.lanLora]);
	}	
	else if ( strncmp( pcParameter[0], "serial", strlen( "serial" ) ) == 0 ) 
	{
		StructEscInfo *pInfo = GetEscInfo();
		pInfo->setting.lanLora = COMM_MODE_SERIAL;
		g_ConnectedServer = 0;
		
		sprintf( pcWriteBuffer, "Net = %s\r\n", strModeSel[pInfo->setting.lanLora]);
	}
	else if ( strncmp( pcParameter[0], "ack", strlen( "ack" ) ) == 0 ) 
	{
		extern void Ack();
		extern U08 plcFromWhere;
		plcFromWhere=GetEscInfo()->setting.lanLora;
		Ack();
		sprintf( pcWriteBuffer, "Send Ack\r\n");
	}
	else if ( strncmp( pcParameter[0], "stat", strlen( "stat" ) ) == 0 ) 
	{
		extern char* GetSTAT();
		
		sprintf( pcWriteBuffer, "STAT: %s\r\n", GetSTAT());
	}
	else if ( strncmp( pcParameter[0], "minmax", strlen( "minmax" ) ) == 0 ) 
	{
		MinmaxCheck(pcWriteBuffer);
	}	
	else if ( strncmp( pcParameter[0], "adc", strlen( "adc" ) ) == 0 ) 
	{		
		AdcCheck(pcWriteBuffer);
		
	}
	else {
			
		arg = atoi(pcParameter[0]);
		txMessage.argument = arg;
		osMessageQueuePut(inputQueueHandle, &txMessage, NULL, NULL);
		
		if(arg != 0) {
			sprintf( pcWriteBuffer, "Change LED delay... test cmd ID %d arg %d \r\n", txMessage.commandID, txMessage.argument);
		}
		else {
			sprintf( pcWriteBuffer, "ERROR, Not supported command\r\n");
		}
	}

	/* There is no more data to return after this single string, so return pdFALSE. */
	return pdFALSE;
}



static BaseType_t prvGPIOCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
  const char *pcParameter[2];
	BaseType_t lParameterStringLength;
	struct IOMessage txMessage;  
	char port, ret;
	int pin;
	GPIO_TypeDef *pPortBase = GPIOA;

	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Obtain the parameter string. */
	pcParameter[0] = FreeRTOS_CLIGetParameter(pcCommandString, 1, &lParameterStringLength);
	sprintf( pcWriteBuffer, "[1]%s", pcParameter[0] );
	pcParameter[1] = FreeRTOS_CLIGetParameter(pcCommandString, 2, &lParameterStringLength);
	sprintf( pcWriteBuffer, "[2]%s", pcParameter[1] );
	configASSERT( pcParameter );
	
	ret = sscanf(pcParameter[1], "%c.%d", &port, &pin);
	if(ret!=2) {
		sprintf( pcWriteBuffer, "Error parshing GPIO port %d\r\n", ret);
		return pdFALSE;
	}
	switch(port) {
		case 'A':
		case 'a':
			pPortBase = GPIOA;
			break;
		case 'B':
		case 'b':
			pPortBase = GPIOB;
			break;
		case 'C':
		case 'c':
			pPortBase = GPIOC;
			break;
		case 'd':
		case 'D':
			pPortBase = GPIOD;
			break;
		case 'e':
		case 'E':
			pPortBase = GPIOE;
			break;
		case 'f':
		case 'F':
			pPortBase = GPIOF;
			break;
		case 'g':
		case 'G':
			pPortBase = GPIOG;
			break;
	}

	if ( strncmp( pcParameter[0], "get", strlen( "get" ) ) == 0 ) {
		txMessage.commandID = CMD_GET_GPIO;		
		sprintf( pcWriteBuffer, "PORT%c pin %d is %d \r\n", port, pin,  HAL_GPIO_ReadPin(pPortBase, 1<<pin));
	}
	else if ( strncmp( pcParameter[0], "set", strlen( "set" ) ) == 0 ) {
		txMessage.commandID = CMD_SET_GPIO;
		HAL_GPIO_WritePin(pPortBase, 1<<pin, GPIO_PIN_SET);
		sprintf( pcWriteBuffer, "Set PORT%c pin %d \r\n", port, pin);
	}
	else if ( strncmp( pcParameter[0], "clear", strlen( "clear" ) ) == 0 ) {
		txMessage.commandID = CMD_CLEAR_GPIO;
		sprintf( pcWriteBuffer, "Clear PORT%c pin %d \r\n", port, pin);
		HAL_GPIO_WritePin(pPortBase, 1<<pin, GPIO_PIN_RESET);
	}
	else {
		sprintf( pcWriteBuffer, "Invalid parameters.. \r\n" );
		return pdFALSE;
	}
	/*
	if ( strncmp( pcParameter[1], "all", strlen( "all" ) ) == 0 ) {
		txMessage.argument = 0xFF;
	}
	else {
		int channel = atoi(pcParameter[1]);
		if (channel < 0 || channel >= MAX_GPIO_CHANNEL) {
			sprintf( pcWriteBuffer, "Invalid parameters.. \r\n" );
			return pdFALSE;
		}	
		else txMessage.argument = channel;	
	}

	osMessageQueuePut(lanTxQueueHandle, &txMessage, NULL, NULL);
	*/
	return pdFALSE;
}



static BaseType_t prvTaskStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
{
const char *const pcHeader = "State   Priority  Stack    #\r\n************************************************\r\n";
BaseType_t xSpacePadding;

	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) pcCommandString;
	( void ) xWriteBufferLen;
	configASSERT( pcWriteBuffer );

	/* Generate a table of task stats. */
	strcpy( pcWriteBuffer, "Task" );
	pcWriteBuffer += strlen( pcWriteBuffer );

	/* Minus three for the null terminator and half the number of characters in
	"Task" so the column lines up with the centre of the heading. */
	configASSERT( configMAX_TASK_NAME_LEN > 3 );
	for( xSpacePadding = strlen( "Task" ); xSpacePadding < ( configMAX_TASK_NAME_LEN - 3 ); xSpacePadding++ )
	{
		/* Add a space to align columns after the task's name. */
		*pcWriteBuffer = ' ';
		pcWriteBuffer++;

		/* Ensure always terminated. */
		*pcWriteBuffer = 0x00;
	}
	strcpy( pcWriteBuffer, pcHeader );
	vTaskList( pcWriteBuffer + strlen( pcHeader ) );

	/* There is no more data to return after this single string, so return
	pdFALSE. */
	return pdFALSE;
}
/*-----------------------------------------------------------*/

#if( configINCLUDE_QUERY_HEAP_COMMAND == 1 )

	static BaseType_t prvQueryHeapCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
	{
		/* Remove compile time warnings about unused parameters, and check the
		write buffer is not NULL.  NOTE - for simplicity, this example assumes the
		write buffer length is adequate, so does not check for buffer overflows. */
		( void ) pcCommandString;
		( void ) xWriteBufferLen;
		configASSERT( pcWriteBuffer );

		sprintf( pcWriteBuffer, "Current free heap %d bytes, minimum ever free heap %d bytes\r\n", ( int ) xPortGetFreeHeapSize(), ( int ) xPortGetMinimumEverFreeHeapSize() );

		/* There is no more data to return after this single string, so return
		pdFALSE. */
		return pdFALSE;
	}

#endif /* configINCLUDE_QUERY_HEAP */
/*-----------------------------------------------------------*/

#if( configGENERATE_RUN_TIME_STATS == 1 )
	
	static BaseType_t prvRunTimeStatsCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
	{
	const char * const pcHeader = "  Abs Time      % Time\r\n****************************************\r\n";
	BaseType_t xSpacePadding;

		/* Remove compile time warnings about unused parameters, and check the
		write buffer is not NULL.  NOTE - for simplicity, this example assumes the
		write buffer length is adequate, so does not check for buffer overflows. */
		( void ) pcCommandString;
		( void ) xWriteBufferLen;
		configASSERT( pcWriteBuffer );

		/* Generate a table of task stats. */
		strcpy( pcWriteBuffer, "Task" );
		pcWriteBuffer += strlen( pcWriteBuffer );

		/* Pad the string "task" with however many bytes necessary to make it the
		length of a task name.  Minus three for the null terminator and half the
		number of characters in	"Task" so the column lines up with the centre of
		the heading. */
		for( xSpacePadding = strlen( "Task" ); xSpacePadding < ( configMAX_TASK_NAME_LEN - 3 ); xSpacePadding++ )
		{
			/* Add a space to align columns after the task's name. */
			*pcWriteBuffer = ' ';
			pcWriteBuffer++;

			/* Ensure always terminated. */
			*pcWriteBuffer = 0x00;
		}

		strcpy( pcWriteBuffer, pcHeader );
		vTaskGetRunTimeStats( pcWriteBuffer + strlen( pcHeader ) );

		/* There is no more data to return after this single string, so return
		pdFALSE. */
		return pdFALSE;
	}
	
#endif /* configGENERATE_RUN_TIME_STATS */
/*-----------------------------------------------------------*/

#if configINCLUDE_TRACE_RELATED_CLI_COMMANDS == 1

	static BaseType_t prvStartStopTraceCommand( char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString )
	{
	const char *pcParameter;
	BaseType_t lParameterStringLength;

		/* Remove compile time warnings about unused parameters, and check the
		write buffer is not NULL.  NOTE - for simplicity, this example assumes the
		write buffer length is adequate, so does not check for buffer overflows. */
		( void ) pcCommandString;
		( void ) xWriteBufferLen;
		configASSERT( pcWriteBuffer );

		/* Obtain the parameter string. */
		pcParameter = FreeRTOS_CLIGetParameter
						(
							pcCommandString,		/* The command string itself. */
							1,						/* Return the first parameter. */
							&lParameterStringLength	/* Store the parameter string length. */
						);

		/* Sanity check something was returned. */
		configASSERT( pcParameter );

		/* There are only two valid parameter values. */
		if( strncmp( pcParameter, "start", strlen( "start" ) ) == 0 )
		{
			/* Start or restart the trace. */
			vTraceStop();
			vTraceClear();
			vTraceStart();

			sprintf( pcWriteBuffer, "Trace recording (re)started.\r\n" );
		}
		else if( strncmp( pcParameter, "stop", strlen( "stop" ) ) == 0 )
		{
			/* End the trace, if one is running. */
			vTraceStop();
			sprintf( pcWriteBuffer, "Stopping trace recording.\r\n" );
		}
		else
		{
			sprintf( pcWriteBuffer, "Valid parameters are 'start' and 'stop'.\r\n" );
		}

		/* There is no more data to return after this single string, so return
		pdFALSE. */
		return pdFALSE;
	}

#endif /* configINCLUDE_TRACE_RELATED_CLI_COMMANDS */
