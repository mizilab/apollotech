/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "apollo.h"

#include "dataformat.h"
#include "queue.h"
#include "esc_proc.h"
#include "io_tasks.h"
#include "ad779x.h"
#include "dac7678.h"
#include "stm32f4_at24c16.h"

#include "semphr.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;
I2C_HandleTypeDef hi2c2;

SPI_HandleTypeDef hspi6;

UART_HandleTypeDef huart5;
UART_HandleTypeDef huart8;
UART_HandleTypeDef huart6;

SRAM_HandleTypeDef hsram1;

typedef StaticTask_t osStaticThreadDef_t;
typedef StaticQueue_t osStaticMessageQDef_t;
typedef StaticSemaphore_t osStaticMutexDef_t;
osThreadId_t defaultTaskHandle;
uint32_t defaultTaskBuffer[ 256 ];
osStaticThreadDef_t defaultTaskControlBlock;
osThreadId_t myTaskLanHandle;
uint32_t myTaskLanBuffer[ 1024 ];
osStaticThreadDef_t myTaskLanControlBlock;
osThreadId_t myTaskInputHandle;
uint32_t myTaskInputBuffer[ 1024 ];
osStaticThreadDef_t myTaskInputControlBlock;
osThreadId_t myTaskOutputHandle;
uint32_t myTaskOutputBuffer[ 1500 ];
osStaticThreadDef_t myTaskOutputControlBlock;
osThreadId_t myTaskCommHandle;
uint32_t myTaskCommBuffer[ 1500 ];
osStaticThreadDef_t myTaskCommControlBlock;
osThreadId_t myTaskLoraHandle;
uint32_t myTaskLoraBuffer[ 1500 ];
osStaticThreadDef_t myTaskLoraControlBlock;
osThreadId_t myTaskLanRxHandle;
uint32_t myTaskLanRxBuffer[ 1024 ];
osStaticThreadDef_t myTaskLanRxControlBlock;
osMessageQueueId_t queueMonitorTxHandle;
osMessageQueueId_t queueRxCommHandle;
uint8_t queueRxCommBuffer[ 256 * sizeof( uint8_t ) ];
osStaticMessageQDef_t queueRxCommControlBlock;
osMutexId_t g_MutexUplinkHandle;
osStaticMutexDef_t g_MutexUplinkControlBlock;
osMutexId_t g_MutexFSMCHandle;
osStaticMutexDef_t g_MutexFSMCControlBlock;
osMutexId_t g_MutexLanHandle;
osStaticMutexDef_t g_MutexLanControlBlock;
/* USER CODE BEGIN PV */






osMessageQueueId_t lanTxQueueHandle;



osMessageQueueId_t outputQueueHandle;
osMessageQueueId_t inputQueueHandle;

void TakeUplink()
{
	xSemaphoreTake(g_MutexUplinkHandle, portMAX_DELAY);
}
void GiveUplink()
{
	xSemaphoreGive(g_MutexUplinkHandle);    
}
void TakeFSMC()
{
//	xSemaphoreTake(g_MutexFSMCHandle, portMAX_DELAY);
}
void GiveFSMC()
{
//	xSemaphoreGive(g_MutexFSMCHandle);    
}

void TakeLan()
{
	xSemaphoreTake(g_MutexLanHandle, portMAX_DELAY);
}
void GiveLan()
{
	xSemaphoreGive(g_MutexLanHandle);    
}


extern void TaskLora(void *argument);
extern void TaskLan(void *argument);
extern void TaskLanRx(void *argument);


extern void vRegisterCLICommands( void );
extern void vUARTCommandConsoleStart( uint16_t usStackSize, UBaseType_t uxPriority );
extern void vOutputString( const char * const pcMessage );
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_FMC_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2C2_Init(void);
static void MX_UART5_Init(void);
static void MX_UART8_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART6_UART_Init(void);
static void MX_ADC1_Init(void);
static void MX_SPI6_Init(void);
void StartDefaultTask(void *argument);
void TaskLan(void *argument);
void TaskInput(void *argument);
void TaskOutput(void *argument);
void TaskComm(void *argument);
void TaskLora(void *argument);
void TaskLanRx(void *argument);

/* USER CODE BEGIN PFP */
void StartIOManagerTask(void *argument);

/**
	redefine putchar to use printf as uart1
*/
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)  //for printf
signed portBASE_TYPE xSerialPutChar( void* pxPort, signed char cOutChar, TickType_t xBlockTime );

PUTCHAR_PROTOTYPE 
{
	xSerialPutChar( USART1, ch, 0);
  //HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
  return ch;
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(GPIO_Pin);
  /* NOTE: This function Should not be modified, when the callback is needed,
           the HAL_GPIO_EXTI_Callback could be implemented in the user file
   */
	extern uint8_t g_apHvOut;
	//extern uint8_t g_apBoardDet;
	extern uint8_t g_apAlarm;
	extern uint8_t g_apAlarmReset;
	
	__HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
	if(GPIO_Pin==GPIO_PIN_3) {		
		if ((GPIO_PIN_RESET == HAL_GPIO_ReadPin (GPIOE, GPIO_PIN_3)))
        {
			g_apHvOut = 1;
		}
		else {
			g_apHvOut = 0;
		}		
	}
	if(GPIO_Pin==GPIO_PIN_4) {
		if ((GPIO_PIN_RESET == HAL_GPIO_ReadPin (GPIOE, GPIO_PIN_4)))
		{
			g_apAlarmReset = 1;
		}
		else {
			g_apAlarmReset = 0;
		}		
	}
	/*
	if(GPIO_Pin==GPIO_PIN_6) {
		g_apBoardDet = HAL_GPIO_ReadPin(GPIOE, GPIO_Pin);
	}*/
	
#if	KEY_EXTI	
#define nSW_UP_Pin GPIO_PIN_6
#define nSW_UP_GPIO_Port GPIOF
#define nSW_DOWN_Pin GPIO_PIN_7
#define nSW_DOWN_GPIO_Port GPIOF
#define nSW_LEFT_Pin GPIO_PIN_8
#define nSW_LEFT_GPIO_Port GPIOF
#define nSW_RIGHT_Pin GPIO_PIN_9
#define nSW_RIGHT_GPIO_Port GPIOF
#define nSW_HV_PWR_Pin GPIO_PIN_10
#define nSW_HV_PWR_GPIO_Port GPIOF
#define nSW_TOGGLE_Pin GPIO_PIN_11
#define nSW_TOGGLE_GPIO_Port GPIOF
#define nSW_SELECT_Pin GPIO_PIN_15
#define nSW_SELECT_GPIO_Port GPIOF
#define nSW_CANCEL_GPIO_Port GPIOC
#define nSW_CANCEL_Pin GPIO_PIN_5
	uint8_t key;
	static struct OutputMessage outputMsg;
	static uint8_t keyPrv  = 0xf0;
//	if(GPIO_Pin==GPIO_PIN_6 || GPIO_Pin==GPIO_PIN_7 || GPIO_Pin==GPIO_PIN_8 || GPIO_Pin==GPIO_PIN_9 || GPIO_Pin==GPIO_PIN_10
//		|| GPIO_Pin==GPIO_PIN_11 ||GPIO_Pin==GPIO_PIN_15) {
	
	key = 0; // clear var		
	key |= HAL_GPIO_ReadPin(nSW_UP_GPIO_Port, nSW_UP_Pin)<<KEY_SW_UP_BIT;
	key |= HAL_GPIO_ReadPin(nSW_DOWN_GPIO_Port, nSW_DOWN_Pin)<<KEY_SW_DOWN_BIT;
	key |= HAL_GPIO_ReadPin(nSW_LEFT_GPIO_Port, nSW_LEFT_Pin)<<KEY_SW_LEFT_BIT;
	key |= HAL_GPIO_ReadPin(nSW_RIGHT_GPIO_Port, nSW_RIGHT_Pin)<<KEY_SW_RIGHT_BIT;
	
	key |= HAL_GPIO_ReadPin(nSW_HV_PWR_GPIO_Port, nSW_HV_PWR_Pin)<<KEY_SW_HV_PWR_BIT;
	key |= HAL_GPIO_ReadPin(nSW_TOGGLE_GPIO_Port, nSW_TOGGLE_Pin)<<KEY_SW_TOGGLE_BIT;
	
	key |= HAL_GPIO_ReadPin(nSW_SELECT_GPIO_Port, nSW_SELECT_Pin)<<KEY_SW_SELECT_BIT;
	key |= HAL_GPIO_ReadPin(nSW_CANCEL_GPIO_Port, nSW_CANCEL_Pin)<<KEY_SW_ESC_CANCEL_BIT;
	
	key = key ^ 0xff; // revert key bits	
	
//	if(key != keyPrv) {
		keyPrv  = key;
		outputMsg.commandID = OUTPUT_KEY;
		outputMsg.value = key;
		osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);		
	//}
#endif	
	//printf("exti %x", GPIO_Pin);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_FMC_Init();
  MX_I2C1_Init();
  MX_I2C2_Init();
  MX_UART5_Init();
  MX_UART8_Init();
  MX_USART1_UART_Init();
  MX_USART6_UART_Init();
  MX_ADC1_Init();
  MX_SPI6_Init();
  /* USER CODE BEGIN 2 */
	vUARTCommandConsoleStart( 1000, osPriorityHigh );
	vRegisterCLICommands();	
	

	
	#ifdef USE_DBOS // Edit by jwaani: enable interrupt, disabled on db-os boot loader
	/** README: If USE_DBOS is defined, IROM1 start address should be 0x8020000, size=0x1E0000 on Option-Target
	 */
	__ASM volatile ("cpsie   i         \n\t"); 
	#endif
  /* USER CODE END 2 */

  osKernelInitialize();

  /* Create the mutex(es) */
  /* definition and creation of g_MutexUplink */
  const osMutexAttr_t g_MutexUplink_attributes = {
    .name = "g_MutexUplink",
    .cb_mem = &g_MutexUplinkControlBlock,
    .cb_size = sizeof(g_MutexUplinkControlBlock),
  };
  g_MutexUplinkHandle = osMutexNew(&g_MutexUplink_attributes);

  /* definition and creation of g_MutexFSMC */
  const osMutexAttr_t g_MutexFSMC_attributes = {
    .name = "g_MutexFSMC",
    .cb_mem = &g_MutexFSMCControlBlock,
    .cb_size = sizeof(g_MutexFSMCControlBlock),
  };
  g_MutexFSMCHandle = osMutexNew(&g_MutexFSMC_attributes);

  /* definition and creation of g_MutexLan */
  const osMutexAttr_t g_MutexLan_attributes = {
    .name = "g_MutexLan",
    .cb_mem = &g_MutexLanControlBlock,
    .cb_size = sizeof(g_MutexLanControlBlock),
  };
  g_MutexLanHandle = osMutexNew(&g_MutexLan_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */

  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of queueMonitorTx */
  const osMessageQueueAttr_t queueMonitorTx_attributes = {
    .name = "queueMonitorTx"
  };
  queueMonitorTxHandle = osMessageQueueNew (512, sizeof(uint16_t), &queueMonitorTx_attributes);

  /* definition and creation of queueRxComm */
  const osMessageQueueAttr_t queueRxComm_attributes = {
    .name = "queueRxComm",
    .cb_mem = &queueRxCommControlBlock,
    .cb_size = sizeof(queueRxCommControlBlock),
    .mq_mem = &queueRxCommBuffer,
    .mq_size = sizeof(queueRxCommBuffer)
  };
  queueRxCommHandle = osMessageQueueNew (256, sizeof(uint8_t), &queueRxComm_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  const osMessageQueueAttr_t lanTxQueue_attributes = {
    .name = "lanTxQueue"
  };
  lanTxQueueHandle = osMessageQueueNew (16, sizeof(struct IOMessage), &lanTxQueue_attributes);







  
  /* definition and creation of cmdRespQueue */
  const osMessageQueueAttr_t inputQueue_attributes = {
    .name = "inputQueue"
  }; //32
  inputQueueHandle = osMessageQueueNew (64, sizeof(struct IOMessage), &inputQueue_attributes);   
  

  
  /* definition and creation of output Queue */
  const osMessageQueueAttr_t outputQueue_attributes = {
    .name = "outputQueue"
  };
  outputQueueHandle = osMessageQueueNew (64, sizeof(struct OutputMessage), &outputQueue_attributes);     //48
  
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  const osThreadAttr_t defaultTask_attributes = {
    .name = "defaultTask",
    .stack_mem = &defaultTaskBuffer[0],
    .stack_size = sizeof(defaultTaskBuffer),
    .cb_mem = &defaultTaskControlBlock,
    .cb_size = sizeof(defaultTaskControlBlock),
    .priority = (osPriority_t) osPriorityLow2,
  };
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* definition and creation of myTaskLan */
  const osThreadAttr_t myTaskLan_attributes = {
    .name = "myTaskLan",
    .stack_mem = &myTaskLanBuffer[0],
    .stack_size = sizeof(myTaskLanBuffer),
    .cb_mem = &myTaskLanControlBlock,
    .cb_size = sizeof(myTaskLanControlBlock),
    .priority = (osPriority_t) osPriorityLow,
  };
  myTaskLanHandle = osThreadNew(TaskLan, NULL, &myTaskLan_attributes);

  /* definition and creation of myTaskInput */
  const osThreadAttr_t myTaskInput_attributes = {
    .name = "myTaskInput",
    .stack_mem = &myTaskInputBuffer[0],
    .stack_size = sizeof(myTaskInputBuffer),
    .cb_mem = &myTaskInputControlBlock,
    .cb_size = sizeof(myTaskInputControlBlock),
    .priority = (osPriority_t) osPriorityLow1,
  };
  myTaskInputHandle = osThreadNew(TaskInput, NULL, &myTaskInput_attributes);

  /* definition and creation of myTaskOutput */
  const osThreadAttr_t myTaskOutput_attributes = {
    .name = "myTaskOutput",
    .stack_mem = &myTaskOutputBuffer[0],
    .stack_size = sizeof(myTaskOutputBuffer),
    .cb_mem = &myTaskOutputControlBlock,
    .cb_size = sizeof(myTaskOutputControlBlock),
    .priority = (osPriority_t) osPriorityLow6,
  };
  myTaskOutputHandle = osThreadNew(TaskOutput, NULL, &myTaskOutput_attributes);

  /* definition and creation of myTaskComm */
  const osThreadAttr_t myTaskComm_attributes = {
    .name = "myTaskComm",
    .stack_mem = &myTaskCommBuffer[0],
    .stack_size = sizeof(myTaskCommBuffer),
    .cb_mem = &myTaskCommControlBlock,
    .cb_size = sizeof(myTaskCommControlBlock),
    .priority = (osPriority_t) osPriorityLow5,
  };
  myTaskCommHandle = osThreadNew(TaskComm, NULL, &myTaskComm_attributes);

  /* definition and creation of myTaskLora */
  const osThreadAttr_t myTaskLora_attributes = {
    .name = "myTaskLora",
    .stack_mem = &myTaskLoraBuffer[0],
    .stack_size = sizeof(myTaskLoraBuffer),
    .cb_mem = &myTaskLoraControlBlock,
    .cb_size = sizeof(myTaskLoraControlBlock),
    .priority = (osPriority_t) osPriorityLow4,
  };
  myTaskLoraHandle = osThreadNew(TaskLora, NULL, &myTaskLora_attributes);

  /* definition and creation of myTaskLanRx */
  const osThreadAttr_t myTaskLanRx_attributes = {
    .name = "myTaskLanRx",
    .stack_mem = &myTaskLanRxBuffer[0],
    .stack_size = sizeof(myTaskLanRxBuffer),
    .cb_mem = &myTaskLanRxControlBlock,
    .cb_size = sizeof(myTaskLanRxControlBlock),
    .priority = (osPriority_t) osPriorityLow3,
  };
  myTaskLanRxHandle = osThreadNew(TaskLanRx, NULL, &myTaskLanRx_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  
//	printf("Check handle %x %x %x %x %x %x %x \r\n"		,(int)defaultTaskHandle,(int)myTaskLanHandle, (int)myTaskInputHandle, (int)myTaskOutputHandle		,(int)myTaskCommHandle , (int)myTaskLoraHandle, (int)myTaskLanRxHandle );


  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 180;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV8;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 8;
  hadc1.Init.DMAContinuousRequests = ENABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = 2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = 3;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_3;
  sConfig.Rank = 4;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_4;
  sConfig.Rank = 5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_5;
  sConfig.Rank = 6;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_6;
  sConfig.Rank = 7;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
  sConfig.Rank = 8;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 100000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief SPI6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI6_Init(void)
{

  /* USER CODE BEGIN SPI6_Init 0 */

  /* USER CODE END SPI6_Init 0 */

  /* USER CODE BEGIN SPI6_Init 1 */

  /* USER CODE END SPI6_Init 1 */
  /* SPI6 parameter configuration*/
  hspi6.Instance = SPI6;
  hspi6.Init.Mode = SPI_MODE_MASTER;
  hspi6.Init.Direction = SPI_DIRECTION_2LINES;
  hspi6.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi6.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi6.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi6.Init.NSS = SPI_NSS_SOFT;
  hspi6.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi6.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi6.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi6.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi6.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI6_Init 2 */

  /* USER CODE END SPI6_Init 2 */

}

/**
  * @brief UART5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART5_Init(void)
{

  /* USER CODE BEGIN UART5_Init 0 */

  /* USER CODE END UART5_Init 0 */

  /* USER CODE BEGIN UART5_Init 1 */

  /* USER CODE END UART5_Init 1 */
  huart5.Instance = UART5;
  huart5.Init.BaudRate = 1200;
  huart5.Init.WordLength = UART_WORDLENGTH_8B;
  huart5.Init.StopBits = UART_STOPBITS_1;
  huart5.Init.Parity = UART_PARITY_NONE;
  huart5.Init.Mode = UART_MODE_TX_RX;
  huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart5.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart5) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART5_Init 2 */

  /* USER CODE END UART5_Init 2 */

}

/**
  * @brief UART8 Initialization Function
  * @param None
  * @retval None
  */
static void MX_UART8_Init(void)
{

  /* USER CODE BEGIN UART8_Init 0 */

  /* USER CODE END UART8_Init 0 */

  /* USER CODE BEGIN UART8_Init 1 */

  /* USER CODE END UART8_Init 1 */
  huart8.Instance = UART8;
  huart8.Init.BaudRate = 115200;
  huart8.Init.WordLength = UART_WORDLENGTH_8B;
  huart8.Init.StopBits = UART_STOPBITS_1;
  huart8.Init.Parity = UART_PARITY_NONE;
  huart8.Init.Mode = UART_MODE_TX_RX;
  huart8.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart8.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart8) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN UART8_Init 2 */

  /* USER CODE END UART8_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  LL_USART_InitTypeDef USART_InitStruct = {0};

  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* Peripheral clock enable */
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
  
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  /**USART1 GPIO Configuration  
  PB6   ------> USART1_TX
  PB7   ------> USART1_RX 
  */
  GPIO_InitStruct.Pin = LL_GPIO_PIN_6|LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
  GPIO_InitStruct.Alternate = LL_GPIO_AF_7;
  LL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* USART1 interrupt Init */
  NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),5, 0));
  NVIC_EnableIRQ(USART1_IRQn);

  /* USER CODE BEGIN USART1_Init 1 */
	#ifdef USE_DBOS // Edit by jwaani: disable DMA, enabled on db-os boot loader.	
	CLEAR_BIT(USART1->CR3, (USART_CR3_SCEN | USART_CR3_IREN | USART_CR3_HDSEL | USART_CR3_DMAR));
	#endif

  /* USER CODE END USART1_Init 1 */
  USART_InitStruct.BaudRate = 115200;
  USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
  USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
  USART_InitStruct.Parity = LL_USART_PARITY_NONE;
  USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
  USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
  USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
  LL_USART_Init(USART1, &USART_InitStruct);
  LL_USART_ConfigAsyncMode(USART1);
  LL_USART_Enable(USART1);
  /* USER CODE BEGIN USART1_Init 2 */
  LL_USART_EnableIT_RXNE(USART1);
  LL_USART_EnableIT_ERROR(USART1);
  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART6_UART_Init(void)
{

  /* USER CODE BEGIN USART6_Init 0 */

  /* USER CODE END USART6_Init 0 */

  /* USER CODE BEGIN USART6_Init 1 */
  
  /* USER CODE END USART6_Init 1 */
  huart6.Instance = USART6;
  huart6.Init.BaudRate = 9600;
  huart6.Init.WordLength = UART_WORDLENGTH_8B;
  huart6.Init.StopBits = UART_STOPBITS_1;
  huart6.Init.Parity = UART_PARITY_NONE;
  huart6.Init.Mode = UART_MODE_TX_RX;
  huart6.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart6.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart6) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART6_Init 2 */

  /* USER CODE END USART6_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);

}

/* FMC initialization function */
static void MX_FMC_Init(void)
{

  /* USER CODE BEGIN FMC_Init 0 */

  /* USER CODE END FMC_Init 0 */

  FMC_NORSRAM_TimingTypeDef Timing = {0};
  FMC_NORSRAM_TimingTypeDef ExtTiming = {0};

  /* USER CODE BEGIN FMC_Init 1 */

  /* USER CODE END FMC_Init 1 */

  /** Perform the SRAM1 memory initialization sequence
  */
  hsram1.Instance = FMC_NORSRAM_DEVICE;
  hsram1.Extended = FMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram1.Init */
  hsram1.Init.NSBank = FMC_NORSRAM_BANK2;
  hsram1.Init.DataAddressMux = FMC_DATA_ADDRESS_MUX_DISABLE;
  hsram1.Init.MemoryType = FMC_MEMORY_TYPE_SRAM;
  hsram1.Init.MemoryDataWidth = FMC_NORSRAM_MEM_BUS_WIDTH_16;
  hsram1.Init.BurstAccessMode = FMC_BURST_ACCESS_MODE_DISABLE;
  hsram1.Init.WaitSignalPolarity = FMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram1.Init.WrapMode = FMC_WRAP_MODE_DISABLE;
  hsram1.Init.WaitSignalActive = FMC_WAIT_TIMING_BEFORE_WS;
  hsram1.Init.WriteOperation = FMC_WRITE_OPERATION_ENABLE;
  hsram1.Init.WaitSignal = FMC_WAIT_SIGNAL_DISABLE;
  hsram1.Init.ExtendedMode = FMC_EXTENDED_MODE_ENABLE;
  hsram1.Init.AsynchronousWait = FMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram1.Init.WriteBurst = FMC_WRITE_BURST_DISABLE;
  hsram1.Init.ContinuousClock = FMC_CONTINUOUS_CLOCK_SYNC_ONLY;
  hsram1.Init.PageSize = FMC_PAGE_SIZE_NONE;
  /* Timing */
  Timing.AddressSetupTime = 15;
  Timing.AddressHoldTime = 15;
  Timing.DataSetupTime = 255;
  Timing.BusTurnAroundDuration = 15;
  Timing.CLKDivision = 16;
  Timing.DataLatency = 17;
  Timing.AccessMode = FMC_ACCESS_MODE_A;
  /* ExtTiming */
  ExtTiming.AddressSetupTime = 15;
  ExtTiming.AddressHoldTime = 15;
  ExtTiming.DataSetupTime = 255;
  ExtTiming.BusTurnAroundDuration = 15;
  ExtTiming.CLKDivision = 16;
  ExtTiming.DataLatency = 17;
  ExtTiming.AccessMode = FMC_ACCESS_MODE_A;

  if (HAL_SRAM_Init(&hsram1, &Timing, &ExtTiming) != HAL_OK)
  {
    Error_Handler( );
  }

  /* USER CODE BEGIN FMC_Init 2 */

  /* USER CODE END FMC_Init 2 */
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(nAP_ALM_STATUS_GPIO_Port, nAP_ALM_STATUS_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, DAC_HV_PWR_CONTROL_Pin|LORA_WAKE_Pin|cLCD_DB3_Pin|cLCD_DB4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, nRELAY_HV_A_POS_Pin|nRELAY_HV_B_POS_Pin|cLCD_OE_Pin|cLCD_DIR_Pin 
                          |nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LAN_RESET_Pin|BUZZER_Pin|nDAC_LDAC_Pin|nDAC_CLR_Pin 
                          |TEST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, nLED_RUN_Pin|nLED_CHARGED_Pin|cLCD_EN_Pin|nLED_ALM_BAT_Pin 
                          |nLED_ALM_OCP_Pin|nLED_ALM_ARC_Pin|nTEMP_CS1_Pin|nTEMP_CS2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, cLCD_RST_Pin|cLCD_RS_Pin|cLCD_RW_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, cLCD_DB0_Pin|nLED_WLINK_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LORA_MODE_Pin|nLORA_RESET_Pin|cLCD_DB1_Pin|cLCD_DB2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, cLCD_DB5_Pin|cLCD_DB6_Pin|cLCD_DB7_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : DET_ENET_ACT_Pin */
  GPIO_InitStruct.Pin = DET_ENET_ACT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DET_ENET_ACT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : EXTI_AP_HV_OUT_Pin EXTI_AP_ALM_RESET_Pin */
  GPIO_InitStruct.Pin = EXTI_AP_HV_OUT_Pin|EXTI_AP_ALM_RESET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : nAP_ALM_STATUS_Pin */
  GPIO_InitStruct.Pin = nAP_ALM_STATUS_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(nAP_ALM_STATUS_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : nAP_BOARD_DET_Pin */
  GPIO_InitStruct.Pin = nAP_BOARD_DET_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(nAP_BOARD_DET_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DAC_HV_PWR_CONTROL_Pin nRELAY_HV_A_POS_Pin nRELAY_HV_B_POS_Pin nDAC_HV_SHUTDOWN_Pin 
                           LORA_WAKE_Pin cLCD_DB3_Pin cLCD_DB4_Pin */
  GPIO_InitStruct.Pin = DAC_HV_PWR_CONTROL_Pin|nRELAY_HV_A_POS_Pin|nRELAY_HV_B_POS_Pin|nDAC_HV_SHUTDOWN_Pin 
                          |LORA_WAKE_Pin|cLCD_DB3_Pin|cLCD_DB4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : nDET_MODULE_CHARGER_Pin nDET_BATTERY_Pin nDET_MODULE_LORA_Pin */
  GPIO_InitStruct.Pin = nDET_MODULE_CHARGER_Pin|nDET_BATTERY_Pin|nDET_MODULE_LORA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : nSW_DOWN_Pin nSW_LEFT_Pin nSW_RIGHT_Pin nSW_HV_PWR_Pin 
                           nSW_TOGGLE_Pin nSW_SELECT_Pin nSW_CANCEL_Pin */
  GPIO_InitStruct.Pin = nSW_DOWN_Pin|nSW_LEFT_Pin|nSW_RIGHT_Pin|nSW_HV_PWR_Pin 
                          |nSW_TOGGLE_Pin|nSW_SELECT_Pin|nSW_CANCEL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : cLCD_OE_Pin cLCD_DIR_Pin */
  GPIO_InitStruct.Pin = cLCD_OE_Pin|cLCD_DIR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : ADC_BAT_V_Pin ADC_BAT_I_Pin ADC_HV_A_V_Pin ADC_HV_B_I_Pin */
  GPIO_InitStruct.Pin = ADC_BAT_V_Pin|ADC_BAT_I_Pin|ADC_HV_A_V_Pin|ADC_HV_B_I_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : nDET_MODULE_ENET_Pin LORA_BUSY_Pin */
  GPIO_InitStruct.Pin = nDET_MODULE_ENET_Pin|LORA_BUSY_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LAN_INT_Pin nSW_UP_Pin */
  GPIO_InitStruct.Pin = LAN_INT_Pin|nSW_UP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : LAN_RESET_Pin BUZZER_Pin nDAC_LDAC_Pin nDAC_CLR_Pin 
                           TEST_Pin */
  GPIO_InitStruct.Pin = LAN_RESET_Pin|BUZZER_Pin|nDAC_LDAC_Pin|nDAC_CLR_Pin 
                          |TEST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : nLED_RUN_Pin nLED_CHARGED_Pin cLCD_EN_Pin nLED_ALM_BAT_Pin 
                           nLED_ALM_OCP_Pin nLED_ALM_ARC_Pin cLCD_DB0_Pin */
  GPIO_InitStruct.Pin = nLED_RUN_Pin|nLED_CHARGED_Pin|cLCD_EN_Pin|nLED_ALM_BAT_Pin 
                          |nLED_ALM_OCP_Pin|nLED_ALM_ARC_Pin|cLCD_DB0_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : cLCD_RST_Pin cLCD_RS_Pin cLCD_RW_Pin cLCD_DB5_Pin 
                           cLCD_DB6_Pin cLCD_DB7_Pin */
  GPIO_InitStruct.Pin = cLCD_RST_Pin|cLCD_RS_Pin|cLCD_RW_Pin|cLCD_DB5_Pin 
                          |cLCD_DB6_Pin|cLCD_DB7_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PRESS_INT1_Pin PRESS_INT2_Pin */
  GPIO_InitStruct.Pin = PRESS_INT1_Pin|PRESS_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : LORA_STAT_Pin */
  GPIO_InitStruct.Pin = LORA_STAT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LORA_STAT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LORA_MODE_Pin nLORA_RESET_Pin cLCD_DB1_Pin cLCD_DB2_Pin */
  GPIO_InitStruct.Pin = LORA_MODE_Pin|nLORA_RESET_Pin|cLCD_DB1_Pin|cLCD_DB2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : nTEMP_CS1_Pin nTEMP_CS2_Pin */
  GPIO_InitStruct.Pin = nTEMP_CS1_Pin|nTEMP_CS2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : nLED_WLINK_Pin */
  GPIO_InitStruct.Pin = nLED_WLINK_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(nLED_WLINK_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI3_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);

  HAL_NVIC_SetPriority(EXTI4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}

/* USER CODE BEGIN 4 */
void ad7793CS1Control(unsigned char State)
{
	if (State == cssEnable) {
		HAL_GPIO_WritePin(nTEMP_CS1_GPIO_Port, nTEMP_CS1_Pin, GPIO_PIN_RESET);
	}
	else {
		HAL_GPIO_WritePin(nTEMP_CS1_GPIO_Port, nTEMP_CS1_Pin, GPIO_PIN_SET);
	}
}

void ad7793CS2Control(unsigned char State)
{
	if (State == cssEnable) {
		HAL_GPIO_WritePin(nTEMP_CS2_GPIO_Port, nTEMP_CS2_Pin, GPIO_PIN_RESET);
	}
	else {
		HAL_GPIO_WritePin(nTEMP_CS2_GPIO_Port, nTEMP_CS2_Pin, GPIO_PIN_SET);
	}
}

unsigned char ad7793RDYState(void)
{
	unsigned char ret=0;
	
	ret = HAL_GPIO_ReadPin(SPI_MISO_GPIO_Port, SPI_MISO_Pin);

	if (ret == 0) return 1;
	else return 0;
}

void ad7793TxByte(unsigned char Data)
{
	HAL_SPI_Transmit(&hspi6, &Data, 1, 0xFFFFFFFF);
}

unsigned char ad7793RxByte(void)
{
	unsigned char pData = 0;

	HAL_SPI_Receive(&hspi6, &pData, 1, 0xFFFFFFFF);

	return pData;
}





/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */
//	uint8_t buffer;
	int count = 0;
//	int value[4];
	uint32_t halTick = HAL_GetTick();
	
	osDelay(100);
	
	HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);	
	
	HAL_GPIO_WritePin(nDAC_CLR_GPIO_Port, nDAC_CLR_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(nDAC_LDAC_GPIO_Port, nDAC_LDAC_Pin, GPIO_PIN_SET);

	dac7678Init(&hi2c1);
	dac7678SetVREF(DAC7678_EXT_REF);
	dac7678ClrMode(DAC7678_CLR_ZERO);
	dac7678LDAC(pdTRUE);
	dac7678Enable();
	dac7678Set(0, 0);	//2.7v
	dac7678Set(1, 0);
	dac7678Set(2, 0);
	dac7678Set(3, 0);
	
	HAL_GPIO_WritePin(nDAC_LDAC_GPIO_Port, nDAC_LDAC_Pin, GPIO_PIN_RESET);
	osDelay(100);
	HAL_GPIO_WritePin(nDAC_LDAC_GPIO_Port, nDAC_LDAC_Pin, GPIO_PIN_SET);
	
	g_checkTaskStart |= CHECK_TASK_DEFAULT;
	
	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(100);
		//printf("Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}
	
	extern uint8_t g_Beep;
	extern uint8_t g_BeepDelay;
	extern uint8_t g_BeepCount;
	

	
	/* Infinite loop */
	for(;;)
	{
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
		osDelay(10);
		count++;

	  
		#if BEEP_IN_IO_TASK==0
		StructEscInfo * pInfo = GetEscInfo();
		if(g_BeepCount)
		{
			static U08 beepPolarity = 0;// = g_BeepCount%2;
			static uint32_t tickBeep = 0;
			if(abs( (int)halTick - (int)tickBeep) > BUZ_DELAY*100) {
				printf("BEEP delay %d\r\n", abs( (int)halTick - (int)tickBeep ) );
				tickBeep = halTick;
				beepPolarity ^= 1;				
			}
			
			if(g_Beep) {
				g_Beep--;
				if(!BUZZER_MUTE) {
					if(beepPolarity) {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);	
					}
				}
			}
			else {
				
				if(!BUZZER_MUTE) {
					if(!beepPolarity) {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);	
					}
				}
				g_BeepCount--;
				if(g_BeepCount!=0) {
					g_Beep = g_BeepDelay*BUZ_DELAY*2;
				}
			}
		}		
		#endif
		
		//if(count%50==0) 
		if( abs( (int)HAL_GetTick()-(int)halTick ) > 500 )
		{
			halTick = HAL_GetTick();
			static uint8_t bRunLed=0;

			//RUN LED 500msec���� ����
			bRunLed^=1;
			HAL_GPIO_WritePin(nLED_RUN_GPIO_Port, nLED_RUN_Pin, bRunLed);
			debug_console(10, "%s: RUN LED %d %d\r\n",__FUNCTION__, bRunLed, HAL_GetTick());
		}

#if 0		
		if(count%200==0) {
			/*
			U08 checkTask;
			checkTask = (uint16_t)(*defaultTaskHandle) & (uint16_t)(*myTaskLanHandle) & (uint16_t)(*myTaskInputHandle) & (uint16_t)(*myTaskOutputHandle)
				& (uint16_t)(*myTaskCommHandle) & (uint16_t)(*myTaskLoraHandle) & (uint16_t)(*myTaskLanRxHandle);
			if(checkTask) 
			*/
	/*
  osThreadInactive        =  0,         ///< Inactive.
  osThreadReady           =  1,         ///< Ready.
  osThreadRunning         =  2,         ///< Running.
  osThreadBlocked         =  3,         ///< Blocked.
  osThreadTerminated      =  4,         ///< Terminated.
  osThreadError           = -1,         ///< Error.
  osThreadReserved        = 0x7FFFFFFF  ///< Prevents enum down-size compiler optimization.
			*/
			printf("Task States default %x Lan %x Input %x Output %x Comm %x Lora %x LanRx %x \r\n"
				,osThreadGetState(defaultTaskHandle) ,osThreadGetState(myTaskLanHandle) ,osThreadGetState(myTaskInputHandle) 
				,osThreadGetState(myTaskOutputHandle) ,osThreadGetState(myTaskCommHandle) ,osThreadGetState(myTaskLoraHandle)
				,osThreadGetState(myTaskLanRxHandle));
		}
#endif
			
	}
  /* USER CODE END 5 */ 
}

/* USER CODE BEGIN Header_TaskLan */
/**
* @brief Function implementing the myTaskLan thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskLan */
__weak void TaskLan(void *argument)
{
  /* USER CODE BEGIN TaskLan */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskLan */
}

/* USER CODE BEGIN Header_TaskInput */
/**
* @brief Function implementing the myTaskInput thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskInput */
__weak void TaskInput(void *argument)
{
  /* USER CODE BEGIN TaskInput */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskInput */
}

/* USER CODE BEGIN Header_TaskOutput */
/**
* @brief Function implementing the myTaskOutput thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskOutput */
__weak void TaskOutput(void *argument)
{
  /* USER CODE BEGIN TaskOutput */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskOutput */
}

/* USER CODE BEGIN Header_TaskComm */
/**
* @brief Function implementing the myTaskComm thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskComm */
__weak void TaskComm(void *argument)
{
  /* USER CODE BEGIN TaskComm */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskComm */
}

/* USER CODE BEGIN Header_TaskLora */
/**
* @brief Function implementing the myTaskLora thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskLora */
__weak void TaskLora(void *argument)
{
  /* USER CODE BEGIN TaskLora */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskLora */
}

/* USER CODE BEGIN Header_TaskLanRx */
/**
* @brief Function implementing the myTaskLanRx thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_TaskLanRx */
__weak void TaskLanRx(void *argument)
{
  /* USER CODE BEGIN TaskLanRx */
  /* Infinite loop */
  for(;;)
  {
    osDelay(1);
  }
  /* USER CODE END TaskLanRx */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM14 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM14) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
