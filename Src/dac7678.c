#include "dac7678.h"

I2C_HandleTypeDef *hi2cDAC;
int dac7678_address = 0x90;
unsigned char off_mode[8];
unsigned char LDAC_reg = 0xFF;

void dac7678Init(I2C_HandleTypeDef *hi2c) {
	hi2cDAC = hi2c;
	
  dac7678Reset(); 		  // Sends Power-On Reset command to DAC
  dac7678Disable();      // Make sure outputs are powered off
}

void dac7678SetVREF(uint8_t _refstate) {
  // Sets reference mode
  // _refstate 0: Internal vref turned off
  // _refstate 1: Internal vref turned on
  unsigned char lsdb;

  if (_refstate) lsdb = 0x10;
  else if (!_refstate) lsdb = 0x00;

  dac7678Transmit(CMD_INTREF_RS, 0x00, lsdb); 
}

void dac7678Reset() {
  // Issues Reset command (Same as Power-On reset)
  dac7678Transmit(CMD_SOFT_RESET, 0x00, 0x00);
}



void dac7678_offModeChannel(unsigned char channel, unsigned char mode) {
  // Configure off mode for specified channel
  off_mode[channel] = mode;
}

void dac7678OffMode(unsigned char mode) {
  // Configure off mode for all channels
  for (unsigned char x = 0; x <= 7; x++){
    off_mode[x] = mode;
  }
}

void dac7678LDAC(uint8_t _state) {
  // Configure LDAC mode for all channels
  if (_state) LDAC_reg = 0x00;
  else LDAC_reg = 0xFF;
	
  dac7678Transmit(CMD_LDAC, LDAC_reg, 0x00);
}

void dac7678LDACChannel(unsigned char _channel, uint8_t _state) {
  // Configure LDAC mode for specified channel
  if (_state) LDAC_reg &= ~(1 << _channel); 
  else LDAC_reg |= (1 << _channel);

  dac7678Transmit(CMD_LDAC, LDAC_reg, 0x00);
}

void dac7678Select(unsigned char _channel) {
  // Select channel for update
  dac7678Transmit(CMD_SELECT + _channel, 0x00, 0x00);
}

void dac7678Enable() {
  // Enables all the channels at once
  dac7678Transmit(CMD_POWER_DOWN, 0x01, 0xE0);
	//only enable A,B,C,D channels
}


void dac7678Disable() {
  // Disables all the channels in one go
  // Doing it one by one is required in case of different off modes per channel
  for (unsigned char x = 0; x <= 7; x++) {
    dac7678DisableChannel(x);
  }
}

void dac7678EnableChannel(unsigned char channel) {
  // Enables the specific DAC output channel
  if (channel < 8) {
    unsigned int x = (unsigned int) (0x20 << channel);  // Channels are offset +5 bits
    unsigned char msdb = (unsigned char)(x >> 8);
    unsigned char lsdb  = x & 0xFF;

    dac7678Transmit(CMD_POWER_DOWN, msdb, lsdb);
  }
}

void dac7678DisableChannel(unsigned char channel) {
  // Disables the specific DAC output channel
  if (channel < 8) {
    unsigned int x = (unsigned int) (0x20 << channel);  // Channels are offset +5 bits
    unsigned char msdb = (unsigned char)(x >> 8);
    msdb |= off_mode[channel];
    unsigned char lsdb  = x & 0xFF;

    dac7678Transmit(CMD_POWER_DOWN, msdb, lsdb);
  }
}

static int nPrevDacValues[8];
void dac7678Set(unsigned char _channel, int _value) {
  // Set specified channel (0-7) to the specified value
  //   Check for out of range values
  if (_value >= 4096 || _value < 0) return;

  if (_channel < 8) { // Check for out of range Channel #
    // Sets the variables
    unsigned char _command = CMD_IS_LDAC_BASE_ADDR + _channel;
	  
	nPrevDacValues[_channel] = _value;


    unsigned int x = (unsigned int) (_value << 4);  // Data is offset +4 bits
    unsigned char msdb = (unsigned char)(x >> 8);
    unsigned char lsdb  = x & 0xFF;

	// Send data to DAC
    dac7678Transmit(_command, msdb, lsdb);
  }
}

void dac7678UpdatePrev(unsigned char _channel) {
  if (_channel < 8) { // Check for out of range Channel #
    // Sets the variables
    unsigned char _command = CMD_WRITE_BASE_ADDR + _channel;

    unsigned int x = (unsigned int) (nPrevDacValues[_channel] << 4);  // Data is offset +4 bits
    unsigned char msdb = (unsigned char)(x >> 8);
    unsigned char lsdb  = x & 0xFF;

	// Send data to DAC
    dac7678Transmit(_command, msdb, lsdb);
  }
}

void dac7678Update(unsigned char _channel, int _value) {
  // Update specified channel (0-7) to the specified value
  //   Check for out of range values
  if (_value >= 4096 || _value < 0) return;

  if (_channel < 8) { // Check for out of range Channel #
    // Sets the variables
    unsigned char _command = CMD_WRITE_BASE_ADDR + _channel;

    unsigned int x = (unsigned int) (_value << 4);  // Data is offset +4 bits
    unsigned char msdb = (unsigned char)(x >> 8);
    unsigned char lsdb  = x & 0xFF;

	// Send data to DAC
    dac7678Transmit(_command, msdb, lsdb);
  }
}

void dac7678ClrMode(int _value) {
// Configures the DAC value to output on all channels when CLR pin is brought low
// Will set the CLR Code register to output either zero, half-range, full range or to ignore the CLR pin
  unsigned char lsdb = _value << 4;

	// Send data to DAC
  dac7678Transmit(CMD_CLEAR_CODE, 0x00, lsdb);
}

unsigned int dac7678ReadChan(unsigned char _command) {
  unsigned int reading = 0;
	uint8_t txData[2];
	uint8_t rxData[2];
	
	txData[0] = _command;
	
	HAL_I2C_Master_Transmit(hi2cDAC, dac7678_address, txData, 1, 1000);
	HAL_I2C_Master_Receive(hi2cDAC, dac7678_address, rxData, 2, 1000);

	reading = rxData[0] << 8;
	reading |= rxData[1];
	reading >>= 4;
/*
  Wire.beginTransmission(dac7678_address); 
  Wire.write(_command);                    // Then send a command byte for the register to be read.
  Wire.endTransmission();
  Wire.requestFrom(dac7678_address, 2);

  if(2 <= Wire.available()) {       // if two bytes were received
    reading = Wire.read();          // receive high byte
    reading = reading << 8;         // shift high byte to be high 8 bits
    reading |= Wire.read();
    reading = reading >> 4;
  }
*/
  return reading;
}

void dac7678Transmit(unsigned char _command, unsigned char _msdb, unsigned char _lsdb) {
  // Transmit the actual command and high and low bytes to the DAC
	
	uint8_t txData[3] = {_command, _msdb, _lsdb};
	
	HAL_I2C_Master_Transmit(hi2cDAC, dac7678_address, txData, 3, 1000);
}
