#ifndef _PROTOCOL_H
#define _PROTOCOL_H

#ifdef __cplusplus

extern "C"
{
#endif
#define LORA_MULTI_SEND	0

#define N_HV_VOLT_OFFSET	31
#define	N_MAX_DEBUG_LEVEL	10
#define N_LOAD_EEPROM_SIZE	(sizeof(StructSetting) + sizeof(StructEscStat) + 2 + N_HV_VOLT_OFFSET + sizeof(StructUerMode)*N_MAX_USER_MODE_SIZE)
	
#define	IS_TOGGLE	IsToggled(GetEscInfo())//(GetEscInfo()->stat.hvSetVolt1 &0x80)?1:0

#define NO_DEVICE_NAME  1

#define N_MAX_CID	4000

#define PACKET_LENGTH_INCLUDING_HEADER	1
#if PACKET_LENGTH_INCLUDING_HEADER
#define N_PACKET_LENGTH_HEADER	6
#else
#define N_PACKET_LENGTH_HEADER	0
#endif

// for CRC table
#define POLYNOMIAL		0x8005
//#define USE_CRC_TABLE
#define USE_XOR_INSTAED_CRC	1

// EEPROM 데이터 체크 용, 데이터 구조가 바뀌면 변경해야 함.
#define EEPROM_CHECKSUM		0x24
#define EEPROM_CHECKSUMB	0x81

// Check temp sensor
#define TEMP_NA	(S16)	0xF5DB	//(-2597)
#define TEMP_MAX	500
#define TEMP_MIN	-258

#define PRESSURE_NA	(S16)	0

/// select use malloc()/free() or Not
#define USE_MALLOC		0

/// Select convert to big endian or Not: Should be CHECK the CPU type!!!
#define TO_BIG_ENDIAN   1

/// data buffer size, In case of using array
#define N_MAX_DATA_BUFFER_SIZE		(512)

/// Packet size defintion: Start code, length, data.. etc
#define N_ST_BYTE_SIZE                (2)
#define N_TARGETID_SIZE				  (2)
#define N_DEVICEID_SIZE				  (1)
#define N_ADDRESS_BYTE_SIZE           (N_TARGETID_SIZE+N_DEVICEID_SIZE)
#define N_CRC_BYTE_SIZE               (2)
#define N_LENGTH_BYTE_SIZE            (1)

#define N_CMD_BYTE_SIZE               (1)
#define N_RESULT_BYTE_SIZE            (1)
#define N_MID_BYTE_SIZE               (1)

#define N_HEADER_SIZE			(N_ADDRESS_BYTE_SIZE + N_CMD_BYTE_SIZE + N_RESULT_BYTE_SIZE + N_MID_BYTE_SIZE)

#define N_PACKET_SIZE_EXCEPT_DATA  (N_ST_BYTE_SIZE + N_LENGTH_BYTE_SIZE + N_HEADER_SIZE + N_CRC_BYTE_SIZE)
#define N_DATA_POSITION            (N_ST_BYTE_SIZE + N_LENGTH_BYTE_SIZE + N_HEADER_SIZE)
//	End data -> 0x0D, 0x0A
#define SCPI_END_LF		'\n'
#define SCPI_END_FF		0x0C
#define SCPI_END_CR		'\r'
#define SCPI_END_SEMI_COLON		';'
#define SCPI_END_DATA	SCPI_END_LF

#define	N_MAX_SCPI_CMD_COUNT	(42)
#define	N_MAX_SCPI_CMD_LENGTH	(22)	// 최대 20 글자
#define	N_MAX_SCPI_PARAM_LENGTH	(16)	// 최대 16 글자? ->  확인 필요

#define	N_MAX_SCPI_SIMPLE_CMDS	5
#define	N_MAX_SCPI_COMPLEX_CMDS	6

#define MAX_STR_LEN 64

#define N_VALID_INFO_SIZE	(sizeof(StructSetting) + 2 + sizeof(StructEscStat))
#define N_SETTING_INFO_SIZE	(sizeof(StructSetting))	

#define N_MAX_USER_MODE_SIZE	3

/// Type definition: Should be CHECK cpu type!
typedef unsigned char U08;   // 8bit
typedef unsigned short U16;  // 16 bit
typedef unsigned int U32;  // 32 bit
typedef unsigned long long U64;  // 64 bit

typedef signed char S08;   // 8bit
typedef signed short S16;  // 16 bit

/*	Macro
*/
#define	Strlen(x)			strlen((char*)(x))
#define Strcpy(dst,src)		strcpy((char*)(dst), (char*)(src))
#define Strtok(dst,src)		strtok((char*)(dst), (char*)(src))
#define Strncmp(dst,src,n)	strncmp((char*)(dst), (char*)(src),n)


// ScpiCmdNumber
enum {
    SCPI_CMD_CLS = 0,	 // clear error queue					 //0
    SCPI_CMD_IDN,	// ESC Controller 식별 코드를 반환, Get ONLY //1
    SCPI_CMD_RST, // reset ESC Controller //2
    SCPI_CMD_SYST_ERR,	// SYSTem command //3

    SCPI_CMD_SYST_VERS, //4

    SCPI_CMD_SYST_BEEP,

    SCPI_CMD_SYST_LOC,

    SCPI_CMD_SYST_REM,

    SCPI_CMD_OUTP_Q,

    SCPI_CMD_OUTP_STAT,

    SCPI_CMD_TOGG_Q,//10

    SCPI_CMD_TOGG,//11

    SCPI_CMD_SOUR_VOLT_PROT_TRIP_Q ,//12 -> 긴 명령이 먼저!
    SCPI_CMD_SOUR_VOLT_Q,
    SCPI_CMD_SOUR_VOLT,


    SCPI_CMD_SOUR_CURR_PROT_TRIP_Q,
	SCPI_CMD_SOUR_CURR_PROT_TRIP,


    SCPI_CMD_MEAS_VOLT_Q,
    SCPI_CMD_MEAS_CURR_Q,


    SCPI_CMD_MEAS_ARC_Q,

    SCPI_CMD_ALM_ARC,


    SCPI_CMD_MEAS_ARC_TRIP_Q,


    SCPI_CMD_CONF_ARC_LEV_Q,//21 -> 순서 주의!
    SCPI_CMD_CONF_ARC_LEV,//22

    SCPI_CMD_CONF_ARC_STAT_Q,
    SCPI_CMD_CONF_ARC_STAT,

    SCPI_CMD_CONF_VOLT_Q,//25
    SCPI_CMD_CONF_VOLT,//26

    SCPI_CMD_CONF_CURR_Q,//27
    SCPI_CMD_CONF_CURR,//28

    SCPI_CMD_CONF_RAMPDOWN_Q,//29
    SCPI_CMD_CONF_RAMPDOWN,//30

    SCPI_CMD_CONF_RAMPUP_Q,//31
    SCPI_CMD_CONF_RAMPUP,//32

	SCPI_CMD_CONF_RAMP_Q,//33
    SCPI_CMD_CONF_RAMP,//34

    // ex> V+2500;V-2500;A+1000;1;0;0 -> chA전압;ChB전압;ChA전류;ChB전류; 출력상태;토글상태;모드상태(Remote/local)
    SCPI_CMD_STAT,//35

    SCPI_CMD_AT_COUN_Q,//36
    SCPI_CMD_AT_COUN,//37

    SCPI_CMD_AT_VOLT_Q, 
    SCPI_CMD_AT_VOLT, //AT_VOLT_LEV,//39

    SCPI_CMD_AT_Q,  //AT_STAT?,//40
    SCPI_CMD_AT,  //AT_STAT,//41	
}	 ;


// Destination type
enum {
    TYPE_ESC = 0,
    TYPE_SERVER,
    TYPE_PLC,
    TYPE_LE
};

// Direction, tx/rx
enum {
    DIR_TX = 0,
    DIR_RX = 1
};

// comm type
enum {
    COMM_MODE_SERIAL=0,
    COMM_MODE_LORA=1,
    COMM_MODE_LAN=2,
	COMM_MODE_AP=3,
	COMM_MODE_REMOCON=4,
};


// toggle type
enum {
	TOGGLE_OFF = 0,
	TOGGLE_ON = 1,

	AUTO_TOGGLE1 = 0x80,
	AUTO_TOGGLE2 = 0x40
};
// Alarm type
enum {
	ALM_NONE = 0,
	ALM_BATT = 1,
	ALM_ARC = 2,
	ALM_OCP = 4,
};

/// Start code
enum CODE_BYTE  {
    CODE_ST = 0xC5,
    CODE_ST2 = 0x7A
};

/// State Machine code definition
enum STATES_OF_COMM  {
    STATE_READY = 0,
    STATE_ST,       //1

        STATE_LENGTH,   //2 : length of data, header+data [targetid, deviceid, cmd,result,mid,data]

    STATE_ADDRESS1_H,   //3
    STATE_ADDRESS1_L,   //4
    STATE_ADDRESS2,     //5

    STATE_CMD,      // 6
    STATE_RESULT,   // 7
    STATE_MID,      // 8

    STATE_DATA,     // 9

    STATE_CRCH,     //10
    STATE_CRCL,     //11

    STATE_PARSE_DONE,
    STATE_ERR_CRC
};


/**
* 2020 04/21 : 삭제 요청 0x2A, 0x4A
**/
/// Custom Cmds
enum CUSTOM_CMDS {
        CCMD_ACK = 1,
        CCMD_NACK = 0x02,
        CCMD_EVENT_REPORT_ALARM = 0x03,
        CCMD_EVENT_REPORT_AUTO_QUERY = 0x04,
        CCMD_REQUEST_ALIVE = 0x05,

        CCMD_GET_MID = 0x21,
        CCMD_GET_HV_OUT_STATUS = 0x22,
        CCMD_GET_RAMP_UPDOWN_TIME = 0x23,
        CCMD_GET_HV_SET_VOLT = 0x24,
        CCMD_GET_AUTO_DISCHARGE = 0x25,
        CCMD_GET_OCP = 0x26,
        CCMD_GET_ARC = 0x27,
        CCMD_GET_BATTERY = 0x28,
        CCMD_GET_TEMP = 0x29,
        CCMD_GET_AUTO_QUERY = 0x2A,
        CCMD_GET_ALL = 0x2B,
        CCMD_GET_REALTIME_GROUP1 = 0x2C,
        CCMD_GET_REALTIME_GROUP2 = 0x2D,

        CCMD_SET_MID = 0x41,
        CCMD_SET_RAMP_UPDOWN_TIME = 0x43,
        CCMD_SET_HV_SET_VOLT = 0x44,
        CCMD_SET_AUTO_DISCHARGE = 0x45,
        CCMD_SET_OCP = 0x46,
        CCMD_SET_ARC = 0x47,
        CCMD_SET_BATTERY = 0x48,

        CCMD_SET_AUTO_QUERY = 0x4A,
        CCMD_SET_ALL = 0x4B,
        CCMD_SET_OCP_ALARM_COUNT_CLEAR= 0x4C,
        CCMD_SET_ARC_ALARM_COUNT_CLEAR = 0x4D,
        CCMD_SET_ALL_ALARM_COUNT_CLEAR = 0x4E,

		CCMD_RESPONSE_ESC = 0x50, // response when hv is On, send least status informations...
		CCMD_READ_INFO_ALL = 0x51,
		CCMD_WRITE_INFO_ALL = 0x52,
		//CCMD_RESET_INFO_ALL = 0x53,
};

/**
* 2020 04/21 : 삭제 요청 4, 5, 7, 9, A
**/
/// Custom cmd results
enum CUSTOM_RESULTS {
        CRESULT_OK = 0x00,
        CRESULT_CMD_NOT_ACCEPTED = 0x01,
        CRESULT_OPERATION_CMD_ERR = 0x2,
        CRESULT_RANGE_ERR = 0x03,
        //CRESULT_NOT_CONNECTED = 0x04,
        //CRESULT_NOT_USE = 0x05,
        CRESULT_NO_RESPONE = 0x06,
        //CRESULT_EMPTY_DATA = 0x07,
        CRESULT_ABNORMAL = 0x08,
        //CRESULT_ALREADY_ENABLE = 0x09,
        //CRESULT_ALREADY_DISABLE = 0x0A,
};


/// custom mid
enum CUSTOM_MIDS {
        CMID_EUI = 0x00,
        CMID_CID = 0x01,
        CMID_SID = 0x02,
        CMID_PID = 0x03,
        CMID_AID = 0x04,
        CMID_HW_VER = 0x05,
        CMID_SERIAL_NUMBER = 0x06,
        CMID_FW_VER = 0x07,

        CMID_HV_OUT_STATUS = 0x08,
        CMID_HV_OUT_STATUS_CH1_VOLT = 0x0B,
        CMID_HV_OUT_STATUS_CH1_CURR = 0x0C,
        CMID_HV_OUT_STATUS_CH2_VOLT = 0x0D,
        CMID_HV_OUT_STATUS_CH2_CURR = 0x0E,

        CMID_RAMP_UP_TIME = 0x0F,

        CMID_HV_SET_VOLT_CH1 = 0x10,
        CMID_HV_SET_VOLT_CH2 = 0x11,

        CMID_AUTO_DISCHARGE_STEP_COUNT = 0x12,
        CMID_AUTO_DISCHARGE_VOLT_STEP1 = 0x13,
        CMID_AUTO_DISCHARGE_VOLT_STEP2 = 0x14,
        CMID_AUTO_DISCHARGE_VOLT_STEP3 = 0x15,
        CMID_AUTO_DISCHARGE_KEEPTIME_STEP1 = 0x16,
        CMID_AUTO_DISCHARGE_KEEPTIME_STEP2 = 0x17,
        CMID_AUTO_DISCHARGE_KEEPTIME_STEP3 = 0x18,

        CMID_RAMP_DOWN_TIME = 0x19,

        CMID_OCP_ALARM_STATUS = 0x2A,
        CMID_OCP_ALARM_CONTROL = 0x2B,
        CMID_OCP_ALARM_EVENT = 0x2C,
        CMID_OCP_ALARM_LIMIT = 0x2E,
        CMID_OCP_ALARM_COUNT = 0x2F,

        CMID_ARC_ALARM_STATUS = 0x30,
        CMID_ARC_ALARM_CONTROL = 0x31,
        CMID_ARC_ALARM_EVENT = 0x32,
        CMID_ARC_ALARM_LIMIT = 0x34,
        CMID_ARC_ALARM_COUNT = 0x35,

        CMID_BATT_STATUS_VOLT = 0x37,
        CMID_BATT_PERCENT = 0x38,
        CMID_BATT_STATUS_CURR = 0x39,
        CMID_BATT_LOW_ALARM_STATUS = 0x3A,
        CMID_BATT_ALARM_CONTROL = 0x3B,
        CMID_BATT_ALARM_LIMIT = 0x3C,
        CMID_BATT_EVENT_CONTROL = 0x3D,

        CMID_CHARGE_CONNECT_STATUS = 0x3E,

        CMID_USER_MODE_SAVE_LOAD = 0x40,
        CMID_WAVE_MODE = 0x41,

        CMID_TEMP1_STATUS = 0x42,
        CMID_TEMP2_STATUS = 0x43,
		CMID_PRESS_STATUS = 0x44,

        CMID_AUTO_QUERY_CONTROL = 0x48,
        CMID_AUTO_QUERY_INVERVAL_TIME = 0x49,
        CMID_HV_OUT_AUTO_TOGGLE = 0x4A,
};

/// State Machine code definition
enum STATES_OF_SCPI  {
    SCPI_STATE_READY = 0,
        SCPI_STATE_CR ,
        SCPI_STATE_ERROR ,
    SCPI_STATE_PARSE_DONE
};

enum TYPE_SCPI_CMD {
        SCPI_UNKOWN_CMD = 0,
        SCPI_SIMPLE_CMD,
        SCPI_COMPLEX_CMD
};




// pack: make 1 byte struct
#pragma pack(push, 1)
typedef struct __PacketApollo {
    U08 ST; // 0xC5
    U08 ST2; // 0xC5

    U08 length;

    // USE H/L byte to avoid endian problem
    U08 targetIdH;
    U08 targetIdL;

    U08 deviceID;

    U08 cmd;
    U08 result;
    U08 mid;

    U08 *data;

    // USE H/L byte to avoid endian problem
    U08 CRCH;
    U08 CRCL;
} StructApolloPacket;

typedef struct __PacketScpi {
    U08 *data;
    U08 length;

        U08 bGet : 4; // command is get(request) -> 1  or set -> 0
        U08 cmdType : 4; // simple or complex

        U08 nCmdOder ;
        U08 nCmdOderSub;

        U08 cmd1[N_MAX_SCPI_CMD_LENGTH]; // first command
        U08 cmd2[N_MAX_SCPI_CMD_LENGTH]; // second command
        U08 cmd3[N_MAX_SCPI_CMD_LENGTH]; // third command
        U08 param[N_MAX_SCPI_PARAM_LENGTH]; // parameter
} StructScpi;
#pragma pack(pop)



// pack: make 1 byte struct
#pragma pack(push, 1)
typedef struct __ESC_STATE {
        //U08 id[64];
        //U08 ver[16];
/* 
	Move to StructEscStatVolatile: NOT needed to Save...
    //U08 hvOut;	// HV Out On/Off : 1/0
    U08 hvOutputStatus;// 0/1 HV Output Status (고전압 출력 ON/OFF상태), R only

    //S16 atVolt;
    //U08 atCount;
    U08 toggleCount;
    U08 hvToggle;
	*/
            // 출젹 세팅 값
//    S16 sourVolt;	// output ON 상태에의 극성과 같은 극성의 전압으로 변경
	
    U08 hvSetVolt1; // 1~30, *100 [V], default = 1kV 고전압 출력 설정, R/W, V x 100
    U08 hvSetVolt2;

    //U08 rampUpDown; // ramp mode: up/down
    U08 rampUpTime; // 3~100, 1step, msec, 고전압 출력이 설정 전압 까지 도달하는 상승시간, msec x 100
    U08 rampDownTime;

    U08 autoDischargeStepCount; // 0~3; R/W
    U08 autoDischargeVoltStep1; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeVoltStep2; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeVoltStep3; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeKeepTimeStep1; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간 R/W
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time
    U08 autoDischargeKeepTimeStep2; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time
    U08 autoDischargeKeepTimeStep3; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time


    U08 ocpAlarmControl;	// 0~1, default=1 R/W (OCP) 알람 기능 ON/OFF
    U08 ocpAlarmEvent;	//0~1, Disable/Enable, R/W (OCP) 알람 상위 이벤트 상위 보고 ON/OFF
    U08 ocpAlarmLimit; //10~250, default 30 R/W (OCP) 알람 기준, uA x 10

    U08 arcAlarmControl; // 0/1, R/W (Arc) 알람 기능 ON/OFF
    U08 arcAlarmEvent; // 0/1, R/W (Arc) 알람 상위 Event 자동 보고 ON/OFF
    U16 arcAlarmLimit; // 0~65000,알람 기준 횟수, default = 250, R/W

    U08 battAlarmControl; // 0/1, R/W,	Event 처리 Enable/Disable 플래그
    U08 battAlarmLimit; // 0~100, default=15, [%], R/W Battery Alarm 기준 전압
    U08 battEventControl; // 0/1 R/W 상위 Event 보고 Enable / Disable


	U08 ocpAlarmCount; // 0~250, deault 0, R only  (OCP) 알람 카운트, 전류 측정 값이 "OCP Alarm Limit" 이상 발생 시 카운트 증가
//Value=250이상 -> "OVER"

	U16 arcAlarmCount; // 0~65000 R	(Arc) 알람 카운트, 전압 측정 값이 "Arc Alarm Limit"이상 발생 시 카운트 증가, 65000이상 -> OVER


    /*
            0 : NON
            (Load/Save NO.) 1/2/3
            (MSB)bit Load / Save

            * 동작 : 3개의 사용자 데이터 중 택 1 ""불러오기"" ""저장""
            * 항목 : [HV Default] 항목들의 현재 설정값 적용

            Device별SET : Controller=Console / Server=GUI등록
    */
    U08 userMode; // User Mode Save/Load, 0/1/2/3
    U08 waveMode; // 1~40, defaut=1, R/W "Controller에 저장된 40개의 Parameter값 중 택1 적용(저장 값 고정) Wave Mode 문서 참조"

    /*
            0 : Disable
            1 : GET MID GROUP 1 항목
            2 : GET MID GROUP 2 항목
    */
    U08 autoQueryControl; // 0/1/2, R/W

    U16 autQueryIntervalTime; // 1~3600, [sec] R/W  자동응답 주기 시간 (max : 1 Hour)

    U08 confArcStat; // arc mode
    U08 confArcLevel; // 아크 알람이 발생하는 횟수 (100~250)


    //S16 confCurr;
    //int confCurrB;

    U08 hvAutoToggle; // Auto toggle on(1)/off(0), hvOutputAutoToggle	ON : HV Output(고전압 출력) Turn ON시 마다 ch-1/ch-2 전압 극성 반대


} StructEscStat;

typedef struct __ESC_STATE_VOLATILE {
    //U08 hvOut;	// HV Out On/Off : 1/0
    U08 hvOutputStatus;// 0/1 HV Output Status (고전압 출력 ON/OFF상태), R only

    //S16 atVolt;
    //U08 atCount;
    U08 toggleCount;
	
} StructEscStatVolatile;

typedef struct __ESC_MEASURE {

        // 측정된 값: 현재 전압/전류 값
        U08 voltA;
        U08 voltB;
        U08 currA;
        U08 currB;

        U08 arc;
        U08 arcTrip;

        S16 apHvSet;
        //int batVolt;
        U08 battStatusVolt; // 0~250, V/10, R only Battery 출력 전압
        U08 battCurr; // 0~100, A/10 R only	Battery 소모전류   ex) 1234 / 100 = 12.34A
        U08 battLowAlarmStatus; // 0/1 R only  Battery Low 알람 상태

        /*
        "* Not Connect : ""Battery Status Voltage"" 값이 감소 추세 인 경우
        * 중천 중        : ""Battery Status Voltage"" 값이 증가 추세 인 경우
        * 충전 완료     : ""Battery Status Voltage"" 값이 12.3V 이상인 경우"
*/
        U08 battChargeConnectStatus; // 0/1/2, R only
        U08 battPercent;
        S16 chargeCurr;
		
		U08 ocpAlarmStatus;	//0~1, normal/Ocurr  (OCP) 알람 상태,  R only

		// 과전압 보호 상태, Over voltage Protection
		//U08 voltPortectTrip;
		//U08 alarmArc;
		U08 arcAlarmStatus; // 0/1 R	(Arc) 알람 상태


        S16 temp1; // -2000~8500, 'C/10, R only PT-100, European Curve
        S16 temp2;
		
		S16 press;
		
		U08 lastBattPercent;
		U08 lastOcp;
		U16 lastArc;


} StructEscMeasure;

typedef struct wiz_NetInfo_apollo
{
   U08 mac[6];  ///< Source Mac Address
   U08 ip[4];   ///< Source IP Address
   U08 sn[4];   ///< Subnet Mask 
   U08 gw[4];   ///< Gateway IP Address
   U08 dns[4];  ///< DNS server IP Address
   U08 dhcp;  ///< 1 - Static, 2 - DHCP
}wiz_NetInfo_apollo;


typedef struct __StrucNetInfo {
        U08 ip[4];
        U08 sm[4];
        U08 gw[4];
        U16 port;
        U08 mac[6];
} StructNetInfo;

typedef struct __UserMode {
    U08 hvSetVolt1; // 1~30, *100 [V], default = 1kV 고전압 출력 설정, R/W, V x 100
    U08 hvSetVolt2;

    //U08 rampUpDown; // ramp mode: up/down
    U08 rampUpTime; // 3~100, 1step, msec, 고전압 출력이 설정 전압 까지 도달하는 상승시간, msec x 100
    U08 rampDownTime;

    U08 autoDischargeStepCount; // 0~3; R/W
    U08 autoDischargeVoltStep1; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeVoltStep2; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeVoltStep3; // MSB=+/-, 5~30 *100V, default 9 Discharge Step1 출력 전압 설정 값, Port A / B (+/-) 극성 반대
    U08 autoDischargeKeepTimeStep1; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간 R/W
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time
    U08 autoDischargeKeepTimeStep2; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time
    U08 autoDischargeKeepTimeStep3; // 0~240, default 0, Discharge Step1 설정 값 도달후 유지시간
                                                                            // 설정 값 도달 시간 = Ramp Up Time +Ramp Down Time


    U08 ocpAlarmControl;	// 0~1, default=1 R/W (OCP) 알람 기능 ON/OFF
    U08 ocpAlarmEvent;	//0~1, Disable/Enable, R/W (OCP) 알람 상위 이벤트 상위 보고 ON/OFF
    U08 ocpAlarmLimit; //10~250, default 30 R/W (OCP) 알람 기준, uA x 10

    U08 arcAlarmControl; // 0/1, R/W (Arc) 알람 기능 ON/OFF
    U08 arcAlarmEvent; // 0/1, R/W (Arc) 알람 상위 Event 자동 보고 ON/OFF
    U16 arcAlarmLimit; // 0~65000,알람 기준 횟수, default = 250, R/W

    U08 battAlarmControl; // 0/1, R/W,	Event 처리 Enable/Disable 플래그
    U08 battAlarmLimit; // 0~100, default=15, [%], R/W Battery Alarm 기준 전압
    U08 battEventControl; // 0/1 R/W 상위 Event 보고 Enable / Disable	
} StructUerMode;

#define N_MAX_SN_LENGTH	13
typedef  struct __StructSetting {
    U08 serialNumber[16]; // Max 13 charactors
	U64 eui;
    U16 cid; // Controller ID, (Source Addr/ Dest. Addr)	1~800, default=1 R/W	Device별SET : Controller=Console       / Server=GUI등록, USB(SAVE, LOAD)
    U08 sid; // Server ID R/W, 1~250 Device별SET : Controller=Console       / Server=GUI등록, USB(SAVE, LOAD)

    U08 pid; // PLC ID, R/W 1~250 "Device별SET : Controller=Console       / Server=GUI등록, USB(SAVE, LOAD)(단, 컨트롤러<->PLC 구성 시 ""PLC ID"" 무시하고 모두 수신)"

    U08 aid[12]; // Admin ID, R/W	Device별SET : Controller=Console       / Server=GUI등록(제조사만:password=admin)
    U08 hwVer;	// R/W, 100~255 Device별SET : Controller=Console       / Server=NON
    U08 swVer; // R, 100~255 Device별SET : Controller=NON         / Server=S/W Source Code
    U08 fwVer; // R, 100~255 Device별SET : Controller=Source Code / Server=NON

    U08 mode; 	// mode: Local/Remote/AP -> default = Local
    U08 lanLora; // lan = 2, LoRa = 1, serial = 0

    U08 protocol; // protocol: SCPI/Custom : 0/1

    U08 debugModeOnOff;

    U08 serialSpeed; // 0~7, 1200,2400,4800 ,9600,...
	U08 serialDatabit;
	U08 serialParity;
	U08 serialStopbit;

    //StructNetInfo server;
	U08 serveIp[4];
	int nPort;
    StructNetInfo leQlight;

    //U08 netInfo[32]; //wiz_NetInfo
	wiz_NetInfo_apollo netInfo;

    S08 temp1Tune;
    S08 temp2Tune;
	
	U08 mute;
	int offsetVoltA;
	int offsetVoltB;
	int offsetCurrA;
	int offsetCurrB;
	
	U08 noRepeatDac;
	
	int cutOffA;
	int cutOffB;
	

} StructSetting;

typedef struct __ESC_INFO {
        U08	infoCheck; // to check eeprom
        U08	infoCheckB; // to check eeprom
        StructSetting setting;
        StructEscStat stat;
		U08 hvOffset[N_HV_VOLT_OFFSET];
		
		StructUerMode userMode[N_MAX_USER_MODE_SIZE];
	
        StructEscMeasure measure;
		StructEscStatVolatile volatile_stat;
} StructEscInfo;
#pragma pack(pop)


extern const U08 StrScpiCommands[N_MAX_SCPI_CMD_COUNT][3][N_MAX_SCPI_CMD_LENGTH];

extern const U08 StrScpiSimpleCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiComplexCommands[][N_MAX_SCPI_CMD_LENGTH];
extern U08 g_dataBuf[N_MAX_DATA_BUFFER_SIZE];

extern const U08 StrScpiSub0SystemCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiSub1OutputCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiSub2SourceCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiSub3MeasureCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiSub4ConfigureCommands[][N_MAX_SCPI_CMD_LENGTH];
extern const U08 StrScpiSub5AutoToggleCommands[][N_MAX_SCPI_CMD_LENGTH];

extern const U08 strError104[];
extern const U08 strError108[];
extern const U08 strError109[];
extern const U08 strError113[];
extern const U08 strError222[];
extern const U08 strError350[];
extern const U08 strError410[];
extern const U08 strError420[];
extern const U08 strError430[];
extern const U08 strError440[];
extern const U08 strError515[];
extern const U08 strError522[];
extern const U08 strError561[];


U16 MakeCRC(StructApolloPacket *packet, int length);
U08 ParsePacket(U08 stateMachine, U08 data, StructApolloPacket* packet);

U08 ParseSCPI(U08 stateMachine, U08 data, StructScpi *packet);

U16 SwapEndian16(U16 from);
U16 MemCopyN(U08*dst, U08*src, U16 len);


void gen_crc_table(void);
U16 update_crc(U16  crc_accum, unsigned char *data_blk_ptr, U16  data_blk_size);

int isStringDouble(char *s) ;
char* rTrim(char* s) ;
char* lTrim(char *s) ;
char* Trim(char *s) ;

int ByteToVolt(U08 x);
float ByteToVoltKV(U08 x);

int ByteToAmpare(U08 x);
int ByteToWord(U08 x, int scale);
float ByteToFloat(U08 x, float scale);
U08 WordToByte(int x, int scale);

U08 VoltToByte(float x);

void GetAllWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo);
void ResponsEscWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo);
void EventReportEscWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo);

void ResetStat(StructEscInfo * pInfo);

S08 CompareInfo(void* a, void*b, int n);
U08 IsToggled(StructEscInfo * pInfo);

U32 xcrc32 (const unsigned char *buf, int len, U32 init);

#ifdef __cplusplus
}
#endif

#endif // _PROTOCOL_H

