/**
  ******************************************************************************
  * @file    dataformat.c
  * @author  jw Lee
  * @version 0.0.0.1
  * @date    03.20.2019
  * @brief   SCPI, Custom-protocol parser
  ******************************************************************************
  * @contact
  * <h2><center>&copy; COPYRIGHT 2019 jw@mizilab.com </center></h2>
  ******************************************************************************
  */

#include <stdio.h>  //  to use NULL
#include <string.h>
#include <ctype.h>

#include "dataformat.h"

#if USE_MALLOC
#include <stdlib.h> // to use malloc()/free()
static U08 *g_dataBuf;
#else
//static
U08 g_dataBuf[N_MAX_DATA_BUFFER_SIZE];
#endif
static int g_nDataOnly = 0;
static int g_dataIndex = 0;
//static int g_nData = 0;


/**
    SCPI Commands: string

"ALM:ARC",	// 아크 Alarm 모드가 2(auto)인 경우, ARC alaram 발생시 요청없이 자동으로 반환 -> ALM:ARC

*/

/* SCPI all commands: Total commands
*/

/*****************
OUTP		9
CONF:VOLT	25
CONF:CURR	27
CONF:RAMP	29
SOUR:VOLT	13
TOGG		11
AT:COUN		34
AT:VOLT:LEV	36
AT:STAT		32
 -> could not while running...

 * paired Command with ? mark should be placed FIRST!
 * 순서대로 검색하므로 ?를 포함하는 명령을 항상 먼저 선언해야함!

*/
/// use with ScpiCmdNumber (enum)
const U08 StrScpiCommands[N_MAX_SCPI_CMD_COUNT][3][N_MAX_SCPI_CMD_LENGTH] = {
    { "CLS" }	 // clear error queue					 //0
    ,{ "IDN?" }	// ESC Controller 식별 코드를 반환, Get ONLY //1
    ,{ "RST" } // reset ESC Controller //2
    ,{ "SYST:ERR?" }	// SYSTem command //3

    ,{ "SYST:VERS?" } //4

    ,{ "SYST:BEEP" , "BEEP" } //5

    ,{ "SYST:LOC", "LOC" } //6

    ,{ "SYST:REM", "REM" } //7

    ,{ "OUTP?" , "OUTP:STAT?" } //8

    ,{ "OUTP:STAT", "OUTP" } //9, 알파벳 순서 때문에 OUTP:STAT 먼저 위치

    ,{ "TOGG?" }//10

    ,{ "TOGG" }//11

    ,{ "SOUR:VOLT:PROT:TRIP?", "VOLT:PROT:TRIP?" }//12 -> 긴 명령이 먼저!
    ,{ "SOUR:VOLT?", "VOLT?" }//13
    ,{ "SOUR:VOLT", "VOLT" }//14


    ,{ "SOUR:CURR:PROT:TRIP?", "CURR:PROT:TRIP?" }//15
	,{ "SOUR:CURR:PROT:TRIP", "CURR:PROT:TRIP" }//16


    ,{ "MEAS:VOLT?", "MEAS?" }//
    ,{ "MEAS:CURR?"  }//


    ,{ "MEAS:ARC?", "ARC?"  }//

    ,{ "ALM:ARC" }//


    ,{ "MEAS:ARC:TRIP?", "ARC:TRIP?"  }//


    ,{ "CONF:ARC:LEV?", }// -> 순서 주의!
    ,{ "CONF:ARC:LEV", }//

    ,{ "CONF:ARC:STAT?", "CONF:ARC?" }//
    ,{ "CONF:ARC:STAT", "CONF:ARC" }//

    ,{ "CONF:VOLT?", }//
    ,{ "CONF:VOLT", }//

    ,{ "CONF:CURR?", }//
    ,{ "CONF:CURR", }//

    ,{ "CONF:RAMPDOWN?", }//
    ,{ "CONF:RAMPDOWN", }//

    ,{ "CONF:RAMPUP?", }//
    ,{ "CONF:RAMPUP", }//

	,{ "CONF:RAMP?", "RAMP?", }//
    ,{ "CONF:RAMP", "RAMP"}//

    // ex> V+2500;V-2500;A+1000;1;0;0 -> chA전압;ChB전압;ChA전류;ChB전류; 출력상태;토글상태;모드상태(Remote/local)
    ,{ "STAT?" }//

    ,{ "AT:COUN?" }//
    ,{ "AT:COUN" }//

    ,{ "AT:VOLT?", "AT:VOLT:LEV?" }//
    ,{ "AT:VOLT", "AT:VOLT:LEV" }//

    ,{ "AT?", "AT:STAT?" }//
    ,{ "AT", "AT:STAT" }//


};


/* SCPI simple commands: 단일명령, sub 커맨드 없음
*/
const U08 StrScpiSimpleCommands[N_MAX_SCPI_SIMPLE_CMDS][N_MAX_SCPI_CMD_LENGTH] = {
    "CLS", // clear error queue
    "IDN",	// ESC Controller 식별 코드를 반환, Get ONLY
    "RST", // reset ESC Controller

    "TOGG", // toggle 상태 반환, Get ONLY
    "STAT"	// ESC 측정 데이터를 한번에 전송 , Get ONLY

};

/* SCPI complex commands: 복함 명령, sub 커맨드 존재
*/
const U08 StrScpiComplexCommands[N_MAX_SCPI_COMPLEX_CMDS][N_MAX_SCPI_CMD_LENGTH] = {
    "SYST",	// SYSTem command
    /* Ex>
    SYST:ERR?
    SYST:VERS?
    SYST:BEEP
    SYST:LOC
    SYST:REM

    */

    "OUTP",			// ESC controller on/off: OUTPut[:STATe] {0|1|OFF|ON}
    /* Ex>
    OUTP 1
    OUTP ON
    OUTP:STAT 1

    OUTP?
    OUTP:STAT?
    */
    "SOUR", 	// out ON 상태에서 출력전압을 변경 (극성 유지)
    /* Ex>
    SOUR:VOLT 2500
    SOUR:VOLT 1200V, -1300V  // -1300V는 무시됨

    SOUR:VOLT:PROT:TRIP?	// 과전압 보호 (Over Voltage Protection) 상태인지 반환
    VOLT:PROT:TRIP?

    SOUR:CURR:PROT:TRIP?	// 과전류 보호 상태인자 반환
    CURR:PROT:TRIP?
    */

    "MEAS",		// 측정된 값 반환
    /* ex>
    MEAS:VOLT?	// 측정된 전압 반환
    MEAS:CURR?	// 측정된 전류값 반환
    MEAS:ARC?	// 발생한 ACR 값 반환
    ARC?

    MEAS:ARC:TRIP?	// arc 알람이 발생했는지 상태 반환
    ARC:TRIP?
    */

    "CONF",		//	설정
    /* Ex>
     * ACR mode 설정
     0: off(request에도 response 안함)
     1: Manual mode (request에만 response)
     2. Auto mode ( request와 상관없이 ARC alaram 발생시 PLC에 알람)


    CONF:ARC:STAT 1
    CONF:ARC 1

    CONF:ARC:LEV 100	// ARC alaram이 발생하는 횟수 설정: 기본 100, 최소 1 최대 250
    CONF:ARC:LEV?		// 아크알람이 발생하는 횟수 (설정값) 반환

    ** Output이 OFF 상태에서만 설정 가능, 1,2 값은 동일해야하며 2가 다르면 값은 무시 극성만 설정
    CONF:VOLT	1000, 1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT	+1000, -1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT	+1000, -1200  // A:+1000, B:-1000으로 설정
    CONF:VOLT	-1000, +1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT?	// 설정값 반환

    CONF:CUR 500 // 전류 제한값 설정
    CONF:CUR?	// 설정된 전류 제한값 반환
    */
    "AT"	// AToggle:[STATe]{0|1|ON|OFF}, Auto Toggle 사용 여부 설정
};

const U08 StrScpiSub0SystemCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "ERR?", // ESC controller Error queue 의 메시지를 반환, 20개 초과시 +350, Too many erross,
                        // "+0, No Error" when no err
    "VER?",
    "BEEP",	// 비프음 발생
    "LOC",		// Change to Local mode
    "REM",		// Change to Remote mode
};

const U08 StrScpiSub1OutputCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "STAT"
};

    /*
    SOUR:VOLT 2500
    SOUR:VOLT 1200V, -1300V  // -1300V는 무시됨

    SOUR:VOLT:PROT:TRIP?	// 과전압 보호 (Over Voltage Protection) 상태인지 반환
    VOLT:PROT:TRIP?

    SOUR:CURR:PROT:TRIP?	// 과전류 보호 상태인자 반환
    CURR:PROT:TRIP?
    */
const U08 StrScpiSub2SourceCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "VOLT",
    "CURR"
};

    /* ex>
    MEAS:VOLT?	// 측정된 전압 반환
    MEAS:CURR?	// 측정된 전류값 반환
    MEAS:ARC?	// 발생한 ACR 값 반환
    ARC?

    MEAS:ARC:TRIP?	// arc 알람이 발생했는지 상태 반환
    ARC:TRIP?

    */
const U08 StrScpiSub3MeasureCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "VOLT",
    "CURR",
    "ARC"
};

    /* Ex>
     * ACR mode 설정
     0: off(request에도 response 안함)
     1: Manual mode (request에만 response)
     2. Auto mode ( request와 상관없이 ARC alaram 발생시 PLC에 알람)


    CONF:ARC:STAT 1
    CONF:ARC 1

    CONF:ARC:LEV 100	// ARC alaram이 발생하는 횟수 설정: 기본 100, 최소 1 최대 250
    CONF:ARC:LEV?		// 아크알람이 발생하는 횟수 (설정값) 반환

    ** Output이 OFF 상태에서만 설정 가능, 1,2 값은 동일해야하며 2가 다르면 값은 무시 극성만 설정
    CONF:VOLT	1000, 1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT	+1000, -1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT	+1000, -1200  // A:+1000, B:-1000으로 설정
    CONF:VOLT	-1000, +1000  // A:+1000, B:-1000으로 설정
    CONF:VOLT?	// 설정값 반환

    CONF:CUR 500 // 전류 제한값 설정
    CONF:CUR?	// 설정된 전류 제한값 반환
    */
const U08 StrScpiSub4ConfigureCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "VOLT",
    "CURR",
    "ARC"
};

/*
    "AT",	// AToggle:[STATe]{0|1|ON|OFF}, Auto Toggle 사용 여부 설정
    "AT:COUN",	// auto toggle 실행 횟수 설정
*/
const U08 StrScpiSub5AutoToggleCommands[][N_MAX_SCPI_CMD_LENGTH] = {
    "COUN"
};

#if 0
const U08 strError104[] = "-104, Data type error\r\n";
const U08 strError108[] = "-108, Parameter not allowed\r\n";
const U08 strError109[] = "-109, Missing parameter\r\n";
const U08 strError113[] = "-113, Undefined header\r\n";
const U08 strError222[] = "-222, Data out of range\r\n";
const U08 strError350[] = "-350, Too many errors\r\n"; // 10개 이상의 오류 발생, CLS 명령 혹은 전원 리셋으로 오류 삭제
const U08 strError410[] = "-410, Query INTERRUPTED\r\n"; // 명령이 수신되었지만 출력버퍼에 이전 명령의 데이터가 있음, 이전 에티터 손실
const U08 strError420[] = "-420, Query UNTERMIATED\r\n";
const U08 strError430[] = "-430, Query DEADLOCKED\r\n";
const U08 strError440[] = "-440, Query UNTERMINATED after indefinite response\r\n";
const U08 strError515[] = "-515, Command not allowed in Remote\r\n";
const U08 strError522[] = "-522, Output buffer overflow\r\n";
const U08 strError561[] = "-561, Output Enabled\r\n"; // Output on인 상태에서 CONF:VOLT 1000 이런 경우...
#else
const U08 strError104[] = "";
const U08 strError108[] = "";
const U08 strError109[] = "";
const U08 strError113[] = "";
const U08 strError222[] = "";
const U08 strError350[] = "";
const U08 strError410[] = "";
const U08 strError420[] = "";
const U08 strError430[] = "";
const U08 strError440[] = "";
const U08 strError515[] = "";
const U08 strError522[] = "";
const U08 strError561[] = "";
#endif



static const unsigned int crc32_table[] =
{
  0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9,
  0x130476dc, 0x17c56b6b, 0x1a864db2, 0x1e475005,
  0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
  0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd,
  0x4c11db70, 0x48d0c6c7, 0x4593e01e, 0x4152fda9,
  0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
  0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011,
  0x791d4014, 0x7ddc5da3, 0x709f7b7a, 0x745e66cd,
  0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
  0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5,
  0xbe2b5b58, 0xbaea46ef, 0xb7a96036, 0xb3687d81,
  0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
  0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49,
  0xc7361b4c, 0xc3f706fb, 0xceb42022, 0xca753d95,
  0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
  0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d,
  0x34867077, 0x30476dc0, 0x3d044b19, 0x39c556ae,
  0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
  0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16,
  0x018aeb13, 0x054bf6a4, 0x0808d07d, 0x0cc9cdca,
  0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
  0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02,
  0x5e9f46bf, 0x5a5e5b08, 0x571d7dd1, 0x53dc6066,
  0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
  0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e,
  0xbfa1b04b, 0xbb60adfc, 0xb6238b25, 0xb2e29692,
  0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
  0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a,
  0xe0b41de7, 0xe4750050, 0xe9362689, 0xedf73b3e,
  0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
  0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686,
  0xd5b88683, 0xd1799b34, 0xdc3abded, 0xd8fba05a,
  0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
  0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb,
  0x4f040d56, 0x4bc510e1, 0x46863638, 0x42472b8f,
  0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
  0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47,
  0x36194d42, 0x32d850f5, 0x3f9b762c, 0x3b5a6b9b,
  0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
  0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623,
  0xf12f560e, 0xf5ee4bb9, 0xf8ad6d60, 0xfc6c70d7,
  0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
  0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f,
  0xc423cd6a, 0xc0e2d0dd, 0xcda1f604, 0xc960ebb3,
  0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
  0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b,
  0x9b3660c6, 0x9ff77d71, 0x92b45ba8, 0x9675461f,
  0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
  0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640,
  0x4e8ee645, 0x4a4ffbf2, 0x470cdd2b, 0x43cdc09c,
  0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
  0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24,
  0x119b4be9, 0x155a565e, 0x18197087, 0x1cd86d30,
  0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
  0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088,
  0x2497d08d, 0x2056cd3a, 0x2d15ebe3, 0x29d4f654,
  0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
  0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c,
  0xe3a1cbc1, 0xe760d676, 0xea23f0af, 0xeee2ed18,
  0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
  0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0,
  0x9abc8bd5, 0x9e7d9662, 0x933eb0bb, 0x97ffad0c,
  0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
  0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};

/*
@deftypefn Extension {unsigned int} crc32 (const unsigned char *@var{buf}, @
  int @var{len}, unsigned int @var{init})
Compute the 32-bit CRC of @var{buf} which has length @var{len}.  The
starting value is @var{init}; this may be used to compute the CRC of
data split across multiple buffers by passing the return value of each
call as the @var{init} parameter of the next.
This is used by the @command{gdb} remote protocol for the @samp{qCRC}
command.  In order to get the same results as gdb for a block of data,
you must pass the first CRC parameter as @code{0xffffffff}.
This CRC can be specified as:
  Width  : 32
  Poly   : 0x04c11db7
  Init   : parameter, typically 0xffffffff
  RefIn  : false
  RefOut : false
  XorOut : 0
This differs from the "standard" CRC-32 algorithm in that the values
are not reflected, and there is no final XOR value.  These differences
make it easy to compose the values of multiple blocks.
@end deftypefn
*/

U32 xcrc32 (const unsigned char *buf, int len, U32 init)
{
  U32 crc = init;
  while (len--)
    {
      crc = (crc << 8) ^ crc32_table[((crc >> 24) ^ *buf) & 255];
      buf++;
    }
  return crc;
}

/**
 * FN: CRC generation
 * Param:
 *      *packet: packet struct pointer
 *      int length: Header + Data length, as protocol
 * */
U16 MakeCRC(StructApolloPacket *packet, int length)
{
//    QString msg;
    U16 crc = 0;
#if PACKET_LENGTH_INCLUDING_HEADER
    int nDataSize = N_PACKET_SIZE_EXCEPT_DATA + length-N_PACKET_LENGTH_HEADER + N_CRC_BYTE_SIZE; // - N_ADDRESS_BYTE_SIZE - N_CMD_BYTE_SIZE - N_MID_BYTE_SIZE - N_RESULT_BYTE_SIZE;
    U16 nDataOnlySize = length-N_PACKET_LENGTH_HEADER; //nDataSize - N_HEADER_SIZE;
#else
    int nDataSize = N_PACKET_SIZE_EXCEPT_DATA + length + N_CRC_BYTE_SIZE; // - N_ADDRESS_BYTE_SIZE - N_CMD_BYTE_SIZE - N_MID_BYTE_SIZE - N_RESULT_BYTE_SIZE;
    U16 nDataOnlySize = length; //nDataSize - N_HEADER_SIZE;
#endif
    U16 nDataPosition = N_DATA_POSITION;
    int nTotalLength = N_PACKET_SIZE_EXCEPT_DATA + nDataOnlySize;
    U08 buffTemp[nTotalLength];


    if(nDataSize <= 0) {
        //qDebug() << "length ERR!";
        return 0;
    }

    if(packet->data==NULL)
    {
//        qDebug() << " data is nullptr";
        return 0;
    }

    // convert packet struct to char array
    MemCopyN(buffTemp, (U08*)packet, nDataPosition);
    MemCopyN(buffTemp + nDataPosition, packet->data, nDataOnlySize);

    //QByteArray arr = QByteArray::fromRawData(buffTemp, nTotalLength);

    //qDebug()<< "Size " << sizeof(packet) << " arr " << arr.length() << " Full len" << CustomProtocol:: getFullLengthApolloPacket(packet) << " sizeof(StructApolloPacket) " << sizeof(StructApolloPacket);

    crc = update_crc(0, (U08 *)buffTemp, nTotalLength - N_CRC_BYTE_SIZE);
#ifndef __cplusplus			
	printf("CRC = %x, len %d\r\n", crc, nTotalLength);
#else
	QString str;
	str.sprintf("CRC = %x, len %d\r\n", crc, nTotalLength);
	qDebug()<<str;
#endif
    /*
    msg = "HEX: ";
    for(int i=0;i<nTotalLength-N_CRC_BYTE_SIZE;i++) {
        QString strByte;
        strByte.sprintf("%2.2X ",0x0ff&buffTemp[i]);
        msg.append(strByte);
    }
    qDebug() << msg;
*/


    packet->CRCH = (crc&0xff00)>>8;
    packet->CRCL = (crc&0x00ff);

//    msg.sprintf(("CRC16 %4.4x high %2.2x low %2.2x"), crc, ((crc & 0xff00)>>8), crc&0x00ff);    qDebug() << msg;


    return crc;
}

U08 ParseSCPI(U08 stateMachine, U08 data, StructScpi *packet)
{

    switch (stateMachine) {
        case SCPI_STATE_READY:// ready, BEFORE ST comming...

            if(data > 0) { // && data <= 127) {
                // skip CR
                if(data == SCPI_END_CR) {
                    g_dataBuf[g_dataIndex++] = data;
                }
                else if(data == SCPI_END_LF || data == SCPI_END_SEMI_COLON) {
                    g_dataBuf[g_dataIndex] = data;
                    packet->data = g_dataBuf;
                    packet->data[g_dataIndex] = NULL; // Add NULL end of data...
                    packet->length = g_dataIndex;

                    g_dataIndex = 0;
                    stateMachine = SCPI_STATE_PARSE_DONE;
                }
                else {
                    // save data to buffer
                    if(g_dataIndex < N_MAX_DATA_BUFFER_SIZE) {
                        g_dataBuf[g_dataIndex++] = data;

                        if(data=='?') { // Request Query
                            packet->bGet = 1;
                        }
                    }
                    else { // OUT of range!
                        g_dataIndex = 0;
                        stateMachine = SCPI_STATE_READY;
                        break;
                    }
                }
            }
            break;

        case SCPI_STATE_CR:// ready, BEFORE ST comming...
            if(data == SCPI_END_LF) {
                g_dataBuf[g_dataIndex++] = data;

                packet->data = g_dataBuf;
                packet->data[g_dataIndex] = NULL; // Add NULL end of data...
                packet->length = g_dataIndex;

                g_dataIndex = 0;
                stateMachine = SCPI_STATE_PARSE_DONE;
            }
            else {
                g_dataIndex = 0;
                stateMachine = SCPI_STATE_READY;
            }

            break;
        case SCPI_STATE_ERROR:
        default:
                break;
    }

    return stateMachine;
}

U08 ParsePacket(U08 stateMachine, U08 data, StructApolloPacket* packet)
{
    //dbg("staet:%x data:%x g_nDataOnly %x index %x\r\n", stateMachine, data, g_nDataOnly, g_dataIndex);

    switch (stateMachine) {
    case STATE_READY:// ready, BEFORE ST comming...
        if(data == CODE_ST) {
            stateMachine = STATE_ST;
            packet->ST = data;
            packet->CRCH = 0; packet->CRCL = 0;
            packet->targetIdH = 0; packet->targetIdL = 0;
            packet->deviceID = 0;

//            g_nData = 0;
        }
        break;
    case STATE_ST:// ready, BEFORE ST comming...
        if(data == CODE_ST2) {
            stateMachine = STATE_LENGTH;
            packet->ST2 = data;
        }
		else {
			stateMachine = STATE_READY;
		}
        break;

    case STATE_LENGTH:
        stateMachine += 1;
        packet->length = data;
        //g_nData = packet->length;
		//printf("Len %d\r\n", data);

        g_nDataOnly = data;// g_nData - N_ADDRESS_BYTE_SIZE - N_CMD_BYTE_SIZE - N_RESULT_BYTE_SIZE - N_MID_BYTE_SIZE;
#if PACKET_LENGTH_INCLUDING_HEADER
        // Include header too
        g_nDataOnly -= N_PACKET_LENGTH_HEADER;
#endif
        g_dataIndex = 0;
#if USE_MALLOC
        if(g_dataBuf!=NULL) {
            free(g_dataBuf);
        }
        g_dataBuf = (U08*)malloc(g_nDataOnly);
#endif
        break;

        // parsing, AFTER Length Come.
    case STATE_ADDRESS1_H: // targetId high
        stateMachine += 1;
        packet->targetIdH = data;

        break;
    case STATE_ADDRESS1_L: // targetId low
        stateMachine += 1;
        packet->targetIdL = data;

        break;

    case STATE_ADDRESS2: //device Id
        stateMachine += 1;
        packet->deviceID = data;

        break;

    case STATE_CMD:
        stateMachine += 1;
        packet->cmd = data;

        break;

    case STATE_RESULT:
        stateMachine += 1;
        packet->result = data;

        break;
    case STATE_MID:
        stateMachine += 1;
        packet->mid = data;

        break;

    case STATE_DATA:
        // save data to buffer
        if(g_dataIndex<sizeof(g_dataBuf)) {
            g_dataBuf[g_dataIndex++] = data;
        }
        else // out of length -> Back to READY
        {
            //dbg("ERROR!, data buffer is full!\r\n");
            stateMachine = STATE_READY;
        }

        g_nDataOnly--; // length is including IDs, CMD, result, MID
        if(g_nDataOnly<=0) {       // last data
            stateMachine += 1; // to Next STATE
            packet->data = g_dataBuf;
        }
        break;


    case STATE_CRCH:
        stateMachine += 1;
        packet->CRCH = data;
        break;

    case STATE_CRCL: // last packet
        packet->CRCL = data;

        U16 rxCrc16 = packet->CRCH<<8 |  packet->CRCL;

        U16 crc16 = MakeCRC(packet, packet->length) & 0x00ffff;
        if(rxCrc16 == crc16) { // CRC match!
            stateMachine = STATE_PARSE_DONE;			
        }
        else
        {
#ifndef __cplusplus			
			printf("ERROR CRC %x %x\r\n"), rxCrc16, crc16;
#endif
            stateMachine = STATE_ERR_CRC;
        }
        break;
    }

    return stateMachine;
}


U16 SwapEndian16(U16 from)
{
    U16 ret = 0;
    ret = ((from&0x00ff)<<8) | ((from&0xff00)>>8);

    return ret;
}

U16 MemCopyN(U08*dst, U08*src, U16 len)
{
    U16 i;

    for(i=0;i<len;i++)
    {
        *(dst+i) = *(src+i);
    }

    return len;
}


#ifndef USE_CRC_TABLE

static U16 crc_table[256];

#else

static U16 crc_table[256] = {0x0000,
0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027,
0x0022, 0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D,
0x8077, 0x0072, 0x0050, 0x8055, 0x805F, 0x005A, 0x804B,
0x004E, 0x0044, 0x8041, 0x80C3, 0x00C6, 0x00CC, 0x80C9,
0x00D8, 0x80DD, 0x80D7, 0x00D2, 0x00F0, 0x80F5, 0x80FF,
0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1, 0x00A0, 0x80A5,
0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1, 0x8093,
0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197,
0x0192, 0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE,
0x01A4, 0x81A1, 0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB,
0x01FE, 0x01F4, 0x81F1, 0x81D3, 0x01D6, 0x01DC, 0x81D9,
0x01C8, 0x81CD, 0x81C7, 0x01C2, 0x0140, 0x8145, 0x814F,
0x014A, 0x815B, 0x015E, 0x0154, 0x8151, 0x8173, 0x0176,
0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162, 0x8123,
0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104,
0x8101, 0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D,
0x8317, 0x0312, 0x0330, 0x8335, 0x833F, 0x033A, 0x832B,
0x032E, 0x0324, 0x8321, 0x0360, 0x8365, 0x836F, 0x036A,
0x837B, 0x037E, 0x0374, 0x8371, 0x8353, 0x0356, 0x035C,
0x8359, 0x0348, 0x834D, 0x8347, 0x0342, 0x03C0, 0x83C5,
0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1, 0x83F3,
0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7,
0x03B2, 0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E,
0x0384, 0x8381, 0x0280, 0x8285, 0x828F, 0x028A, 0x829B,
0x029E, 0x0294, 0x8291, 0x82B3, 0x02B6, 0x02BC, 0x82B9,
0x02A8, 0x82AD, 0x82A7, 0x02A2, 0x82E3, 0x02E6, 0x02EC,
0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2, 0x02D0, 0x82D5,
0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1, 0x8243,
0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264,
0x8261, 0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E,
0x0234, 0x8231, 0x8213, 0x0216, 0x021C, 0x8219, 0x0208,
0x820D, 0x8207, 0x0202 };
#endif

void gen_crc_table(void)
{
    #ifndef USE_CRC_TABLE
    U16	i, j;
    U16	crc_accum;

    for(i=0; i<256; i++)
    {
        crc_accum = ((U16)i<<8);
        for(j=0; j<8; j++)
        {
            if(crc_accum & 0x8000L)
                crc_accum = (crc_accum << 1) ^ POLYNOMIAL;
            else
                crc_accum = (crc_accum << 1);
        }
        crc_table[i] = crc_accum;

    }
    #endif
    return;
}


/*
     ___________
      8005 | XX 00 00
                yy yy ---> CRC

      8005 | XX ZZ 00 00
                yy yy 00
                   ww ww ---> CRC'
*/
U16 update_crc(U16  crc_accum, unsigned char *data_blk_ptr,U16  data_blk_size)
{
    U16 i, j;
#if USE_XOR_INSTAED_CRC
	crc_accum = 0;
	for(j=0; j<data_blk_size; j++) {
		crc_accum ^= (*data_blk_ptr++);
	}
#else
    static char bInited = 0;
    if(bInited==0)
    {
        gen_crc_table(); // call Just 1 time using static variable
        bInited = 1;
    }

    for(j=0; j<data_blk_size; j++)
    {
        i = ((U16 )(crc_accum >> 8) ^ *data_blk_ptr++) & 0x00ff;
        crc_accum = (crc_accum << 8) ^ crc_table[i];
    }
#endif	
    return crc_accum;
}


int isStringDouble(char *s)
{
  size_t size = strlen(s);
  if (size == 0) return 0;

  for (int i = 0; i < (int) size; i++) {
    if (s[i] == '.' || s[i] == '-' || s[i] == '+') continue;
    if (s[i] < '0' || s[i] > '9') return 0;
  }

  return 1;
}


char* rTrim(char* s)
{
  char t[MAX_STR_LEN];
  char *end;


  strcpy(t, s);
  end = t + strlen(t) - 1;
  while (end != t && isspace(*end))
    end--;
  *(end + 1) = '\0';
  s = t;

  return s;
}


char* lTrim(char *s)
{
  char* begin;
  begin = s;

  while (*begin != '\0') {
    if (isspace(*begin))
      begin++;
    else {
      s = begin;
      break;
    }
  }

  return s;
}


char* Trim(char *s)
{
  return rTrim(lTrim(s));
}

float ByteToVoltKV(U08 x)
{
    float ret = 0;

    if(x==0) {
        return ret;
    }

    if((x&0x080)==0x080) {
        ret = -((x&0x07F)*100./1000.0);
    }
    else {
        ret = ((x&0x07F)*100.0/1000.0);
    }

    return ret;
}
int ByteToVolt(U08 x)
{
    int ret = 0;

    if(x==0) {
        return ret;
    }

    if((x&0x080)==0x080) {
        ret = -((x&0x07F)*100);
    }
    else {
        ret = ((x&0x07F)*100);
    }

    return ret;
}
int ByteToAmpare(U08 x)
{
    return ByteToWord(x, 10);
}

U08 AmpareToByte(int x)
{
    return WordToByte(x, 10);
}

int ByteToWord(U08 x, int scale)
{
    int ret = 0;

    if(x==0) {
        return ret;
    }

    if((x&0x080)==0x080) {
        ret = -((x&0x07F)*scale);
    }
    else {
        ret = ((x&0x07F)*scale);
    }

    return ret;
}

float ByteToFloat(U08 x, float scale)
{
    float ret = 0;

    if(x==0) {
        return ret;
    }

    if((x&0x080)==0x080) {
        ret = -((x&0x07F)*scale);
    }
    else {
        ret = ((x&0x07F)*scale);
    }

    return ret;
}

U08 WordToByte(int x, int scale)
{
    U08 ret = 0;

    if(x==0) {
        return ret;
    }

    if(x<0) {
        x = -x;
        if(x/scale<0) {
            ret = ((U08)(x/scale)) | 0x80;
        }
        else {
            ret = (U08)(x/scale);
        }
        ret |= 0x80;
    }
    else {
        if(x/scale<0) {
            ret = ((U08)(x/scale)) | 0x80;
        }
        else {
            ret = (U08)(x/scale);
        }
    }

    return ret;
//#define VoltToByte(x)	(((float(x/100.0))<0? (((U08)(x/100.0))|0x80): ((U08)(x/100.0)))
}

U08 VoltToByte(float x)
{
    U08 ret = 0;

    if(x==0) {
        return ret;
    }

    if(x<0) {
        x = -x; // - 0.001;
        if(x/100<0) {
            ret = ((U08)(x/100)) | 0x80;
        }
        else {
            ret = (U08)(x/100);
        }
        ret |= 0x80;
    }
    else {
        x = x ; //+ 0.001;
        if(x/100<0) {
            ret = ((U08)(x/100)) | 0x80;
        }
        else {
            ret = (U08)(x/100);
        }
    }

    return ret;
//#define VoltToByte(x)	(((float(x/100.0))<0? (((U08)(x/100.0))|0x80): ((U08)(x/100.0)))

}


void GetAllWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo)
{
//    int i;
//    for(i=0;i<packet->length;i++)      dbg("data %x at %d", packet->data[i], i);
    int nBufPos = 0;
    U08 dataH, dataL;
    U16 data16;


    /// Add No 1~5
    pInfo->volatile_stat.hvOutputStatus = packet->data[nBufPos]; // No 1,
    nBufPos += 1;

    pInfo->measure.voltA = ( packet->data[nBufPos] ); //2
    nBufPos += 1;

    pInfo->measure.currA = (packet->data[nBufPos]); //3
    nBufPos += 1;

    pInfo->measure.voltB = (packet->data[nBufPos]); //4
    nBufPos += 1;

    pInfo->measure.currB = (packet->data[nBufPos]); //5
    nBufPos += 1;
	

    pInfo->stat.rampUpTime = packet->data[nBufPos]; // No 6, mid 0x0f
    nBufPos += 1;

    pInfo->stat.hvSetVolt1 = packet->data[nBufPos]; // No 7, mid 0x10
    nBufPos += 1;
    pInfo->stat.hvSetVolt2 = packet->data[nBufPos]; // No 8, mid 0x11
    nBufPos += 1;

    pInfo->stat.autoDischargeStepCount = packet->data[nBufPos]; // No 9, mid 0x12
    nBufPos += 1;
    pInfo->stat.autoDischargeVoltStep1 = packet->data[nBufPos]; // No 10, mid 0x13
    nBufPos += 1;
    pInfo->stat.autoDischargeVoltStep2 = packet->data[nBufPos]; // No 11, mid 0x14
    nBufPos += 1;
    pInfo->stat.autoDischargeVoltStep3 = packet->data[nBufPos]; // No 12, mid 0x15
    nBufPos += 1;
    pInfo->stat.autoDischargeKeepTimeStep1 = packet->data[nBufPos]; // No 13, mid 0x16
    nBufPos += 1;
    pInfo->stat.autoDischargeKeepTimeStep2 = packet->data[nBufPos]; // No 14, mid 0x17
    nBufPos += 1;
    pInfo->stat.autoDischargeKeepTimeStep3 = packet->data[nBufPos]; // No 15, mid 0x18
    nBufPos += 1;

    pInfo->stat.rampDownTime = packet->data[nBufPos]; // No 16, mid 0x19
    nBufPos += 1;

    pInfo->measure.ocpAlarmStatus = packet->data[nBufPos]; // No 17, mid 0x2A
    nBufPos += 1;
    pInfo->stat.ocpAlarmControl = packet->data[nBufPos]; // No 18, mid 0x2B
    nBufPos += 1;
    pInfo->stat.ocpAlarmEvent = packet->data[nBufPos]; // No 19, mid 0x2C
    nBufPos += 1;
    pInfo->stat.ocpAlarmLimit = packet->data[nBufPos]; // No 20, mid 0x2E
    nBufPos += 1;
    pInfo->stat.ocpAlarmCount = packet->data[nBufPos]; // No 21, mid 0x2F
    nBufPos += 1;

    pInfo->measure.arcAlarmStatus = packet->data[nBufPos]; // No 22, mid 0x30
    nBufPos += 1;
    pInfo->stat.arcAlarmControl = packet->data[nBufPos]; // No 23, mid 0x31
    nBufPos += 1;
    pInfo->stat.arcAlarmEvent = packet->data[nBufPos]; // No 24, mid 0x32
    nBufPos += 1;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->stat.arcAlarmLimit = data16; //packet->data[nBufPos]<<8 + packet->data[nBufPos+1]; // No 25, mid 0x34
    nBufPos += 2;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->stat.arcAlarmCount = data16; // No 26, mid 0x35
    nBufPos += 2;

    pInfo->measure.battStatusVolt = packet->data[nBufPos]; // No 27, mid 0x37
    nBufPos += 1;
    pInfo->measure.battPercent = packet->data[nBufPos]; // No 38, mid 0x38, Battery Percent
    nBufPos += 1;
    pInfo->measure.battCurr = packet->data[nBufPos]; // No 29, mid 0x39
    nBufPos += 1;
    pInfo->measure.battLowAlarmStatus = packet->data[nBufPos]; // No 30, mid 0x3A
    nBufPos += 1;

    pInfo->stat.battAlarmControl = packet->data[nBufPos]; // No 31, mid 0x3B
    nBufPos += 1;
    pInfo->stat.battAlarmLimit = packet->data[nBufPos]; // No 32, mid 0x3C
    nBufPos += 1;
    pInfo->stat.battEventControl = packet->data[nBufPos]; // No 33, mid 0x3D
    nBufPos += 1;


    pInfo->measure.battChargeConnectStatus = packet->data[nBufPos]; // No 34, mid 0x3E
    nBufPos += 1;

    pInfo->stat.userMode = packet->data[nBufPos]; // No 35, mid 0x40
    nBufPos += 1;
    pInfo->stat.waveMode = packet->data[nBufPos]; // No 36, mid 0x41
    nBufPos += 1;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->measure.temp1 = data16; // No 37, mid 0x42
    nBufPos += 2;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->measure.temp2 = data16; // No 38, mid 0x43
    nBufPos += 2;
	
	dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->measure.press = data16; // No 39, Pressure
    nBufPos += 2;


    pInfo->stat.autoQueryControl = packet->data[nBufPos]; // No 40, mid 0x48
    nBufPos += 1;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->stat.autQueryIntervalTime = data16; // No 41, mid 0x49
    nBufPos += 2;

    pInfo->stat.hvAutoToggle = packet->data[nBufPos]; // No 42, mid 0x4A
    nBufPos += 1;

}

// Use On Server
void ResponsEscWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo)
{
    int nBufPos = 0;
    U08 dataH, dataL;
    U16 data16;

    pInfo->volatile_stat.hvOutputStatus = packet->data[nBufPos]; // No 1,
    nBufPos += 1;

    pInfo->measure.voltA = ( packet->data[nBufPos] ); //2
    nBufPos += 1;

    pInfo->measure.currA = packet->data[nBufPos]; //3
    nBufPos += 1;

    pInfo->measure.voltB = (packet->data[nBufPos]); //4
    nBufPos += 1;

    pInfo->measure.currB = packet->data[nBufPos]; //5
    nBufPos += 1;


    pInfo->measure.battPercent = packet->data[nBufPos]; // 6
    nBufPos += 1;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->measure.temp1 = data16; // 7
    nBufPos += 2;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->measure.temp2 = data16; // 8
    nBufPos += 2;

    pInfo->measure.battStatusVolt = packet->data[nBufPos]; // 9
    nBufPos += 1;
}



// Use On Server
void EventReportEscWriteToInfo(StructApolloPacket* packet, StructEscInfo *pInfo)
{
    int nBufPos = 0;
    U08 dataH, dataL;
    U16 data16;

    pInfo->volatile_stat.hvOutputStatus = packet->data[nBufPos]; // No 1,
    nBufPos += 1;

    pInfo->measure.voltA = ( packet->data[nBufPos] ); //2
    nBufPos += 1;

    pInfo->measure.currA = packet->data[nBufPos]; //3
    nBufPos += 1;

    pInfo->measure.voltB = (packet->data[nBufPos]); //4
    nBufPos += 1;

    pInfo->measure.currB = packet->data[nBufPos]; //5
    nBufPos += 1;


    pInfo->measure.ocpAlarmStatus = packet->data[nBufPos]; // 6
    nBufPos += 1;

    pInfo->stat.ocpAlarmCount = packet->data[nBufPos]; // 7
    nBufPos += 1;

    pInfo->measure.arcAlarmStatus = packet->data[nBufPos]; // 8
    nBufPos += 1;

    dataH = packet->data[nBufPos];
    dataL = packet->data[nBufPos+1];
    data16 = dataH<<8 | dataL;
    pInfo->stat.arcAlarmCount = data16; // 9, 10
    nBufPos += 2;

    pInfo->measure.battLowAlarmStatus = packet->data[nBufPos]; // 11
    nBufPos += 1;

    pInfo->measure.battPercent =  packet->data[nBufPos]; // 12
    nBufPos += 1;
}


void ResetStat(StructEscInfo * pInfo)
{
	int i;
	
	for(i=0;i<N_HV_VOLT_OFFSET;i++) {
		pInfo->hvOffset[i] = 0;
	}
	
	StructNetInfo server = { 	.ip = {192, 168, 0, 23},
								.sm = {255,255,255,0},
								.gw = {192, 168, 0, 1},
								.port = 3123,
								.mac = {0x4b, 0xcb, 0x7b, 0xf7, 0xbb, 0x25},
							};
	StructNetInfo le = { 	.ip = {192, 168, 0, 120},
								.sm = {255,255,255,0},
								.gw = {192, 168, 0, 1},
								.port = 20000,
								.mac = {0x4b, 0xcb, 0x7b, 0xf7, 0xbb, 0x25},
	};
	
	memset((void*)pInfo, 0x00, sizeof(StructEscInfo));
							
	//memcpy((char*)(&pInfo->setting.server), (char*)&server, sizeof(StructNetInfo));
	pInfo->setting.nPort = server.port;
	memcpy((char*)(&pInfo->setting.leQlight), (char*)&le, sizeof(StructNetInfo));
							
	pInfo->setting.debugModeOnOff = 1; // debug mode off=0, on=1
	
	pInfo->setting.protocol = 1; // custom protocol = 1
	//const char strSerialSpeed[][7]	= {"1200", "2400", "4800", "9600","19200", "38400", "57600", "115200"};
	pInfo->setting.serialSpeed = 6;//57600

	pInfo->setting.cid = 1;
	pInfo->setting.sid = 1;
	pInfo->setting.pid = 1;
							
	memset(&pInfo->setting.aid,0,sizeof(pInfo->setting.aid));
	
	pInfo->setting.lanLora = COMM_MODE_LORA; // serial =0,  lora = 1, lan = 2
	
	
	pInfo->infoCheck = EEPROM_CHECKSUM; // to check EEPROM
	pInfo->infoCheckB = EEPROM_CHECKSUMB; // to check EEPROM
	
//		strcpy((char*)pInfo->stat.id, "ESC Controller SOA 2.5K 0\nV2.0.016");
//		strcpy((char*)pInfo->stat.ver, "2019.9 ");
	
	//pInfo->setting.serialNumber = 25001;
	strcpy((char*)pInfo->setting.serialNumber, "WL300CHG25001");
	pInfo->setting.fwVer = 100; // Fixed value
	pInfo->setting.hwVer = 100; // Fixed value	
	
	pInfo->setting.temp1Tune = 0;
	pInfo->setting.temp2Tune = 0;
	
	pInfo->volatile_stat.hvOutputStatus = 0;
	
	pInfo->volatile_stat.toggleCount = 0;
	
/// ######### Default values......	
	
	pInfo->stat.rampDownTime = 3; // Min = 3, *100 mSec
	pInfo->stat.rampUpTime = 3;	

	pInfo->stat.hvSetVolt1 = 10;
	pInfo->stat.hvSetVolt2 = 10 | 0x80;
	
	pInfo->stat.autoDischargeStepCount = 0;
	pInfo->stat.autoDischargeVoltStep1 = 9; //*100 V
	pInfo->stat.autoDischargeVoltStep2 = 9;
	pInfo->stat.autoDischargeVoltStep3 = 9;	
	pInfo->stat.autoDischargeKeepTimeStep1 = 0; // Sec
	pInfo->stat.autoDischargeKeepTimeStep2 = 0;
	pInfo->stat.autoDischargeKeepTimeStep3 = 0;
	
	pInfo->stat.ocpAlarmEvent = 0;
	pInfo->stat.ocpAlarmCount = 0;
	pInfo->measure.ocpAlarmStatus = 0;
    pInfo->stat.ocpAlarmControl = 0;
	/// #############  전류 제한이 걸리면 출력이 안나감.
	pInfo->stat.ocpAlarmLimit = 30;  //*10 [uA], default 30uA
	
	pInfo->stat.arcAlarmEvent = 0;
	pInfo->stat.arcAlarmCount = 0;
	pInfo->measure.arcAlarmStatus = 0;
	pInfo->stat.arcAlarmControl = 0;
	pInfo->stat.arcAlarmLimit = 250;
	
	
	pInfo->stat.battAlarmControl = 1;
	pInfo->stat.battEventControl = 0;
	pInfo->stat.battAlarmLimit = 2; // %, 아폴로요청 15 -> 2
	
	pInfo->measure.battLowAlarmStatus = 0;	
	pInfo->measure.battPercent = 100;
	
	pInfo->stat.userMode = 0;
	pInfo->stat.waveMode = 1;	
	
	pInfo->stat.autoQueryControl = 0;
	pInfo->stat.autQueryIntervalTime = 5; //Sec

	pInfo->measure.temp1 = 0;
	pInfo->measure.temp2 = 25;
	
	pInfo->stat.hvAutoToggle = AUTO_TOGGLE2;
}

S08 CompareInfo(void* a, void*b, int n)
{
	S08 ret = 0;
	int i;	
	
	for(i=0;i<n;i++) {
		if( *((U08*)a+i) != *((U08*)b+i) ) {
			ret = -1;
			break;
		}
	}
	return ret;
}

U08 IsToggled(StructEscInfo * pInfo)
{
	U08 ret = 0;
	
	if(pInfo->stat.hvSetVolt1==0) {
		if((pInfo->stat.hvSetVolt2 & 0x80)==0) {
			ret = 1;
		}		
	}
	else if((pInfo->stat.hvSetVolt1 & 0x80)!=0) {
		ret = 1;
	}
	
	return ret;
}
