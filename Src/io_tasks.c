/**
  ******************************************************************************
  * @file    io_tasks.c
  * @author  jw Lee
  * @version 0.0.0.1
  * @date    09.09.2019
  * @brief   Tasks for IO
  ******************************************************************************
  * @contact
  * <h2><center>&copy; COPYRIGHT 2019 jw@mizilab.com </center></h2>
  ******************************************************************************
  */
  
#include "io_tasks.h"
#include "esc_info.h"

#include "dac7678.h"
#include "lorawan.h"
#include "ad779x.h"

#include <math.h>


#define dbg_ioTask debug_console

#define IsHvOn	IS_HV_ON	

enum {
	LanStatReady = 0,
	LanStatSocket = 1,
	LanStatConnected = 2
};

//// ################################# variables
// Send all info Auto...
uint8_t g_sendAlive = 0;
uint8_t g_autoInfoTime = 20;
uint8_t g_ackAll = 0;

U08 plcFromWhere;

U08 g_APMode = 0;

/**
adc 0: Batt volt
adc 1: Batt curr

adc 2: Hv A volt
adc 3: Hv A curr
adc 4: Hv B Volt
adc 5: Hv B Curr

adc 6: Hv V Set
adc 7: Charge curr
*/
volatile uint32_t g_adc_result[N_ADC_CHANNELS];

//U08 serverip[4] = {192,168,0,55};
const U08 BattChargePercent[] =
{
1,
4 ,
6 ,
8 ,
10 ,
12 ,
13 ,
15 ,
17 ,
19 ,
21 ,
23 ,
25 ,
27 ,
29 ,
31 ,
33 ,
35 ,
37 ,
38 ,
40 ,
42 ,
44 ,
46 ,
48 ,
50 ,
52 ,
54 ,
56 ,
58 ,
60 ,
62 ,
63 ,
65 ,
67 ,
69 ,
71 ,
73 ,
75 ,
77 ,
79 ,
81 ,
83 ,
85 ,
87 ,
88 ,
90 ,
92 ,
94 ,
96 ,
98 ,
100 	
};
const float BattChargeVolt[] =
{
9.60,
10.10,
10.50,
10.70,
10.80,
10.90,
11.00,
11.05,
11.10,
11.15,
11.20,
11.30,
11.40,
11.42,
11.44,
11.46,
11.48,
11.49,
11.50,
11.53,
11.56,
11.59,
11.60,
11.64,
11.67,
11.70,
11.74,
11.77,
11.80,
11.83,
11.86,
11.88,
11.90,
11.92,
11.94,
11.96,
11.98,
12.00,
12.02,
12.04,
12.06,
12.08,
12.09,
12.10,
12.11,
12.12,
12.13,
12.14,
12.15,
12.16,
12.17,
12.20
};


const U08 BattDischargePerent[] =
{
2,
3,
4,
5,
6,
6,
7,
8,
9,
10,
10,
11,
12,
13,
13,
14,
15,
16,
17,
17,
18,
19,
20,
21,
21,
22,
23,
24,
25,
25,
26,
27,
28,
29,
29,
30,
31,
32,
33,
33,
34,
35,
36,
37,
37,
38,
39,
40,
40,
41,
42,
43,
44,
44,
45,
46,
47,
48,
48,
49,
50,
51,
52,
52,
53,
54,
55,
56,
56,
57,
58,
59,
60,
60,
61,
62,
63,
63,
64,
65,
66,
67,
67,
68,
69,
70,
71,
71,
72,
73,
74,
75,
75,
76,
77,
78,
79,
79,
80,
81,
82,
83,
83,
84,
85,
86,
87,
87,
88,
89,
90,
90,
91,
92,
93,
94,
94,
95,
96,
97,
98,
98,
99,
100	
};

const float BattDischargeVolt[] =
{
7.70,
8.10,
8.40,
8.60,
8.70,
8.80,
8.90,
9.00,
9.10,
9.20,
9.30,
9.35,
9.40,
9.50,
9.55,
9.60,
9.70,
9.75,
9.80,
9.85,
9.90,
9.92,
9.94,
9.96,
9.98,
10.00,
10.03,
10.06,
10.08,
10.10,
10.14,
10.17,
10.20,
10.22,
10.24,
10.26,
10.28,
10.30,
10.32,
10.34,
10.36,
10.38,
10.40,
10.42,
10.44,
10.46,
10.48,
10.50,
10.51,
10.52,
10.53,
10.54,
10.56,
10.58,
10.60,
10.61,
10.62,
10.64,
10.66,
10.68,
10.70,
10.71,
10.73,
10.74,
10.76,
10.78,
10.80,
10.83,
10.86,
10.88,
10.90,
10.92,
10.94,
10.96,
10.98,
11.00,
11.04,
11.06,
11.08,
11.10,
11.14,
11.17,
11.20,
11.24,
11.26,
11.28,
11.30,
11.31,
11.32,
11.34,
11.36,
11.38,
11.40,
11.42,
11.44,
11.49,
11.48,
11.50,
11.52,
11.54,
11.56,
11.58,
11.60,
11.62,
11.64,
11.67,
11.70,
11.72,
11.74,
11.76,
11.78,
11.80,
11.82,
11.84,
11.86,
11.88,
11.90,
11.92,
11.94,
11.96,
11.98,
12.00,
12.10,
12.20
};

const char strHvState[][30] = {
	"HV_STATE_READY",
	"HV_STATE_ON_START",
	"HV_STATE_RAMPUP",
	"HV_STATE_RAMPUP_DONE",
	"HV_STATE_OFF_START",
	"HV_STATE_STEPDOWN1",
	"HV_STATE_STEPUP1",
	"HV_STATE_KEEP_1",
	"HV_STATE_STEPDOWN2",
	"HV_STATE_STEPUP2",
	"HV_STATE_KEEP_2",
	"HV_STATE_STEPDOWN3",
	"HV_STATE_STEPUP3",
	"HV_STATE_KEEP_3",
	"HV_STATE_RAMPDOWN_START",
	"HV_STATE_RAMPDOWN_DONE",	
};
U08 hvOutState = 0;
uint32_t g_readyTick=0;

float g_press = 0;	
uint8_t g_pressError = 0;


static U08 hvAutoToggle2Done = 0;
static U08 hvAutoToggle1Done = 0;

static int nRampCount=0, nRampCountMax=0;
static int targetVoltA=0, targetVoltB=0;
static int targetVoltAPrev=0, targetVoltBPrev=0;
static int dacCurr;

static U08 bRampDone=1;
static U08 bRampChangeDone=1;
static U08 bRampToggleDone=1;
static U08 bRampToggleRelayChange=0;
static uint32_t rampStartHalTick=0;
static uint32_t prevHalTick=0;

#define N_MAX_HV_VOLT_TABLE	30
const static uint16_t tHvVoltTable[N_MAX_HV_VOLT_TABLE+1] =
{	
	0,
//	100V 200V 300V 400V 500V
97 ,
200 ,
303 ,
406 ,
509 ,
612 ,
715 ,
819 ,
922 ,
1025 ,
1128 ,
1231 ,
1334 ,
1437 ,
1540 ,
1644 ,
1747 ,
1850 ,
1953 ,
2056 ,
2160 ,
2263 ,
2366 ,
2471 ,
2574 ,
2677 ,
2781 ,
2884 ,
2987 ,
3090 

	/*
	113, 226, 341, 454, 567, 
	682, 795, 909, 1023, 1137, 
	1250, 1365, 1478, 1591, 1706, 
	1819, 1932, 2047, 2160, 2274, 
	2388, 2502, 2615, 2730, 2843, 
	2956, 3071, 3184, 3297, 3462
	*/
};
const static uint16_t tHvVoltTableB[N_MAX_HV_VOLT_TABLE+1] =
{	
	0,
105 ,
210 ,
313 ,
416 ,
519 ,
620 ,
723 ,
826 ,
929 ,
1032 ,
1135 ,
1238 ,
1343 ,
1444 ,
1547 ,
1652 ,
1753 ,
1856 ,
1961 ,
2064 ,
2167 ,
2270 ,
2373 ,
2477 ,
2579 ,
2683 ,
2786 ,
2889 ,
2992 ,
3095 	
//	100V 200V 300V 400V 500V
};

static uint8_t *g_HvVoltTableOffset;
/*
static uint8_t g_HvVoltTableOffset[N_MAX_HV_VOLT+1] =
{
	0,
};
*/

#define N_MAX_MONITOR_TX_LENGTH		24+1600 //512
char g_txMonitorMsg[N_MAX_MONITOR_TX_LENGTH];

int	g_txMonitorLength;
char g_monitorReady;

net_mon_p_t g_resMonitorData;
uint8_t resData[32+N_MAX_MONITOR_TX_LENGTH] = {1, 0, 2, 0, 0x54, 0xfa, 0x6f, 0, 0x25
	, 0, 0, 0, 0, 0, 0x72, 0, 0, 0, 0, 0
	, 1, 1, 1, 1, 8, 0x15, 0x77, 0, 0, 0,0x3f, 0x6a};
//,'T', 'e', 's', 't', 0x00, 0x00};
static uint8_t rxMonitorMsg[N_MAX_MONITOR_TX_LENGTH];

static uint8_t lanRxBuf[N_MAX_MONITOR_TX_LENGTH];

U08 g_dnFWMode = 0;
U08 netDlMode;

U08 prevToggleMode =0;

U08 prevV1=0, prevV2=0;
U08 prevToggle =0;
uint8_t g_hvOutOnPrev=0;
uint32_t g_hvOutChangeWait = 0;

uint8_t hv_pwr_control=0;
uint8_t nCursorPosX=0, nCursorPosXPrev=0, nCursorPosY=0,nCursorPosYPrev=0, bCursorOn=0, bCursorOnPrev=0;
uint8_t bCursorBlink=0;
StructApolloPacket g_InfoPacket;

U08 updateCLcdNow = 0;

uint8_t cursor_x;
uint8_t cursor_y;

int16_t chA_V  = 2500;
int16_t chA_uA = 200;
int16_t chB_V  = -1000;
int16_t chB_uA = 300;


uint8_t g_LoraConnected = 0;
uint8_t bFirstCommPath = 0;

uint8_t g_checkTaskStart = 0;
//static 	struct IOMessage cmdMessage;
uint8_t g_ConnectedServer = 0; // When server is connected, do not change LAN/LORA MODE

static uint8_t buzCount=0;
static uint8_t clcdState = CLCD_STATE_BOOT; // state of char LCD.
static U08 waitForHvPwr = 0;

static int prevBattV;
extern wiz_NetInfo *g_pWizNetInfo;
#define CLCD_STR_LEN	21
static char lcdLineBuf0[CLCD_STR_LEN] = "CH1:OUTPUT OFF123456";
static char lcdLineBuf1[CLCD_STR_LEN] = "CH1:OUTPUT OFF123456";
static char lcdLineBuf0Prv[CLCD_STR_LEN] = "CH1:OUTPUT OFF123456";
static char lcdLineBuf1Prv[CLCD_STR_LEN] = "CH1:OUTPUT OFF123456";

uint8_t qlightCmd[] = {0x57, 0x00, 0x64, 0x64, 0x64, 0x64, 0x64, 0x64, 0x00, 0x00};

uint8_t g_lanBuf[N_PLC_TX_BUFFER_SIZE];
uint8_t g_lanBufCount;
uint8_t g_lanInit = 0;
uint8_t g_lanCableConnected = 1;

static unsigned long conv[2]; 
static float temperature[2];
tAD779X_Device pTEMP1;
tAD779X_Device pTEMP2;

uint8_t g_apBoardMode=0;
uint8_t g_apBoardDet=0;
uint8_t g_apAlarm=0;
uint8_t g_apAlarmReset=0;
uint8_t g_apHvOut = 0;
int g_volt=0;
static    struct IOMessage rxCmdMessage;

static	struct OutputMessage outputMsg;
static uint8_t keyProcIng = 0;
int g_measure_V1;
int g_measure_V2;
int g_measure_C1;
int g_measure_C2;
static U08 bBatLow = 0;

int g_min[4];
int g_max[4];

U08 g_testBattLow = 0;
U08 battLowCount = 0;
uint16_t battFullCount = 0;

extern I2C_HandleTypeDef hi2c2;

//// ################################# extern functions
extern  void TakeUplink(void);
extern void GiveUplink(void);

extern ADC_HandleTypeDef hadc1;

extern Node_Info nodeInfo;

extern void ad7793CS2Control(unsigned char State);
extern void ad7793CS1Control(unsigned char State);
extern unsigned char ad7793RDYState(void);
extern void ad7793TxByte(unsigned char Data);
extern unsigned char ad7793RxByte(void);

extern void TakeFSMC(void);
extern void GiveFSMC(void);

extern void TakeLan();
extern void GiveLan();


extern SRAM_HandleTypeDef hsram1;
extern osMessageQueueId_t lanTxQueueHandle;
extern osMessageQueueId_t queueMonitorTxHandle;

extern osThreadId_t myTaskLoraHandle;
extern osThreadId_t myTaskLanHandle;
extern osThreadId_t myTaskLanRxHandle;


void DefaultMode(StructEscInfo * pInfo)
{
	ResetModeWave();

	dbg_ioTask(1, "Reset to Default values...\r\n");
	dbg_ioTask(1, "A %d B %d Ramp Up %dmSec Down %dmSec AutoDischarge Step %d\r\n", 
		ByteToVolt(pInfo->stat.hvSetVolt1), ByteToVolt(pInfo->stat.hvSetVolt2),
		pInfo->stat.rampUpTime*100, pInfo->stat.rampDownTime*100,
		pInfo->stat.autoDischargeStepCount);
	dbg_ioTask(1, "Step1 volt %d Step1 time %d Step2 volt %d Step2 time %d Step3 volt %d Step3 time %d\r\n",
		ByteToVolt(pInfo->stat.autoDischargeVoltStep1) ,pInfo->stat.autoDischargeKeepTimeStep1,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep2,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep3
	);	}

void LoadUserMode(StructEscInfo * pInfo)
{
	U08 nLoad = pInfo->stat.userMode;
	
	if(nLoad==0) {
		DefaultMode(pInfo);
	}
	else {	
		memcpy( &pInfo->stat.hvSetVolt1, &pInfo->userMode[(nLoad-1)%N_MAX_USER_MODE_SIZE] ,sizeof(StructUerMode));
	}
	
	dbg_ioTask(1, "User mode loaded from %d \r\n", nLoad);
	dbg_ioTask(1, "A %d B %d Ramp Up %dmSec Down %dmSec AutoDischarge Step %d\r\n", 
		ByteToVolt(pInfo->stat.hvSetVolt1), ByteToVolt(pInfo->stat.hvSetVolt2),
		pInfo->stat.rampUpTime*100, pInfo->stat.rampDownTime*100,
		pInfo->stat.autoDischargeStepCount);
	dbg_ioTask(1, "Step1 volt %d Step1 time %d Step2 volt %d Step2 time %d Step3 volt %d Step3 time %d\r\n",
		ByteToVolt(pInfo->stat.autoDischargeVoltStep1) ,pInfo->stat.autoDischargeKeepTimeStep1,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep2,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep3
	);	
}
void SaveUserMode(StructEscInfo * pInfo)
{
	U08 nLoad = pInfo->stat.userMode;
	
	if(nLoad==0) {
		dbg_ioTask(1, "%s Error! can not save to slot 0\r\n", __FUNCTION__);
		return;
	}

	memcpy(&pInfo->userMode[(nLoad-1)%N_MAX_USER_MODE_SIZE], &pInfo->stat.hvSetVolt1 ,sizeof(StructUerMode));
	
	dbg_ioTask(1, "User mode saved to %d \r\n", nLoad);
	dbg_ioTask(1, "A %d B %d Ramp Up %dmSec Down %dmSec AutoDischarge Step %d\r\n", 
		ByteToVolt(pInfo->stat.hvSetVolt1), ByteToVolt(pInfo->stat.hvSetVolt2),
		pInfo->stat.rampUpTime*100, pInfo->stat.rampDownTime*100,
		pInfo->stat.autoDischargeStepCount);
	dbg_ioTask(1, "Step1 volt %d Step1 time %d Step2 volt %d Step2 time %d Step3 volt %d Step3 time %d\r\n",
		ByteToVolt(pInfo->stat.autoDischargeVoltStep1) ,pInfo->stat.autoDischargeKeepTimeStep1,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep2,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep3
	);	
	SaveEscInfo();
}
void LoadWaveMode(StructEscInfo * pInfo)
{
	U08 nLoad = pInfo->stat.waveMode;
	U08 volt = 0;
	const U08 autoDischargeVolt[3] = {9, 9, 9};
	const U08 autoDischargeTime[3] = {0,0, 0};

	if(nLoad>=1 && nLoad<=16) {
		volt = nLoad + 15 -1;
		pInfo->stat.hvSetVolt1 = volt;
		pInfo->stat.hvSetVolt2 = volt | 0x80;		
		
		pInfo->stat.rampDownTime = 3; // 1 sec -> 300mSec, Request by Apollo
		pInfo->stat.rampUpTime = 3;
		
		pInfo->stat.autoDischargeStepCount = 0;
	}
	else if(nLoad>=17 && nLoad<=32) {
		volt = nLoad -2;
		pInfo->stat.hvSetVolt1 = volt;
		pInfo->stat.hvSetVolt2 = volt | 0x80;		
		
		pInfo->stat.rampDownTime = 3;// 1 sec -> 300mSec, Request by Apollo
		pInfo->stat.rampUpTime = 3;

		pInfo->stat.autoDischargeStepCount = 3;
		pInfo->stat.autoDischargeVoltStep1 = autoDischargeVolt[0];
		pInfo->stat.autoDischargeVoltStep2 = autoDischargeVolt[1];
		pInfo->stat.autoDischargeVoltStep3 = autoDischargeVolt[2];
		pInfo->stat.autoDischargeKeepTimeStep1 = autoDischargeTime[0];
		pInfo->stat.autoDischargeKeepTimeStep2 = autoDischargeTime[1];
		pInfo->stat.autoDischargeKeepTimeStep3= autoDischargeTime[2];
	}
	else {
		dbg_ioTask(1, "Out of range! %d \r\n", nLoad);
	}
	
	dbg_ioTask(1, "Wave mode Set to %d \r\n", nLoad);
	dbg_ioTask(1, "A %d B %d Ramp Up %dmSec Down %dmSec AutoDischarge Step %d\r\n", 
		ByteToVolt(pInfo->stat.hvSetVolt1), ByteToVolt(pInfo->stat.hvSetVolt2),
		pInfo->stat.rampUpTime*100, pInfo->stat.rampDownTime*100,
		pInfo->stat.autoDischargeStepCount);
	dbg_ioTask(1, "Step1 volt %d Step1 time %d Step2 volt %d Step2 time %d Step3 volt %d Step3 time %d\r\n",
		ByteToVolt(pInfo->stat.autoDischargeVoltStep1) ,pInfo->stat.autoDischargeKeepTimeStep1,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep2,
		ByteToVolt(pInfo->stat.autoDischargeVoltStep2) ,pInfo->stat.autoDischargeKeepTimeStep3
	);
}

U08 SetHvVolt(U08 ch, U08 value) 
{
	char ret = 0;
	
	if((value&0x7f) > N_MAX_HV_VOLT ) {
		value = N_MAX_HV_VOLT | (value&0x80);
		dbg_ioTask(1, "%s: Over volt range! %d %d, change B\r\n", __FUNCTION__, ch, value);
	}
		
	
	if(ch==1) {
		GetEscInfo()->stat.hvSetVolt1 = value;
		
		// Volt A/B both positive
		if((GetEscInfo()->stat.hvSetVolt1&0x80)!=0x80 && (GetEscInfo()->stat.hvSetVolt2&0x80)!=0x80) {
			GetEscInfo()->stat.hvSetVolt2 |= 0x80;
			dbg_ioTask(1, "%s: A/B Both positive! %d %d, change B\r\n", __FUNCTION__, ch, value);
			ret = 1;
		}
		// Volt A/B both negative
		else if((GetEscInfo()->stat.hvSetVolt1&0x80)==0x80 && (GetEscInfo()->stat.hvSetVolt2&0x80)==0x80) {
			GetEscInfo()->stat.hvSetVolt2 &= ~0x80;
			dbg_ioTask(1, "%s: A/B Both negative! %d %d, change B\r\n", __FUNCTION__, ch, value);
			ret = 1;
		}
	}
	else if(ch==2) {
		GetEscInfo()->stat.hvSetVolt2 = value;
		
		// Volt A/B both positive
		if((GetEscInfo()->stat.hvSetVolt1&0x80)!=0x80 && (GetEscInfo()->stat.hvSetVolt2&0x80)!=0x80) {
			GetEscInfo()->stat.hvSetVolt1 |= 0x80;
			dbg_ioTask(1, "%s: A/B Both positive! %d %d, change A\r\n", __FUNCTION__, ch, value);
			ret = 1;
		}
		// Volt A/B both negative
		else if((GetEscInfo()->stat.hvSetVolt1&0x80)==0x80 && (GetEscInfo()->stat.hvSetVolt2&0x80)==0x80) {
			GetEscInfo()->stat.hvSetVolt1 &= ~0x80;
			dbg_ioTask(1, "%s: A/B Both negative! %d %d, change A\r\n", __FUNCTION__, ch, value);
			ret = 1;
		}		
	}
	else {
		dbg_ioTask(1, "%s: Out of channel! %d\r\n", __FUNCTION__, ch);
	}
	
	return ret;
}

void Reboot()
{
	NVIC_SystemReset();
}

void W5300Select(void)
{
	HAL_GPIO_WritePin(LAN_CS_GPIO_Port, LAN_CS_Pin, GPIO_PIN_RESET);
}

void W5300DeSelect(void)
{
	HAL_GPIO_WritePin(LAN_CS_GPIO_Port, LAN_CS_Pin, GPIO_PIN_SET);
}

void W5300WriteData(uint32_t addr, iodata_t data)
{
	HAL_SRAM_Write_16b(&hsram1, (uint32_t *)addr, &data, 1);
}

iodata_t W5300ReadData(uint32_t addr)
{
	iodata_t readBuf;
	HAL_SRAM_Read_16b(&hsram1, (uint32_t *)addr, &readBuf, 1);
	return readBuf;
}

void W5300HardwareReset(void)
{
#if TEST_W5300_OFF
	HAL_GPIO_WritePin(LAN_RESET_GPIO_Port, LAN_RESET_Pin, GPIO_PIN_RESET);
	osDelay(10);
#else
	HAL_GPIO_WritePin(LAN_RESET_GPIO_Port, LAN_RESET_Pin, GPIO_PIN_RESET);
	osDelay(1);
	HAL_GPIO_WritePin(LAN_RESET_GPIO_Port, LAN_RESET_Pin, GPIO_PIN_SET);
	osDelay(10);
#endif
}



int Net_Conf(wiz_NetInfo *wiznet_info)
{
	wiz_NetInfo getInfo;
    ctlnetwork(CN_SET_NETINFO, (void*) wiznet_info);
	ctlnetwork(CN_GET_NETINFO, (void*) &getInfo);
	
	int i;
	int ret = 0;
	for(i=0;i<sizeof(getInfo);i++) {
		if( (*((char*)&getInfo+i)) != (*((char*)wiznet_info+i)) ) {
			ret = 1;
			break;
		}
	}
    //display_Net_Info();
	return ret;
}

void clcdDelay(uint8_t delay)
{
	//osDelay(delay);
	volatile uint8_t i, j;
	
	for(i=0;i<delay;i++)
	{
		for(j=0;j<40;j++) {
			__NOP();
		}
	}
	
}

uint8_t clcdReadBusy()
{
	uint8_t ret=0;
	
	clcdDelay(1);	return ret;
	
    LCD_RS(0); 
	//LCD_RW(1);
	LCD_WR(1);
	LCD_RD(1);	
    	
    
	
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	
	GPIO_InitStruct.Pin = cLCD_DB7_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(cLCD_DB7_GPIO_Port, &GPIO_InitStruct);		
	
 //   LCD_EN(1);
	LCD_WR(1);
	LCD_RD(0);	
    clcdDelay(1);
//    LCD_EN(0);
	LCD_WR(1);
	LCD_RD(1);	
	
//	HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_RESET);

	ret = HAL_GPIO_ReadPin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin)<<7;
	
	GPIO_InitStruct.Pin = cLCD_DB7_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(cLCD_DB7_GPIO_Port, &GPIO_InitStruct);	
	
//	LCD_RW(0);
	LCD_WR(1);
	LCD_RD(1);	

	return ret;
}

void LCD_WAIT()	
{
	//while(clcdReadBusy() & 0x80);
	
	clcdDelay(10);
	uint8_t flag = clcdReadBusy();	
	while(flag & 0x80)	{
		clcdDelay(10);
		flag = clcdReadBusy();
	} 
	
}


void clcdSend4bitHigh(uint8_t cmd)
{
	if( cmd & 0x80) {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_RESET);
	}
	
	if( cmd & 0x40) {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x20) {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x10) {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_RESET);
	}	
}
void clcdSend4bitLow(uint8_t cmd)
{
	if( cmd & 0x08) {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_RESET);
	}
	
	if( cmd & 0x04) {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x02) {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x01) {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_RESET);
	}	
}


void clcdWriteCmdHigh(uint8_t cmd)
{
   //HAL_GPIO_WritePin(nLCD_CS_GPIO_Port, nLCD_CS_Pin, GPIO_PIN_RESET);
   //clcdDelay(1);
    //LcdDataBus = (cmd & 0xF0);     //Send higher nibble
	clcdSend4bitHigh(cmd);
	
	LCD_RS(0);   // Send LOW pulse on RS pin for selecting Command register

    LCD_EN(0);
	
	
	LCD_RW(0);	
    //LCD_RW = 0;   // Send LOW pulse on RW pin for Write operation
	
    LCD_EN(1);   // Generate a High-to-low pulse on EN pin	
    clcdDelay(1);
    LCD_EN(0);
	
	LCD_WAIT();

}
void clcdWriteCmdLow(uint8_t cmd)
{
   //HAL_GPIO_WritePin(nLCD_CS_GPIO_Port, nLCD_CS_Pin, GPIO_PIN_RESET);
   //clcdDelay(1);
    //LcdDataBus = (cmd & 0xF0);     //Send higher nibble
	clcdSend4bitLow(cmd);
	
	LCD_RS(0);   // Send LOW pulse on RS pin for selecting Command register

    LCD_EN(0);
	
	
	LCD_RW(0);	
    //LCD_RW = 0;   // Send LOW pulse on RW pin for Write operation
	
    LCD_EN(1);   // Generate a High-to-low pulse on EN pin	
    clcdDelay(1);
    LCD_EN(0);
	
	LCD_WAIT();
}



void clcdSend8bit(uint8_t cmd)
{
	if( cmd & 0x80) {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB7_GPIO_Port, cLCD_DB7_Pin, GPIO_PIN_RESET);
	}
	
	if( cmd & 0x40) {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB6_GPIO_Port, cLCD_DB6_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x20) {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB5_GPIO_Port, cLCD_DB5_Pin, GPIO_PIN_RESET);
	}
	if( cmd & 0x10) {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB4_GPIO_Port, cLCD_DB4_Pin, GPIO_PIN_RESET);
	}	
	
	if( cmd & 0x08) {
		HAL_GPIO_WritePin(cLCD_DB3_GPIO_Port, cLCD_DB3_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB3_GPIO_Port, cLCD_DB3_Pin, GPIO_PIN_RESET);
	}	
	if( cmd & 0x04) {
		HAL_GPIO_WritePin(cLCD_DB2_GPIO_Port, cLCD_DB2_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB2_GPIO_Port, cLCD_DB2_Pin, GPIO_PIN_RESET);
	}	
	if( cmd & 0x02) {
		HAL_GPIO_WritePin(cLCD_DB1_GPIO_Port, cLCD_DB1_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB1_GPIO_Port, cLCD_DB1_Pin, GPIO_PIN_RESET);
	}	
	if( cmd & 0x01) {
		HAL_GPIO_WritePin(cLCD_DB0_GPIO_Port, cLCD_DB0_Pin, GPIO_PIN_SET);
	}
	else {
		HAL_GPIO_WritePin(cLCD_DB0_GPIO_Port, cLCD_DB0_Pin, GPIO_PIN_RESET);
	}	
}
void clcdWrite(uint8_t lcdData, uint8_t bIsChar)
{

#if USE_CLCD_4BIT_DB
	//LCD_RS(1);   // Send LOW pulse on RS pin for selecting Command register
	LCD_RS(bIsChar);   // Send LOW pulse on RS pin for selecting Command register

    LCD_EN(0);
	
    //LcdDataBus = (cmd & 0xF0);     //Send higher nibble
	clcdSend4bitHigh(lcdData);
	
	
	LCD_RW(0);	
    //LCD_RW = 0;   // Send LOW pulse on RW pin for Write operation
	
    LCD_EN(1);   // Generate a High-to-low pulse on EN pin	
    LCD_WAIT();clcdDelay(1);
    LCD_EN(0);
	
	LCD_WAIT();
    //clcdDelay(1);

    //LcdDataBus = ((cmd<<4) & 0xF0); //Send Lower nibble
	clcdSend4bitLow(lcdData);
	
	
    LCD_EN(1);   // Generate a High-to-low pulse on EN pin	
    LCD_WAIT();clcdDelay(1);
    LCD_EN(0);


	LCD_WAIT();
	LCD_RW(1);	
#else
	
	LCD_RS(bIsChar);   // Send LOW pulse on RS pin for selecting Command register
	
	clcdSend8bit(lcdData);
	LCD_EN(0);	

	LCD_RW(0);	
    
	
	LCD_EN(1);	
    clcdDelay(1);
    LCD_EN(0);	

	LCD_WAIT();
	LCD_RW(1);	
#endif		
}


void clcdWriteCmd(uint8_t cmd)
{
   clcdWrite(cmd, 0);
}

void clcdWriteChar(uint8_t c)
{
  clcdWrite(c, 1);
}

void clcdSetCursor(uint8_t line, uint8_t pos)
{
   //1st line 00H ~ 13H
   //2nd line 40H ~ 53H

   uint8_t buffer;

   if (line != 0 && line != 1) return;
   if (pos > 19) return;
   
   buffer = line ? 0xC0 : 0x80;
   buffer |= pos;

	clcdWriteCmd(buffer);

   
   //HAL_GPIO_WritePin(nLCD_CS_GPIO_Port, nLCD_CS_Pin, GPIO_PIN_SET);   
}	

void clcdShiftLeft()
{
	clcdWriteCmd(0x18);
}

void clcdShiftRight()
{
	clcdWriteCmd(0x1C);
}

void clcdClear()
{
	clcdWriteCmd(0x01);	//clear
	clcdWriteCmd(0x02); //goto home
}

void clcdCursor(uint8_t enable)
{
	if (enable) clcdWriteCmd(CURSOR_SET);
	else clcdWriteCmd(0x0C);
//	if (enable) clcdWriteCmd(0x10);
//	else clcdWriteCmd(0x00);
}


void clcdInit()
{
//	waitForHvPwr = 10;// Do not refresh after init for a while...
    LCD_OE(0);	
	LCD_DIR(0);	
	
    LCD_EN(1);	
	LCD_RW(1);
	LCD_RST(1);
		
	LCD_RST(0);
	osDelay(10);
	LCD_RST(1);
	
	
	//return; // No init sequence
		
	//LCD_RST(0);	osDelay(1);	LCD_RST(1);
	//osDelay(100);
	
#if USE_CLCD_4BIT_DB==0		
    	
	clcdWriteCmd(0x38);
	clcdWriteCmd(0x0C);
	clcdWriteCmd(0x01);
	//osDelay(2);
	clcdWriteCmd(0x06);

	clcdWriteCmd(0x80);
#else
	/*
	1. display clear	-> 0x01
	2. Set address to 00H, DD-RAM	-> 0x8X, X=ADD
	3. Display onoff control	-> 0000 1 D C B, D:all, C:Cursor, B:Blink
	4. Entry mode set -> 0000 0 1 I/D S
	5. Function Set 	-> 0 0 1 I/F N X BR1 BR0, I/F: 4/8bit, N:lines, BR:brightless
	
	*/
	//clcdCursor(0);
//	clcdWriteCmdHigh(0x00);	clcdWriteCmdHigh(0x10);	osDelay(2); // Clear CMD need 1.52mSec delay
	
	//clcdWriteCmdHigh(0x20);
//	clcdWriteCmdHigh(0x20);	clcdWriteCmdHigh(0x80);
	//osDelay(1);
	
	clcdWriteCmdHigh(0x00);
	clcdWriteCmdHigh(0xE0);
	//osDelay(1);
	
//	clcdWriteCmdHigh(0x00);	clcdWriteCmdHigh(0x10);	osDelay(2); // Clear CMD need 1.52mSec delay
	clcdWriteCmdHigh(0x00);
	clcdWriteCmdHigh(0x10);
	osDelay(2); // Clear CMD need 1.52mSec delay

	clcdWriteCmdHigh(0x00);
	clcdWriteCmdHigh(0x60);
	
#endif
	//clcdClear();	clcdCursor(0);
}

uint8_t g_Beep;
uint8_t g_BeepDelay;
uint8_t g_BeepCount;
void BeepN(U08 n) 
{
	g_BeepDelay = n;
	g_BeepCount = n*2;

	g_Beep = BUZ_DELAY*n;
	if(n>2) {
		g_Beep = BUZ_DELAY*n*4;
		g_BeepDelay = n;
	}
}

U08 g_testArc = 0;
U08 g_testOcp = 0;
U08 g_testBat = 0;
int prevVolt1[N_MAX_EDGE_COUNT] = {0};
int prevVolt2[N_MAX_EDGE_COUNT] = {0};
int prevCurr1[N_MAX_EDGE_COUNT] = {0};
int prevCurr2[N_MAX_EDGE_COUNT] = {0};
U08 edgeCount=0;

U08 g_lastAlarm = 0;

U08 bRiging[4];

static U08 bArcEventSent = 0;
static U08 bOcpEventSent = 0;
static U08 bBatEventSent = 0;

static uint32_t prevTickArc = 0;
static uint32_t prevTickOcp = 0;
static U08 prevOcpLed = 0;

void InitRising(void)
{
	int i, j;
	
	for(j=0;j<4;j++) {
		bRiging[j] = 1;
		int *arr=prevVolt1;
		switch(j) {
			case 0:
				arr = prevVolt1;					
				break;
			case 1:
				arr = prevVolt2;					
				break;
			case 2:
				arr = prevCurr1;					
				break;
			case 3:
				arr = prevCurr2;
				break;
		}
		
		for(i=0;i<N_MAX_EDGE_COUNT;i++) {
			arr[i] = 0;				
		}
	}		
	bArcEventSent = 0;
	bBatEventSent = 0;
	bOcpEventSent = 0;
}

	static U08 bOcpPrev = 0;

U08 UpdateAlarmLED(StructEscInfo *pInfo, U08 ledOnly)
{
	U08 ret = 0;
	static U08 alarmSet = 0;
	uint32_t tickNow = HAL_GetTick();
		
	U08 i, j;
	if(IS_HV_ON) {
		if(prevVolt1[edgeCount] != N_ARC_MAX) {
			prevVolt1[edgeCount] = g_measure_V1;
		}
		if(prevVolt2[edgeCount] != N_ARC_MAX) {
			prevVolt2[edgeCount] = g_measure_V2;
		}
		if(prevCurr1[edgeCount] != N_ARC_MAX) {
			prevCurr1[edgeCount] = g_measure_C1;		
		}
		if(prevCurr2[edgeCount] != N_ARC_MAX) {
			prevCurr2[edgeCount] = g_measure_C2;
		}
		edgeCount++;
		if(edgeCount>=N_MAX_EDGE_COUNT) {
			edgeCount = 0;
		}
		
		for(j=0;j<4;j++) {
			bRiging[j] = 1;
			int *arr=prevVolt1;
			switch(j) {
				case 0:
					arr = prevVolt1;					
					break;
				case 1:
					arr = prevVolt2;					
					break;
				case 2:
					arr = prevCurr1;					
					break;
				case 3:
					arr = prevCurr2;
					break;
			}
			for(i=0;i<N_MAX_EDGE_COUNT;i++) {
				if(arr[(edgeCount+i + N_MAX_EDGE_COUNT -1)%N_MAX_EDGE_COUNT] == N_ARC_MAX) {
					bRiging[j] = 0;
					break;
				}
				if( arr[(edgeCount+i + N_MAX_EDGE_COUNT -1)%N_MAX_EDGE_COUNT] + N_EDGE_JITTER< arr[(edgeCount+i -1 + N_MAX_EDGE_COUNT -1)%N_MAX_EDGE_COUNT] ) 
				{
					bRiging[j] = 0;
					break;
				}
			}
		}
	}
	else {
		InitRising();
			
	}
	
	if( abs((int)(tickNow-g_readyTick)) <300 ) {		
		if(IS_HV_ON) {
			//dbg_ioTask(1, "%s Skip 300mSec\r\n", __FUNCTION__);
			//return 0;
		}
	}
	
	static U08 bArcPrev1 = 0;
	static U08 bArcPrev2 = 0;
	static U08 bBattPrev = 0;

	if(ledOnly==0) {
		
		if(g_testOcp) {
			pInfo->measure.currA = pInfo->stat.ocpAlarmLimit+1;
			dbg_ioTask(1, "TEST OCP\r\n");
		}
		if(g_testArc) {
			pInfo->measure.currA = 77+1;
			dbg_ioTask(1, "TEST ARC\r\n");
			//g_testArc = 0;
		}
		
		// Check OCP Alarm
		
		if(hvOutState == HV_STATE_READY && IS_HV_ON==0) {
		//if(IS_HV_ON==0) {
			bOcpPrev = 0;
		}
	#if ALARM_CONTROL_HV_ONLY
		if(hvOutState == HV_STATE_READY && IS_HV_ON) {
	#else	
		if(pInfo->stat.ocpAlarmControl && IS_HV_ON) {
	#endif
			if(pInfo->stat.ocpAlarmLimit < pInfo->measure.currA
				|| pInfo->stat.ocpAlarmLimit < pInfo->measure.currB) 
			{
				if(bOcpPrev==0) {
					pInfo->measure.ocpAlarmStatus = 1;
					if(tickNow - prevTickOcp > 3) {
						prevTickOcp = tickNow;
						pInfo->stat.ocpAlarmCount++;
						dbg_ioTask(1, "OCP Limit over! %d %d %d\r\n", pInfo->stat.ocpAlarmLimit, pInfo->measure.currA, pInfo->measure.currB);
					}
					else {
						dbg_ioTask(1, "OCP Over, not in Time %d %d %d %d\r\n", pInfo->stat.ocpAlarmLimit, pInfo->measure.currA, pInfo->measure.currB, tickNow - prevTickOcp);
					}
		
	#if ALARM_CONTROL_HV_ONLY
					if(pInfo->stat.ocpAlarmControl) {
						ret |= ALM_OCP;
					}
	#else				
					ret |= ALM_OCP;
	#endif
					
					if(pInfo->measure.currA > pInfo->measure.currB) {
						pInfo->measure.lastOcp = pInfo->measure.currA;
						g_lastAlarm = ALM_OCP;
					}
					else {
						pInfo->measure.lastOcp = pInfo->measure.currB;
						g_lastAlarm = ALM_OCP;
					}
					
					
					
					bOcpPrev = 1;
				}
			}
		}		
		if(g_testOcp) {
			g_testOcp = 0;
			pInfo->measure.ocpAlarmStatus = 1;
			ret |= ALM_OCP;

			
			if(pInfo->measure.currA > pInfo->measure.currB) {
				pInfo->measure.lastOcp = pInfo->measure.currA;
				g_lastAlarm = ALM_OCP;
			}
			else {
				pInfo->measure.lastOcp = pInfo->measure.currB;
				g_lastAlarm = ALM_OCP;
			}
			
			pInfo->stat.ocpAlarmCount++;
			
			if(pInfo->stat.ocpAlarmEvent) {				
				RespEventReport();
			}		
		}	
		
		// Check ARC Alarm
		if(!IS_HV_ON || hvOutState != HV_STATE_READY) {
			prevTickArc = tickNow;
		}
		
	#if ALARM_CONTROL_HV_ONLY
		else if(hvOutState == HV_STATE_READY && IS_HV_ON) {
	#else	
		if(pInfo->stat.arcAlarmControl) {
	#endif
			//if(ARC_CONDITION_CURR < pInfo->measure.currA || ARC_CONDITION_CURR < pInfo->measure.currB) 
			//(g_measure_V1*N_MAX_HV_VOLT/VOLT_HV_ADC_MAX); 	
			if(bArcPrev1) {			
				if(g_measure_V1 < LIMIT_ARC-N_EDGE_JITTER) {
					bArcPrev1 = 0;
					dbg_ioTask(3, "Clear riging ARC %d\r\n", g_measure_V1);
					
					for(i=0;i<N_MAX_EDGE_COUNT;i++) {
						prevVolt1[i] = g_measure_V1;
					}
				}
			}
			if(bArcPrev2) {			
				if(g_measure_V2 < LIMIT_ARC-N_EDGE_JITTER) {
					bArcPrev2 = 0;
					dbg_ioTask(3, "Clear riging  ARC %d\r\n", g_measure_V2);
					
					for(i=0;i<N_MAX_EDGE_COUNT;i++) {
						prevVolt2[i] = g_measure_V2;
					}
				}
			}		
			
			if( g_measure_V1 > LIMIT_ARC || g_measure_V2 > LIMIT_ARC)
			{
				U08 bArc = 0;
				//bRiging[0] = 1;
				//bRiging[1] = 1;
				//dbg_ioTask(3, "%s %d %d\r\n", __FUNCTION__, bRiging[0], bRiging[1]);
				if(bRiging[0] && g_measure_V1 > LIMIT_ARC) {					
					if(prevVolt1[0] != N_ARC_MAX) {					
						bArcPrev1 = 1;
						bArc |= bArcPrev1;
					}				
					for(i=0;i<N_MAX_EDGE_COUNT;i++) {
						prevVolt1[i] = N_ARC_MAX; //prevVolt1[i] + N_EDGE_JITTER+1;
					}
				}
				if(bRiging[1] && g_measure_V2 > LIMIT_ARC) {					
					if(prevVolt1[0] < N_ARC_MAX) {					
						bArcPrev2 = 1;
						bArc |= (bArcPrev2<<1);
					}
					for(i=0;i<N_MAX_EDGE_COUNT;i++) {
						prevVolt2[i] = N_ARC_MAX; //prevVolt2[i] + N_EDGE_JITTER+1;
					}
				}
				
				
				if(bArc) {					
					if(g_testArc) {
						g_testArc = 0;
					}
					if(tickNow - prevTickArc > N_MIN_ALARAM_DURATION) {
						prevTickArc = tickNow;
						pInfo->stat.arcAlarmCount++;				
						dbg_ioTask(1, "ARC Occurred %x %d/%d I=%d %d\r\n", bArc, pInfo->stat.arcAlarmCount, pInfo->stat.arcAlarmLimit, pInfo->measure.currA, pInfo->measure.currB);
					}
					
					if(pInfo->stat.arcAlarmCount == pInfo->stat.arcAlarmLimit) {
						dbg_ioTask(1, "ARC Limit Over %d/%d I=%d %d\r\n", pInfo->stat.arcAlarmCount, pInfo->stat.arcAlarmLimit, pInfo->measure.currA, pInfo->measure.currB);
						pInfo->measure.arcAlarmStatus = 1;
						pInfo->stat.arcAlarmCount++;
						
						g_lastAlarm = ALM_ARC;
						
	#if ALARM_CONTROL_HV_ONLY					
					if(pInfo->stat.arcAlarmControl) {					
						ret |= ALM_ARC;					
					}
	#else
					ret |= ALM_ARC;					
	#endif
						
					pInfo->measure.lastArc = pInfo->stat.arcAlarmCount;
					
					}
					

				}
				else {
					
				}
			}
			else if(abs(g_max[0]-g_min[0]) > LIMIT_ARC_MINMAX || abs(g_max[1]-g_min[1]) > LIMIT_ARC_MINMAX || g_testArc) {
				U08 bArc = 1;
				bArcPrev1 = 1;
				bArcPrev2 = 1;
				bArc |= bArcPrev1;
				for(i=0;i<N_MAX_EDGE_COUNT;i++) {
					prevVolt1[i] = N_ARC_MAX; //prevVolt2[i] + N_EDGE_JITTER+1;
					prevVolt2[i] = N_ARC_MAX; //prevVolt2[i] + N_EDGE_JITTER+1;
				}
				if(g_testArc) {
					g_testArc = 0;
				}
				
				if(tickNow - prevTickArc > N_MIN_ALARAM_DURATION) {
					prevTickArc = tickNow;
					pInfo->stat.arcAlarmCount++;				
					dbg_ioTask(1, "ARC MinMAX Occurred %x %d/%d I=%d %d %d %d %d %d\r\n"
						, bArc, pInfo->stat.arcAlarmCount, pInfo->stat.arcAlarmLimit, pInfo->measure.currA, pInfo->measure.currB
						,g_min[0], g_max[0],g_min[1], g_max[1]);

					// Clear Min/Max values...
					g_min[0] = ADC_V_REF; g_max[0] = 0;
					g_min[1] = ADC_V_REF; g_max[1] = 0;
					g_min[2] = ADC_V_REF; g_max[2] = 0;
					g_min[3] = ADC_V_REF; g_max[3] = 0;				
				}
				// ARC Alarm only ONCE at limit==count
				if(pInfo->stat.arcAlarmCount == pInfo->stat.arcAlarmLimit) {
					dbg_ioTask(1, "ARC Limit Over %d/%d I=%d %d\r\n", pInfo->stat.arcAlarmCount, pInfo->stat.arcAlarmLimit, pInfo->measure.currA, pInfo->measure.currB);
					pInfo->measure.arcAlarmStatus = 1;
					pInfo->stat.arcAlarmCount++;
					
					
					g_lastAlarm = ALM_ARC;
					
#if ALARM_CONTROL_HV_ONLY					
				if(pInfo->stat.arcAlarmControl) {					
					ret |= ALM_ARC;					
				}
#else
				ret |= ALM_ARC;					
#endif
					
				pInfo->measure.lastArc = pInfo->stat.arcAlarmCount;
				
				}
			}
		}
	}
	
	
	// Update LED
#if ALARM_CONTROL_HV_ONLY
	static U08 prevArcLed = 0;
	if(prevArcLed != pInfo->measure.arcAlarmStatus) {
#else	
	if(pInfo->stat.arcAlarmControl) {
#endif
		if(pInfo->measure.arcAlarmStatus) {
			if(pInfo->stat.arcAlarmEvent) {	
				if(bArcEventSent==0) {
					bArcEventSent = 1;
					RespEventReport();
				}
			}
			HAL_GPIO_WritePin(nLED_ALM_ARC_GPIO_Port, nLED_ALM_ARC_Pin, GPIO_PIN_RESET);
			EventQlight(ret);
			dbg_ioTask(3, "ARC LED ON\r\n");
		}
		else {
			HAL_GPIO_WritePin(nLED_ALM_ARC_GPIO_Port, nLED_ALM_ARC_Pin, GPIO_PIN_SET);
			EventQlight(ret);
			dbg_ioTask(3, "ARC LED OFF\r\n");
		}
		prevArcLed = pInfo->measure.arcAlarmStatus;
//		LeTxProc(pInfo);
	}
	else {
		//pInfo->measure.arcAlarmStatus = 0;
		//HAL_GPIO_WritePin(nLED_ALM_ARC_GPIO_Port, nLED_ALM_ARC_Pin, GPIO_PIN_SET);
	}
	
#if ALARM_CONTROL_HV_ONLY
	
	if(prevOcpLed != pInfo->measure.ocpAlarmStatus) {
		prevOcpLed = pInfo->measure.ocpAlarmStatus;
#else
	if(pInfo->stat.ocpAlarmControl) {
#endif
		if(pInfo->measure.ocpAlarmStatus) {
			if(pInfo->stat.ocpAlarmEvent) {
				if(bOcpEventSent==0) {
					bOcpEventSent = 1;							
					RespEventReport();
				}
			}
			HAL_GPIO_WritePin(nLED_ALM_OCP_GPIO_Port, nLED_ALM_OCP_Pin, GPIO_PIN_RESET);
			EventQlight(ret);
			dbg_ioTask(4, "OCP LED ON\r\n");
		}	
		else {
			HAL_GPIO_WritePin(nLED_ALM_OCP_GPIO_Port, nLED_ALM_OCP_Pin, GPIO_PIN_SET);
			EventQlight(ret);
			dbg_ioTask(4, "OCP LED OFF\r\n");
		}
		
//		LeTxProc(pInfo);
	}
	else {
		//pInfo->measure.ocpAlarmStatus = 0;
		//HAL_GPIO_WritePin(nLED_ALM_OCP_GPIO_Port, nLED_ALM_OCP_Pin, GPIO_PIN_SET);
	}

	if(DET_BATTERY) {
	#if ALARM_CONTROL_HV_ONLY
		static U08 prevBattLed = 0;
		//if(prevBattLed != pInfo->measure.battLowAlarmStatus) {
		if(hvOutState == HV_STATE_READY) {
	#else	
		if(pInfo->stat.battAlarmControl) {
	#endif
			static U08 oldBatLowStatus = 0;
			if(pInfo->measure.battLowAlarmStatus) {
				if(bBattPrev==0) {
					bBattPrev = 1;		
					
					HAL_GPIO_WritePin(nLED_ALM_BAT_GPIO_Port, nLED_ALM_BAT_Pin, GPIO_PIN_RESET);
					prevBattLed = pInfo->measure.battLowAlarmStatus;
					g_lastAlarm = ALM_BATT;
					pInfo->measure.lastBattPercent = pInfo->measure.battPercent;
					
		#if ALARM_CONTROL_HV_ONLY==0
					if(pInfo->stat.battAlarmControl) {
						ret |= ALM_BATT;
					}
		#else
					ret |= ALM_BATT;
		#endif
					BattLowLight(bBattPrev); //pInfo->measure.battLowAlarmStatus);					
					//EventQlight(ret);
					if(pInfo->stat.battEventControl) {				
						if(oldBatLowStatus != pInfo->measure.battLowAlarmStatus) {
							dbg_ioTask(2, "Batt low state %d %dV %d%%\r\n",pInfo->measure.battLowAlarmStatus, pInfo->measure.battStatusVolt, pInfo->measure.battPercent);
							RespEventReport();
							
						}
					}		
				}
			}
			else {
				if(bBattPrev) {
					HAL_GPIO_WritePin(nLED_ALM_BAT_GPIO_Port, nLED_ALM_BAT_Pin, GPIO_PIN_SET);
					bBattPrev = 0;
					BattLowLight(bBattPrev);
					//EventQlight(ret);
					prevBattLed = pInfo->measure.battLowAlarmStatus;
					if(pInfo->stat.battEventControl) {				
						if(oldBatLowStatus != pInfo->measure.battLowAlarmStatus) {
							dbg_ioTask(2, "Batt low state %d %dV %d%%\r\n",pInfo->measure.battLowAlarmStatus, pInfo->measure.battStatusVolt, pInfo->measure.battPercent);
							RespEventReport();
							
						}
					}	
				}
			}
			oldBatLowStatus = pInfo->measure.battLowAlarmStatus;
			
		}
	}
	else {
		pInfo->measure.battLowAlarmStatus = 0;
		HAL_GPIO_WritePin(nLED_ALM_BAT_GPIO_Port, nLED_ALM_BAT_Pin, GPIO_PIN_SET);
	}
	
	if(ret != 0) {		
		dbg_ioTask(2, "ALARM OCCUR! %x\r\n", ret);
		alarmSet = 1;
	}
	else {
		alarmSet = 0;
		if(hvOutState==HV_STATE_ALARM) {
			hvOutState =  HV_STATE_READY;
		}
	}
	
	if(ret==0) {
		//g_lastAlarm = ALM_NONE;
	}
	
	return ret | (alarmSet<<7);
}

void BattLowLight(U08 onoff) 
{
	static U08 prevOnoffBattLowLED=0;
	
	struct IOMessage cmdLeMessage;
	
	if(prevOnoffBattLowLED==onoff) {
		return;
	}
	prevOnoffBattLowLED=onoff;
	dbg_ioTask(1, "Batt low Alarm!\r\n");
	
	cmdLeMessage.commandID = CMD_QLIGHT_TEST;
	if(onoff) {
		cmdLeMessage.argument = QLIGHT_ORANGE_ON;
	}
	else {
		cmdLeMessage.argument = QLIGHT_ORANGE_OFF;
	}
	osMessageQueuePut(lanTxQueueHandle, &cmdLeMessage, NULL, NULL);	
	dbg_ioTask(1, "Alarm %s!\r\n", __FUNCTION__);
}
static U08 prevAlarmType = 0;
void EventQlight(U08 alarmType)
{
	struct IOMessage cmdLeMessage;
	U08 light = QLIGHT_RED_ON;
	
	
	if(prevAlarmType != alarmType) {
		prevAlarmType = alarmType;
		if(alarmType==0) {
			light = QLIGHT_RED_OFF;
		}
	}
//	pInfo->measure.battLowAlarmStatus?QLIGHT_ORANGE_ON:QLIGHT_ORANGE_OFF
	cmdLeMessage.commandID = CMD_QLIGHT;
	cmdLeMessage.argument = light;
	osMessageQueuePut(lanTxQueueHandle, &cmdLeMessage, NULL, NULL);		
	dbg_ioTask(2, "Alarm %s!\r\n", __FUNCTION__);
}
void ChgQlight(U08 onoff)
{
	static U08 prevOnoff=0;
	
	struct IOMessage cmdLeMessage;
	
	if(prevOnoff==onoff) {
		return;
	}
	prevOnoff=onoff;
	dbg_ioTask(3, "Batt Charge Complete Alarm! %d\r\n", onoff);
	
//	pInfo->measure.battLowAlarmStatus?QLIGHT_ORANGE_ON:QLIGHT_ORANGE_OFF
	cmdLeMessage.commandID = CMD_QLIGHT_TEST;
	if(onoff) {
		cmdLeMessage.argument = QLIGHT_GREEN_ON;
	}
	else {
		cmdLeMessage.argument = QLIGHT_GREEN_OFF;
	}
	osMessageQueuePut(lanTxQueueHandle, &cmdLeMessage, NULL, NULL);		
	dbg_ioTask(1, "Alarm %s!\r\n", __FUNCTION__);
}

void HvQlight(U08 onoff)
{
	struct IOMessage cmdLeMessage;
	
//	pInfo->measure.battLowAlarmStatus?QLIGHT_ORANGE_ON:QLIGHT_ORANGE_OFF
	cmdLeMessage.commandID = CMD_QLIGHT_TEST;
	if(onoff) {
		cmdLeMessage.argument = QLIGHT_BLUE_ON;
	}
	else {
		//cmdLeMessage.argument = QLIGHT_ALL_OFF;// QLIGHT_BLUE_OFF;
		cmdLeMessage.argument = QLIGHT_BLUE_OFF;
	}
	osMessageQueuePut(lanTxQueueHandle, &cmdLeMessage, NULL, NULL);	
	dbg_ioTask(4, "%s onoff? %d\r\n", __FUNCTION__, onoff);	
}

U08 ReadKey() {
	U08 key = 0; // clear var		
	key |= HAL_GPIO_ReadPin(nSW_UP_GPIO_Port, nSW_UP_Pin)<<KEY_SW_UP_BIT;
	key |= HAL_GPIO_ReadPin(nSW_DOWN_GPIO_Port, nSW_DOWN_Pin)<<KEY_SW_DOWN_BIT;
	key |= HAL_GPIO_ReadPin(nSW_LEFT_GPIO_Port, nSW_LEFT_Pin)<<KEY_SW_LEFT_BIT;
	key |= HAL_GPIO_ReadPin(nSW_RIGHT_GPIO_Port, nSW_RIGHT_Pin)<<KEY_SW_RIGHT_BIT;
	
	key |= HAL_GPIO_ReadPin(nSW_HV_PWR_GPIO_Port, nSW_HV_PWR_Pin)<<KEY_SW_HV_PWR_BIT;
	key |= HAL_GPIO_ReadPin(nSW_TOGGLE_GPIO_Port, nSW_TOGGLE_Pin)<<KEY_SW_TOGGLE_BIT;
	
	key |= HAL_GPIO_ReadPin(nSW_SELECT_GPIO_Port, nSW_SELECT_Pin)<<KEY_SW_SELECT_BIT;
	key |= HAL_GPIO_ReadPin(nSW_CANCEL_GPIO_Port, nSW_CANCEL_Pin)<<KEY_SW_ESC_CANCEL_BIT;
	
	key = key ^ 0xff; // revert key bits	

	return key;
}

void flashEnvRead(U08 *buf, U32 size)
{
    U32 endAddr;
    U32 rest;
    U32 *pData = (U32 *)(void *)buf;
    U32 Address = 0x00;

    rest = size % 4;
    endAddr = ENV_FLASH_START_ADDR + size + rest;
    Address = ENV_FLASH_START_ADDR;

    while (Address < endAddr)
    {
        *pData = *(U32 *)Address;
        Address += 4;
        pData++;
    }

}
#define FLASH_COMPLETE	1

typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;
u8 flashEnvSave(u8 *buf, u32 size)
{
    u32      Address = 0x00;
    volatile U08 FLASHStatus;
    volatile TestStatus MemoryProgramStatus;
	
	static FLASH_EraseInitTypeDef EraseInitStruct;
	EraseInitStruct.TypeErase = FLASH_TYPEERASE_SECTORS;
//	EraseInitStruct.pa

    FLASHStatus = FLASH_COMPLETE;
    u32 endAddr;
    u32 rest;
    u32 *pData;

    MemoryProgramStatus = PASSED;

    rest = size % 4;
    endAddr = ENV_FLASH_START_ADDR + size + rest;

    if (endAddr > ENV_FLASH_END_ADDR)
    {
        printf("Flash Env size is too big...\r\n");
        return(0);
    }

    /* Unlock the Flash Program Erase controller */
    //FLASH_Unlock();
	HAL_FLASH_Unlock();

    /* Clear All pending flags */
    //FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGSERR );
	/*
    if (FLASH_EraseSector(ENV_FLASH_SECTOR, VoltageRange_3) != FLASH_COMPLETE)	
    {
        while (1)
        {
        }
    }*/
	FLASH_Erase_Sector(FLASH_SECTOR_11, VOLTAGE_RANGE_3);

    //printf("Erase done...\r\n");
    Address = ENV_FLASH_START_ADDR;
    pData = (u32 *)(void *)buf;


    while ((Address < endAddr) && (FLASHStatus == FLASH_COMPLETE))
    {
		HAL_FLASH_Program(TYPEPROGRAM_WORD, Address, *pData++);
        //FLASHStatus = FLASH_ProgramWord(Address, *pData++);
        Address = Address + 4;
    }


    /* Check the corectness of written data */
    Address = ENV_FLASH_START_ADDR;
    pData = (u32 *)(void *)buf;

    while ((Address < endAddr) && (MemoryProgramStatus != FAILED))
    {
        if ((*(U32 *)Address) != *pData)
        {
            MemoryProgramStatus = FAILED;

            #if 0
            printf("Address  : %08X \r\n", (unsigned int)Address);
            printf("*Address : %08X \r\n", (unsigned int)*(U32 *)Address);
            printf("*pData   : %08X \r\n", (unsigned int)*pData);
            #endif
        }

        Address += 4;
        pData++;
    }

    if (MemoryProgramStatus == FAILED)
    {
        printf("Error Env save...\r\n");
    }

   
    //FLASH_Lock();
	HAL_FLASH_Lock();

    return(MemoryProgramStatus);
} /* end of flashEnvSave */

StructEscInfo infoPrev;
void TaskInput(void *argument)
{
#if KEY_EXTI==0		
	uint8_t key;
	uint8_t keyPrv  = 0;
	uint8_t keyCnt;
	uint32_t keyPrevTick = 0;
	uint32_t keyNowTick = 0;
#endif	
//	uint8_t apChk = 0;
//	uint8_t apChkPrv = 0xff;
//	uint16_t apChkCnt = 0;	
//	int volt = 0;
	//uint8_t apAlarm = 0;
	//uint8_t apAlarmReset = 0;
	
	uint16_t tick = 0;
	uint8_t buzCount = 0;

	
	StructEscInfo *pInfo;
//	uint16_t txValue;  
	
//	int i;
	
    uint16_t cntDelay = 0;
	uint16_t delay = 100;
	
	pInfo = GetEscInfo();
	
	g_HvVoltTableOffset = pInfo->hvOffset;

	while( (g_checkTaskStart|CHECK_TASK_INPUT) != CHECK_TASK_STARTED) {
		osDelay(100);
		dbg_ioTask(9, "Wait until other task up %x\r\n", g_checkTaskStart);
	}

	//	dbg_ioTask(1, "size of g_pWizNetInfo %d size of info %d\r\n", sizeof(wiz_NetInfo), sizeof(StructEscInfo));
	g_checkTaskStart |= CHECK_TASK_INPUT;
	
	memcpy((void*)&infoPrev, pInfo, sizeof(infoPrev));

	extern int g_eepromLoadRet;
	dbg_ioTask(0, "EEPROM %s IP %d.%d.%d.%d debug level=%d\r\n>", g_eepromLoadRet==0?"OK":"Error, reset to initial values...", 
				GetNetInfo()->ip[0], GetNetInfo()->ip[1], GetNetInfo()->ip[2], GetNetInfo()->ip[3],
				pInfo->setting.debugModeOnOff);
	clcdState = CLCD_STATE_INITIAL; // Enter to init menu after EEPROM check
	
	//const U08 dummy[] = "Test dummy data for boot loader 123456789";
	//dbg_ioTask(1, "%s\r\n", dummy);
	uint8_t pressStat = MPL3115A_Init();	
	if(pressStat!=0) {
		dbg_ioTask(1, "Error Init Pressure Stat = %x\r\n", pressStat);
		g_pressError = 1;
	}

	
#if USE_DBOS	
	U08	envFlashBuf[ENV_MAX_SIZE];
	
	flashEnvRead(envFlashBuf, ENV_MAX_SIZE);	
	//extern const char env_flash[ENV_MAX_SIZE];
	//memcpy(envFlashBuf, env_flash, ENV_MAX_SIZE);
	env_item_t * envItem = (env_item_t *)(void *)envFlashBuf;
	
	U08 bCompare = 0;
	bCompare |= CompareInfo( (void*)envItem->etherSet.ip, (void*)pInfo->setting.netInfo.ip, 4);
	bCompare |= CompareInfo( (void*)envItem->etherSet.gw, (void*)pInfo->setting.netInfo.gw, 4);
	bCompare |= CompareInfo( (void*)envItem->etherSet.sm, (void*)pInfo->setting.netInfo.sn, 4);
	bCompare |= CompareInfo( (void*)envItem->etherSet.mac, (void*)pInfo->setting.netInfo.mac, 6);
	if( bCompare !=0) {
		dbg_ioTask(1, "Net is diff with boot loader\r\n");
		memcpy(envItem->etherSet.ip, pInfo->setting.netInfo.ip, 4);
		memcpy(envItem->etherSet.gw, pInfo->setting.netInfo.gw, 4);
		memcpy(envItem->etherSet.sm, pInfo->setting.netInfo.sn, 4);
		memcpy(envItem->etherSet.mac, pInfo->setting.netInfo.mac, 6);
		
		flashEnvSave(envFlashBuf, ENV_MAX_SIZE);
		flashEnvRead(envFlashBuf, ENV_MAX_SIZE);	
		dbg_ioTask(1, "FLASH IP %d.%d.%d.%d\r\n", envItem->etherSet.ip[0], envItem->etherSet.ip[1], envItem->etherSet.ip[2], envItem->etherSet.ip[3]);
	}
#endif
	
	dbg_ioTask(10, "size of info %d\r\n", N_VALID_INFO_SIZE);
#if 0	
	int i;
	int dac;
	for(i=0;i<=30;i++) {
		dac = SetDacVolt(0, i*100);
		//printf("%d\r\n", i*100);
		printf("%d\r\n", dac);
	}
#endif

	if(!BUZZER_MUTE) {
		// boot up Beep
		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
		osDelay(10);
		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
		osDelay(30);
		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
		osDelay(10);
		HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
	}
  /* Infinite loop */
	for(;;)
	{
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
		
		tick++;
		osDelay(INPUT_TASK_DELAY);
		
#if 1
		if(tick%100==0) {   // check every 1000 mSec
			//dbg_ioTask(1, "CHECK Setting changed? %d %x %x\r\n", tick, key, keyPrv);
				
			if(CompareInfo(pInfo, &infoPrev, N_VALID_INFO_SIZE) != 0) {
				dbg_ioTask(1, "Setting has been changed!\r\n");
				SaveEscInfo();
				BaudRateChange(pInfo->setting.serialSpeed);
				memcpy((void*)&infoPrev, pInfo, sizeof(infoPrev));
			}			
		}
		if(tick%500==0) {
			g_press = MPL3115A_Read_Press();
			//if(g_press != 0) 
			{
				pInfo->measure.press = g_press;
			}
			//MPL3115A_Read_Temp();
			//dbg_ioTask(3, "PRESS %f\r\n", g_press);
		}
#endif
		
#if KEY_EXTI==0		
		// Read key values...		
		// read each pins..	
		key = ReadKey(); // clear var		
		
		
		// FIXED key error: 키 오동작 방지, 노이즈 혹은 동시 입력시 이전값 초기화.
		if(keyCnt>50) {
			keyCnt = 0;
			keyPrv = 0;
		}
		if(key != keyPrv) {		
			keyNowTick = HAL_GetTick();
			keyCnt++;
			//if(keyCnt>12) { // key pressed!, over 100ms				
			if( (HAL_GetTick()-keyPrevTick) > INPUT_KEY_TIMEOUT) {
				keyCnt = 0;
				keyPrv = key;
				keyCnt = 0;							

				// send key cmd msg
#if REPEAT_KEY==0
				outputMsg.commandID = OUTPUT_KEY;
				outputMsg.value = key;
				osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);	
#else
				outputMsg.commandID = OUTPUT_KEY;
				outputMsg.value = key;
				osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);	
				
				if(key == keyPrv && key!=0x00) {					
					while(key == keyPrv) {
						key = ReadKey();
						osDelay(10);
						keyCnt++;
						if(keyCnt>100) {
							outputMsg.commandID = OUTPUT_KEY_PRESSING;
							break;
						}
						else {
							outputMsg.commandID = OUTPUT_KEY;
						}
						//dbg_ioTask(5, "%x", key);
					}
					
					if(outputMsg.commandID == OUTPUT_KEY_PRESSING) {
						keyCnt = 0;						
						while(key == keyPrv) {
							key = ReadKey();
							osDelay(100-BUZ_DELAY);
							keyCnt++;						
							outputMsg.commandID = OUTPUT_KEY_PRESSING;
							outputMsg.value = key;
							osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);	
							//dbg_ioTask(5, "%x", key);
							BEEP();
							if(!BUZZER_MUTE) {
								HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
							}
							osDelay(BUZ_DELAY/2);
							HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);	
						}
					}
				}
				
				keyCnt = 0;
#endif
			}
			
			keyPrevTick = keyNowTick;
		}
#endif
		
		
#if 1
		// queue msg process... -> dac, Buz... from CLI
		if (osMessageQueueGetCount(inputQueueHandle) > 0) 
		{				
			osMessageQueueGet(inputQueueHandle, &rxCmdMessage, NULL, NULL);
			
			switch(rxCmdMessage.commandID) {				
				case CMD_ALARM_BATT_LOW:
					dbg_ioTask(1, "alarm! batt %d  arc %d ocp %d\r\n", pInfo->measure.battLowAlarmStatus, pInfo->measure.arcAlarmStatus, pInfo->measure.ocpAlarmStatus);
					//RespEventReport();					EventQlight();
				break;
				case CMD_ALARM_ARC:
					dbg_ioTask(1, "alarm! batt %d  arc %d ocp %d\r\n", pInfo->measure.battLowAlarmStatus, pInfo->measure.arcAlarmStatus, pInfo->measure.ocpAlarmStatus);
					//pInfo->measure.arcAlarmStatus=1;
					//pInfo->stat.arcAlarmCount=99;
					//RespEventReport();					EventQlight();
					break;
				case CMD_ALARM_OCP:
					dbg_ioTask(1, "alarm! batt %d  arc %d ocp %d\r\n", pInfo->measure.battLowAlarmStatus, pInfo->measure.arcAlarmStatus, pInfo->measure.ocpAlarmStatus);
					//pInfo->measure.ocpAlarmStatus=1;
					//pInfo->stat.ocpAlarmCount=1;
					//RespEventReport();					EventQlight();
					break;
				
					

				case CMD_TEST:
				delay = 10*rxCmdMessage.argument;
				if(delay > 300) {
					delay = 20;
				}
				switch(rxCmdMessage.argument) {
					case 1:
						RelayControl(0);					
						HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
						dbg_ioTask(3, "DAC_SHDN SET\r\n");
						break;
					case 2:
						RelayControl(1);
						HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);				
					dbg_ioTask(1, "DAC_SHDN RESET\r\n");
						
					break;
					
					//TODO: add curr set...
					case 5:
						//dac7678Set(0, 3350/10);	//2.7v volt A
						dac7678Set(1, 3350/10);	//2.7v volt B
						dac7678Set(3, 3474);	//2.8v curr A
//						dac7678Set(3, 3350/10);	//2.7v curr B
						
						UpdateDac();
					break;
						
					case 6:
//						dac7678Set(0, 3350/10*2);	//2.7v
						dac7678Set(3, 3474);	//2.8v curr A
						dac7678Set(1, 3350/10*2);	//2.7v // volt B
//						dac7678Set(2, 3350/10*2);	//2.7v
//						dac7678Set(3, 3350/10*2);	//2.7v
						
						UpdateDac();
					break;
					case 7:
						//dac7678Set(0, 3350/10*4);	//2.7v
						dac7678Set(3, 3474);	//2.8v curr A
						dac7678Set(1, 3350/10*4);	//2.7v
//						dac7678Set(2, 3350/10*4);	//2.7v
//						dac7678Set(3, 3350/10*4);	//2.7v
						
						UpdateDac();
					break;
					case 8:
//						dac7678Set(0, 3350/10*8);	//2.7v
						dac7678Set(3, 3474);	//2.8v curr A
						dac7678Set(1, 3350/10*8);	//2.7v
//						dac7678Set(2, 3350/10*8);	//2.7v
//						dac7678Set(3, 3350/10*8);	//2.7v
						
						UpdateDac();
					break;
					case 9:
//						dac7678Set(0, 3350);	//2.7v
						dac7678Set(3, 3474);	//2.8v curr A
						dac7678Set(1, 3350);	//2.7v
//						dac7678Set(2, 3350);	//2.7v
//						dac7678Set(3, 3350);	//2.7v
						
						UpdateDac();
					break;
				}
				
				break;
				
				case CMD_BUZ:		
					buzCount = BUZ_DELAY/10+1;
				/*
					for(i=0;i<rxCmdMessage.argument;i++) {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
						osDelay(BUZ_DELAY);
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
						osDelay(BUZ_DELAY);
					}
				*/
				break;
				
				case CMD_KEY:										
				break;
				
				default:
					//dbg("Unkown cmd: %x\r\n\r>", rxCmdMessage.commandID);
				break;
				
			}
			
		}
		else  {
			// TEST: blink LED..
			if( tick % (delay) == 0) {
				cntDelay++;
				if(cntDelay%2==0) {
					HAL_GPIO_WritePin(TEST_GPIO_Port, TEST_Pin, GPIO_PIN_RESET);						
				}
				else {
					HAL_GPIO_WritePin(TEST_GPIO_Port, TEST_Pin, GPIO_PIN_SET);					
				}						
			}
		}		
#endif		
  }
}

int8_t LeTxProc(StructEscInfo *pInfo)
{
	uint8_t sn = SN_LE_TX;
	int8_t ret = 0;
	
	static uint8_t bIsLEConnected = 0;		
	static uint8_t bHaveDataToQlight = 0;
	static uint8_t nConnectLanTry = 0;
	static uint32_t nDelayRetry = 0;
	
	struct IOMessage lanMessage;  
	
	static uint32_t prevTick = 0;
	uint32_t nowTick;
	
	nowTick = HAL_GetTick();
	
#if	TEST_NO_LE_TX
	return 0;
#endif
	
	/*
	if(g_lanCableConnected==0) {
//		dbg_ioTask(1, "Skip TxLE: No LAN Connected!\r\n>");
		osDelay(5000);
		return 0;
	}		
	
	if( (nowTick - prevTick) < 200) {
		dbg_ioTask(1, "LE transfer is too soon, wait next...\r\n", nowTick, prevTick);
		return 0;
	}
*/	
	// Qlight control...
	if (osMessageQueueGetCount(lanTxQueueHandle) > 0)  {				
		osMessageQueueGet(lanTxQueueHandle, &lanMessage, NULL, NULL);
		
			if(g_lanCableConnected==0) {
				dbg_ioTask(3, "Skip TxLE: No LAN Connected!\r\n>");
				//osDelay(5000);
				return 0;
			}		
			
			if( (nowTick - prevTick) < 200) {
				dbg_ioTask(1, "LE transfer is too soon, wait next...\r\n", nowTick, prevTick);
				return 0;
			}

		
		U08 almStatus = IS_HV_ON<<4 | pInfo->measure.ocpAlarmStatus<<2 | pInfo->measure.arcAlarmStatus<<1 | pInfo->measure.battLowAlarmStatus;
		dbg_ioTask(1, "Alarm to LE status %x, Cmd id=%x arg=%x\r\n",almStatus, lanMessage.commandID, lanMessage.argument);
		switch(lanMessage.commandID)
		{
		//					Red	Orange	Green Blue	White, Sound // No white
		//			0, 1		2,	3,		4,		5,	6,		7	
			case CMD_QLIGHT_TEST:
					if( (lanMessage.argument&QLIGHT_RED_ON)==QLIGHT_RED_ON ) { // Red On
						qlightCmd[2] = 0x01;
					}
					else if( (lanMessage.argument&QLIGHT_RED_OFF)==QLIGHT_RED_OFF ) { // Red Off
						qlightCmd[2] = 0x00;
					}
					if( (lanMessage.argument&QLIGHT_ORANGE_ON)==QLIGHT_ORANGE_ON ) { 
						qlightCmd[3] = 0x01;
					}
					else if( (lanMessage.argument&QLIGHT_ORANGE_OFF)==QLIGHT_ORANGE_OFF ) {
						qlightCmd[3] = 0x00;
					}
					if( (lanMessage.argument&QLIGHT_GREEN_ON)==QLIGHT_GREEN_ON ) {
						qlightCmd[4] = 0x01;
					}
					else if( (lanMessage.argument&QLIGHT_GREEN_OFF)==QLIGHT_GREEN_OFF ) {
						qlightCmd[4] = 0x00;
					}
					if( (lanMessage.argument&QLIGHT_BLUE_ON)==QLIGHT_BLUE_ON ) {
						qlightCmd[5] = 0x01;
					}
					else if( (lanMessage.argument&QLIGHT_BLUE_OFF)==QLIGHT_BLUE_OFF ) {
						qlightCmd[5] = 0x00;
					}
					
					if( (lanMessage.argument&QLIGHT_ALL_OFF)==QLIGHT_ALL_OFF ) {
						qlightCmd[2] = 0x00;
						qlightCmd[3] = 0x00;
						qlightCmd[4] = 0x00;
						qlightCmd[5] = 0x00;						
					}
					
					
					bHaveDataToQlight = 1;
					nConnectLanTry = 0;		
			break;				
/*
		초록색: 완충(90~100%)
		 파란색: 고압 출력
		 빨간색: 배터리(삭제) , ocp, arc
		 노란색: 배터리 low limit 이하
		*/				
			case CMD_QLIGHT:				
				if( (almStatus & (0x02|0x04)) != 0 ) { //, ARC or OCP Red On
					qlightCmd[2] = 0x01;
					
				}
				else  {
					qlightCmd[2] = 0x00;
					
				}
				if( (almStatus & 0x01)!=0 ) { // Batt low , Orange On
					qlightCmd[3] = 0x01;
				}
				else {	// if( (lanMessage.argument&QLIGHT_ORANGE_OFF)==QLIGHT_ORANGE_OFF ) {
					qlightCmd[3] = 0x00;
				}
	/*
				if( almStatus & 0x10 ) // Hv out On, Blue
					qlightCmd[4] = 0x01;
				}
				else if( (lanMessage.argument&QLIGHT_GREEN_OFF)==QLIGHT_GREEN_OFF ) {
					qlightCmd[4] = 0x00;
				}*/
				if( (almStatus & (1<<4))!=0 ) { // Hv out On, Blue
					qlightCmd[5] = 0x01;
				}
				else { //if( (lanMessage.argument&QLIGHT_BLUE_OFF)==QLIGHT_BLUE_OFF ) {
					qlightCmd[5] = 0x00;
				}
				
				bHaveDataToQlight = 1;				
				break;
		}
	}
	else {
		if(nConnectLanTry>=3) {
			nDelayRetry--;
			if(nDelayRetry>0) {
				//osDelay(10);
				//dbg_ioTask(1, "LE is not online %d %d\r\n", nConnectLanTry, nDelayRetry);
				bHaveDataToQlight = 0;
				return -1;
			}
			else {
				dbg_ioTask(1, "LE is not online %d %d\r\n", nConnectLanTry, nDelayRetry);
				nConnectLanTry = 0;
			}
		}		
		
		if(bHaveDataToQlight==1) {
			if(nConnectLanTry>3) {
				dbg_ioTask(1, "ERROR to connect LE TIMEOUT\r\n");
				bHaveDataToQlight = 0;
			}
			else 	{			

				if(bIsLEConnected==0) {		
					sn = SN_LE_TX; // use sn=1, on Qlight...
					// Init socket
					if((ret = socket(sn, Sn_MR_TCP, pInfo->setting.leQlight.port, 0x00)) == sn)
					{
						ret = connect(sn, pInfo->setting.leQlight.ip, pInfo->setting.leQlight.port);
						if(ret == SOCK_OK )
						{
							nConnectLanTry = 0;
							bIsLEConnected = 1;
						}
						else {
							dbg_ioTask(1, "ERROR to connect LE %d\r\n", ret);
							//ret = disconnect(sn);
							nConnectLanTry++;
							nDelayRetry = 200;							
						}
					}
					else {
						nConnectLanTry++;
						osDelay(10);
					}
				}
				
				else {
					//osDelay(10);										
					ret = send(sn,qlightCmd, sizeof(qlightCmd));
					osDelay(5);  // need some delay before Disconnect...
					if(ret>0) {
						dbg_ioTask(2, "Send to qlight R%x O%x G%x B%x OK %d \r\n\r>",qlightCmd[2], qlightCmd[3], qlightCmd[4], qlightCmd[5], ret);
						ret = disconnect(sn);
						bIsLEConnected = 0;
						bHaveDataToQlight = 0;
					}
					else {
						ret = disconnect(sn);
						bIsLEConnected = 0;
						//bHaveDataToQlight = 1; // Retry..
						osDelay(100);
						dbg_ioTask(1, "Send to qlight error %d\r\n\r>", ret);
					}
					
				}

			}
		}
	}		
  
	return bIsLEConnected;
	
}

static uint8_t nConnectTry = 0;
static uint8_t bServerConnect = 0;
int8_t LanTxProc(StructEscInfo *pInfo)
{
	uint8_t sn = SN_SERVER_TX;
	int8_t ret = 0;
//	static uint8_t LanTaskStatus = 0;
		
	static uint8_t bTestSend = 0;
	static uint8_t bIsLanTxConnected = 0;
	
	
#if TEST_NO_LAN_TX
	return 0;
#endif
	
	if(g_lanCableConnected==0) {
		//dbg_ioTask(1, "Skip Tx: No LAN Connected!\r\n>");
		osDelay(5000);
		return 0;
	}	
	if(nConnectTry> 10) {
		//dbg_ioTask(1, "[LAN]Skip TX: Failed to socket() over %d\r\n\r>", nConnectTry);
		//return 0;
	}
	if(bServerConnect==0) {
		return 0;
	}
	
	if(pInfo->setting.lanLora==COMM_MODE_LAN || LAN_LORA_DUAL) { 
		// TODO: Lan TX task...
		sn = SN_SERVER_TX; // use ns=1, on For PLC or Server...
		if(bIsLanTxConnected==LanStatReady) {
			// Init socket
			if((ret = socket(sn, Sn_MR_TCP, pInfo->setting.nPort, 0x00)) == sn)
			{
				bIsLanTxConnected=LanStatSocket;
				dbg_ioTask(2, "[LAN_TX] socket() Done \r\n");					
			}
			else {
				dbg_ioTask(2, "[LAN_TX] Failed to socket() %d %d\r\n\r>", ret, nConnectTry);
				nConnectTry++;						
				osDelay(500);
				bIsLanTxConnected=LanStatReady;
			}
			if(nConnectTry>200) {
				nConnectTry = 0;
				dbg_ioTask(2, "[LAN_TX] Connection error...Pls Check server! %d\r\n\r>", ret);
			}			
		}
		else if(bIsLanTxConnected==LanStatSocket) {			
			ret = connect(sn, pInfo->setting.serveIp, pInfo->setting.nPort);
			switch(ret) {
				case SOCK_OK:				
				dbg_ioTask(2, "[LAN_TX] connected \r\n");					
				bIsLanTxConnected=LanStatConnected;
				break;
				
				case SOCKERR_SOCKINIT:
				dbg_ioTask(2, "[LAN_TX] Failed SOCKERR_SOCKINIT %d %d\r\n\r>", ret, nConnectTry);
				bIsLanTxConnected=LanStatReady;
				break;
				
				case SOCKERR_TIMEOUT:				
				dbg_ioTask(2, "[LAN_TX] Failed SOCKERR_TIMEOUT %d %d\r\n\r>", ret, nConnectTry);
				ret = close(sn);
				bIsLanTxConnected=LanStatReady;
					break;
			
				default:
				nConnectTry++;
				//ret = disconnect(sn);
				dbg_ioTask(2, "[LAN_TX] Failed to connect() %d %d\r\n\r>", ret, nConnectTry);
				osDelay(500);
				//osDelay(100*nConnectTry);
				if(nConnectTry> 10) {
					//bServerConnect = 0;
				}
				break;
			}			
		}		
		else if(bIsLanTxConnected==LanStatConnected)  {
			if(bTestSend==0) {						
				ret = 1;
				ret = send(sn,(uint8_t* )"hello, apollo", 13);
				bTestSend = 1;
				if(ret>0) {
					osDelay(30); // need some delay before Disconnect...
					dbg_ioTask(2, "[LAN_TX] Tx Dummy to %d.%d.%d.%d OK %d \r\n\r>",pInfo->setting.serveIp[0], pInfo->setting.serveIp[1], pInfo->setting.serveIp[2], pInfo->setting.serveIp[3], ret);

					//ret = LanConnect(sn);
					//bIsLanTxConnected = LanStatReady;
				}
				else {
					dbg_ioTask(2, "[LAN_TX] Tx Dummy error %d\r\n\r>", ret);
					ret = disconnect(sn);
					bTestSend = 0;
					bIsLanTxConnected = LanStatReady;
					//goto INIT_LAN_W5300;
				}
				
			}
			else if(g_lanBufCount>0) {
				TakeUplink();						
				ret = send(sn,(uint8_t* )g_lanBuf, g_lanBufCount);						

				if(ret>0) {						
					dbg_ioTask(2, "[LAN_TX] Tx to %d.%d.%d.%d OK %d \r\n\r>",pInfo->setting.serveIp[0], pInfo->setting.serveIp[1], pInfo->setting.serveIp[2], pInfo->setting.serveIp[3], ret);
//					dbg_ioTask(2, "[LAN_TX] Tx Done %d\r\n", ret);
					g_lanBufCount = 0;	
				}
				else {
					dbg_ioTask(2, "[LAN_TX] Tx error %d Connet again...\r\n>", ret);
					bIsLanTxConnected = LanStatReady;
					bTestSend = 1; // 재전송
					ret = disconnect(sn);
				}						
				
				GiveUplink();	
			}
		}	
	}
	else {
		bTestSend = 0;
		if(bIsLanTxConnected) {
			ret = disconnect(sn);
		}
	}	
	return bIsLanTxConnected;
}

#define MAX_SOCK_NUM        (8)
#define ALIGN(X)    __attribute__ ((aligned (X)))
ethBuf_t        ethBuf; // ALIGN(2);

u32 flashGetSector(u32 addr) 
{ 
    u32 sector = 0;

    if ((addr < ADDR_FLASH_SECTOR_1) &&
        (addr >= ADDR_FLASH_SECTOR_0))
    {
        sector = FLASH_SECTOR_0;
    }
    else if ((addr < ADDR_FLASH_SECTOR_2) &&
             (addr >= ADDR_FLASH_SECTOR_1))
    {
        sector = FLASH_SECTOR_1;
    }
    else if ((addr < ADDR_FLASH_SECTOR_3) &&
             (addr >= ADDR_FLASH_SECTOR_2))
    {
        sector = FLASH_SECTOR_2;
    }
    else if ((addr < ADDR_FLASH_SECTOR_4) &&
             (addr >= ADDR_FLASH_SECTOR_3))
    {
        sector = FLASH_SECTOR_3;
    }
    else if ((addr < ADDR_FLASH_SECTOR_5) &&
             (addr >= ADDR_FLASH_SECTOR_4))
    {
        sector = FLASH_SECTOR_4;
    }
    else if ((addr < ADDR_FLASH_SECTOR_6) &&
             (addr >= ADDR_FLASH_SECTOR_5))
    {
        sector = FLASH_SECTOR_5;
    }
    else if ((addr < ADDR_FLASH_SECTOR_7) &&
             (addr >= ADDR_FLASH_SECTOR_6))
    {
        sector = FLASH_SECTOR_6;
    }
    else if ((addr < ADDR_FLASH_SECTOR_8) &&
             (addr >= ADDR_FLASH_SECTOR_7))
    {
        sector = FLASH_SECTOR_7;
    }
    else if ((addr < ADDR_FLASH_SECTOR_9) &&
             (addr >= ADDR_FLASH_SECTOR_8))
    {
        sector = FLASH_SECTOR_8;
    }
    else if ((addr < ADDR_FLASH_SECTOR_10) &&
             (addr >= ADDR_FLASH_SECTOR_9))
    {
        sector = FLASH_SECTOR_9;
    }
    else if ((addr < ADDR_FLASH_SECTOR_11) &&
             (addr >= ADDR_FLASH_SECTOR_10))
    {
        sector = FLASH_SECTOR_10;
    }
    else if ((addr < FLASH_END_ADDR + 1) &&
             (addr >= ADDR_FLASH_SECTOR_11))
    {
        sector = FLASH_SECTOR_11;
    }
    else
    {
        sector = FLASH_SECTOR_11 + 1;
    }

    return(sector);
} 
U08 flashErase(u32 startAddr, u32 size)
{
    volatile TestStatus MemoryProgramStatus;

    u32 endAddr;
    u32 startSector;
    u32 endSector; 

    MemoryProgramStatus = PASSED;
    endAddr = startAddr + size;

    if (endAddr > FLASH_END_ADDR+1)
    {
        printf("Flash Env size is too big...\r\n");
        return(0);
    }

    /* Unlock the Flash Program Erase controller */
    HAL_FLASH_Unlock();

    /* Clear pending flags (if any) */
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

    /* Get the number of the start and end sectors */
    startSector = flashGetSector(startAddr); 
    endSector   = flashGetSector(startAddr + size); 

	dbg_ioTask(1, "Addr %d %d %x\r\n", startSector, endSector, size);
    
	int i;
	//for (i = startSector; i < endSector; i += 8)
	for (i = startSector; i < endSector; i ++)
    {   
		FLASH_Erase_Sector(i, VOLTAGE_RANGE_3);
		osDelay(100);
		//dbg_ioTask(1, "_E");

        if (netDlMode)
        {
            //mprintf("E_");			
        }
    }
	dbg_ioTask(1, "End erase %d\r\n", i);

    HAL_FLASH_Lock();

    return(MemoryProgramStatus);
}

U08 flashWrite(u32 startAddr, u8 *buf, u32 size)
{
    u32 Address = 0x00;
    volatile U08 FLASHStatus;
    volatile TestStatus MemoryProgramStatus;

    FLASHStatus = HAL_OK; //FLASH_COMPLETE;
    u32 endAddr;
    u32 rest;
    u32 *pData;

    MemoryProgramStatus = PASSED;

    rest = size % 4;
    endAddr = startAddr + size + rest;

    if (endAddr > FLASH_END_ADDR)
    {
        printf("Flash Env size is too big...\r\n");
        return(0);
    }

    /* Unlock the Flash Program Erase controller */
    HAL_FLASH_Unlock();

    /* Clear All pending flags */
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR |
                    FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);


    Address = startAddr;
    pData = (u32 *)(void *)buf;

	//int n = 0;
    while ((Address < endAddr) && (FLASHStatus == HAL_OK))
    {
		//dbg_ioTask(1, "D=%x ", *pData);
		FLASHStatus = HAL_FLASH_Program(TYPEPROGRAM_WORD, Address, *pData++);		
        //FLASHStatus = FLASH_ProgramWord(Address, *pData++);
        Address = Address + 4;
		/*
		if(n++ > 0x10000/16) {
			n = 0;
			osDelay(200);
		}*/
    }

    /* Check the corectness of written data */
    Address = startAddr;
    pData = (u32 *)(void *)buf;

    while ((Address < endAddr) && (MemoryProgramStatus != FAILED))
    {
        if ((*(U32 *)Address) != *pData)
        {
            MemoryProgramStatus = FAILED;

            #if 1
            printf("Address  : %08X \r\n", (unsigned int)Address); 
            printf("*Address : %08X \r\n", (unsigned int)*(U32 *)Address); 
            printf("*pData   : %08X \r\n", (unsigned int)*pData); 
            #endif
        }

        Address += 4;
        pData++;
    }

    if (MemoryProgramStatus == FAILED)
    {
        printf("Error Env save...\r\n");
    }

    #if 0
    printf("MemoryProgramStatus : %d \r\n", MemoryProgramStatus);

    for (u32 i=FLASH_START_ADDR; i<FLASH_START_ADDR+size; i+=4)
    {
        printf("StartAddr[%08X] : %08X \r\n", i, (*(vu32*)i));
    }
    #endif

    HAL_FLASH_Lock();

    return(MemoryProgramStatus);
}

uint8_t bNoW5300 = !DET_LAN;
uint8_t bCnLan = DET_LAN_LINK;

void InitLan(void)
{
/////////////////////////////W5300 TEST//////////////////////////////
	unsigned char W5300FifoSize[2][8] = {{2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2}};
	
	W5300HardwareReset();
	osDelay(100);
		
		
#if TEST_W5300_OFF		
#else
	W5300DeSelect();

	reg_wizchip_bus_cbfunc(W5300ReadData, W5300WriteData);
	reg_wizchip_cs_cbfunc(W5300Select,W5300DeSelect);
	reg_wizchip_cris_cbfunc(vPortEnterCritical,vPortExitCritical);
	
	
	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)W5300FifoSize) == -1)
	{
		bNoW5300 = 1;
		dbg_ioTask(1, "W5300 initialized fail %d %d\r\n", bNoW5300, bCnLan);
		g_lanCableConnected = 0;
	}
	

	if(Net_Conf(GetNetInfo())) {
		bNoW5300 = 1;
		dbg_ioTask(1, "W5300 read info fail %d %d\r\n", bNoW5300, bCnLan);
		g_lanCableConnected = 0;
		
	}
	
#if PING_ENABLE
	//Enable PING: over write settings same as boot loader...
	netmode_type nMode = wizchip_getnetmode();
	nMode &= ~NM_PINGBLOCK;
	nMode |= MR_FS;
	setIMR(0xff);
	setRTR(400);
	setRCR(3);
	wizchip_setnetmode(nMode);
#endif
#endif		
	osDelay(1000); //org=4000
}

void TaskLan(void *argument)
{
	StructEscInfo *pInfo;
//	struct IOMessage lanMessage;  
	pInfo = GetEscInfo();

	uint16_t nCntLan = 0;
	uint16_t tick = 0;
	uint8_t i;
		
		
    U32  len;
    U08  *txBuf;
    U08   subCmd;
    U32  wrAddr;
    U16  dataLen;
    static u8 dlType = 0;
		
		
	InitEscInfo();
	BaudRateChange(pInfo->setting.serialSpeed);
		
	if(bNoW5300) {
		printf("No W5300 %d %d\r\n", bNoW5300, bCnLan);
		g_lanCableConnected = 0;
		osDelay(100);
	}	
	else {
		InitLan();
	}
	
	
	g_checkTaskStart |= CHECK_TASK_LAN;
	g_lanInit = 1;
	
	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(100);
		dbg_ioTask(9, "Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}
	
	
  /* Infinite loop */
	for(;;)
	{		
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
		
		/*
		for(i=0;i<10;i++) {
			osDelay(1); // delay for other task first...
			bCnLan = DET_LAN_LINK;
			nCntLan += bCnLan;
		}
		*/
		if(g_dnFWMode) {
		}
		else {
			osDelay(10); // delay for other task first...
		}
		bCnLan = DET_LAN_LINK;
		nCntLan += bCnLan;

		if(bNoW5300) {
#if SWITCH_MODE_DET			
			if(DET_LORA) {
				pInfo->setting.lanLora = COMM_MODE_LORA;
			}
			else {
				pInfo->setting.lanLora = COMM_MODE_SERIAL;
			}
#endif
			dbg_ioTask(1, "%s: No Ethernet Module! End task mode = %d\r\n", __FUNCTION__, pInfo->setting.lanLora);
			
			
			status = osThreadTerminate (myTaskLanHandle);                         // stop the thread
			if (status == osOK) {
				// Thread was terminated successfully
			}
			else {
				printf("%s: Failed to terminate thread\r\n", __FUNCTION__);
				// Failed to terminate a thread
			}			
			
			osDelay(100);
			continue;
		}

		// TEST lan...
		//loopback_tcps(1, g_lanBuf, 3123);
		tick++;
		
#if 1
		if(tick %50 == 0) {						
			if(nCntLan==0) {
				dbg_ioTask(2, "LAN Connection NO? %d %d\r\n", bCnLan, nCntLan);
				g_lanCableConnected = 0;
				continue;
			}
			else {
				//dbg_ioTask(2, "LAN Connection YES? %d %d\r\n", bCnLan, nCntLan);
				g_lanCableConnected = 1;
			}
			nCntLan = 0;
		}
#endif
		if(tick%5==0) 		
		{		
			LeTxProc(pInfo);
			
			// Monitor by LAN
			do
			{				
				int ret=0;
				U16 size=0;
				U16 nMointorPort = 41011;

				osDelay(50);

				if(g_lanCableConnected==0) {
					//dbg_ioTask(1, "Skip RX:No LAN Connected!\r\n>");
					osDelay(5000);								
					continue;
				}
				if((g_checkTaskStart&CHECK_TASK_LAN)==0) {
					osDelay(100);
					continue;
				}
				TakeLan();
				uint8_t sn_sr = getSn_SR(SN_MONITOR_RX);
				switch(sn_sr) 
				{ 
				case SOCK_ESTABLISHED: 
					if(getSn_IR(SN_MONITOR_RX) & Sn_IR_CON) 
					{ 
						setSn_IR(SN_MONITOR_RX,Sn_IR_CON); 
					} 
					g_monitorReady = 1;
					break; 
					
				case SOCK_CLOSE_WAIT: 
					if((ret=disconnect(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Disconnect error %d \r\n",ret); 
					if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP,nMointorPort ,0x00)) != SN_MONITOR_RX) dbg_ioTask(1, "[LAN-M] CLOSE_WAIT Socket error %d \r\n",ret); 
					if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Listen error %d \r\n",ret); 
					break; 
				
				case SOCK_INIT: 
					if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] INIT Listen error %d \r\n",ret); 			
					break; 
				
				case SOCK_CLOSED:
					g_monitorReady = 0;
					if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP, nMointorPort,0x00)) != SN_MONITOR_RX) dbg_ioTask(1, "[LAN-M] CLOSED Socket error %d \r\n",ret); 
					osDelay(500);
					break; 
				
				case SOCK_LISTEN:
					break;
				
				default: 
					//dbg_ioTask(1, "Rx state error: %d\r\n", sn_sr);
					if(sn_sr == 243) {
						if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP,nMointorPort,0x00)) != SN_MONITOR_RX) {
							dbg_ioTask(2, "[LAN-M] Socket error %d \r\n",ret); 
						}
						if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) {
							dbg_ioTask(2, "[LAN-M] Listen error %d \r\n",ret); 
						}
						//Ack();
					}
					break; 
				}
				GiveLan();
						
				if(osMessageQueueGetCount(queueMonitorTxHandle) > 0) {
					g_txMonitorLength = 0;
					while(osMessageQueueGetCount(queueMonitorTxHandle) > 0) {
						char cOutChar;
						osMessageQueueGet(queueMonitorTxHandle, &cOutChar, NULL, NULL);	
						g_txMonitorMsg[g_txMonitorLength] = cOutChar;
						g_txMonitorLength++;
						if(g_txMonitorLength>=N_MAX_MONITOR_TX_LENGTH) {
							break;
						}
					}
					resData[2] = g_txMonitorLength;
					memcpy(&resData[32],g_txMonitorMsg, g_txMonitorLength);
					
					if(g_dnFWMode==0) {
						TakeLan();
						//ret = send(SN_MONITOR_RX,resData, 32 + g_txMonitorLength); // respons
						GiveLan();
					}					
				}

				TakeLan();
				if((size = getSn_RX_RSR(SN_MONITOR_RX)) > 0) 
				{ 
					
					if(size > DATA_BUF_SIZE) {
						//size = DATA_BUF_SIZE; 
						//dbg_ioTask(1, "OVER SIZE %d\r\n", size);
					}
					
					ret = recv(SN_MONITOR_RX,rxMonitorMsg,size); 	
					GiveLan();
					
					if(ret == SOCKERR_SOCKSTATUS) {
						dbg_ioTask(2, "\r\n[LAN-M] Socket ERR %d\r\n",ret); 
						if((ret=disconnect(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Disconnect error %d \r\n",ret); 
					}
					
					ret = 0;
					if(g_dnFWMode==0) {
						for(i=0;i<size;i++) {
							if(rxMonitorMsg[i]!=0) {
								ret = 1;
							}
						}
						
						if(ret==0) {
							dbg_ioTask(1, "NULL rxMonitorMsg\r\n");
							size = 0;
							GiveUplink();	
							osDelay(5000);
							
						}
					}
					else {
						if(size>512) {
							//dbg_ioTask(1, "DATA\r\n");
						}
					}
					
					net_mon_p_t *pMsg = (net_mon_p_t *)rxMonitorMsg;
					if(size>0) {
						nConnectTry = 0;							
						//dbg_ioTask(1, "\r\n[LAN-M] Rx %d %x\r\n", size, rxMonitorMsg[6]); 
						dbg_ioTask(4, "\r\n[LAN-M] cmd %x, %x, l=%d, addr=%x \r\n",
							pMsg->cmd, pMsg->sub, pMsg->len, pMsg->addr);
					}
					
					if(size>=32) {	
						if(pMsg->cmd == NET_TCP_CMD_STR) {
							char xReturned;
							extern char cInputString[ 50 ];//, cLastInputString[ 50 ];
							extern uint8_t ucInputIndex;
							extern char *pcOutputString;	
							typedef void * xComPortHandle;
							extern xComPortHandle xPort;
							extern void vSerialPutString( xComPortHandle pxPort, const signed char * const pcString, unsigned short usStringLength );
							extern BaseType_t FreeRTOS_CLIProcessCommand( const char * const pcCommandInput, char * pcWriteBuffer, size_t xWriteBufferLen  );
								
							ucInputIndex = size-32;
								
							do
							{
								/* Get the next output string from the command interpreter. */
								xReturned = FreeRTOS_CLIProcessCommand( (const char *)(rxMonitorMsg+32), pcOutputString, 1000 );

								/* Write the generated string to the UART. */
								vSerialPutString( xPort, ( signed char * ) pcOutputString, ( unsigned short ) strlen( pcOutputString ) );

							} while( xReturned != pdFALSE );				
						}
						else {
							//dbg_ioTask(1, "FW Up: c=%x s=%x %d\r\n", pMsg->cmd, pMsg->sub, pMsg->len);
							memcpy(ethBuf.rxBuf, (U08*)pMsg, size);// sizeof(net_mon_p_t));
							switch(pMsg->cmd) {
								case NET_TCP_CMD_DOWNLOAD:
								//---------------------------------------------
								subCmd  = ((net_mon_p_t *)ethBuf.rxBuf)->sub;
								dataLen = ((net_mon_p_t *)ethBuf.rxBuf)->len;
								wrAddr  = ((net_mon_p_t *)ethBuf.rxBuf)->addr;

								switch (subCmd)
								{
									case NET_DL_CMD_START:
										g_dnFWMode = 1;
										//----------------------------
										//mprintf("addr:%08lX, len:%d\r\n", wrAddr, dataLen);
										if (wrAddr == 0)
										{
											dbg_ioTask(1, "Start DL...\r\n");
											dlType = NET_DL_TYPE_APP;
										}
										break;

									case NET_DL_CMD_ERASE:
										//----------------------------
										if (dlType == NET_DL_TYPE_APP)
										{
											netDlMode = ON;
											dbg_ioTask(1, "Start Erase...\r\n");
											flashErase(APP_BACKUP_START_ADDR, APP_PGM_MAX_SIZE);
											netDlMode = OFF;
										}
										break;

									case NET_DL_CMD_DATA:
										//----------------------------
										if (dlType == NET_DL_TYPE_APP)
										{
											//dbg_ioTask(1, "Start Write...\r\n");
											flashWrite(APP_BACKUP_START_ADDR + wrAddr,
													   &ethBuf.rxBuf[sizeof(net_mon_p_t)], dataLen);
										}
										break;

									case NET_DL_CMD_DATA_END:
										//----------------------------
										break;

									case NET_DL_CMD_RESET:
										//----------------------------
										if (dlType == NET_DL_TYPE_APP)
										{
											NVIC_SystemReset();
											//osTmr10(TASK_NET, NET_CMD_TCP_CONSOLE_RESET, NET_TT_MAIN_RESET, TT_ONETIME, 10, 0);
											dlType = 0;
										}
										break;

									default:
										//----------------------------
										break;
								}

								txBuf = ethBuf.txBuf;

								((net_mon_p_t *)txBuf)->cmd = NET_TCP_CMD_DOWNLOAD_ACK;
								((net_mon_p_t *)txBuf)->sub = subCmd;
								((net_mon_p_t *)txBuf)->len = 0;

								//len = send(TCP_CONSOLE_SOCK, txBuf, sizeof(net_mon_p_t));
								TakeLan();
								len = send(SN_MONITOR_RX,txBuf, sizeof(net_mon_p_t));
								//dbg_ioTask(1, "ACK");
								GiveLan();
								
								break;

								case NET_TCP_CMD_DOWNLOAD_ACK:
									//---------------------------------------------
									//mputs("Rcv... NET_TCP_CMD_DOWNLOAD_ACK \r\n");
									break;

								case NET_TCP_CMD_KEEP_ALIVE:
									//---------------------------------------------
									//netTcpConsoleTimer = 0;
									break;								
								}
/*							
							if(pMsg->cmd == NET_TCP_CMD_DOWNLOAD) {
							switch(pMsg->sub) {
								case NET_DL_CMD_START:
									g_dnFWMode = 1;
									g_resMonitorData.cmd = NET_TCP_CMD_DOWNLOAD_ACK;
									g_resMonitorData.sub = NET_DL_CMD_START;
									g_resMonitorData.len = 0;
									memset(g_resMonitorData.extra, 0, 24);
									TakeLan();
									ret = send(SN_MONITOR_RX, (void*)&g_resMonitorData, sizeof(g_resMonitorData)); // respons
									GiveLan();
									break;
								
								case NET_DL_CMD_ERASE:
									g_dnFWMode = 1;
									g_resMonitorData.cmd = NET_TCP_CMD_DOWNLOAD;
									g_resMonitorData.sub = NET_DL_CMD_START;
									g_resMonitorData.len = 0;
									memset(g_resMonitorData.extra, 0, 24);
									TakeLan();
									ret = send(SN_MONITOR_RX, (void*)&g_resMonitorData, sizeof(g_resMonitorData)); // respons
									GiveLan();
									break;
							}
							*/
						}
					}			
					//ret = send(sn,resData, 32 + ); // respons test
					
					memset(rxMonitorMsg, 0, size);
					
				} 
				else {
					GiveLan();
				}
			}	while(0);
		}
  }
}

void TaskMonitorLan()
{
	
	StructEscInfo *pInfo;
	pInfo = GetEscInfo();
	
	int i=0;
	int tick=0;
	//uint8_t sn = SN_MONITOR_RX;
	uint16_t size = 0; // sentsize=0;
	uint32_t nRxErrCount = 0;
	int32_t ret;
	
	
	while(1) 
	{				
		U16 nMointorPort = 41011;

		osDelay(50);

		if(g_lanCableConnected==0) {
			//dbg_ioTask(1, "Skip RX:No LAN Connected!\r\n>");
			osDelay(5000);								
			continue;
		}
		if((g_checkTaskStart&CHECK_TASK_LAN)==0) {
			osDelay(100);
			continue;
		}
		TakeLan();
		uint8_t sn_sr = getSn_SR(SN_MONITOR_RX);
		switch(sn_sr) 
		{ 
		case SOCK_ESTABLISHED: 
			if(getSn_IR(SN_MONITOR_RX) & Sn_IR_CON) 
			{ 
				setSn_IR(SN_MONITOR_RX,Sn_IR_CON); 
			} 
			g_monitorReady = 1;
			break; 
			
		case SOCK_CLOSE_WAIT: 
			if((ret=disconnect(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Disconnect error %d \r\n",ret); 
			if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP,nMointorPort ,0x00)) != SN_MONITOR_RX) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Socket error %d \r\n",ret); 
			if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Listen error %d \r\n",ret); 
			break; 
		
		case SOCK_INIT: 
			if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] INIT Listen error %d \r\n",ret); 			
			break; 
		
		case SOCK_CLOSED:
			g_monitorReady = 0;
			if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP, nMointorPort,0x00)) != SN_MONITOR_RX) dbg_ioTask(2, "[LAN-M] CLOSED Socket error %d \r\n",ret); 
			osDelay(500);
			break; 
		
		case SOCK_LISTEN:
			break;
		
		default: 
			//dbg_ioTask(1, "Rx state error: %d\r\n", sn_sr);
			if(sn_sr == 243) {
				if((ret=socket(SN_MONITOR_RX,Sn_MR_TCP,nMointorPort,0x00)) != SN_MONITOR_RX) {
					dbg_ioTask(2, "[LAN-M] Socket error %d \r\n",ret); 
				}
				if( (ret = listen(SN_MONITOR_RX)) != SOCK_OK) {
					dbg_ioTask(2, "[LAN-M] Listen error %d \r\n",ret); 
				}
				//Ack();
			}
			break; 
		}
		GiveLan();
				
		if(osMessageQueueGetCount(queueMonitorTxHandle) > 0) {
			g_txMonitorLength = 0;
			while(osMessageQueueGetCount(queueMonitorTxHandle) > 0) {
				char cOutChar;
				osMessageQueueGet(queueMonitorTxHandle, &cOutChar, NULL, NULL);	
				g_txMonitorMsg[g_txMonitorLength] = cOutChar;
				g_txMonitorLength++;
				if(g_txMonitorLength>=N_MAX_MONITOR_TX_LENGTH) {
					break;
				}
			}
			resData[2] = g_txMonitorLength;
			memcpy(&resData[32],g_txMonitorMsg, g_txMonitorLength);
			
			if(g_dnFWMode==0) {
				TakeLan();
				ret = send(SN_MONITOR_RX,resData, 32 + g_txMonitorLength); // respons
				GiveLan();
			}
		}

		TakeLan();
		if((size = getSn_RX_RSR(SN_MONITOR_RX)) > 0) 
		{ 
			
			if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE; 
			
			ret = recv(SN_MONITOR_RX,rxMonitorMsg,size); 	
			GiveLan();
			
			if(ret == SOCKERR_SOCKSTATUS) {
				dbg_ioTask(2, "\r\n[LAN-M] Socket ERR %d\r\n",ret); 
				if((ret=disconnect(SN_MONITOR_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN-M] CLOSE_WAIT Disconnect error %d \r\n",ret); 
			}
			
			if(size>0) {
				nConnectTry = 0;							
			}
			
			dbg_ioTask(2, "\r\n[LAN-M] Rx %d %x\r\n", size, rxMonitorMsg[6]); 
			
			if(size>32) {								
				char xReturned;
				extern char cInputString[ 50 ];//, cLastInputString[ 50 ];
				extern uint8_t ucInputIndex;
				extern char *pcOutputString;	
				typedef void * xComPortHandle;
				extern xComPortHandle xPort;
				extern void vSerialPutString( xComPortHandle pxPort, const signed char * const pcString, unsigned short usStringLength );
				extern BaseType_t FreeRTOS_CLIProcessCommand( const char * const pcCommandInput, char * pcWriteBuffer, size_t xWriteBufferLen  );
					
				ucInputIndex = size-32;
					
				do
				{
					/* Get the next output string from the command interpreter. */
					xReturned = FreeRTOS_CLIProcessCommand( (const char *)(rxMonitorMsg+32), pcOutputString, 1000 );

					/* Write the generated string to the UART. */
					vSerialPutString( xPort, ( signed char * ) pcOutputString, ( unsigned short ) strlen( pcOutputString ) );

				} while( xReturned != pdFALSE );				
				
				
			}			
			//ret = send(sn,resData, 32 + ); // respons test
			
			memset(rxMonitorMsg, 0, size);
			
		} 
		else {
			GiveLan();
		}
	}
}



void TaskLanRx(void *argument)
{
	StructEscInfo *pInfo;
	pInfo = GetEscInfo();
	
	int i=0;
	int tick=0;
	//uint8_t sn = SN_SERVER_RX;
	uint16_t size = 0; // sentsize=0;
	uint32_t nRxErrCount = 0;
	int32_t ret;
	uint8_t lanLoraMode=0xff; // set initial 0xff
	
	uint8_t waitLan = 0;
	uint8_t *lanRxBuffer = lanRxBuf;

	
	//GET_LoraNode_Info(&temp);
	g_checkTaskStart |= CHECK_TASK_LANRX;	
	
	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(100);
		dbg_ioTask(9, "Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}
	
	
	while(1) {
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
		
		if(!DET_LAN) {
			dbg_ioTask(1, "%s: No Lan Module! End task mode = %d\r\n", __FUNCTION__, pInfo->setting.lanLora);
			osDelay(1000);
			
			
			status = osThreadTerminate (myTaskLanRxHandle);                         // stop the thread
			if (status == osOK) {
				// Thread was terminated successfully
			}
			else {
				printf("%s: Failed to terminate thread\r\n", __FUNCTION__);
				// Failed to terminate a thread
			}			
		}			
		
		osDelay(10);
		if(g_ConnectedServer==COMM_MODE_LORA) {
			osDelay(1000);			
			//dbg_ioTask(1, "Skip Lan task...\r\n");
			continue;
		}
			
		if(GetEscInfo()->setting.lanLora==COMM_MODE_LAN || bFirstCommPath || LAN_LORA_DUAL) 
		{ // Lora Off
		#if BLINK_WLINK			
				//HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
		#endif
			
			
#if TEST_NO_LAN_RX==0

			for (;;){	
				osDelay(10);
				tick++;
				if(g_lanCableConnected==0) {
					//dbg_ioTask(1, "Skip RX:No LAN Connected!\r\n>");
					osDelay(5000);								
					continue;
				}
				if((g_checkTaskStart&CHECK_TASK_LAN)==0) {
					osDelay(100);
					continue;
				}
				TakeLan();				

				uint8_t sn_sr = getSn_SR(SN_SERVER_RX);
				switch(sn_sr) 
				{ 
				case SOCK_ESTABLISHED: 
					if(getSn_IR(SN_SERVER_RX) & Sn_IR_CON) 
					{ 
						setSn_IR(SN_SERVER_RX,Sn_IR_CON); 
					} 
					break; 
					
				case SOCK_CLOSE_WAIT: 
					if((ret=disconnect(SN_SERVER_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN] CLOSE_WAIT Disconnect error %d \r\n",ret); 
					if((ret=socket(SN_SERVER_RX,Sn_MR_TCP,pInfo->setting.nPort ,0x00)) != SN_SERVER_RX) dbg_ioTask(2, "[LAN] CLOSE_WAIT Socket error %d \r\n",ret); 
					if( (ret = listen(SN_SERVER_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN] CLOSE_WAIT Listen error %d \r\n",ret); 
					break; 
				
				case SOCK_INIT: 
					if( (ret = listen(SN_SERVER_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN] INIT Listen error %d \r\n",ret); 
					break; 
				
				case SOCK_CLOSED: 
					if((ret=socket(SN_SERVER_RX,Sn_MR_TCP,pInfo->setting.nPort,0x00)) != SN_SERVER_RX) dbg_ioTask(2, "[LAN] CLOSED Socket error %d \r\n",ret); 
					osDelay(500);
					if( (ret = listen(SN_SERVER_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN] CLOSED Listen error %d \r\n",ret); 
					osDelay(500);
					break; 
				
				case SOCK_LISTEN:
					break;
				
				default: 
					//dbg_ioTask(1, "? : %d", getSn_SR(sn));		//unknown status
					//dbg_ioTask(1, "Rx state error: %d\r\n", sn_sr);
					if(sn_sr == 243) {
					//if((ret=disconnect(sn)) != SOCK_OK) dbg_ioTask(1, "[LAN] Disconnect error %d \r\n",ret); 
						if((ret=socket(SN_SERVER_RX,Sn_MR_TCP,pInfo->setting.nPort,0x00)) != SN_SERVER_RX) dbg_ioTask(2, "[LAN] Socket error %d \r\n",ret); 
						if( (ret = listen(SN_SERVER_RX)) != SOCK_OK) dbg_ioTask(2, "[LAN] Listen error %d \r\n",ret); 
						//Ack();
					}
					break; 
				}

				if(tick%20==0) {
					//dbg_ioTask(1, "[LAN] %d\r\n", sn_sr);
				}

				if((size = getSn_RX_RSR(SN_SERVER_RX)) > 0) 
				{ 
		#if BLINK_WLINK			
			//	HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_RESET);
//					HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
		#endif
					
					if(size > DATA_BUF_SIZE) size = DATA_BUF_SIZE; 
					ret = recv(SN_SERVER_RX,lanRxBuffer,size); 	
					
					if(size>0) {
						nConnectTry = 0;	
						g_ConnectedServer=COMM_MODE_LAN;
#if SWITCH_MODE_DET
						pInfo->setting.lanLora = COMM_MODE_LAN;
#endif
					}
					
					ret = 0;
					for(i=0;i<size;i++) {
						if(lanRxBuffer[i]!=0) {
							ret = 1;
						}
					}
					if(ret==0) {
						//dbg_ioTask(1, "NULL rxMonitorMsg\r\n");
						size = 0;
						GiveUplink();	
						osDelay(5000);
						continue;
					}					
					
					dbg_ioTask(2, "\r\n[LAN] Rx %d %x\r\n", size, lanRxBuffer[6]); 
					bServerConnect = 1;
					
					for (i = 0; i < size; i++) {								
						PLC_FROM_WHERE = COMM_MODE_LAN; // Check from lan
						// Send rx bytes to plcRx buffer
						
						enQueue(lanRxBuffer[i]);
						
					}						
					memset(lanRxBuffer, 0, size);
					//ret = send(sn,(uint8_t* )"OK\r\n", 4); // respons
					//if(ret <= 0) 
					nRxErrCount = 0;
					waitLan = 0; // No more wait on RX
				} 
				else {
					nRxErrCount++;
					if(nRxErrCount>1000) {
						//dbg_ioTask(1, "No rx...while %d\r\n", nRxErrCount);
						nRxErrCount = 0;
					}
				}
				
				if(g_lanBufCount>0) {
					dbg_ioTask(3, "Have TX data %d\r\n", g_lanBufCount);
#if 0
					LanTxProc(pInfo);
#else
					TakeUplink();
					ret = send(SN_SERVER_RX,(uint8_t* )g_lanBuf, g_lanBufCount);	
					dbg_ioTask(3, "TX ret %d\r\n", ret);
					g_lanBufCount = 0;						
					GiveUplink();						
#endif
				}
				GiveLan();
				
				if(GetEscInfo()->setting.lanLora!=COMM_MODE_LAN) { // LAN Off
					break;
				}
				else osDelay(10);
			}
#endif
			
		}
	}
}

uint8_t loraPayload[TXLEN] = {0};

void TaskLora(void *argument)
{
	
	//	Send_Info temp2; 	// disabled by jwaani: to fix warnning
	int downlinkReceived = 0;
//	struct LoraMessage reqSend;	// disabled by jwaani: to fix warnning
	int32_t send_log;
	
	StructEscInfo *pInfo;
	pInfo = GetEscInfo();
	
	int i=0;
	int tick=0;
	//uint8_t sn = SN_SERVER_RX;
	uint16_t size = 0; // sentsize=0;
	uint32_t nRxErrCount = 0;
	int32_t ret;
	uint8_t lanLoraMode=0xff; // set initial 0xff
	uint8_t prevLORANODE_BUSY_STATUS;
	
	uint8_t waitLan = 0;
	static uint16_t nLoraRxLen = 0;
	uint32_t tickRxLora = HAL_GetTick();

	
	//GET_LoraNode_Info(&temp);
	g_checkTaskStart |= CHECK_TASK_LORA;	
	
	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(100);
		dbg_ioTask(9, "Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}
	
	osStatus status;
	
// check lora module
#if SWITCH_MODE_DET
	if(!DET_LORA) {			
		if(DET_LAN) {
			pInfo->setting.lanLora = COMM_MODE_LAN;
		}
		else {
			pInfo->setting.lanLora = COMM_MODE_SERIAL;
		}
		dbg_ioTask(1, "%s: No Lora Module! End task mode = %d\r\n", __FUNCTION__, pInfo->setting.lanLora);
		osDelay(5000);
		
		status = osThreadTerminate (myTaskLoraHandle);                         // stop the thread
		if (status == osOK) {
			// Thread was terminated successfully
		}
		else {
			printf("%s: Failed to terminate thread\r\n", __FUNCTION__);
			// Failed to terminate a thread
		}
	}
#endif
	
	while(1) {		
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}
		
		
		if(lanLoraMode != GetEscInfo()->setting.lanLora) { // loraOnOff state changed
			lanLoraMode = GetEscInfo()->setting.lanLora;
			
			g_LoraConnected = 0;
			
			if(g_ConnectedServer==COMM_MODE_LAN) { // Stop Lora
				osDelay(1000);				
				dbg_ioTask(1, "Skip Lora task...\r\n");
				continue;
			}								
	
			if(GetEscInfo()->setting.lanLora==COMM_MODE_LORA ||  LAN_LORA_DUAL) {	// Lora On				
				g_LoraConnected = 0;
				
				if(!DET_LORA) {
					dbg_ioTask(1, "No LORA Module!\r\n");
					osDelay(5000);
					continue;					
				}
				LoraNode_Init();

#if	LORA_USE_ABP==1
				LoraNode_ABP_Config();				nodeInfo.NET_Mode = 0;
				osDelay(10000);
				//nodeInfo.Join = 1;
				
#else
				//LoraNode_ABP_Config();				nodeInfo.NET_Mode = 0;
				
				LoraNode_OTAA_Config();				nodeInfo.NET_Mode = 1;
				
				
				i = 0;
				
				dbg_ioTask(3, "Try to join LoRa\r\n");
				dbg_ioTask(3, "Waiting for LoRa join");
				for(;;)
				{
					
					LoraNode_STA_Check(&nodeInfo);
					if (nodeInfo.Join) {			
						dbg_ioTask(3, "\r\nLoRa Joined! at %d sec...\r\n>", i/2+1);
						g_LoraConnected = 1;
						break;
					}
					if(i % 2 == 0) {
						dbg_ioTask(4, ".", i/2+1);
					}
					i++;
#if BLINK_WLINK			
					HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
#endif								
					osDelay(500);
					/*
					if(GetEscInfo()->setting.lanLora!=COMM_MODE_LORA) { // Lora Off
						dbg_ioTask(3, "LAN Mode!");
						g_LoraConnected = 0;
						break;
					}	
					*/					
					
					if(g_ConnectedServer==COMM_MODE_LAN) { // Stop Lora
						status = osThreadTerminate (myTaskLoraHandle);                         // stop the thread
						if (status == osOK) {
							// Thread was terminated successfully
						}
						else {
							printf("%s: Failed to terminate thread\r\n", __FUNCTION__);
							// Failed to terminate a thread
						}
						
						break;
					}
				}
#endif
			
				//loraPayload[0] = 1;
				
#if 1
				send_log = 1;
#define SEND_FIRST_PACK	1				
#if SEND_FIRST_PACK
				StructApolloPacket firstUplinkPack;
				U08 firstData[10] = {0xEF};
				
				StructApolloPacket *pAck = &firstUplinkPack;
				
				// Set data format...
				pAck->ST = CODE_ST;
				pAck->ST2 = CODE_ST2;
				pAck->deviceID = 0x00;
				pAck->data = (U08*) firstData;
				

				pAck->cmd = CCMD_REQUEST_ALIVE; //CCMD_ACK;
				pAck->result = CRESULT_OK;
				pAck->mid = 0x00;
				
				pAck->targetIdH = (GetEscInfo()->setting.cid)>>8;
				pAck->targetIdL = GetEscInfo()->setting.cid;
				
				// Set data...
				pAck->length = 1; 
				#if PACKET_LENGTH_INCLUDING_HEADER
				// Include header too
				pAck->length += N_PACKET_LENGTH_HEADER;  
				#endif
				
				//pAck->CRCH = EEPROM_CHECKSUM; 				pAck->CRCL = EEPROM_CHECKSUMB;
				MakeCRC(pAck, pAck->length);
				U08 crc[2];
				crc[0] = pAck->CRCH;
				crc[1] = pAck->CRCL;
								
				memcpy(loraPayload, (char*)pAck, N_DATA_POSITION);
				memcpy(loraPayload +  N_DATA_POSITION, (char*)pAck->data, 1);
				memcpy(loraPayload +  N_DATA_POSITION+1, (char*)crc, 2);
				

#endif				
				do {
#if BLINK_WLINK			
					HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
#endif					
					#if SEND_FIRST_PACK
					send_log = 1;
					send_log = LoraNode_Write_Receive_Bytes(loraPayload, 12, 1);
					//send_log = LoraNode_Write_Receive_Bytes((uint8_t*)pAck, sizeof(StructApolloPacket), 1);
					//nodeInfo.Join = 1; send_log = loraReqUplink((uint8_t*)pAck, 12, 0);
					
					#else
					send_log = LoraNode_Write_Receive_Bytes(loraPayload, 10, 1);
					#endif
					if(send_log!=1) osDelay(1000);
					//osDelay(5000);
					dbg_ioTask(3, "First uplink %d at %d\r\n>", send_log, HAL_GetTick());
					
					//osDelay(3000);
					if(g_ConnectedServer==COMM_MODE_LAN) { // Stop Lora
						break;
					}					
					
				}
				while(send_log!=1);
				
#if SWITCH_MODE_DET
				if(send_log==1 && g_LoraConnected == 1) {
					g_ConnectedServer = COMM_MODE_LORA;
					pInfo->setting.lanLora = COMM_MODE_LORA;				
				}
#endif
				
				osDelay(1000);
#endif				

				memset(AT_Data_buf,0,RXLEN);	       // clear the buffer and wPoint 
				RxIdx = 0;
				
				prevLORANODE_BUSY_STATUS = 0;
				g_LoraConnected = 1;
				nodeInfo.Join = 1;
				dbg_ioTask(3, "START LORA RX...\r\n");
				
				for(;;)
				{
					osStatus status;
					status = osThreadYield();                              // 
					if (status != osOK)  {
						// thread switch not occurred, not in a thread function
						printf("Task %s ERROR\r\n", __FUNCTION__);
					}		
					
					osDelay(1);
					LoraNode_Wake_Sleep(MODE_WAKEUP); // TEST by jwaani @20200527
					

#if LORA_TX_DIFF_TASK
					
					if(pInfo->setting.lanLora==COMM_MODE_LORA) { 
						TakeUplink();
#if BLINK_WLINK			
						HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
#endif									
						// Send data to LoRa

	
						extern U08 g_loraTxBufIndex;
						extern U08 g_loraTxSet[LORA_N_MAX_BUF_COUNT];
						extern U08 g_loraTxSizes[LORA_N_MAX_BUF_COUNT];
						extern U08 g_loraTxBufs[LORA_N_MAX_BUF_COUNT][N_PLC_TX_BUFFER_SIZE+12];
						
						for(int indexLora=0;indexLora<LORA_N_MAX_BUF_COUNT;indexLora++) {
							if(g_loraTxSizes[indexLora]>0) {
								osDelay(200); //org 500
								ret = loraReqUplink(g_loraTxBufs[indexLora], g_loraTxSizes[indexLora], LORA_TX_CONFIRM);
								if (ret != 1) {
									dbg_ioTask(2, "[LORA]Response %d %d\r\n", g_loraTxSizes[indexLora], ret);
								}
								int nLoraErrCnt=0;
								while(ret!=1) {
									osDelay(500);
									ret = loraReqUplink(g_loraTxBufs[indexLora], g_loraTxSizes[indexLora], LORA_TX_CONFIRM);
									dbg_ioTask(2, "[LORA]Response %d %d\r\n", g_loraTxSizes[indexLora], ret);
									if(nLoraErrCnt++>4) {
										dbg_ioTask(2, "[LORA]Errer TX %d %d\r\n", nLoraErrCnt, ret);
										Reboot();
									}
								}
								dbg_ioTask(2, "[LoRa Tx Done %d]\r\n", ret);
								dbg_ioTask(2, "[LORA] Rx-Tx interval %d\r\n", HAL_GetTick()-tickRxLora);
								g_loraTxSizes[indexLora] = 0;
								
								break;
							}
						}

						GiveUplink();							
					}
#endif											
					

					if (AT_Data_buf[0] > 0 && RxIdx >= (AT_Data_buf[0] + 1)) {
						downlinkReceived ++;
						dbg_ioTask(3, "\r\n[LoRa Received] %d %x %x\r\n", RxIdx, AT_Data_buf[0], AT_Data_buf[1]);
						for (i = 0; i < AT_Data_buf[0]; i++) {
							dbg_ioTask(4, "0x%02x ", AT_Data_buf[i+1]);
							
#if LORA_USE_DIRECT_BUFFER==0										
							PLC_FROM_WHERE = COMM_MODE_LORA;
							
							enQueue(AT_Data_buf[i+1]);							
#endif							
						}
						dbg_ioTask(4, "\r\n ");

					
						
#if BLINK_WLINK			
					HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
#endif							
						memset(AT_Data_buf,0,RXLEN);	       // clear the buffer and wPoint 
						RxIdx = 0;
						
					}
					else if(RxIdx>0) {
						tickRxLora = HAL_GetTick();
					}
					else if(RxIdx==0) {
						osDelay(10);
#if ACK_FOR_JOIN != 0
						if(HAL_GetTick()-tickRxLora > ACK_FOR_JOIN) {
							tickRxLora = HAL_GetTick();
							plcFromWhere = COMM_MODE_LORA;
							Ack();
						}
#endif
					}
					
					if (prevLORANODE_BUSY_STATUS == 0 && LORANODE_BUSY_STATUS == 1) {
						prevLORANODE_BUSY_STATUS = 1;
						memset(AT_Data_buf,0,RXLEN);	       // clear the buffer and wPoint 
						RxIdx = 0;
					}
					
					prevLORANODE_BUSY_STATUS = LORANODE_BUSY_STATUS;
				
					if(tick++%(500)==0) {
						//dbg_ioTask(1, "lora Rx alive %d\r\n>", tick/1000);
						//dbg_ioTask(2, "Joined? %d\r\n", LoraNode_GetJoined());
					}
					
					if(GetEscInfo()->setting.lanLora!=COMM_MODE_LORA) { // Lora Off
						break;
					}
				}								
				
			}
		}
		else {
			osDelay(200);
		}
	}
}

U08 prevRelay = 0xff; // Error FIX: Conflict of Relay on Toggled start...
void RelayControl(uint8_t value) 
{
// Off DAC when relay changed
	if(prevRelay!=value) {
//		dac7678Set(DAC_CH_CURR_1, 3474);	//2.8v curr A
//		dac7678Set(DAC_CH_CURR_2, 3474);	//2.8v curr B
		dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
		dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel  B						
		
		// Apply DAC
		UpdateDac();
		osDelay(20);
		dbg_ioTask(4, "relay Changed\r\n");
	}
	else {
		dbg_ioTask(5, "Relay no changed!\r\n");
		return;
	}
	
	if(value==0) {		// A Positive, B Negative
		dbg_ioTask(3, "relay A POS %d\r\n", value);
		
		HAL_GPIO_WritePin(nRELAY_HV_A_POS_GPIO_Port, nRELAY_HV_A_POS_Pin, GPIO_PIN_RESET);
#if USE_4_LINE_RELAY_CONTROL		
		HAL_GPIO_WritePin(nRELAY_HV_A_NEG_GPIO_Port, nRELAY_HV_A_NEG_Pin, GPIO_PIN_RESET);
#endif		
	
		HAL_GPIO_WritePin(nRELAY_HV_B_POS_GPIO_Port, nRELAY_HV_B_POS_Pin, GPIO_PIN_SET);
#if USE_4_LINE_RELAY_CONTROL				
		HAL_GPIO_WritePin(nRELAY_HV_B_NEG_GPIO_Port, nRELAY_HV_B_NEG_Pin, GPIO_PIN_SET);
#endif		
							
	}
	else  {	//  A Negative, B Positive
		dbg_ioTask(3, "relay B POS %d\r\n", value);
		
		HAL_GPIO_WritePin(nRELAY_HV_A_POS_GPIO_Port, nRELAY_HV_A_POS_Pin, GPIO_PIN_SET);
#if USE_4_LINE_RELAY_CONTROL		
		HAL_GPIO_WritePin(nRELAY_HV_A_NEG_GPIO_Port, nRELAY_HV_A_NEG_Pin, GPIO_PIN_SET);
#endif		
		
		HAL_GPIO_WritePin(nRELAY_HV_B_POS_GPIO_Port, nRELAY_HV_B_POS_Pin, GPIO_PIN_RESET);
#if USE_4_LINE_RELAY_CONTROL				
		HAL_GPIO_WritePin(nRELAY_HV_B_NEG_GPIO_Port, nRELAY_HV_B_NEG_Pin, GPIO_PIN_RESET);								
#endif		
	}

	
	prevRelay = value;
	
}

void SendDac(U08 ch, U08 b)
{
	struct OutputMessage outputMsg;
	
	int voltA = ByteToVolt(b);
	int dacValue;
	if(voltA<0) voltA = -voltA; //abs
	
	//1.9
	dacValue= ( voltA *VOLT_HV_DAC_MAX_KV/VOLT_STEP * ADC_STEP) / DAC_VREF;
	//dacValue = voltA*VOLT_MAX/4095;
	//1.111261872455902/3.3*2.7; // [kv]에서 dac 출력으로 스케일 변환 계산값. 	
	
	//dbg_ioTask(1, "dacvalue? %d %d %d\r\n",  dacValue, voltA, ByteToVolt(b));
	#if 0
	dac7678Set(ch, dacValue);	//2.7v = 3350
	
	UpdateDac();
	#else
	outputMsg.commandID = OUTPUT_DAC;
	outputMsg.channel = ch;
	outputMsg.value = dacValue; //(voltA * 4095) / voltA;
	osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
	#endif
}


int SetDacVolt(U08 ch, int voltA)
{
	//int voltA = ByteToVolt(b);
	int dacValue;
	if(voltA<0) voltA = -voltA; //abs

#if USE_HV_VOLT_TABLE
	// Dac value from table
	U08 index = (voltA/100);
	if(index>N_MAX_HV_VOLT_TABLE) {
		dbg_ioTask(1, "Index over! %d\r\n", index);
		index = N_MAX_HV_VOLT_TABLE;
	}
	
	U08 offset = g_HvVoltTableOffset[index];
	if(ch==DAC_CH_VOLT_1) {
		dacValue = tHvVoltTable[index] + offset;
	}
	else if(ch==DAC_CH_VOLT_2) {
		dacValue = tHvVoltTableB[index] + offset;
	}
	//dacValue = tHvVoltTable[index];
	
	if(dacValue > VOLT_HV_DAC_MAX_STEP) {
		dbg_ioTask(1, "DAC Step over! %d\r\n", dacValue);
		dacValue = VOLT_HV_DAC_MAX_STEP;
	}
	
	dbg_ioTask(9, "ch:%d %dV DAC %d i=%d\r\n",ch, voltA, dacValue, index);
	//dbg_ioTask(4, "%dV DAC %d i=%d offset=%d\r\n", voltA, dacValue, index, offset);	
#else	
	//1.111261872455902/3.3*2.7; // [kv]에서 dac 출력으로 스케일 변환 계산값. 			
	if(ch==DAC_CH_VOLT_1) {
		dacValue = ( voltA *VOLT_HV_DAC_MAX_KV/VOLT_STEP * ADC_STEP) / DAC_VREF;
	}
	else if(ch==DAC_CH_VOLT_2) {
		dacValue = ( voltA *VOLT_HV_DAC_MAX_KV/VOLT_STEP * ADC_STEP) / DAC_VREF + 7;
	}
	dbg_ioTask(9, "Volt %d DAC %d \r\n", voltA, dacValue);	
#endif	

	dac7678Set(ch, dacValue);	//2.7v = 3350

#if 0	
	dbg_ioTask(1, "HV Table\r\n");
	static U08 prtTable=0;
	if(prtTable==0) {
		prtTable = 1;
		int i;
		for(i=0;i<=30;i++) {
			dacValue= ( (i*100) *VOLT_HV_DAC_MAX_KV/VOLT_STEP * ADC_STEP) / DAC_VREF;
			dbg_ioTask(1, "%d, ", dacValue);
		}
		dbg_ioTask(1, "\r\n");
	}
#endif

// Init Min/Max ADC values
	g_min[0] = ADC_V_REF; g_max[0] = 0;
	g_min[1] = ADC_V_REF; g_max[1] = 0;
	g_min[2] = ADC_V_REF; g_max[2] = 0;
	g_min[3] = ADC_V_REF; g_max[3] = 0;
	
	return dacValue;
}
void UpdateDac()
{
	
#if 0	
	HAL_GPIO_WritePin(nDAC_CLR_GPIO_Port, nDAC_CLR_Pin, GPIO_PIN_RESET);
	osDelay(DAC_DELAY);
	HAL_GPIO_WritePin(nDAC_CLR_GPIO_Port, nDAC_CLR_Pin, GPIO_PIN_SET);	
	osDelay(DAC_DELAY);
#endif
	
	HAL_GPIO_WritePin(nDAC_LDAC_GPIO_Port, nDAC_LDAC_Pin, GPIO_PIN_RESET);
	osDelay(DAC_DELAY);
	HAL_GPIO_WritePin(nDAC_LDAC_GPIO_Port, nDAC_LDAC_Pin, GPIO_PIN_SET);
}
int SetDacCurr(U08 ch, U08 value)
{
//	struct OutputMessage outputMsg;
	
	int dacValue;
	int curr=value*10;
	
	if(curr>N_MAX_HV_CURR) {
		//dbg_ioTask(1, "OVERCurr? %d %d %d\r\n",  dacValue, curr, value*10);
		curr = N_MAX_HV_CURR;
	}
	if(curr<10) {
		curr = 0;
	}
	
//	dacValue=  curr * (CURR_HV_DAC_MAX*DAC_STEP/N_MAX_HV_CURR / DAC_VREF);
	dacValue = curr*DAC_STEP/DAC_VREF*CURR_HV_DAC_MAX_mA/N_MAX_HV_CURR;

	if(GetEscInfo()->stat.ocpAlarmControl == 0) 
	{
		//dbg_ioTask(1, "OCP Off %d -> %d %d %d\r\n",  dacValue, CURR_HV_DAC_MAX, curr, value*10);
		//dacValue = CURR_HV_DAC_MAX;
	}
//	dbg_ioTask(10, "dacCurr? %d %d %d\r\n",  dacValue, curr, value*10);
//	dacValue = 3400;
	dac7678Set(ch, dacValue);	//2.7v = 3350
	
	return dacValue;
}
 
void SendDacCurr(U08 ch, U08 value)
{
//	struct OutputMessage outputMsg;
	
	int dacValue;
	int curr=value*10;
	
	if(curr>2500) {
		curr = 2500;
	}
	if(curr<10) {
		curr = 10;
	}
	
	dacValue= ( curr *CURR_MAX/CURR_STEP * ADC_STEP) / DAC_VREF; ///2700*3474;
	dacValue = 3474; // TODO: current limit need fix
	//dbg_ioTask(1, "dacCurr? %d %d %d\r\n",  dacValue, curr, value*10);
	#if 1
	dac7678Set(ch, dacValue);	//2.7v = 3350
	
	UpdateDac();
	#else
	outputMsg.commandID = OUTPUT_DAC;
	outputMsg.channel = ch;
	// 2.0V Max .15
	outputMsg.value = dacValue;
	osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL);
	#endif
}
 


void HvPwrContrl(U08 onoff) {
	dbg_ioTask(3, "%s HV PWR %d\r\n", __FUNCTION__, onoff);		
	if(onoff) {
		HAL_GPIO_WritePin(DAC_HV_PWR_CONTROL_GPIO_Port, DAC_HV_PWR_CONTROL_Pin, GPIO_PIN_SET);
		//osDelay(100);
		//osDelay(100);	clcdInit();
		//osDelay(100);	OnRefreshCLcd(clcdState, pInfo);
		//waitForHvPwr = 1;
		//dbg_ioTask(1, "wait until HV PWR stable...\r\n");		

		// Shutdown dac SET -> on
		HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);

	}
	else {
		HAL_GPIO_WritePin(DAC_HV_PWR_CONTROL_GPIO_Port, DAC_HV_PWR_CONTROL_Pin, GPIO_PIN_RESET);		
		
		// Shutdown dac SET -> off
		HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);
		
	}
}


U08 hvToggleMode(StructEscInfo *pInfo, U08 onoff)
{
	
//	if(prevToggle!=onoff) {
	if(prevToggleMode != onoff) 	
	{
	//	dbg_ioTask(5, "%s %d\r\n", __FUNCTION__, onoff);		
		dbg_ioTask(1, "%s %d %d\r\n", __FUNCTION__,prevToggleMode, onoff);		
		
		if(onoff==AUTO_TOGGLE1) {
			pInfo->stat.hvAutoToggle = AUTO_TOGGLE1;
		}
		else if(onoff==AUTO_TOGGLE2) {
			pInfo->stat.hvAutoToggle = AUTO_TOGGLE2;
		}
		else if(onoff==(AUTO_TOGGLE2|AUTO_TOGGLE1)) {
			pInfo->stat.hvAutoToggle = AUTO_TOGGLE1 | AUTO_TOGGLE2;
		}
		else {
			pInfo->stat.hvAutoToggle = 0;
		}

		pInfo->volatile_stat.toggleCount++;  // count increase
		prevToggleMode = onoff;
		return 1;
	}
	else {
		dbg_ioTask(1, "%s No CHANGE %d %d\r\n", __FUNCTION__,prevToggleMode, onoff);		
		prevToggleMode = onoff;
		return 0;
	}
}

void hvOnOffDoStepUp(StructEscInfo *pInfo, uint32_t tickNow, uint8_t step)
{
	int voltA, voltB;

	if(pInfo->stat.autoDischargeStepCount<step) {
		dbg_ioTask(5, "PASS %s! next=%d %d\r\n",strHvState[hvOutState], nRampCountMax, tickNow);
		hvOutState++;
		return;
	}
	
	nRampCount = tickNow - rampStartHalTick;
	
	if(nRampCount >= nRampCountMax) {
		nRampCount = nRampCountMax;
	}
	voltA = targetVoltA*(nRampCount)/nRampCountMax;
	voltB = targetVoltA*(nRampCount)/nRampCountMax;

	if(step==1 || step==3) {	 // Reverse on step1, 3
		if(!IS_TOGGLE) {
			RelayControl(0);	
		}
		else {
			RelayControl(1);	
		}	
	}
	else {
		if(IS_TOGGLE) {
			RelayControl(0);	
		}
		else {
			RelayControl(1);	
		}	
	}
	
		
	SetDacVolt(DAC_CH_VOLT_1, voltA);
	SetDacVolt(DAC_CH_VOLT_2, voltB);
	

	UpdateDac();
	osDelay(5);
	
	dbg_ioTask(8, "%s: %d %d,%d Diff %d %d\r\n",__FUNCTION__, voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			
	
	if(nRampCount == nRampCountMax) {
		nRampCount = 0;
		rampStartHalTick = tickNow;
		targetVoltAPrev = targetVoltA;
		targetVoltBPrev = targetVoltB;
		
		dbg_ioTask(5, "DONE %s! next=%d %d\r\n",strHvState[hvOutState], nRampCountMax, tickNow);
		switch(step) {
			case 1:
			targetVoltA = pInfo->stat.autoDischargeVoltStep1;
			targetVoltB = pInfo->stat.autoDischargeVoltStep1;
			nRampCountMax = pInfo->stat.autoDischargeKeepTimeStep1*1000; // 1 Sec on Auto discharge							
//			hvOutState = HV_STATE_KEEP_1;
			break;
			case 2:
			targetVoltA = pInfo->stat.autoDischargeVoltStep2;
			targetVoltB = pInfo->stat.autoDischargeVoltStep2;
			nRampCountMax = pInfo->stat.autoDischargeKeepTimeStep2*1000; // 1 Sec on Auto discharge
//			hvOutState = HV_STATE_KEEP_2;
			break;
			case 3:
			targetVoltA = pInfo->stat.autoDischargeVoltStep3;
			targetVoltB = pInfo->stat.autoDischargeVoltStep3;
			nRampCountMax = pInfo->stat.autoDischargeKeepTimeStep3*1000; // 1 Sec on Auto discharge
//			hvOutState = HV_STATE_KEEP_3;
			break;
			
			default: // ramp down
			targetVoltA = 0;
			targetVoltB = 0;
			nRampCountMax = pInfo->stat.rampDownTime*100; 
			break;
			
		}		
		hvOutState++;
		
		
	}	
}

void hvOnOffDoStepDown(StructEscInfo *pInfo, uint32_t tickNow, uint8_t step)
{
	int voltA, voltB;
	
	if(pInfo->stat.autoDischargeStepCount<step) {
		dbg_ioTask(5, "PASS %s! next=%d %d\r\n",strHvState[hvOutState], nRampCountMax, tickNow);
		hvOutState++;
		return;
	}
	nRampCount = tickNow - rampStartHalTick;
	if(nRampCount >= nRampCountMax) {
		nRampCount = nRampCountMax;
	}
	voltA = targetVoltA*(nRampCountMax-nRampCount)/nRampCountMax;
	voltB = targetVoltB*(nRampCountMax-nRampCount)/nRampCountMax;
	SetDacVolt(DAC_CH_VOLT_1, voltA);
	SetDacVolt(DAC_CH_VOLT_2, voltB);


	UpdateDac();
	
	osDelay(5);
	dbg_ioTask(8, "%s: %d %d,%d Diff %d %d\r\n",__FUNCTION__, voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			

	if(nRampCount == nRampCountMax) {
		nRampCount = 0;
		rampStartHalTick = tickNow;
			
		dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
		dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel B						
		
		targetVoltAPrev = targetVoltA = 0;
		targetVoltBPrev = targetVoltB = 0;				
		
		// Apply DAC
		UpdateDac();
		
	
		dbg_ioTask(5, "DONE %s! %d\r\n",strHvState[hvOutState], tickNow);		
		
		nRampCountMax = pInfo->stat.rampUpTime*100; 
		switch(step) {
			case 1:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep1);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep1);
//			nRampCountMax = 1000; // 1 Sec on Auto discharge							
			hvOutState = HV_STATE_STEPUP1;
			break;
			case 2:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep2);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep2);
			hvOutState = HV_STATE_STEPUP2;
			break;
			case 3:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep3);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep3);
			hvOutState = HV_STATE_STEPUP3;
			break;
			
			default: // ramp down
			targetVoltA = 0;
			targetVoltB = 0;
			break;
			
		}
		
		dbg_ioTask(2, "Tartget Next %d  %d\r\n",targetVoltA, targetVoltB);		
	}	
}
void hvOnOffDoStepKeep(StructEscInfo *pInfo, uint32_t tickNow, uint8_t step)
{
	int voltA, voltB;
	
	if(pInfo->stat.autoDischargeStepCount<step) {
		dbg_ioTask(5, "PASS %s! next=%d %d\r\n",strHvState[hvOutState], nRampCountMax, tickNow);
		hvOutState++;
/*		
		if(step==3) {
			targetVoltAPrev = targetVoltA;
			targetVoltBPrev = targetVoltB;
			nRampCountMax = pInfo->stat.rampDownTime*100; 
			hvOutState =  HV_STATE_RAMPDOWN_START;			
		}*/
		return;
	}
	
	nRampCount = tickNow - rampStartHalTick;
			
	if(nRampCount >= nRampCountMax) {
		nRampCount = nRampCountMax;
	}
	// KEEP Step 1 Voltage
	osDelay(10);
	
	dbg_ioTask(8, "%s: %d %d,%d Diff %d %d\r\n",__FUNCTION__, targetVoltA, targetVoltB, nRampCount, tickNow-prevHalTick,  tickNow);			
	
	if(nRampCount == nRampCountMax) {
		nRampCount = 0;
		rampStartHalTick = tickNow;

		dbg_ioTask(5, "DONE %s! next=%d %d\r\n",strHvState[hvOutState], nRampCountMax, tickNow);
		switch(step) {
			case 1:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep1);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep1);
//			hvOutState =  HV_STATE_STEPDOWN2;
			break;
			case 2:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep2);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep2);
//			hvOutState =  HV_STATE_STEPDOWN3;
			break;
			case 3:
			targetVoltA = ByteToVolt(pInfo->stat.autoDischargeVoltStep3);
			targetVoltB = ByteToVolt(pInfo->stat.autoDischargeVoltStep3);
			break;
			
			default: // ramp down
//			hvOutState =  HV_STATE_RAMPDOWN_START;
			break;
			
		}
		targetVoltAPrev = targetVoltA;
		targetVoltBPrev = targetVoltB;
		hvOutState++;
		nRampCountMax = pInfo->stat.rampDownTime*100; 
		
	}	
}
void hvOnOffAlarm(StructEscInfo *pInfo, uint32_t tickNow)
{
	hvOutState =  HV_STATE_READY ;
	HvQlight(0);
	nRampCountMax = 0;
	HvPwrContrl(0);
	
	
	dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
	dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel B						
	SetDacCurr(DAC_CH_CURR_1, 0); 
	SetDacCurr(DAC_CH_CURR_2, 0); 
	// Apply DAC
	UpdateDac();
	
	targetVoltAPrev = targetVoltA = 0;
	targetVoltBPrev = targetVoltB = 0;

	bRampDone = 1;				
	dbg_ioTask(2, "%s OFF by ALARM! %d\r\n",__FUNCTION__, tickNow);	
}

void hvOnOffDoRampStart(StructEscInfo *pInfo, uint32_t tickNow)
{
	hvOutState =  HV_STATE_RAMPUP;
	HvQlight(1);
	/*
	if(pInfo->stat.hvAutoToggle&AUTO_TOGGLE1) {
		if(hvAutoToggle1Done==0) {
			hvAutoToggle1Done = 1;
			swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);				
			dbg_ioTask(2, "HV_STATE_RAMPUP auto toggle %d\r\n", IS_TOGGLE);
		}
	}
	*/
	
	if(IS_TOGGLE) {
		RelayControl(0);	
	}
	else {
		RelayControl(1);	
	}

	dacCurr = SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
	dacCurr = SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);

	// Shutdown dac SET -> ON
	HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
	dbg_ioTask(3, "DAC_SHDN SET\r\n");
	
	// 12V DC-DC On		
	HvPwrContrl(1);			
	targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
	targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);
	nRampCount = 0;			
	nRampCountMax = pInfo->stat.rampUpTime*100; // *100 mSec
	bRampDone = 0;
	rampStartHalTick = tickNow;

}

void hvOnOffDoRampDown(StructEscInfo *pInfo, uint32_t tickNow)
{
	int voltA, voltB;

	nRampCount = tickNow - rampStartHalTick;
			
	if(nRampCount >= nRampCountMax) {
		nRampCount = nRampCountMax;
	}
	voltA = targetVoltAPrev*(nRampCountMax-nRampCount)/nRampCountMax;
	voltB = targetVoltBPrev*(nRampCountMax-nRampCount)/nRampCountMax;
	SetDacVolt(DAC_CH_VOLT_1, voltA);
	SetDacVolt(DAC_CH_VOLT_2, voltB);
	UpdateDac();
	
	dbg_ioTask(7, "%s %d %d,%d Diff %d %d\r\n",__FUNCTION__, voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			

	if(nRampCount == nRampCountMax) {
		if( (pInfo->stat.hvAutoToggle&AUTO_TOGGLE2) && hvOutState ==  HV_STATE_RAMPUP_AT2_DOWN) {
			hvOutState =  HV_STATE_ON_START;
			swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
			dbg_ioTask(2, "AT2_DOWN Done \r\n");
			bRampDone = 0;
		}
		else {
			hvOutState =  HV_STATE_READY;
			HvQlight(0);
			nRampCountMax = 0;
			HvPwrContrl(0);
			dbg_ioTask(2, "OFF-DONE! %d\r\n", tickNow);
			bRampDone = 1;				
			SetDacCurr(DAC_CH_CURR_1, 0); 
			SetDacCurr(DAC_CH_CURR_2, 0); 
			// Apply DAC
			UpdateDac();
		}
		
		// Shutdown dac SET -> off
		HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);
		dbg_ioTask(3, "DAC_SHDN RESET\r\n");
		
		dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
		dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel B						
//		SetDacCurr(DAC_CH_CURR_1, 0); 
//		SetDacCurr(DAC_CH_CURR_2, 0); 
		// Apply DAC
		UpdateDac();
		
		targetVoltAPrev = targetVoltA = 0;
		targetVoltBPrev = targetVoltB = 0;
			
	}		
}

void hvOnOffDoRampUp(StructEscInfo *pInfo, uint32_t tickNow)
{
	int voltA, voltB;
	int dacA, dacB;
	nRampCount = tickNow - rampStartHalTick;
	
	if(nRampCount >= nRampCountMax) {
		nRampCount = nRampCountMax;
	}
	voltA = targetVoltA*(nRampCount)/nRampCountMax;
	voltB = targetVoltB*(nRampCount)/nRampCountMax;
	dacA = SetDacVolt(DAC_CH_VOLT_1, voltA);
	dacB = SetDacVolt(DAC_CH_VOLT_2, voltB);


	UpdateDac();
	
	targetVoltAPrev = targetVoltA;
	targetVoltBPrev = targetVoltB;
	
	dbg_ioTask(7, "UP %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			
	
	if(nRampCount == nRampCountMax) {
		nRampCountMax = 0;
		
		targetVoltAPrev = targetVoltA;
		targetVoltBPrev = targetVoltB;
		//dbg_ioTask(1, "RAMP UP DONE! %d %d %d %d\r\n", voltA, voltB, tickNow, bRampDone);
		
		if( (pInfo->stat.hvAutoToggle&AUTO_TOGGLE2) && (hvOutState == HV_STATE_RAMPUP_AT2_UP)) {
		dbg_ioTask(2, "UP-DONE AT2! %d(%d, %d, %dmV) %d(%d, %d, %dmV) \r\n\tocp %d(%d, %dmV) tick %d\r\n",
			targetVoltA,pInfo->stat.hvSetVolt1, dacA, dacA*DAC_VREF/DAC_STEP,
			targetVoltB, pInfo->stat.hvSetVolt2, dacB, dacB*DAC_VREF/DAC_STEP,
			pInfo->stat.ocpAlarmLimit, dacCurr, dacCurr*DAC_VREF/DAC_STEP, tickNow);
			hvOutState = HV_STATE_RAMPUP_AT2_UP_DONE;
			bRampDone = 0;
		}
		else {
			dbg_ioTask(2, "UP-DONE! %d(%d, %d, %dmV) %d(%d, %d, %dmV) \r\n\tocp %d(%d, %dmV) tick %d\r\n",
				targetVoltA,pInfo->stat.hvSetVolt1, dacA, dacA*DAC_VREF/DAC_STEP,
				targetVoltB, pInfo->stat.hvSetVolt2, dacB, dacB*DAC_VREF/DAC_STEP,
				pInfo->stat.ocpAlarmLimit, dacCurr, dacCurr*DAC_VREF/DAC_STEP, tickNow);
			hvOutState =  HV_STATE_READY ; //HV_STATE_RAMPUP_DONE;
			bRampDone = 1;			
		}
		/*
		if( (pInfo->stat.hvAutoToggle&AUTO_TOGGLE2)) {
			if( hvAutoToggle2Done==0 ) {
				dbg_ioTask(1, "AT2! \r\n");
				hvAutoToggle2Done = 1;
				
				swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
			}			
			else  {
				hvAutoToggle2Done = 0;
				
			}
			
			
		}		
		else {
		}
		*/
	}	
}


U08 IsAlarm(U08 control) 
{
	StructEscInfo *pInfo = GetEscInfo();	
	
	U08 alarmControl = ((pInfo->stat.ocpAlarmControl)<<2) | ((pInfo->stat.arcAlarmControl) <<1 
				| (pInfo->stat.battAlarmControl) );
	
	U08 alarmStatus = pInfo->measure.ocpAlarmStatus<<2 | pInfo->measure.arcAlarmStatus<<1 | pInfo->measure.battLowAlarmStatus;	
	
	if(control) {	
		return (alarmControl & alarmStatus);
	}
	else {
		return alarmStatus;
	}
}

U08 hvOnOffRamp(U08 onoff, int rampCount)
{
	U08 ret = 0;
	StructEscInfo *pInfo = GetEscInfo();	
	int voltA, voltB;
	uint32_t tickNow = HAL_GetTick();
	int dacA, dacB;
	
	//dbg_ioTask(1, "%d %s %d\r\n", hvOutState, strHvState[hvOutState], tickNow);

	//dbg_ioTask(1, "HV_OUT_ONOFF %s %d %d %d\r\n",__func__, onoff , pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
	if(onoff==0) { // Off			
		if(hvOutState ==  HV_STATE_OFF_START) {		
			hvAutoToggle2Done = 0;			
			hvAutoToggle1Done = 0;

			
			if(pInfo->stat.autoDischargeStepCount) {
				hvOutState =  HV_STATE_STEPDOWN1;
				targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
				targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);

				//nRampCountMax = 1000; // 1 Sec on Auto discharge
				nRampCountMax = pInfo->stat.rampDownTime*100; // *100 mSec
				OnRemoteCLcd(CLCD_STATE_AUTO_DISCHARGING,pInfo);
				dbg_ioTask(1, "Start HV_STATE_STEPDOWN1 %d\r\n", nRampCountMax);
			}
			else {
				hvOutState =  HV_STATE_RAMPDOWN_START;
				targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
				targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);

				nRampCountMax = pInfo->stat.rampDownTime*100; // *100 mSec
				dbg_ioTask(1, "Start RAMPDOWN %d\r\n", nRampCountMax);
			}
			
			nRampCount = 0;			
			bRampDone = 0;
			rampStartHalTick = tickNow;
		}
		else if(hvOutState ==  HV_STATE_STEPDOWN1) {
			hvOnOffDoStepDown(pInfo, tickNow, 1);
		}
		else if(hvOutState ==  HV_STATE_STEPUP1) {
			hvOnOffDoStepUp(pInfo, tickNow, 1);
		}
		else if(hvOutState ==  HV_STATE_KEEP_1) {
			hvOnOffDoStepKeep(pInfo, tickNow, 1);		
		}
		else if(hvOutState ==  HV_STATE_STEPDOWN2) {
			hvOnOffDoStepDown(pInfo, tickNow, 2);			
		}
		else if(hvOutState ==  HV_STATE_STEPUP2) {
			hvOnOffDoStepUp(pInfo, tickNow, 2);			
		}
		else if(hvOutState ==  HV_STATE_KEEP_2) {
			hvOnOffDoStepKeep(pInfo, tickNow, 2);
		}
		else if(hvOutState ==  HV_STATE_STEPDOWN3) {
			hvOnOffDoStepDown(pInfo, tickNow, 3);
		}
		else if(hvOutState ==  HV_STATE_STEPUP3) {
			hvOnOffDoStepUp(pInfo, tickNow, 3);		
		}		
		else if(hvOutState ==  HV_STATE_KEEP_3) {
			hvOnOffDoStepKeep(pInfo, tickNow, 3);
		}		
		else if(hvOutState ==  HV_STATE_RAMPDOWN_START) 
		{
			hvOnOffDoRampDown(pInfo, tickNow);			
		}
		else if(hvOutState == HV_STATE_RAMPDOWN_DONE) {
			hvOutState = HV_STATE_READY;
			nRampCount = 0;			
			bRampDone = 1;
			dbg_ioTask(1, "FORCE READY\r\n");
		}
	}
	else { //On		Ramp up
		if(hvOutState ==  HV_STATE_RAMPUP_AT2_UP_START) {
			if( (pInfo->stat.hvAutoToggle&AUTO_TOGGLE2)) {
				if(pInfo->stat.hvAutoToggle&AUTO_TOGGLE1) {
					if(hvAutoToggle1Done==0) {
						hvAutoToggle1Done = 1;
						swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);				
						dbg_ioTask(2, "HV_STATE_RAMPUP auto toggle %d\r\n", IS_TOGGLE);
					}
				}
				
				hvOutState =  HV_STATE_RAMPUP_AT2_UP;			
				if(IS_TOGGLE) {
					RelayControl(0);
				}
				else {
					RelayControl(1);	
				}
				dacCurr = SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
				dacCurr = SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);

				// Shutdown dac SET -> ON
				HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
				dbg_ioTask(3, "DAC_SHDN SET\r\n");
				
				// 12V DC-DC On		
				HvPwrContrl(1);			
				targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
				targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);
				nRampCount = 0;			
				nRampCountMax = 1*100; // *100 mSec
				bRampDone = 0;
				rampStartHalTick = tickNow;	
				dbg_ioTask(7, "Start RAMPUP AT2 %d\r\n", nRampCountMax);				
			}
			else {
				hvOutState =  HV_STATE_ON_START;
				if(pInfo->stat.hvAutoToggle&AUTO_TOGGLE1) {
					if(hvAutoToggle1Done==0) {
						hvAutoToggle1Done = 1;
						swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);				
						dbg_ioTask(2, "HV_STATE_RAMPUP auto toggle %d\r\n", IS_TOGGLE);
					}
				}
				
				hvOnOffDoRampStart(pInfo, tickNow);
			}
		}
		else if(hvOutState ==  HV_STATE_RAMPUP_AT2_UP) {			
			hvOnOffDoRampUp(pInfo, tickNow);			
		}
		else if(hvOutState ==  HV_STATE_RAMPUP_AT2_UP_DONE) {
			// Start ramp down AT2
			hvOutState =  HV_STATE_RAMPUP_AT2_DOWN;
			targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
			targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);

			nRampCountMax = 1*100; // *100 mSec
			dbg_ioTask(7, "Start RAMPDOWN AT2 %d\r\n", nRampCountMax);
			
			nRampCount = 0;			
			bRampDone = 0;
			rampStartHalTick = tickNow;			
		}
		else if(hvOutState ==  HV_STATE_RAMPUP_AT2_DOWN) {
			hvOnOffDoRampDown(pInfo, tickNow);
		}
		else if(hvOutState ==  HV_STATE_ON_START) {
			hvOnOffDoRampStart(pInfo, tickNow);
			/*
			hvOutState =  HV_STATE_RAMPUP;
			HvQlight(1);
			
			if(pInfo->stat.hvAutoToggle&AUTO_TOGGLE1) {
				if(hvAutoToggle1Done==0) {
					hvAutoToggle1Done = 1;
					swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);				
					dbg_ioTask(2, "HV_STATE_RAMPUP auto toggle %d\r\n", IS_TOGGLE);
				}
			}
			
			if(IS_TOGGLE) {
				RelayControl(0);	
			}
			else {
				RelayControl(1);	
			}

			dacCurr = SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
			dacCurr = SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);

			// Shutdown dac SET -> ON
			HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
			dbg_ioTask(3, "DAC_SHDN SET\r\n");
			
			// 12V DC-DC On		
			HvPwrContrl(1);			
			targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
			targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);
			nRampCount = 0;			
			nRampCountMax = pInfo->stat.rampUpTime*100; // *100 mSec
			bRampDone = 0;
			rampStartHalTick = tickNow;
			*/
		}
		else if(hvOutState ==  HV_STATE_RAMPUP) {	
			hvOnOffDoRampUp(pInfo, tickNow);		

		}
	}	

	//dbg_ioTask(1, " %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);
	
	ret = nRampCount==nRampCountMax?1:0;	

	
	prevHalTick = tickNow;
	
	return ret;
}

/**
출력 중 Toggle, up/down -> 600 mSec
*/
void changeHvToggleRamp(StructEscInfo *pInfo, int rampCount)
{
	int voltA, voltB;
	int dacA, dacB;
	uint32_t tickNow = HAL_GetTick();
	static int nRampDownCount = 1;
	static int nRampUpCount = 1;

	if(rampCount==0) {			

		targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
		targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);
		nRampCount = 0;			
		//nRampCountMax = HV_CHANGE_TIME * 2; // Set 300*2 mSec for Toggle
		nRampDownCount = pInfo->stat.rampDownTime*100+1; // +1 => Prevent 0 devide
		nRampUpCount = pInfo->stat.rampUpTime*100+1;
		nRampCountMax = nRampDownCount + nRampUpCount; // Set ramp down mSec for Toggle *100
		bRampToggleDone = 0;
		rampStartHalTick = tickNow;
		bRampToggleRelayChange = 1;
	}
	else {
		nRampCount = tickNow - rampStartHalTick;
		
		if(nRampCount >= nRampCountMax) {
			nRampCount = nRampCountMax;
		}
		
		//if(rampCount<HV_CHANGE_TIME) { // ramp down to 0
		if(rampCount<nRampDownCount) { // ramp down to 0
//			voltA = (targetVoltAPrev)*(nRampCountMax/2-nRampCount)/(nRampCountMax/2);
//			voltB = (targetVoltAPrev)*(nRampCountMax/2-nRampCount)/(nRampCountMax/2);
			voltA = (targetVoltAPrev)*(nRampDownCount-nRampCount)/(nRampDownCount);
			voltB = (targetVoltAPrev)*(nRampDownCount-nRampCount)/(nRampDownCount);
			SetDacVolt(DAC_CH_VOLT_1, voltA);
			SetDacVolt(DAC_CH_VOLT_2, voltB);
			SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
			SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
			UpdateDac();
			
			dbg_ioTask(8, "T-DN %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			

		}		
		else { // ramp up
			if(bRampToggleRelayChange) {
				bRampToggleRelayChange = 0;
				dbg_ioTask(7, "T-Zero %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			
				
				if(pInfo->stat.hvAutoToggle & AUTO_TOGGLE1) {
					if(hvAutoToggle1Done==0) {
						hvAutoToggle1Done = 1;
						swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
//					hvToggleMode(pInfo,AUTO_TOGGLE1); //when auto toggle					
						dbg_ioTask(2, "T-Zero auto toggle %d\r\n", IS_TOGGLE);					
					}
				}				
				if(IS_TOGGLE) {
					RelayControl(0);	
				}
				else {
					RelayControl(1);	
				}
				dbg_ioTask(7, "Relay Changed\r\n");

			}
			voltA = (targetVoltA)*(nRampUpCount-((nRampCountMax)-nRampCount))/(nRampUpCount);
			voltB = (targetVoltB)*(nRampUpCount-((nRampCountMax)-nRampCount))/(nRampUpCount);
			dacA = SetDacVolt(DAC_CH_VOLT_1, voltA);
			dacB = SetDacVolt(DAC_CH_VOLT_2, voltB);
			
			SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
			SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
			UpdateDac();
			
			dbg_ioTask(8, "T-UP %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			
			
			if(nRampCount == nRampCountMax) {
				voltA = (targetVoltA);
				voltB = (targetVoltB);
				dacA = SetDacVolt(DAC_CH_VOLT_1, voltA);
				dacB = SetDacVolt(DAC_CH_VOLT_2, voltB);
				
				SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
				SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
				UpdateDac();
				
				nRampCountMax = 0;
				bRampToggleDone = 1;
				targetVoltAPrev = targetVoltA;
				targetVoltBPrev = targetVoltB;
				
				dbg_ioTask(2, "Toggle-DONE! %d(%d, %d, %dmV) %d(%d, %d, %dmV) ocp %d tick %d\r\n",
					targetVoltA,pInfo->stat.hvSetVolt1, dacA, dacA*DAC_VREF/ADC_STEP,
					targetVoltB, pInfo->stat.hvSetVolt2, dacB, dacB*DAC_VREF/ADC_STEP,
					pInfo->stat.ocpAlarmLimit, tickNow);
				
			}
		}	
	}
	prevHalTick = tickNow;
}

/**
출력 중 변경은 최소 ramp time = 300 mSec 적용
*/
void changeHvOutRamp(StructEscInfo *pInfo, int rampCount)
{
	int deltaV = 0;
	int voltA, voltB;
	int dacA, dacB;
	uint32_t tickNow = HAL_GetTick();
	
	if(rampCount==0) {			
		if(targetVoltA != ByteToVolt(pInfo->stat.hvSetVolt1)) {
			deltaV = ByteToVolt(pInfo->stat.hvSetVolt1) - targetVoltA;
			targetVoltA = ByteToVolt(pInfo->stat.hvSetVolt1);
		}
		if(targetVoltB != ByteToVolt(pInfo->stat.hvSetVolt2)) {
			deltaV = ByteToVolt(pInfo->stat.hvSetVolt2) - targetVoltB;
			targetVoltB = ByteToVolt(pInfo->stat.hvSetVolt2);
		}
		nRampCount = 0;			
		//nRampCountMax = HV_CHANGE_TIME; // Set 300 mSec for change volt
		if(deltaV < 0) {
			nRampCountMax = pInfo->stat.rampDownTime*100;
		}
		else {
			nRampCountMax = pInfo->stat.rampUpTime*100;
		}
		bRampChangeDone = 0;
		rampStartHalTick = tickNow;
	}
	else {	
		nRampCount = tickNow - rampStartHalTick;
		
		if(nRampCount >= nRampCountMax) {
			nRampCount = nRampCountMax;
		}
		voltA = targetVoltAPrev + (targetVoltA-targetVoltAPrev)*(nRampCount)/nRampCountMax;
		voltB = targetVoltBPrev + (targetVoltB-targetVoltBPrev)*(nRampCount)/nRampCountMax;
		dacA = SetDacVolt(DAC_CH_VOLT_1, voltA);
		dacB = SetDacVolt(DAC_CH_VOLT_2, voltB);
		SetDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
		SetDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
		UpdateDac();
		
		
		dbg_ioTask(8, "CH %d %d,%d Diff %d %d\r\n", voltA, voltB, nRampCount, tickNow-prevHalTick,  tickNow);			
		
		if(nRampCount == nRampCountMax) {
			nRampCountMax = 0;
			bRampChangeDone = 1;
			targetVoltAPrev = targetVoltA;
			targetVoltBPrev = targetVoltB;
			
			
			dbg_ioTask(2, "Change-DONE! %d(%d, %d, %dmV) %d(%d, %d, %dmV) ocp %d tick %d\r\n",
					targetVoltA,pInfo->stat.hvSetVolt1, dacA, dacA*DAC_VREF/ADC_STEP,
					targetVoltB, pInfo->stat.hvSetVolt2, dacB, dacB*DAC_VREF/ADC_STEP,
					pInfo->stat.ocpAlarmLimit, tickNow);
		}
	}
	prevHalTick = tickNow;
}

void hvOnOff(U08 onoff)
{
	StructEscInfo *pInfo = GetEscInfo();	
	
	HvQlight(onoff);
	
	dbg_ioTask(2, "HV_OUT_ONOFF %s %d %d %d\r\n",__func__, onoff , pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
	if(onoff==0) { // Off		
#if HVPWR_ALWAYS_ON==0		
		HvPwrContrl(0);
#endif
		
		// Shutdown dac SET -> off
		HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);
		dbg_ioTask(3, "DAC_SHDN RESET\r\n");
		
		//dac7678Set(DAC_CH_CURR_1, 0);	//2.8v curr A
		//dac7678Set(DAC_CH_CURR_2, 0);	//2.8v curr B
		dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
		dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel  B						
		
		// Apply DAC
		UpdateDac();

	}
	else { //On		
		//IS_TOGGLE = pInfo->stat.hvSetVolt1&0x80?1:0;		
		if(pInfo->stat.hvAutoToggle&AUTO_TOGGLE1) {
			swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);

			dbg_ioTask(2, "HV_OUT_ONOFF auto toggle %d\r\n", IS_TOGGLE);
		}
		if(IS_TOGGLE) {
			RelayControl(0);	
		}
		else {
			RelayControl(1);	
		}
		dbg_ioTask(2, "Relay on\r\n");


		// Off DAC when relay changed
		if(prevRelay!=IS_TOGGLE) {
			dac7678Set(DAC_CH_CURR_1, 3474);	//2.8v curr A to MAX
			dac7678Set(DAC_CH_CURR_2, 3474);	//2.8v curr B to MAX
			dac7678Set(DAC_CH_VOLT_1, 0);	//2.7v = 3350 channel A
			dac7678Set(DAC_CH_VOLT_2, 0);	//2.7v = 3350 channel  B						
			
			// Apply DAC
			UpdateDac();
			dbg_ioTask(2, "relay Changed\r\n");
		}
		// Shutdown dac SET -> ON
		HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
		dbg_ioTask(3, "DAC_SHDN SET\r\n");
		//osDelay(100);
		
		// 12V DC-DC On		
#if HVPWR_ALWAYS_ON==0		
		HvPwrContrl(1);
		
		
#endif

		SendDac(DAC_CH_VOLT_1, pInfo->stat.hvSetVolt1);
		SendDac(DAC_CH_VOLT_2, pInfo->stat.hvSetVolt2);
		SendDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
		SendDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
		dbg_ioTask(3, "DAC set\r\n");
		
	}
}

#define	USE_AVERAGE_FILTER	1
void AverageInit(int value, int arr[N_AVERAGE_COUNT])
{
	uint8_t i;
	
	for(i=0;i<N_AVERAGE_COUNT;i++) {
		arr[i] = value;
	}
}

int AverageFilter(int arr[N_AVERAGE_COUNT])
{
	long sum = 0;
	int ret;
	uint8_t i;
#if SKIP_MIN_MAX
	int min, max;
	uint8_t nMinMax = 0;
	
	min = ADC_V_REF;
	max = 0;
	for(i=0;i<N_AVERAGE_COUNT;i++) {
		if(arr[i] < min) {
			min = arr[i];
		}
		if(arr[i] > max) {
			max = arr[i];
		}
	}
	for(i=0;i<N_AVERAGE_COUNT;i++) {
		if( arr[i] == min || arr[i] == max) {
			nMinMax++;
			continue;
		}
		sum += arr[i];
	}
		
	ret = sum/(N_AVERAGE_COUNT-nMinMax);
//	dbg_ioTask(10, "%d Skip min/max %d %d, %d\r\n",ret,  min, max, nMinMax);
	
#else
	for(i=0;i<N_AVERAGE_COUNT;i++) {
		sum += arr[i];
	}
		
	ret = sum/(N_AVERAGE_COUNT);
#endif
	
	return ret;
}

#if USE_AVERAGE_FILTER
	int calVoltA[N_AVERAGE_COUNT];
	int calVoltB[N_AVERAGE_COUNT];
	int calCurrA[N_AVERAGE_COUNT];
	int calCurrB[N_AVERAGE_COUNT];
	
	int battVolt[N_AVERAGE_COUNT];
	int battCurr[N_AVERAGE_COUNT];
	int battChargeCurr[N_AVERAGE_COUNT];
	int apVoltSet[N_AVERAGE_COUNT];
#else
	long calVoltA=0, calVoltB=0, calCurrA=0, calCurrB=0, countSum=0;
	
#endif

#define	N_MAX_CURRENT_TABLE	31		
const int currToggleOffRangeTableA[N_MAX_CURRENT_TABLE][2] = {
			{0, 320},
{320, 400},
{400, 400},
{480, 480},
{480, 560},
{640, 720},
{640, 720},
{720, 800},
{800, 880},
{880, 960},
{960, 1040},
{1040, 1120},
{1120, 1200},
{1200, 1280},
{1360, 1440},
{1360, 1440},
{1440, 1520},
{1520, 1600},
{1600, 1680},
{1680, 1760},
{1760, 1840},
{1840, 1920},
{1920, 2000},
{2000, 2080},
{2080, 2160},
{2160, 2240},
{2160, 2240},
{2320, 2400},
{2320, 2400},
{2400, 2480},
{2560, 2560}

		};
		const int currToggleOffTableA[N_MAX_CURRENT_TABLE] = {
0,
3 ,
7 ,
10 ,
13 ,
17 ,
20 ,
23 ,
27 ,
30 ,
33 ,
36 ,
40 ,
43 ,
46 ,
50 ,
53 ,
56 ,
59 ,
63 ,
66 ,
69 ,
72 ,
75 ,
79 ,
82 ,
86 ,
89 ,
92 ,
96 ,
99 
		};
		
		const int currToggleOnRangeTableA[N_MAX_CURRENT_TABLE][2] = {
{240, 240},
{320, 320},
{480, 560},
{560, 640},
{720, 720},
{800, 880},
{880, 960},
{1040, 1120},
{1120, 1200},
{1280, 1360},
{1360, 1440},
{1520, 1600},
{1600, 1680},
{1680, 1760},
{1760, 1840},
{1920, 2000},
{2000, 2080},
{2080, 2160},
{2240, 2320},
{2320, 2400},
{2400, 2480},
{2480, 2560},
{2560, 2640},
{2560, 2640},
			
		};
		const int currToggleOnTableA[N_MAX_CURRENT_TABLE] = {
0 ,
4 ,
8 ,
13 ,
17 ,
22 ,
26 ,
30 ,
35 ,
39 ,
44 ,
48 ,
52 ,
57 ,
61 ,
66 ,
70 ,
74 ,
79 ,
83 ,
88 ,
92 ,
96 ,
99 ,

		};
		
		const int currToggleOffRangeTableB[N_MAX_CURRENT_TABLE][2] = {
{0, 360},
{360, 360},
{360, 440},
{440, 520},
{520, 520},
{520, 600},
{680, 680},
{680, 760},
{760, 840},
{920, 1000},
{920, 1000},
{1080, 1080},
{1080, 1160},
{1240, 1240},
{1240, 1320},
{1320, 1400},
{1400, 1480},
{1480, 1560},
{1560, 1640},
{1640, 1720},
{1800, 1720},
{1800, 1880},
{1880, 1960},
{1960, 1960},
{2040, 2120},
{2120, 2200},
{2200, 2200},
{2280, 2360},
{2360, 2440},
{2440, 2440},
{2520, 2520}
		};
		const int currToggleOffTableB[N_MAX_CURRENT_TABLE] = {
0 ,
3 ,
6 ,
10 ,
13 ,
16 ,
20 ,
23 ,
26 ,
29 ,
33 ,
36 ,
39 ,
42 ,
46 ,
49 ,
52 ,
56 ,
59 ,
62 ,
65 ,
69 ,
72 ,
75 ,
78 ,
82 ,
85 ,
88 ,
92 ,
95 ,
98 ,


		};
		
		const int currToggleOnRangeTableB[N_MAX_CURRENT_TABLE][2] = {
{120, 200},
{280, 280},
{360, 440},
{440, 520},
{600, 680},
{680, 760},
{760, 840},
{920, 1000},
{1000, 1080},
{1160, 1160},
{1240, 1320},
{1320, 1400},
{1400, 1480},
{1560, 1640},
{1670, 1720},
{1720, 1800},
{1800, 1880},
{1960, 2040},
{2040, 2120},
{2120, 2200},
{2280, 2280},
{2360, 2440},
{2440, 2520},
{2520, 2600},
		};
		const int currToggleOnTableB[N_MAX_CURRENT_TABLE] = {
0 ,
5 ,
9 ,
14 ,
18 ,
23 ,
28 ,
32 ,
37 ,
41 ,
46 ,
50 ,
55 ,
60 ,
64 ,
69 ,
73 ,
78 ,
82 ,
87 ,
92 ,
96 ,
98 ,
98 ,
		};
		

#define	N_MAX_VOLTAGE_TABLE	31	//84
const int voltToggleOffRangeTableA[N_MAX_VOLTAGE_TABLE][2] = {	
{0, 40},
{40, 90},
{100, 163},
{210, 255},
{300, 339},
{359, 425}, // 500
{450, 505},
{520, 595},
{605, 674},
{710, 761},
{800, 855}, // 1000
{880, 933},
{945, 1015},
{1030, 1105},
{1125, 1173},
{1210, 1290}, // 1500
{1300, 1350},
{1350, 1439},
{1450, 1525},
{1530, 1629},
{1635, 1685}, // 2000
{1699, 1783},
{1803, 1867},
{1870, 1953},
{1923, 2045},
{2045, 2113}, // 2500
{2113, 2172},
{2210, 2275},
{2285, 2355},
{2365, 2443},
{2445, 2486}
};	
const int voltToggleOffTableA[N_MAX_VOLTAGE_TABLE] = {	
0 ,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30

};		
const int voltToggleOnRangeTableA[N_MAX_VOLTAGE_TABLE][2] = {	
{0, 40},
{40, 90},
{100, 166},
{166, 258},
{248, 350},
{330, 415}, // 500
{415, 505},
{499, 590},
{585, 675},
{664, 760},
{753, 845}, // 1000
{835, 925},
{920, 1008},
{1008, 1097},
{1130, 1189},
{1199, 1260}, // 1500
{1310, 1344},
{1364, 1430},
{1446, 1510},
{1524, 1599},
{1610, 1705}, // 2000
{1709, 1765},
{1805, 1861},
{1851, 1930},
{1946, 2016},
{2030, 2104}, // 2500
{2110, 2185},
{2205, 2271},
{2280, 2360},
{2350, 2450},
{2453, 2600}
};	
const int voltToggleOnTableA[N_MAX_VOLTAGE_TABLE] = {	
0,
 1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30,

};
		
const int voltToggleOffRangeTableB[N_MAX_VOLTAGE_TABLE][2] = {	
{0, 40},
{40, 116},
{116, 181},
{181, 263},
{305, 345},
{375, 441}, //500
{451, 505},
{550, 599},
{630, 691},
{721, 773},
{805, 851}, // 1000
{861, 932},
{952, 1015},
{1030, 1099},
{1131, 1200},
{1210, 1273}, // 1500
{1303, 1359},
{1385, 1431},
{1451, 1505},
{1520, 1610},
{1620, 1699}, // 2000
{1710, 1777},
{1802, 1847},
{1855, 1939},
{1955, 2025},
{2035, 2115}, //2500
{2133, 2195},
{2215, 2280},
{2295, 2351},
{2355, 2435},
{2455, 2600}
};	
const int voltToggleOffTableB[N_MAX_VOLTAGE_TABLE] = {	
0 ,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30
};		
const int voltToggleOnRangeTableB[N_MAX_VOLTAGE_TABLE][2] = {	
{0, 40},
{61, 116},
{156, 181},
{229, 258},
{327, 345},
{335, 421}, // 500v
{451, 520},
{558, 610},
{645, 693},
{693, 763},
{763, 845}, // 1000v
{832, 921},
{921, 1004},
{996, 1090},
{1075, 1170},
{1157, 1258}, // 1500
{1249, 1340},
{1328, 1420},
{1403, 1510},
{1490, 1595}, 
{1572, 1670}, // 2000
{1660, 1760},
{1746, 1848},
{1828, 1928},
{1908, 2010},
{1986, 2090}, // 2500
{2063, 2199},
{2158, 2272},
{2222, 2345},
{2316, 2439},
{2450, 2600}
};	
const int voltToggleOnTableB[N_MAX_VOLTAGE_TABLE] = {	
 0,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
17,
18,
19,
20,
21,
22,
23,
24,
25,
26,
27,
28,
29,
30
};
		

void GetCurrentFromTable(StructEscInfo *pInfo, 
	const int pTableA[] ,	const int ppRangeTableA[N_MAX_CURRENT_TABLE][2], 
	const int pTableB[], const int ppRangeTableB[N_MAX_CURRENT_TABLE][2])
{
	int i;
	uint8_t bFoundRange;
	int currA, currB;
	
	currA = 0;
	bFoundRange  = 0;
	for(i=0;i<N_MAX_CURRENT_TABLE;i++) {
		if( g_measure_C1 >= ppRangeTableA[i][0] 
			&& g_measure_C1 <= ppRangeTableA[i][1] ) 
		{
			currA = pTableA[i];
			bFoundRange  = 1;
			break;
		}
	}
	if(bFoundRange==0) {
		for(i=0;i<N_MAX_CURRENT_TABLE-1;i++) {
			if( g_measure_C1 >= ppRangeTableA[i][0] 
				&& g_measure_C1 <= ppRangeTableA[i+1][0] ) 
			{
				currA = pTableA[i];
				bFoundRange  = 1;
				break;
			}
		}
		if(bFoundRange==0) {
			if(g_measure_C1 < ppRangeTableA[0][0]) {
				currA = 0;
			}
			else {
				currA = g_measure_C1*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;	
				dbg_ioTask(5, "%s: NOT found on table %d\r\n",__FUNCTION__, g_measure_C1);
			}
		}
	}	
	
	currB = 0;
	bFoundRange  = 0;
	for(i=0;i<N_MAX_CURRENT_TABLE;i++) {
		if( g_measure_C2 >= ppRangeTableB[i][0] 
			&& g_measure_C2 <= ppRangeTableB[i][1] ) 
		{
			currB = pTableB[i];
			bFoundRange  = 1;
			break;
		}
	}
	if(bFoundRange==0) {
		for(i=0;i<N_MAX_CURRENT_TABLE-1;i++) {
			if( g_measure_C2 >= ppRangeTableB[i][0] 
				&& g_measure_C2 <= ppRangeTableB[i+1][0] ) 
			{
				currB = pTableB[i];
				bFoundRange  = 1;
				break;
			}
		}
		if(bFoundRange==0) {
			if(g_measure_C2 < ppRangeTableB[0][0]) {
				currB = 0;
			}
			else {
				currB = g_measure_C2*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;	
				dbg_ioTask(5, "%s: NOT found on table %d\r\n",__FUNCTION__, g_measure_C2);
			}
		}
	}	
	
//	currA = g_measure_C1*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX/2.5 + pInfo->stat.hvSetVolt1/100.0/2.0;
//	currB = g_measure_C2*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX/2.5 + pInfo->stat.hvSetVolt1/100.0/2.0;
	currA = g_measure_C1*100/73/30;
	currB = g_measure_C2*100/73/30;
	
	if(currA <= pInfo->setting.cutOffA/10) {
		currA = 0;
	}
	if(currB <= pInfo->setting.cutOffB/10) {
		currB = 0;
	}
	
	// 290 uA 이하는 0으로 cut off
	if(!IS_HV_ON && hvOutState == HV_STATE_READY) {
		pInfo->measure.currA = 0;
		pInfo->measure.currB = 0;
	}
	else {
		//currA = currB = 20;
		pInfo->measure.currA = currA;
		pInfo->measure.currB = currB;
	}
			
	static int tickPrev = 0;
	
	if(	HAL_GetTick() - tickPrev > 2000) {
		tickPrev = HAL_GetTick();
		dbg_ioTask(9, "Curr %d %d\r\n", g_measure_C1, currA);
	}
}



void GetVoltFromTable(StructEscInfo *pInfo, 
	const int pTableA[] ,	const int ppRangeTableA[N_MAX_VOLTAGE_TABLE][2], 
	const int pTableB[], const int ppRangeTableB[N_MAX_VOLTAGE_TABLE][2])
{
	int i;
	uint8_t bFoundRange;
	int voltA, voltB;
	
	voltA = 0;
	bFoundRange  = 0;
	for(i=0;i<N_MAX_VOLTAGE_TABLE;i++) {
		if( g_measure_V1 >= ppRangeTableA[i][0] 
			&& g_measure_V1 <= ppRangeTableA[i][1] ) 
		{
			voltA = pTableA[i];
			bFoundRange  = 1;
			break;
		}
	}
	if(bFoundRange==0) {
		for(i=0;i<N_MAX_VOLTAGE_TABLE-1;i++) {
			if( g_measure_V1 >= ppRangeTableA[i][0] 
				&& g_measure_V1 <= ppRangeTableA[i+1][0] ) 
			{
				voltA = pTableA[i];
				bFoundRange  = 1;
				break;
			}
		}
		if(bFoundRange==0) {
			if(g_measure_V1 < ppRangeTableA[0][0]) {
				voltA = 0;
			}
			else if(g_measure_V1 > ppRangeTableA[N_MAX_VOLTAGE_TABLE-1][1]) {
				voltA = N_MAX_HV_VOLT;
			}
			else {
				voltA = g_measure_V1*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;	
				dbg_ioTask(4, "%s: NOT found on table %d\r\n",__FUNCTION__, g_measure_V1);
			}
		}
	}	
	
	voltB = 0;
	bFoundRange  = 0;
	for(i=0;i<N_MAX_VOLTAGE_TABLE;i++) {
		if( g_measure_V2 >= ppRangeTableB[i][0] 
			&& g_measure_V2 <= ppRangeTableB[i][1] ) 
		{
			voltB = pTableB[i];
			bFoundRange  = 1;
			break;
		}
	}
	if(bFoundRange==0) {
		for(i=0;i<N_MAX_VOLTAGE_TABLE-1;i++) {
			if( g_measure_V2 >= ppRangeTableB[i][0] 
				&& g_measure_V2 <= ppRangeTableB[i+1][0] ) 
			{
				voltB = pTableB[i];
				bFoundRange  = 1;
				break;
			}
		}
		if(bFoundRange==0) {
			if(g_measure_V2 < ppRangeTableB[0][0]) {
				voltB = 0;
			}
			else if(g_measure_V2 > ppRangeTableB[N_MAX_VOLTAGE_TABLE-1][1]) {
				voltB = N_MAX_HV_VOLT;
			}
			else {
				voltB = g_measure_V2*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;	
				dbg_ioTask(4, "%s: NOT found on table %d\r\n",__FUNCTION__, g_measure_V2);
			}
		}
	}	
	
//	pInfo->measure.voltA = (g_measure_V1*N_MAX_HV_VOLT/VOLT_HV_ADC_MAX); 			
//	pInfo->measure.voltB = (g_measure_V2*N_MAX_HV_VOLT/VOLT_HV_ADC_MAX); 			
	
	//Bug FIX: HV Off 할때 전압 바로 0으로 표시
	if(!IS_HV_ON && hvOutState == HV_STATE_READY) {
		pInfo->measure.voltA = 0;
		pInfo->measure.voltB = 0;		
	}
	else {
		if(IS_TOGGLE) {
			pInfo->measure.voltA = voltA | 0x80;
			pInfo->measure.voltB = voltB;
		}
		else {
			pInfo->measure.voltA = voltA;
			pInfo->measure.voltB = voltB | 0x80;
		}		
	}
}

void AdcCheck(char* buff) 
{
	StructEscInfo *pInfo = GetEscInfo();
/**
adc 0: Batt volt
adc 1: Batt curr

adc 2: Hv A volt
adc 3: Hv A curr
adc 4: Hv B Volt
adc 5: Hv B Curr

adc 6: Hv V Set
adc 7: Charge curr
		
		Vref = 3.3V
*/		
		float batVolt;
		//float batCurr;
//		int chargeCurr;
		
	// Vref = 3.3V, Bat R = 1.5K Ohm : 10K Ohm -> 1.5/11.5
	batVolt = pInfo->measure.battStatusVolt;
	
	//batCurr = g_adc_result[ESC_ADC_CH_BAT_I] *3.3/4095.0/1.5*11.5;
	float internalTemp = g_adc_result[ESC_ADC_CH_INTERNAL_TEMP]*ADC_V_REF/ADC_STEP;
	internalTemp = (0.76-internalTemp/1000)/0.0025 + 25.0;
	dbg_ioTask(4, "Internal temp %d %3.1f\r\n",g_adc_result[ESC_ADC_CH_INTERNAL_TEMP]*ADC_V_REF/ADC_STEP,  internalTemp);
	
	sprintf( buff, "BATT V: %2.1fV (%dmV) Batt I: %dmA (%d[mV])  \r\nHV V=A:%d(%dmV) B:%d(%dmV) I=A:%1.2f(%dmV) B:%1.2f(%dmV) \r\nt1:%d t2:%d Press:%dmmHg AP volt:%d\r\n"
		,(batVolt/(float)10.0),g_adc_result[ESC_ADC_CH_BAT_V]*ADC_V_REF/ADC_STEP //,  g_adc_result[ESC_ADC_CH_BAT_V]
		,pInfo->measure.battCurr*10 ,g_adc_result[ESC_ADC_CH_BAT_I]*ADC_V_REF/ADC_STEP
		//,pInfo->measure.chargeCurr 
		//,g_adc_result[ESC_ADC_CH_CHARGE]	
		,ByteToVolt(pInfo->measure.voltA), g_measure_V1 //g_adc_result[ESC_ADC_CH_HV_A_OUT_V],
		,ByteToVolt(pInfo->measure.voltB), g_measure_V2 //g_adc_result[ESC_ADC_CH_HV_B_OUT_V],
		,(pInfo->measure.currA/100.0), g_measure_C1 // g_adc_result[ESC_ADC_CH_HV_A_OUT_I],
		,(pInfo->measure.currB/100.0), g_measure_C2 // g_adc_result[ESC_ADC_CH_HV_B_OUT_I],
		,pInfo->measure.temp1, pInfo->measure.temp2			
		,pInfo->measure.press, pInfo->measure.apHvSet
	);	
}

void TaskCheck(char *buff) 
{
	extern osThreadId_t defaultTaskHandle;
	extern osThreadId_t myTaskLanHandle;
	extern osThreadId_t myTaskInputHandle;
	extern osThreadId_t myTaskOutputHandle;
	extern osThreadId_t myTaskCommHandle;
	extern osThreadId_t myTaskLoraHandle;
	extern osThreadId_t myTaskLanRxHandle;		
	
	sprintf(buff,"Task States default %x Lan %x Input %x Output %x Comm %x Lora %x LanRx %x \r\n"
		,osThreadGetState(defaultTaskHandle) ,osThreadGetState(myTaskLanHandle) ,osThreadGetState(myTaskInputHandle) 
		,osThreadGetState(myTaskOutputHandle) ,osThreadGetState(myTaskCommHandle) ,osThreadGetState(myTaskLoraHandle)
		,osThreadGetState(myTaskLanRxHandle));			
}
void MinmaxCheck(char *buff)
{
	sprintf( buff, "min/max V1:%d~%d V2:%d~%d, C1:%d~%d C2:%d~%d\r\n"
		,g_min[0], g_max[0], g_min[1], g_max[1]
		,g_min[2], g_max[2], g_min[3], g_max[3]
	
	);	
}

uint8_t g_systemCheck = 0;
uint32_t g_nSystemCheckCount = 300;

static U08 prevHVOnoff = 0;
void SetHvOnOFF(U08 onoff)
{
	//if(prevHVOnoff != onoff) 
	{
		if(hvOutState != HV_STATE_READY ) { //>= HV_STATE_RAMPUP_AT2_UP_START && hvOutState <= HV_STATE_ALARM) {
			if(onoff==0) {				
				hvOutState = HV_STATE_RAMPDOWN_DONE;			
				hvOnOff(0);					
				prevHVOnoff = onoff;		
				GetEscInfo()->volatile_stat.hvOutputStatus = onoff;
				OnRefreshCLcd(CLCD_STATE_INITIAL, GetEscInfo());
				bRampDone = 0;			
				nRampCount = 1;

				dbg_ioTask(4, "ON? %d S %d %d D %d\r\n",onoff, hvOutState,clcdState, bRampDone);
			}
			else {
				prevHVOnoff = onoff;		
				GetEscInfo()->volatile_stat.hvOutputStatus = onoff;
			}
		}		
		else {
			dbg_ioTask(4, "ON! %d S %d %d D %d\r\n",onoff, hvOutState,clcdState, bRampDone);
			prevHVOnoff = onoff;		
			GetEscInfo()->volatile_stat.hvOutputStatus = onoff;
		}
		
	}
	//else {
		//dbg_ioTask(1, "ON* %d S %d %d D %d\r\n",onoff, hvOutState,clcdState, bRampDone);
	//}
}

static U08 alarmStatCheck = 0;
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	static int nAdcCh = 0;	
	//g_adc_result[nAdcCh] = HAL_ADC_GetValue(hadc);   	
	nAdcCh++;
	if(nAdcCh>=8) {
		nAdcCh = 0;
	}      
	//HAL_ADC_Start_IT(&hadc1);	
	


#if ADC_INTERRUPT
	static int averageIndex = 0;
	StructEscInfo *pInfo = GetEscInfo();
	calVoltA[averageIndex] = g_adc_result[ESC_ADC_CH_HV_A_OUT_V];
	calVoltB[averageIndex] = g_adc_result[ESC_ADC_CH_HV_B_OUT_V];
	calCurrA[averageIndex] = g_adc_result[ESC_ADC_CH_HV_A_OUT_I];
	calCurrB[averageIndex] = g_adc_result[ESC_ADC_CH_HV_B_OUT_I];
	
	battCurr[averageIndex] = g_adc_result[ESC_ADC_CH_BAT_I];
	//battChargeCurr[averageIndex] = g_adc_result[ESC_ADC_CH_CHARGE];
	apVoltSet[averageIndex] = g_adc_result[ESC_ADC_CH_AP_HV_V];

	
	averageIndex++;
	if(averageIndex>=N_AVERAGE_COUNT) {
		averageIndex = 0;
		g_measure_V1 = AverageFilter(calVoltA)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetVoltA;
		g_measure_V2 = AverageFilter(calVoltB)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetVoltB;
		g_measure_C1 = AverageFilter(calCurrA)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetCurrA;
		g_measure_C2 = AverageFilter(calCurrB)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetCurrB;
		if(g_min[0]>g_measure_V1) {
			g_min[0] = g_measure_V1;
		}
		if(g_max[0]<g_measure_V1) {
			g_max[0] = g_measure_V1;
		}
		if(g_min[1]>g_measure_V2) {
			g_min[1] = g_measure_V2;
		}
		if(g_max[1]<g_measure_V2) {
			g_max[1] = g_measure_V2;
		}
		
		if(g_min[2]>g_measure_C1) {
			g_min[2] = g_measure_C1;
		}
		if(g_max[2]<g_measure_C1) {
			g_max[2] = g_measure_C1;
		}
		if(g_min[3]>g_measure_C2) {
			g_min[3] = g_measure_C2;
		}
		if(g_max[3]<g_measure_C2) {
			g_max[3] = g_measure_C2;
		}
		
	}
	
	battVolt[averageIndex] = g_adc_result[ESC_ADC_CH_BAT_V];
	
	
	static int nAdcCount = 0;
	
	nAdcCount++;
	if(nAdcCount>N_AVERAGE_COUNT) {
		nAdcCount = 0;
		//alarmStatCheck = UpdateAlarmLED(pInfo, 0);
	}
	
#endif	
}

void TaskOutput(void *argument)
{	
	StructEscInfo *pInfo;
	
	uint16_t tick = 0;
	uint32_t halTick = 0, halTickOnRefresh = 0, halTickOnRefreshCursor = 0;
	int i;
	
	struct OutputMessage outputMsg;
	uint8_t channel;
	int16_t value, value2;
	
	uint8_t key;

	uint8_t nUplinkWait = 0;
	
	int averageIndex=0;
	

	uint8_t bBattCharging = 0;
	U08 battVoltPrev = 0;
	uint8_t battChargeComplete=0;

	
	float batDiff;
	int batPercent;
	int nBattDetCount = 0;
	
	RelayControl(0);
	
#if HVPWR_ALWAYS_ON
	HvPwrContrl(1);
#endif
	


	prevBattV = g_adc_result[ESC_ADC_CH_BAT_V];

	pTEMP1.CSControl 	= &ad7793CS1Control;
	pTEMP1.RDYState 	= &ad7793RDYState;
	pTEMP1.TxByte			= &ad7793TxByte;
	pTEMP1.RxByte			= &ad7793RxByte;
  	
	pTEMP2.CSControl 	= &ad7793CS2Control;
	pTEMP2.RDYState 	= &ad7793RDYState;
	pTEMP2.TxByte			= &ad7793TxByte;
	pTEMP2.RxByte			= &ad7793RxByte;

	AD779X_Init(&pTEMP1);
	AD779X_Init(&pTEMP2);

#if USE_ADC_DMA
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)g_adc_result, N_ADC_CHANNELS);
#else
	HAL_ADC_Start_IT(&hadc1);	
#endif


/// Get informations...
	pInfo = GetEscInfo();
	
	osMessageQueueReset(outputQueueHandle);
	
	osDelay(100);
	clcdInit();


	UpdateLcdData(STR_GREET_0, 0);	
	UpdateLcdData(STR_GREET_1, 1);	
	clcdCursor(0);
#if BOARD_REV2==0
	HAL_GPIO_WritePin(nLED_CHARGING_GPIO_Port, nLED_CHARGING_Pin, GPIO_PIN_SET);
#endif

	g_checkTaskStart |= CHECK_TASK_OUTPUT;
	
#if USE_AVERAGE_FILTER
	AverageInit(g_adc_result[ESC_ADC_CH_HV_A_OUT_V], calVoltA);
	AverageInit(g_adc_result[ESC_ADC_CH_HV_B_OUT_V], calVoltB);
	AverageInit(g_adc_result[ESC_ADC_CH_HV_A_OUT_I], calCurrA);
	AverageInit(g_adc_result[ESC_ADC_CH_HV_B_OUT_I], calCurrB);
	
	AverageInit(g_adc_result[ESC_ADC_CH_BAT_V], battVolt);
	AverageInit(g_adc_result[ESC_ADC_CH_BAT_I], battCurr);
//	AverageInit(g_adc_result[ESC_ADC_CH_CHARGE], battChargeCurr);
	AverageInit(g_adc_result[ESC_ADC_CH_AP_HV_V], apVoltSet);
	
#endif

	while( (g_checkTaskStart& (CHECK_TASK_INPUT)) != CHECK_TASK_INPUT) {
		osDelay(100);
		dbg_ioTask(9, "Wait until INPUT task up %x %s\r\n", g_checkTaskStart, __FUNCTION__);
	}


  /* Infinite loop */
	for(;;)
	{
		osStatus status;
		status = osThreadYield();                              // 
		if (status != osOK)  {
			// thread switch not occurred, not in a thread function
			printf("Task %s ERROR\r\n", __FUNCTION__);
		}		
				
		tick++;
		halTick = HAL_GetTick();
		if(g_nSystemCheckCount==0) {
			g_nSystemCheckCount = 300;
		}
		if((g_systemCheck!=0) && (tick%(g_nSystemCheckCount)==0)) {
			char tmpBuf[128];
			TaskCheck(tmpBuf);
			dbg_ioTask(1, "chk tick=%d \r\n%s\r\n",HAL_GetTick(), tmpBuf);			
			AdcCheck(tmpBuf);
			dbg_ioTask(1, "%s\r\n", tmpBuf);	
			MinmaxCheck(tmpBuf);
			dbg_ioTask(1, "%s\r\n", tmpBuf);	
		}
		#if BEEP_IN_IO_TASK
		//if(count%(BUZ_DELAY)==0) 
		if(g_BeepCount)
		{
			U08 beepPolarity = g_BeepCount%2;
			if(g_Beep) {
				g_Beep--;
				if(!BUZZER_MUTE) {
					if(beepPolarity) {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);	
					}
				}
			}
			else {
				
				if(!BUZZER_MUTE) {
					if(!beepPolarity) {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
					}
					else {
						HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);	
					}
				}
				g_BeepCount--;
				if(g_BeepCount!=0) {
					g_Beep = g_BeepDelay*BUZ_DELAY;
				}
			}
		}		
		#endif
		
		if(hvOutState ==  HV_STATE_READY) {
			g_readyTick = HAL_GetTick();
		}
		
		alarmStatCheck = UpdateAlarmLED(pInfo, 0);
		
		if( ( alarmStatCheck & 0x80) ) {
			dbg_ioTask(1, "Alarm Stat %x\r\n", alarmStatCheck);
			if((hvOutState ==  HV_STATE_READY || hvOutState >=  HV_STATE_RAMPUP_AT2_UP_START) ) {
		//if( ( alarmStat & 0x80) && (hvOutState ==  HV_STATE_READY || hvOutState >=  HV_STATE_ON_START) )		{
			
				if( IsAlarm(1) != 0 ) {
					if( IS_HV_ON ) {
						dbg_ioTask(1, "Has Alarm, Stop Output\r\n");
						SetHvOnOFF(0);
						OnRemoteCLcd(CLCD_STATE_STOP, pInfo);
						hvOutState =  HV_STATE_ALARM;
						hvOnOffAlarm(pInfo, HAL_GetTick());
						BeepN(4);
					}
				}		
				if(IS_HV_ON) {
					osDelay(1);
				}
				else {
					osDelay(10);
				}
			}
		}	
		else {	
			
	#if AP_MODE_ON
			// Read AP
			U08 apChk = 0, apChkCnt, apHvOut, apAlarmReset;
			static U08 almStatus = 0;
			static U08 apChkPrv = 0;
			static U08 prevMode = 0;
			static U08 prevOnoffApHv = 0;
			static U16 prevVolt = 0;
			
			// Active Low
			g_apBoardDet = DET_AP; //HAL_GPIO_ReadPin(nAP_BOARD_DET_GPIO_Port, nAP_BOARD_DET_Pin)==0?1:0;
			apHvOut = g_apHvOut; //HAL_GPIO_ReadPin(nAP_HV_OUT_GPIO_Port, nAP_HV_OUT_Pin);
			apAlarmReset = g_apAlarmReset; // HAL_GPIO_ReadPin(nAP_ALM_RESET_GPIO_Port, nAP_ALM_RESET_Pin);
			
			// output
			//apAlarm = HAL_GPIO_ReadPin(nAP_ALM_STATUS_GPIO_Port, nAP_ALM_STATUS_Pin);
			
			apChk = g_apBoardDet | g_apHvOut<<1 | g_apAlarm<<2 |g_apAlarmReset<<3;
			
			if(g_apBoardDet) {
				// Check Alarm...
				almStatus = alarmStatCheck; //UpdateAlarmLED(pInfo);
				//if(almStatus != ALM_NONE && IsAlarm(0) != 0 && IS_HV_ON) {  // && almStatus != ALM_BATT) {
				//if(IsAlarm(0) != 0 && IS_HV_ON) {
				if(IsAlarm(0) != 0 && g_apHvOut) {
					if(g_apAlarmReset==1) { // clear Alarm
						g_apAlarmReset = 0;
						
						// Clear all Alarms on Cmd
						ClearAlarm(pInfo);
						almStatus = UpdateAlarmLED(pInfo, 1);
						OnRemoteCLcd(CLCD_STATE_INITIAL,pInfo);	
						dbg_ioTask(3, "APMODE CLR ALM! %d\r\n", almStatus);

						
						prevOnoffApHv = 0;
					}
					HAL_GPIO_WritePin(nAP_ALM_STATUS_GPIO_Port, nAP_ALM_STATUS_Pin, GPIO_PIN_RESET);
					//printf("Alarm Reset %x\r\n", g_apAlarmReset);
					/*
					if(g_apHvOut==0) {
						dbg_ioTask(1, "AP Out OFF with ALARM\r\n");
						SetHvOnOFF(0); // AP Mode, HV OFF

						//pInfo->setting.lanLora = prevMode;
						g_APMode = 0;
						prevOnoffApHv = g_apHvOut;
					}
					*/
				}			
				else { // No alarm...
					HAL_GPIO_WritePin(nAP_ALM_STATUS_GPIO_Port, nAP_ALM_STATUS_Pin, GPIO_PIN_SET);
					//printf("Alarm Reset %x\r\n", g_apAlarmReset);
					// Check AP hv Onoff
					if(prevOnoffApHv != g_apHvOut) {
						if(g_apHvOut) {				
							//U08 prvVoltSign = pInfo->stat.hvSetVolt1 & 0x80;
							
							if(!IS_TOGGLE) {
								dbg_ioTask(1, "AP Out ON volt %d %d\r\n", pInfo->stat.hvSetVolt1, VoltToByte( pInfo->measure.apHvSet));
							
								SetHvVolt(1, VoltToByte( pInfo->measure.apHvSet));
								SetHvVolt(2, VoltToByte( pInfo->measure.apHvSet) | 0x80);
							}
							else {
								dbg_ioTask(1, "AP Out ON volt %d %d - TOGGLE\r\n", pInfo->stat.hvSetVolt1, VoltToByte( pInfo->measure.apHvSet));
							
								SetHvVolt(2, VoltToByte( pInfo->measure.apHvSet));
								SetHvVolt(1, VoltToByte( pInfo->measure.apHvSet) | 0x80);
							}
							
							prevVolt = pInfo->stat.hvSetVolt1;							
							prevMode = pInfo->setting.lanLora;
							
							//pInfo->setting.lanLora = COMM_MODE_AP;
							g_APMode = 1;
							prevOnoffApHv = g_apHvOut;
							
							SetHvOnOFF(1); // AP Mode, HV ON
							hvOutState =  HV_STATE_READY;
							bRampDone = 0;	
							nRampCount = 0;
							hvOnOffRamp(IS_HV_ON, nRampCount); // AP					
						}
						else {
							dbg_ioTask(1, "AP Out OFF\r\n");
							SetHvOnOFF(0); // AP Mode, HV OFF

							//pInfo->setting.lanLora = prevMode;
							g_APMode = 0;
							prevOnoffApHv = g_apHvOut;
							
							
							if(IsAlarm(0) != 0) {
								// Clear all Alarms on Cmd
								ClearAlarm(pInfo);
								almStatus = UpdateAlarmLED(pInfo, 1);
								OnRemoteCLcd(CLCD_STATE_INITIAL,pInfo);	
								dbg_ioTask(3, "APMODE CLR ALM! %d\r\n", almStatus);
							}

						}
						OnRemoteCLcd(0,pInfo);
					}
						
					// Check AP hv volt changing...
					if(IS_HV_ON && g_apHvOut) {
						if( abs(prevVolt-pInfo->measure.apHvSet) > 90 )
						{
							//SetHvVolt(1, VoltToByte( pInfo->measure.apHvSet));	
							//SetHvVolt(2, pInfo->stat.hvSetVolt1 ^0x80);							
							//SetHvVolt(2, pInfo->stat.hvSetVolt1>0?(pInfo->stat.hvSetVolt1 |0x80):0);
							if(!IS_TOGGLE) {
								dbg_ioTask(1, "AP Out ON volt %d %d\r\n", pInfo->stat.hvSetVolt1, VoltToByte( pInfo->measure.apHvSet));
							
								SetHvVolt(1, VoltToByte( pInfo->measure.apHvSet));
								SetHvVolt(2, VoltToByte( pInfo->measure.apHvSet) | 0x80);
							}
							else {
								dbg_ioTask(1, "AP Out ON volt %d %d - TOGGLE\r\n", pInfo->stat.hvSetVolt1, VoltToByte( pInfo->measure.apHvSet));
							
								SetHvVolt(2, VoltToByte( pInfo->measure.apHvSet));
								SetHvVolt(1, VoltToByte( pInfo->measure.apHvSet) | 0x80);
							}
							
							prevVolt = pInfo->measure.apHvSet;
							hvOutState =  HV_STATE_READY;
				
							dbg_ioTask(3, "AP volt Changed! 0x%x det=%d onoff=%d adc=%d %d %d\r\n"
							, apChk, g_apBoardDet, g_apHvOut, pInfo->measure.apHvSet, pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
						}		
					}			
				}
				
			}
			
	#endif			
			if(bRampDone==0) {
				//if(nRampCount < nRampCountMax && nRampCountMax!=0) {				
				if(hvOutState !=  HV_STATE_READY) {
					prevV1 = pInfo->stat.hvSetVolt1;
					prevV2 = pInfo->stat.hvSetVolt2;				
					prevToggle = IS_TOGGLE;
					
					
					if(g_hvOutOnPrev!=IS_HV_ON) {
						g_hvOutOnPrev = IS_HV_ON;						
						dbg_ioTask(1, "HV On/OFF changed %d %d\r\n", nRampCount, g_hvOutOnPrev);
						nRampCount = 0;
					}
					else {
						nRampCount++;
						hvOnOffRamp(IS_HV_ON, nRampCount); // Not READY
						//dbg_ioTask(1, "NOT ready %d\r\n", nRampCount);
					}
					
					osDelay(1);
					//continue;
				}
				else {
					osDelay(1);	
				}
			}		
			else if(bRampChangeDone==0) {
				nRampCount++;
				changeHvOutRamp(pInfo, nRampCount);
				osDelay(1);
				//continue;
			}
			else if(bRampToggleDone==0) {
				nRampCount++;
				changeHvToggleRamp(pInfo, nRampCount);
				osDelay(1);
				//continue;
			}
			else if(IS_HV_ON && (hvOutState ==  HV_STATE_READY)) {
			//else if(hvOutState ==  HV_STATE_READY) {
				uint32_t  tickNow;
				static uint32_t  tickPrev=0;
				tickNow = HAL_GetTick();
				if((tickPrev-tickNow)>300) {
					int dacCurr;
					int dacA, dacB;
					tickPrev = tickNow;
					
					if(pInfo->setting.noRepeatDac==0) {
						dac7678UpdatePrev(DAC_CH_CURR_1);
						dac7678UpdatePrev(DAC_CH_CURR_2);
						dac7678UpdatePrev(DAC_CH_VOLT_1);
						dac7678UpdatePrev(DAC_CH_VOLT_2);
						UpdateDac();
						
						dbg_ioTask(10, "RestartDAC! %d(%d, %d, %dmV) %d(%d, %d, %dmV) \r\n\tocp %d(%d, %dmV) tick %d\r\n",
							targetVoltA,pInfo->stat.hvSetVolt1, dacA, dacA*DAC_VREF/DAC_STEP,
							targetVoltB, pInfo->stat.hvSetVolt2, dacB, dacB*DAC_VREF/DAC_STEP,
							pInfo->stat.ocpAlarmLimit, dacCurr, dacCurr*DAC_VREF/DAC_STEP, tickNow);					
					}
						
					osDelay(OUTPUT_TASK_DELAY);	
				}
				osDelay(OUTPUT_TASK_DELAY);	
			}
			else {
				osDelay(OUTPUT_TASK_DELAY);	
			}
			
			
			if(g_hvOutOnPrev!=IS_HV_ON) {
				g_hvOutChangeWait = 100;
				g_hvOutOnPrev = IS_HV_ON;
				
				if(hvOutState ==  HV_STATE_READY) {
					dbg_ioTask(4, "CHANGE ON/OFF %d %d\r\n", IS_HV_ON, hvOutState);
					nRampCount = 0;
					
					// TEST by jwaani @2020-0602
					if(bRampDone==100) {
					}
					else {
						bRampDone = 0;
						if(IsHvOn) {
							hvOutState =  HV_STATE_RAMPUP_AT2_UP_START; //HV_STATE_ON_START;
						}
						else {
							hvOutState =  HV_STATE_OFF_START;
						}
						hvOnOffRamp(IS_HV_ON, nRampCount); // Change ON/OFF
					}
					continue;
				}
				else {
					dbg_ioTask(1, "INSTANT ON/OFF %d %d\r\n", IS_HV_ON, hvOutState);
					nRampCount = 0;
					bRampDone = 0;
					hvOutState =  HV_STATE_READY;
					hvOnOff(IS_HV_ON);
					
					continue;
				}
			}
			

			// bRampDone == 0 -> not a change or toggle
			if(IS_HV_ON && bRampDone==1) {				
				// HV Toggle!
				if(prevToggle != IS_TOGGLE) {
					 prevV1 = pInfo->stat.hvSetVolt1;
					 prevV2 = pInfo->stat.hvSetVolt2;
					
					prevToggle = IS_TOGGLE;
					g_hvOutChangeWait = 100;
					nRampCount = 0;
					dbg_ioTask(5, "HV TOGGLE!\r\n");
					changeHvToggleRamp(pInfo, nRampCount);
				}
				// HV volt changed
				else if( prevV1 != pInfo->stat.hvSetVolt1 || prevV2 != pInfo->stat.hvSetVolt2) {
					 prevV1 = pInfo->stat.hvSetVolt1;
					 prevV2 = pInfo->stat.hvSetVolt2;
					
					prevToggle = IS_TOGGLE;
					g_hvOutChangeWait = 100;
					nRampCount = 0;
					dbg_ioTask(5, "HV CHANGE!\r\n");
					changeHvOutRamp(pInfo, nRampCount);
				}
			}

		}


#if ADC_INTERRUPT==0
		calVoltA[averageIndex] = g_adc_result[ESC_ADC_CH_HV_A_OUT_V];
		calVoltB[averageIndex] = g_adc_result[ESC_ADC_CH_HV_B_OUT_V];
		calCurrA[averageIndex] = g_adc_result[ESC_ADC_CH_HV_A_OUT_I];
		calCurrB[averageIndex] = g_adc_result[ESC_ADC_CH_HV_B_OUT_I];
		
		battCurr[averageIndex] = g_adc_result[ESC_ADC_CH_BAT_I];
		//battChargeCurr[averageIndex] = g_adc_result[ESC_ADC_CH_CHARGE];
		apVoltSet[averageIndex] = g_adc_result[ESC_ADC_CH_AP_HV_V];

		
		averageIndex++;
		if(averageIndex>=N_AVERAGE_COUNT) {
			averageIndex = 0;
		}
		
		g_measure_V1 = AverageFilter(calVoltA)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetVoltA;
		g_measure_V2 = AverageFilter(calVoltB)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetVoltB;
		g_measure_C1 = AverageFilter(calCurrA)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetCurrA;
		g_measure_C2 = AverageFilter(calCurrB)*ADC_V_REF/ADC_STEP + pInfo->setting.offsetCurrB;
		
		if(g_min[0]>g_measure_V1) {
			g_min[0] = g_measure_V1;
		}
		if(g_max[0]<g_measure_V1) {
			g_max[0] = g_measure_V1;
		}
		if(g_min[1]>g_measure_V2) {
			g_min[1] = g_measure_V2;
		}
		if(g_max[1]<g_measure_V2) {
			g_max[1] = g_measure_V2;
		}
		
		if(g_min[2]>g_measure_C1) {
			g_min[2] = g_measure_C1;
		}
		if(g_max[2]<g_measure_C1) {
			g_max[2] = g_measure_C1;
		}
		if(g_min[3]>g_measure_C2) {
			g_min[3] = g_measure_C2;
		}
		if(g_max[3]<g_measure_C2) {
			g_max[3] = g_measure_C2;
		}

		battVolt[averageIndex] = g_adc_result[ESC_ADC_CH_BAT_V];
#endif

		U08 bCheckBat = 0;
		if(g_hvOutChangeWait>0) 		
		{ // Hv out 동작 이후 배터리 측정 안함.
			g_hvOutChangeWait--;
			bCheckBat = 0;
			if(g_hvOutChangeWait==1) {
				dbg_ioTask(5, "Restart Read ADC...\r\n");	
				bCheckBat = 1;
			}
		}
		else {
			bCheckBat = 1;
		}
		
		if(bCheckBat) {
//			battVolt[averageIndex] = g_adc_result[ESC_ADC_CH_BAT_V];


			if(g_testBattLow & 0x01) {				
				pInfo->measure.battStatusVolt = BATT_MIN*10;
			}
			else if(g_testBattLow&0x10) {
				pInfo->measure.battStatusVolt = BATT_MAX*10;
			}		
			else {
				// Vref = 3.3V, Bat R = 1.5K Ohm : 10K Ohm -> 1.5/11.5, byte save 0~250[1/10 V] -> 0.06178266178266178266178266178266
				pInfo->measure.battStatusVolt =	AverageFilter(battVolt)*ADC_V_REF/ADC_STEP*BATT_COEFITIENT/ADC_V_REF;
			}
			
			
#if USE_BATT_PERCENT_TABLE==0			
			// 최대  최소  계산
			//batDiff = ((AverageFilter(battVolt)*ADC_V_REF/ADC_STEP*BATT_COEFITIENT/ADC_V_REF)-BATT_MIN*10.)/10.0;
			batDiff = (pInfo->measure.battStatusVolt-BATT_MIN*10.)/10.0;
			batPercent = (batDiff)/(BATT_MAX-BATT_MIN)*100;
			if(batPercent<1) {
				batPercent = 1;
			}
			else if(batPercent > 100) {
				batPercent = 100;
			}
			pInfo->measure.battPercent = batPercent;
#else			
			int indexV;
			float battVolt = pInfo->measure.battStatusVolt/10.0;
			if(DET_CHARGER) {
				for(indexV=0;indexV<sizeof(BattChargePercent);indexV++) {
					if(battVolt <= BattChargeVolt[indexV]) {
						pInfo->measure.battPercent = BattChargePercent[indexV];
						//dbg_ioTask(3, "Charging BATT %d %d %d\r\n",battADC, pInfo->measure.battPercent, indexV);
						break;
					}
				}
				if(indexV>=sizeof(BattChargePercent)-1) {
					pInfo->measure.battPercent = BattChargePercent[sizeof(BattChargePercent)-1];
				}
			}
			else {
				for(indexV=0;indexV<sizeof(BattDischargePerent);indexV++) {
					if(battVolt <= BattDischargeVolt[indexV]) {
						pInfo->measure.battPercent = BattDischargePerent[indexV];
						//dbg_ioTask(3, "Discharging BATT %d %d %d\r\n",battADC, pInfo->measure.battPercent, indexV);
						break;
					}
				}
				if(indexV>=sizeof(BattDischargePerent)-1) {
					pInfo->measure.battPercent = BattDischargePerent[sizeof(BattDischargePerent)-1];
				}
			}
			
#endif
			
			//pInfo->measure.chargeCurr = g_adc_result[ESC_ADC_CH_CHARGE]*2;
			
			// Check Battery det
			U08 batt_Det = DET_BATTERY;
			if(batt_Det) {
				nBattDetCount++;
			}
			else {
				nBattDetCount = 0;
			}
			
			if(nBattDetCount>50) {
				nBattDetCount = 50;
#if ALARM_CONTROL_HV_ONLY				
				if( pInfo->measure.battStatusVolt < 130 && pInfo->measure.battStatusVolt > 80) { // Battery is present				
#else
				if( pInfo->stat.battAlarmControl && pInfo->measure.battStatusVolt < 130 && pInfo->measure.battStatusVolt > 80) { // Battery is present				
#endif					
					//if(IS_HV_ON==0) {
					if(hvOutState ==  HV_STATE_READY) {
						if(battVoltPrev==0) { // init prev volt
							battVoltPrev = pInfo->measure.battStatusVolt;
						}
						if(battVoltPrev != pInfo->measure.battStatusVolt) {					
							if(bBattCharging==0) {						
								//if((int)(pInfo->measure.battStatusVolt-battVoltPrev) >= BATT_CHG_CHK_COUNT) {
								if(pInfo->measure.chargeCurr > 100) {
									dbg_ioTask(2, "Battery volt changed %d %d\r\n", battVoltPrev, pInfo->measure.battStatusVolt);					
									bBattCharging = 1;
		#if BOARD_REV2==0							
									HAL_GPIO_WritePin(nLED_CHARGING_GPIO_Port, nLED_CHARGING_Pin, GPIO_PIN_RESET);
		#endif
									pInfo->measure.battChargeConnectStatus = 1;
									battVoltPrev = pInfo->measure.battStatusVolt;
								}
							}
							else {
								//if((int)(battVoltPrev-pInfo->measure.battStatusVolt) >= BATT_CHG_CHK_COUNT) {
								if(pInfo->measure.chargeCurr < 100) {
									dbg_ioTask(2, "Battery volt changed %d %d\r\n", battVoltPrev, pInfo->measure.battStatusVolt);					
									bBattCharging = 0;
		#if BOARD_REV2==0
									HAL_GPIO_WritePin(nLED_CHARGING_GPIO_Port, nLED_CHARGING_Pin, GPIO_PIN_SET);
		#endif
									pInfo->measure.battChargeConnectStatus = 0;
									battVoltPrev = pInfo->measure.battStatusVolt;
								}
							}
						}
						
						if(pInfo->measure.battPercent>=BATT_COMPLETE_PERCENT) {
							if(battChargeComplete==0) {								
								battFullCount++;
								if(battFullCount>BATT_COMPLETE_TIME) {
									battFullCount = 0;
									battChargeComplete = 1;									
									HAL_GPIO_WritePin(nLED_CHARGED_GPIO_Port, nLED_CHARGED_Pin, GPIO_PIN_RESET);
									ChgQlight(battChargeComplete);									
								}
							}
						}
						//else if(pInfo->measure.battPercent < 89) {
						else {
							if(battChargeComplete==1) {						
								battFullCount++;
								if(battFullCount>BATT_COMPLETE_TIME) {
									battFullCount = 0;
									HAL_GPIO_WritePin(nLED_CHARGED_GPIO_Port, nLED_CHARGED_Pin, GPIO_PIN_SET);
									battChargeComplete = 0;
									battFullCount = 0;
									ChgQlight(battChargeComplete);
								}
							}
						}
						
					}
					if(pInfo->measure.battPercent < pInfo->stat.battAlarmLimit -BAT_LOW_DELTA) {
						battLowCount++;
						if(battLowCount>BATT_WARN_TIME) {
#if ALARM_CONTROL_HV_ONLY
							if(1) {
#else
							if(pInfo->stat.battAlarmControl) {
#endif
								pInfo->measure.battLowAlarmStatus = 1;
								//HAL_GPIO_WritePin(nLED_ALM_BAT_GPIO_Port, nLED_ALM_BAT_Pin, GPIO_PIN_RESET);					
							}
						}
					}
					if(pInfo->measure.battPercent > pInfo->stat.battAlarmLimit +BAT_LOW_DELTA) {
						battLowCount = 0;
#if ALARM_CONTROL_HV_ONLY
						if(1) {
#else
						if(pInfo->stat.battAlarmControl) {
#endif

							pInfo->measure.battLowAlarmStatus = 0;
							//HAL_GPIO_WritePin(nLED_ALM_BAT_GPIO_Port, nLED_ALM_BAT_Pin, GPIO_PIN_SET);
						}
						
					}
					
					
					if(bBatLow!=pInfo->measure.battLowAlarmStatus) {
						bBatLow = pInfo->measure.battLowAlarmStatus;				
						dbg_ioTask(3, "BAT LOW Changed %d %d %d\r\n", pInfo->measure.battLowAlarmStatus, pInfo->measure.battLowAlarmStatus, pInfo->measure.battPercent);						
						//UpdateAlarmLED(pInfo, 1);
					}						
				}
			}
			else { // NO Battery
			}
		}				
		
		// Charging current, except 300mA Operation current -> *2.45 or * 3.3
		pInfo->measure.battCurr = AverageFilter(battCurr)*ADC_V_REF/ADC_STEP*2.35/10; 	//g_adc_result[ESC_ADC_CH_BAT_I]*3000/4095;
		pInfo->measure.chargeCurr = AverageFilter(battChargeCurr);
		
		pInfo->measure.apHvSet = (AverageFilter(apVoltSet)-535)*3000/4095*3/2.0;

		
		//0.29846696513363180029846696513363
		// X *3300/4095 [mV], *1/2.8/10 [uA]
		//pInfo->measure.currA = g_measure_C1*0.01551282051282051282051282051282; //0.0287807430664573521716378859236; //*ADC_V_REF/ADC_STEP/CURR_MAX; // 0.86342;			
		

		
		//pInfo->measure.currA = g_measure_C1*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;
		//pInfo->measure.currB = g_measure_C2*N_MAX_HV_CURR_MA/CURR_HV_ADC_MAX;	

		int **currRangeTalbeA = (int**)currToggleOffRangeTableA;
		int *currTableA = (int*)currToggleOffTableA;
		int **currRangeTalbeB = (int**)currToggleOffRangeTableB;
		int *currTableB = (int*)currToggleOffTableB;
		uint8_t bFoundRange  = 0;
		
		
		if(IS_TOGGLE==0) {
			GetCurrentFromTable(pInfo, currToggleOffTableA, currToggleOffRangeTableA,
				currToggleOffTableB, currToggleOffRangeTableB);
			
			GetVoltFromTable(pInfo, voltToggleOffTableA, voltToggleOffRangeTableA,
				voltToggleOffTableB, voltToggleOffRangeTableB);
		}
		else {
			GetCurrentFromTable(pInfo, currToggleOnTableA, currToggleOnRangeTableA,
				currToggleOnTableB, currToggleOnRangeTableB);
			
			GetVoltFromTable(pInfo, voltToggleOnTableA, voltToggleOnRangeTableA,
				voltToggleOnTableB, voltToggleOnRangeTableB);
		}
		
		// mV to kV
		//pInfo->measure.voltA = (g_measure_V1*N_MAX_HV_VOLT/VOLT_HV_ADC_MAX); 			
		//pInfo->measure.voltB = (g_measure_V2*N_MAX_HV_VOLT/VOLT_HV_ADC_MAX); 			
		
		
		#if DISP_CURR_ZERO
		pInfo->measure.currA =0;
		pInfo->measure.currB=0;
		#endif
		
		
		// update ADC result every 100 mSec 
		if(tick) {		
			
			if(pInfo->measure.currA>250) {
				//pInfo->measure.currA = 100;
			}
			if(pInfo->measure.currB>250) {
				//pInfo->measure.currB = 100;
			}
			
			conv[0] = AD779X_ReadDataRegister24(&pTEMP1);
			conv[1] = AD779X_ReadDataRegister24(&pTEMP2);

			temperature[0] = ((((conv[0]*1.0479)/(16777216*16))/0.00021)/100.0 - 1.0) / 0.00385 * 100.0;
			temperature[1] = ((((conv[1]*1.0479)/(16777216*16))/0.00021)/100.0 - 1.0) / 0.00385 * 100.0;
		
#if TEST_TEMPERATURE
			pInfo->measure.temp1 = 241 + pInfo->setting.temp1Tune;
			pInfo->measure.temp2 = 257 + pInfo->setting.temp2Tune;
#else
			pInfo->measure.temp1 = temperature[0]/(float)10.0 + pInfo->setting.temp1Tune;
			pInfo->measure.temp2 = temperature[1]/(float)10.0 + pInfo->setting.temp2Tune;
#endif
			if(batPercent<=1) {
				batPercent = 1;
			}
			else if(batPercent>100) {
				batPercent = 100;
			}
			
			
		}

		extern osMessageQueueId_t queueRxCommHandle;
		if (osMessageQueueGetCount(queueRxCommHandle) > 0 && tick%10==0)  {
			
			uint32_t rxCount = osMessageQueueGetCount(queueRxCommHandle);
			dbg_ioTask(1, "RxData Queue %d :", rxCount);
			uint32_t nLoopCount = 0;
			while (rxCount > 0)  {
				U08 rxData;
				osMessageQueueGet(outputQueueHandle, &rxData, NULL, NULL);
				dbg_ioTask(1, "%x ", rxData);
				rxCount = osMessageQueueGetCount(queueRxCommHandle);
				if(nLoopCount>256) {
					dbg_ioTask(1, "Overbuffer!!!\r\n");
					break;
				}
			}
			dbg_ioTask(1, "\r\n");
		}
		
		// Output process...
		if (osMessageQueueGetCount(outputQueueHandle) > 0) 
		{				
			osMessageQueueGet(outputQueueHandle, &outputMsg, NULL, NULL);
			channel = outputMsg.channel;
			value = outputMsg.value;
			value2 = outputMsg.value2;
			
			switch(outputMsg.commandID) {
				case OUTPUT_CMID_HV_SET_VOLT_CH1:
				SendDac(DAC_CH_VOLT_1, pInfo->stat.hvSetVolt1);
				SendDacCurr(DAC_CH_CURR_1, DAC_OCP_CONTROL_MAX_UA);
				break;
				case OUTPUT_CMID_HV_SET_VOLT_CH2:
				SendDac(DAC_CH_VOLT_2, pInfo->stat.hvSetVolt2);
				SendDacCurr(DAC_CH_CURR_2, DAC_OCP_CONTROL_MAX_UA);
				break;
				case CMD_HVOUT_BOOST:
					hv_pwr_control ^= 1;
					dbg_ioTask(1, "hv out control %d\r\n", hv_pwr_control);
					if(hv_pwr_control==0) {
						HvPwrContrl(0);
					}
					else {
						HvPwrContrl(1);
					}
					
					break;
				case OUTPUT_HV_OUT_ONOFF:					
					SetHvOnOFF(value);
					dbg_ioTask(1, "OUTPUT_HV_OUT_ONOFF %d\r\n", IS_HV_ON);
					OnRemoteCLcd(0, pInfo);
					
					
				break;
					
				case OUTPUT_TOGGLE:	
					dbg_ioTask(3, "OUTPUT_TOGGLE %d\r\n", value);	
					//dbg_ioTask(4, "HV TOGGLE\r\n");
					swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);				
				
					break;
				
				case OUTPUT_DAC:
					//dbg_ioTask(1, "DAC %d %d\r\n", channel, value);
					// channel 0, 1 -> Volt A/B
					//outputMsg.value = (value * 4095) / 3300;
					dac7678Set(channel, value);	//2.7v = 3350
				
					UpdateDac();
				break;
					
				case OUTPUT_RELAY:
					RelayControl(value);
					
				break;
					
				case OUTPUT_SHUTDOWN:
#if DAC_ALWAYS_ON
				HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET); //On
				//HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET); //Off
#else
					if(value==0) {						
						HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_SET);
						dbg_ioTask(3, "DAC_SHDN SET\r\n");
					}
					else {						
						HAL_GPIO_WritePin(nDAC_HV_SHUTDOWN_GPIO_Port, nDAC_HV_SHUTDOWN_Pin, GPIO_PIN_RESET);
						dbg_ioTask(1, "DAC_SHDN RESET\r\n");
					}
#endif
					dbg_ioTask(1, "shutdown %d -> %s\r\n", value, value==0?"ON":"OFF");
				break;
					
				case OUTPUT_KEY_PRESSING:
					key = value & 0x00ff;
					if(key!=0) {						
						clcdState = OnKeyCLcd(clcdState, pInfo, key);
						//debug_console("key pressing: %x\r\n", key);

					}					
					break;
				case OUTPUT_KEY:
					/*
					Output	Toggle			UP
									LEFT		 RIGHT
					Esc		Select			DOWN
					*/
					key = value & 0x00ff;
					if(key!=0) {
						if(keyProcIng==0) {
							//keyProcIng = 1;
							BEEP();
							clcdState = OnKeyCLcd(clcdState, pInfo, key);

							switch(key) {
								case KEY_SW_UP:
									dbg_ioTask(5, "key : %x UP\r\n", key);
									break;
								case KEY_SW_DOWN:
									dbg_ioTask(5, "key : %x DN\r\n", key);
									break;
								case KEY_SW_LEFT:
									dbg_ioTask(5, "key : %x LEFT\r\n", key);
									break;
								case KEY_SW_RIGHT:
									dbg_ioTask(5, "key : %x RIGHT\r\n", key);
									break;
								case KEY_SW_HV_PWR:
									dbg_ioTask(5, "key : %x HV\r\n", key);
									break;
								case KEY_SW_TOGGLE:
									dbg_ioTask(5, "key : %x TG\r\n", key);
									break;
								case KEY_SW_SELECT:
									dbg_ioTask(5, "key : %x SEL\r\n", key);
									break;
								case KEY_SW_ESC_CANCEL:
									dbg_ioTask(5, "key : %x ESC\r\n", key);
									break;
							}	
			
						}
					}
				break;
					
				case CMD_CLCD_UPDATE:
					//clcdState = UpdateCLcd(clcdState, pInfo, key,1);
				OnRefreshCLcd(clcdState, pInfo);
					
				break;
					
				case OUTPUT_MEAS_ARC:					
					dbg_ioTask(1, "\rmeasure!\r\n>");
					
					break;
				
				case OUTPUT_BUZ:				
					buzCount = BUZ_DELAY;
				//HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
				//osDelay(100);
				//HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
				break;
				
				case OUTPUT_SYST_LOC:
					pInfo->setting.mode = ESC_MODE_LOCAL;
				break;
				case OUTPUT_SYST_REM:
					pInfo->setting.mode = ESC_MODE_REMOTE;
				break;
				
				case OUTPUT_SOUR_VOLT:
					//pInfo->stat.sourVolt = (int16_t)value;
					SetHvVolt(1, VoltToByte((int16_t)value));
					SetHvVolt(2,  VoltToByte((int16_t)-value));
					break;				
				
				case OUTPUT_CONF_VOLT:
					SetHvVolt(1, VoltToByte((int16_t)value));
					SetHvVolt(2, VoltToByte((int16_t)-value));
					dbg_ioTask(1, "OUTPUT_CONF_VOLT %d %d %d\r\n>", value, pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
					break;
				
				case OUTPUT_CONF_VOLT2:
					SetHvVolt(1, VoltToByte((int16_t)value));
					SetHvVolt(2, VoltToByte((int16_t)value2));
					dbg_ioTask(1, "OUTPUT_CONF_VOLT2 %d %d %d\r\n>", value, pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
					break;				
				
				case OUTPUT_CONF_CURR:
					pInfo->stat.ocpAlarmLimit = (int16_t)value/10;
					break;
				
				case OUTPUT_CONF_ARC_STAT:
					pInfo->stat.confArcStat = value;
					break;
				case OUTPUT_CONF_ARC_LEV:
					pInfo->stat.arcAlarmLimit = value;
				break;

				case OUTPUT_AT_COUN:
					pInfo->volatile_stat.toggleCount = value;
				break;
				case OUTPUT_AT_VOLT:
					//pInfo->stat.atVolt = value;
					SetHvVolt(1, VoltToByte(value));
					SetHvVolt(2, VoltToByte(-value));
				break;
				case OUTPUT_AT_STAT:
					pInfo->stat.hvAutoToggle = value;
				break;
			}
			
		}
		else  {
			if(buzCount>0) {
				buzCount--;
				if(!BUZZER_MUTE) {
					HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_SET);
				}
			}
			else {
				HAL_GPIO_WritePin(BUZZER_GPIO_Port, BUZZER_Pin, GPIO_PIN_RESET);
			}
			
			//if((tick*OUTPUT_TASK_DELAY)%(OUTPUT_LCD_REFRESH_TIME)==0 ||updateCLcdNow) 
			if( abs((int)(halTick-halTickOnRefresh))>OUTPUT_LCD_REFRESH_TIME ||updateCLcdNow) 				
			{	
				halTickOnRefresh = halTick;
				if(waitForHvPwr>0) {
	//				if(g_adc_result[ESC_ADC_CH_BAT_V] >= prevBattV)
					{
						waitForHvPwr--;
						dbg_ioTask(2, "wait for HV Pwr... %d %d %d\r\n", waitForHvPwr, g_adc_result[ESC_ADC_CH_BAT_V], prevBattV);
					}
				}
				else {
					//OnRefreshCLcd(clcdState, pInfo);	
					//if((tick*OUTPUT_TASK_DELAY)%(OUTPUT_LCD_REFRESH_CURSOR_TIME)==0) {
					if( abs((int)(halTick-halTickOnRefreshCursor))>OUTPUT_LCD_REFRESH_CURSOR_TIME) {
						halTickOnRefreshCursor = halTick;
						OnRefreshCLcdCursor();
					}
					else {
						OnRefreshCLcd(clcdState, pInfo);	
					}
								
				}
			}
			
			
			if(tick%50==0) {
				if(nUplinkWait!=0) {
					nUplinkWait--;
				}
#if BLINK_WLINK
				HAL_GPIO_WritePin(nLED_WLINK_GPIO_Port, nLED_WLINK_Pin, GPIO_PIN_SET);
#endif
				
				
				
			}
#if 0
			if((tick%(100*g_autoInfoTime)==0 && g_sendAlive) 
				&&	((g_LoraConnected && pInfo->setting.lanLora==COMM_MODE_LORA) || pInfo->setting.lanLora==COMM_MODE_LAN) )
			{
				g_ackAll = 0;
				//extern void GetAllProc(StructApolloPacket* packet);
				//extern void Ack();
				//g_InfoPacket.cmd = CCMD_GET_ALL;
				extern U08 bUplinkBusy;
				if(bUplinkBusy) {
					dbg_ioTask(1, "uplink is busy\r\n");
					nUplinkWait = 100;
					bUplinkBusy = 0;
				}
				else if(nUplinkWait==0) {
					if(pInfo->setting.lanLora==COMM_MODE_LORA) {
						if(g_LoraConnected) {
							//GetAllProc(&g_InfoPacket);
							//Ack();
							g_InfoPacket.cmd = CCMD_RESPONSE_ESC;
							SendStatusAutoProc(&g_InfoPacket);
							/*
							extern  void TakeUplink();
							extern void GiveUplink();
							char tmp = '\n';
							memcpy(g_lanBuf, &tmp, 1);
							TakeUplink();
							g_lanBufCount = 1;		
							GiveUplink();							
							*/
							dbg_ioTask(1, "Send alive to lora\r\n");
						}
					}
					else { // serial mode
						//GetAllProc(&g_InfoPacket);
						g_InfoPacket.cmd = CCMD_RESPONSE_ESC;
						SendStatusAutoProc(&g_InfoPacket);
					}
				}
				else {
					dbg_ioTask(1, "wait uplink... %d\r\n", nUplinkWait);
				}
			}
#endif
			
			
		}	
		
  }	
}


static U08 prevBaudrate = 0;
void BaudRateChange(U08 index) 
{
	StructEscInfo *pInfo = GetEscInfo();
	const int nSerialSpeed[]	= {1200, 2400, 4800, 9600,19200, 38400, 57600, 115200};
	
	if(prevBaudrate != index) {
		prevBaudrate = index;
		dbg_ioTask(3, "Baudrate changed!\r\n");
	}
	else {
		return ;
	}

	huart5.Instance = UART5;
	huart5.Init.BaudRate = nSerialSpeed[index%sizeof(nSerialSpeed)];
	
	if(pInfo->setting.serialDatabit==1) {
		huart5.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else {
		huart5.Init.WordLength = UART_WORDLENGTH_8B;
	}
	
	if(pInfo->setting.serialStopbit==1) {
		huart5.Init.StopBits = UART_STOPBITS_2;
	}
	else {
		huart5.Init.StopBits = UART_STOPBITS_1;
	}
	
	if(pInfo->setting.serialParity==2) {
		huart5.Init.Parity = UART_PARITY_ODD;
	}
	else if(pInfo->setting.serialParity==1) {
		huart5.Init.Parity = UART_PARITY_EVEN;
	}
	else {
		huart5.Init.Parity = UART_PARITY_NONE;
	}
	
	huart5.Init.Mode = UART_MODE_TX_RX;
	huart5.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	//huart5.Init.OverSampling = UART_OVERSAMPLING_8;
	
	huart5.Init.OverSampling = UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&huart5) != HAL_OK)
	{
		Error_Handler();
	}
	else {
		dbg_ioTask(5, "%s %d\r\n", __FUNCTION__, huart5.Init.BaudRate);
		extern  uint8_t plcRxData;
		HAL_UART_Receive_IT(&huart5, &plcRxData, 1);
	}
}


uint8_t UpdateLcdState(uint8_t state, StructEscInfo *pInfo, uint8_t key)
{
	uint8_t nextState = 0;
	
	if(state >= CLCD_STATE_AUTO_DISCHARGING) { // include CLCD_STATE_STOP
		if(state == CLCD_STATE_STOP) {
			if (key & ALLOWED_KEY_ALL) {
				if(key == KEY_SW_HV_PWR) {
					nextState = CLCD_STATE_HV_OUTPUT;
					SetHvOnOFF(1);
					dbg_ioTask(4, "HV ON again\r\n");
				}
				else if(key == KEY_SW_TOGGLE) {
					dbg_ioTask(4, "HV TOGGLE\r\n");
					swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
				}
				else if(key == KEY_SW_SELECT) {
					dbg_ioTask(4, "GOTO SETUP\r\n");
					nextState = CLCD_STATE_SETUP_0;	
					//OnRemoteCLcd(CLCD_STATE_INITIAL, pInfo);					
				}
				else {				
					nextState = CLCD_STATE_INITIAL;						
					//OnRemoteCLcd(CLCD_STATE_INITIAL, pInfo);
					dbg_ioTask(4, "Clear ALARM\r\n");
				}
				return nextState;
			}			
		}
		else {
			switch(key)
			{
				case KEY_SW_HV_PWR:
				case KEY_SW_TOGGLE:
				case KEY_SW_SELECT:
					if(g_apBoardMode) {	 // Not allowed clear Alarm by Key, when AP Mode						
						nextState = state;
					}
					else {
						nextState = 0;
						
	//					pInfo->measure.arcAlarmStatus = 0;
	//					pInfo->stat.arcAlarmCount = 0;
						
	//					pInfo->measure.ocpAlarmStatus = 0;
	//					pInfo->stat.ocpAlarmCount = 0;
						
	//					pInfo->measure.battLowAlarmStatus = 0;					
					}
				break;
				default:
					nextState = state;
				break;
			}
		
		
			return nextState;
		}
	}
	
	else if(state == CLCD_STATE_INITIAL || state==CLCD_STATE_BOOT) {
		if(g_apBoardMode) {
			if(key!=KEY_SW_SELECT) {
				dbg_ioTask(2, "AP MODE!\r\n");
				return CLCD_STATE_INITIAL;
			}
		}
	}
	else if(state == CLCD_STATE_HV_OUTPUT) {
		if(g_apBoardMode) {
			dbg_ioTask(2, "AP MODE!\r\n");
			return CLCD_STATE_HV_OUTPUT;
		}
	}

	switch(key)
	{
		case KEY_SW_UP:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_UP_BIT];
		break;
		case KEY_SW_DOWN:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_DOWN_BIT];
		break;
		case KEY_SW_LEFT:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_LEFT_BIT];
		break;
		case KEY_SW_RIGHT:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_RIGHT_BIT];
		break;

		case KEY_SW_HV_PWR:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_HV_PWR_BIT];
		break;
		case KEY_SW_TOGGLE:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_TOGGLE_BIT];
		break;
		
		case KEY_SW_SELECT:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_SELECT_BIT];
		break;
		case KEY_SW_ESC_CANCEL:
			nextState = CLCD_MENU_TREES[state].subMenus[KEY_SW_ESC_CANCEL_BIT];
		break;
	}

	
	return nextState;
}



void ValueChangeDischarge(uint8_t bUpDown, int *value, int unit, uint8_t mode)
{
	int minV=0, maxV=3000;
	if(unit==0) { 
		unit = 1;
	}
	
	switch(mode) { 
		case VALUE_MODE_VOLT:		// Voltage: -3000~3000
		minV = -3000;
		maxV = 3000;
		break;
		case VALUE_MODE_AMP:		// Ampare: -3000~3000
		minV = 0;
		maxV = 2500;
		break;
		
		case VALUE_MODE_CONTROL:		// control 0/1
		minV = 0;
		maxV = 1;
		break;
		
		case 3:		// Voltage: -3000~3000
		minV = -3000;
		maxV = 3000;
		break;
	}
	
	
	if(bUpDown==VALUE_UP) { // Up
		//if (*value / unit == 9) *value -= 9*unit;
//		else
			*value += unit;		
	}
	else {
	//	if (*value / unit == 9) *value += 9*unit;
		//else 
		*value -= unit;		
	}
	
	if(*value<minV) {
		*value = minV;
	}
	if(*value>maxV) {
		*value = maxV;
	}	
	dbg_ioTask(5, "%s: v %d unit %d, mode %d\r\n",__FUNCTION__, *value, unit, mode);
}

void ValueChange(uint8_t bUpDown, int *value, int unit, uint8_t mode)
{
	int minV=-3000, maxV=3000;
	if(unit==0) { 
		unit = 1;
	}
	
	switch(mode) { 
		case VALUE_MODE_VOLT:		// Voltage: -3000~3000
		minV = -3000;
		maxV = 3000;
		break;
		case VALUE_MODE_AMP:		// Ampare: -3000~3000
		minV = 0;
		maxV = 2500;
		break;
		
		case VALUE_MODE_CONTROL:		// control 0/1
		minV = 0;
		maxV = 1;
		break;
		
		case 3:		// Voltage: -3000~3000
		minV = -3000;
		maxV = 3000;
		break;
	}
	
	
	if(bUpDown==VALUE_UP) { // Up
		//if (*value / unit == 9) *value -= 9*unit;
//		else
			*value += unit;		
	}
	else {
	//	if (*value / unit == 9) *value += 9*unit;
		//else 
		*value -= unit;		
	}
	
	if(*value<minV) {
		*value = minV;
	}
	if(*value>maxV) {
		*value = maxV;
	}	
	dbg_ioTask(5, "%s: v %d unit %d, mode %d\r\n",__FUNCTION__, *value, unit, mode);
}
	
const U08 cusorOnOfftTble[] = {5, 0};
int setV1_cursor=0, setV2_cursor=0;

U08 cursorByte;
U08 cursorByteDigit;
U16 cursorByteMax, cursorByteMin;
U16 cursorByteValue;

void InitCursorByte(U16 min, U16 max, U16 initValue) 
{
	cursorByteMin = min;
	cursorByteMax = max;

	if(max<10) {
		cursorByteDigit = 1;
		cursorByte = 0;
	}
	else if(max<100) {
		cursorByteDigit = 2;
		cursorByte = 1;
	}
	else if(max<1000) {
		cursorByteDigit = 3;
		cursorByte = 2;
	}
	else if(max<10000) {
		cursorByteDigit = 4;
		cursorByte = 3;
	}
	else {
		cursorByteDigit = 5;
		cursorByte = 4;
	}
	cursorByteValue = initValue;
	
	if(initValue<10) {
		nCursorPosX = cursorByte;
	}
	else if(initValue<100) {
		nCursorPosX = cursorByte - 1;
	}
	else if(initValue<1000) {
		nCursorPosX = cursorByte - 2;
	}
	else if(initValue<10000) {
		nCursorPosX = cursorByte - 3;
	}
	else {
		nCursorPosX = 0;
	}
	
	dbg_ioTask(5, "Init: %d %d~%d, %d digit\r\n", initValue, min, max, cursorByteDigit);
}

#define KeyCursorByte(key) if (key == KEY_SW_UP) IncreaseCursorByte();\
	else if(key == KEY_SW_DOWN) DecreaseCursorByte();\
	else if (key == KEY_SW_LEFT) LeftCursorByte(); \
	else if(key == KEY_SW_RIGHT) RightCursorByte(); \
	else if(key==KEY_SW_ESC_CANCEL) {  nextState = state-1; dbg_ioTask(1, "Cancel to %d\r\n",nextState);bCursorOn = 0;}
	
void LeftCursorByte()
{
	nCursorPosX--;
	if(nCursorPosX>cursorByteDigit) {
		nCursorPosX = 0;
	}
}
void RightCursorByte()
{
	nCursorPosX++;
	if(nCursorPosX>cursorByteDigit-1) {
		nCursorPosX = cursorByteDigit-1;
	}
}
void IncreaseCursorByte()
{
	switch(cursorByteDigit-nCursorPosX-1) {
		case 0:
			cursorByteValue += 1;
			break;
		case 1:
			cursorByteValue += 10;
			break;
		case 2:
			cursorByteValue += 100;
			break;
		case 3:
			cursorByteValue += 1000;
			break;
		case 4:
			cursorByteValue += 10000;
			break;
		default:
			cursorByteValue++;
	}
	if(cursorByteValue>=cursorByteMax) {
		cursorByteValue = cursorByteMax;
	}
	dbg_ioTask(5, "%s: v %d \r\n", __FUNCTION__, cursorByteValue);
}
void DecreaseCursorByte()
{
	switch(cursorByteDigit-nCursorPosX-1) {
		case 0:
			cursorByteValue -= 1;
			break;
		case 1:
			cursorByteValue -= 10;
			break;
		case 2:
			cursorByteValue -= 100;
			break;
		case 3:
			cursorByteValue -= 1000;
			break;
		case 4:
			cursorByteValue -= 10000;
			break;
		default:
			cursorByteValue--;
	}

	if(cursorByteValue>=cursorByteMax) {
		cursorByteValue = cursorByteMin;
	}
	if(cursorByteValue<=cursorByteMin) {
		cursorByteValue = cursorByteMin;
	}	
	dbg_ioTask(5, "%s: v %d \r\n", __FUNCTION__, cursorByteValue);
}

const U08 cusorSetV[] = {3, 12}; // {1,3, 10, 12};
U08 cusorSetVIndex = 0;

const U08 cusorOutputV[] = {3, 5}; 
U08 cusorOutputVIndex = 0;
//                            01234567890123456789
const char strOnControl[] =  " *ON   OFF";
const char strOffControl[] = "  ON  *OFF";
const char strYes[]	=		 " *YES  NO";
const char strNo[]	=		 "  YES *NO";
const U08 cusorOnOff[] = {6, 1};
U08 cursorOnOffIndex=0;

const char strSerialSpeed[][7]	= {"1200", "2400", "4800", "9600","19200", "38400", "57600", "115200"};
const char strModeLanLora[][7]	= {"Serial", "Lora", "Lan"};
const char strPorotocol[][7]	= {"SCPI", "Custom"};
const char strAutoToggle[][sizeof("No Auto Toggle")+1]	= {"No Auto Toggle", "Auto Toggle1", "Auto Toggle2"};

const U08 cusorIp[] = {0,1,2, 4,5,6, 8,9,10, 12,13,14};
U08 cusorIpIndex = 0;
U08 cursorIpSet[4];

const U08 cusorOutputvoltage[] = {1, 3, 10, 12};
U08 cusorOutputvoltageIndex = 0;

const U08 cusorAutoDischargeSetp[] = {1, 3};
U08 cusorOAutoDischargeSetpIndex = 0;


uint8_t OnKeyCLcd(uint8_t state, StructEscInfo *pInfo, uint8_t key)
{
	uint8_t nextState;
	//static uint8_t prevState=CLCD_STATE_INITIAL;
	
	nextState = UpdateLcdState(state, pInfo, key);
	
	// AP 4.8
	if(0) { //(g_apBoardDet && g_apHvOut) {
	}
	else {
#if CLEAR_ALARMS_ANY_EVENT		
		if(pInfo->measure.ocpAlarmStatus) {
			pInfo->measure.ocpAlarmStatus = 0;
			dbg_ioTask(3, "CLEAR OCP BY KEY\r\n");
			UpdateAlarmLED(pInfo, 1);
		}
		if(pInfo->measure.arcAlarmStatus) {
			pInfo->measure.arcAlarmStatus = 0;
			dbg_ioTask(3, "CLEAR ARC BY KEY\r\n");
			UpdateAlarmLED(pInfo, 1);
		}
		if(pInfo->measure.battLowAlarmStatus) {
			pInfo->measure.battLowAlarmStatus = 0;
			dbg_ioTask(3, "CLEAR BATT BY KEY\r\n");
			UpdateAlarmLED(pInfo, 1);
		}		
#endif
	}
	
	if(nextState==CLCD_STATE_NA) {	// 메뉴 이동이 아닌 경우
		//TODO: Do key proc...
		nextState = state; // go back prev state
		//dbg_ioTask(1, "No Menu move %d %d\r\n", state, key);
		
		switch(state) {
			case CLCD_STATE_STOP:
				/*
				if (key == KEY_SW_HV_PWR || key == KEY_SW_TOGGLE || key == KEY_SW_SELECT || key == KEY_SW_ESC_CANCEL) {
					if(key == KEY_SW_HV_PWR) {
						nextState = CLCD_STATE_HV_OUTPUT;
						SetHvOnOFF(1);
						dbg_ioTask(2, "HV ON again\r\n");
					}
					else if(key == KEY_SW_TOGGLE) {
						dbg_ioTask(2, "HV TOGGLE\r\n");
						swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
					}
					else if(key == KEY_SW_SELECT) {
						dbg_ioTask(2, "GOTO SETUP\r\n");
						nextState = CLCD_STATE_SETUP_0;						
					}
					else {
						nextState = CLCD_STATE_INITIAL;
						dbg_ioTask(3, "Clear ALARM\r\n");
					}
				}
				else {
					dbg_ioTask(3, "Press ESC...\r\n");
				}*/
				break;
		case CLCD_STATE_INITIAL:			
			SetHvOnOFF(0);  // ON KEY INITIAL, HV OFF
			
			if (key == KEY_SW_TOGGLE) { // 출력 토글
				//IS_TOGGLE ^= 1;
				swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
			}
			
			break;
		
		case CLCD_STATE_HV_OUTPUT:		
		{			
			if(g_apBoardMode) { // Skip key action when AP Mode
				dbg_ioTask(4, "No key action on AP MODE...\r\n");
				break;
			}
			else {
				if (key == KEY_SW_ESC_CANCEL || key== KEY_SW_HV_PWR) {	
					state = CLCD_STATE_INITIAL;
					dbg_ioTask(1, "KEYs OFF\r\n");
					SetHvOnOFF(0); // On key HV OFF
				}			
				if (key == KEY_SW_TOGGLE) { // 출력 토글
					//IS_TOGGLE ^= 1;
					swap_sign(&pInfo->stat.hvSetVolt1, &pInfo->stat.hvSetVolt2);
				}
				
				if(key==KEY_SW_SELECT) {
					nCursorPosY ^= 1;
				}
				if (key == KEY_SW_LEFT) {
					cusorOutputVIndex = (cusorOutputVIndex-1+sizeof(cusorOutputV))%sizeof(cusorOutputV);
					nCursorPosX=cusorOutputV[cusorOutputVIndex];
				}
				if(key == KEY_SW_RIGHT) {					
					cusorOutputVIndex = (cusorOutputVIndex+1+sizeof(cusorOutputV))%sizeof(cusorOutputV);
					nCursorPosX=cusorOutputV[cusorOutputVIndex];
				}
				if (key == KEY_SW_UP || key == KEY_SW_DOWN) {						
					int8_t bUpDn=1;
					uint8_t tmpV;
					int8_t minus = 0;
					
					if(nCursorPosY==0) {
						tmpV = pInfo->stat.hvSetVolt1;
					}
					else {
						tmpV = pInfo->stat.hvSetVolt2;
					}
					
					if(tmpV&0x80) { // process - value
						tmpV = tmpV & 0x7f;
						//bUpDn = -bUpDn;
						minus = 1;
					}

					if(cusorOutputVIndex==0) {
						bUpDn*=10;
					}
					 // - 고압 출력 중 음전원 up/down 반전 오류 수정.
					if(minus) {
						if (key == KEY_SW_DOWN) {								
							tmpV+=bUpDn;
						}
						else {
							tmpV-=bUpDn;
						}
					}
					else {
						if (key == KEY_SW_UP) {								
							tmpV+=bUpDn;
						}
						else {
							tmpV-=bUpDn;
						}
					}
					/*
					if(tmpV<-30) {
						tmpV=-30;
					}*/
					if(tmpV>MAX_VOLT_SET) {
						tmpV=MAX_VOLT_SET;
					}
					else if(tmpV<=MIN_VOLT_SET) {
						tmpV = MIN_VOLT_SET;
					}
					
					if(minus) {
						tmpV |= 0x80;
					}
					if(nCursorPosY==0) {
						pInfo->stat.hvSetVolt1=tmpV;
					}
					else {
						pInfo->stat.hvSetVolt2=tmpV;
					}
					dbg_ioTask(4, "%d %d %d\r\n", tmpV , pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);		
					
				}	
			}
		}
			break;
				
		// 중분류 메뉴는 메뉴 이동만...
		case CLCD_STATE_SETUP_0:
		case CLCD_STATE_SETUP_1:
		case CLCD_STATE_SETUP_2:
		case CLCD_STATE_SETUP_3:
		case CLCD_STATE_SETUP_4:
		case CLCD_STATE_SETUP_5:
		case CLCD_STATE_SETUP_6:
		case CLCD_STATE_SETUP_7:
		case CLCD_STATE_SETUP_8:
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE:	
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE_CURSOR:	
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				if(cursorOnOffIndex) {
					pInfo->stat.hvAutoToggle |= AUTO_TOGGLE1;
				}
				else {
					pInfo->stat.hvAutoToggle &= ~AUTO_TOGGLE1;
				}
				
				nextState = CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE;
				bCursorOn = 0;
			}
			break;			
			
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2:	
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2_CURSOR:	
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				if(cursorOnOffIndex) {
					pInfo->stat.hvAutoToggle |= AUTO_TOGGLE2;
				}
				else {
					pInfo->stat.hvAutoToggle &= ~AUTO_TOGGLE2;
				}				
				nextState = CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2;
				bCursorOn = 0;
			}
			break;					
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE:
		bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE_CURSOR:		
		{
			if (key == KEY_SW_LEFT) {
				cusorOutputvoltageIndex = (cusorOutputvoltageIndex -1 + sizeof(cusorOutputvoltage))%sizeof(cusorOutputvoltage);
				nCursorPosX = cusorOutputvoltage[cusorOutputvoltageIndex];
			}
			else if (key == KEY_SW_RIGHT) {
				cusorOutputvoltageIndex = (cusorOutputvoltageIndex +1 + sizeof(cusorOutputvoltage))%sizeof(cusorOutputvoltage);
				nCursorPosX = cusorOutputvoltage[cusorOutputvoltageIndex];
			}
			else if (key == KEY_SW_UP || key == KEY_SW_DOWN) {
				int *pVolt = &setV1_cursor;
				uint8_t bUpDn;
				
				if(cusorOutputvoltageIndex>=2) {
					pVolt = &setV2_cursor;
				}
				
				// decide Up/Down direction
				if (key == KEY_SW_UP) {
					bUpDn = VALUE_UP;
				}
				else {
					bUpDn = VALUE_DOWN;
				}
					
				if(cusorOutputvoltageIndex==0 || cusorOutputvoltageIndex==2) {
					ValueChange(bUpDn, pVolt, 1000, VALUE_MODE_VOLT);				
				}
				else {
					ValueChange(bUpDn, pVolt, 100, VALUE_MODE_VOLT);				
				}				
			}		
			else if(key==KEY_SW_SELECT) { // save & exit
				if(SetHvVolt(1, VoltToByte(setV1_cursor))==0) {
					SetHvVolt(2, VoltToByte(setV2_cursor));
				}
				nextState = CLCD_STATE_SETUP_0_SUB_VOLTAGE;
				bCursorOn = 0;
			}
		}
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL_CURSOR:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.autoDischargeStepCount = cursorByteValue;
				nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL;
				bCursorOn = 0;
			}	
			break;
			

		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC:
			bCursorOn = 0;
		break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR:
			{
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				switch(state) {
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR:
					pInfo->stat.autoDischargeKeepTimeStep1 = (cursorByteValue);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC;
				break;
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR:
					pInfo->stat.autoDischargeKeepTimeStep2 = (cursorByteValue);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC;
				break;
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR:					
					pInfo->stat.autoDischargeKeepTimeStep3 = (cursorByteValue);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC;
				break;
				}				
				
				bCursorOn = 0;
			}		
		}		
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR:
		{
			if (key == KEY_SW_LEFT) {
				cusorOAutoDischargeSetpIndex = (cusorOAutoDischargeSetpIndex -1 + sizeof(cusorAutoDischargeSetp))%sizeof(cusorAutoDischargeSetp);
				nCursorPosX = cusorAutoDischargeSetp[cusorOAutoDischargeSetpIndex];
			}
			else if (key == KEY_SW_RIGHT) {
				cusorOAutoDischargeSetpIndex = (cusorOAutoDischargeSetpIndex +1 + sizeof(cusorAutoDischargeSetp))%sizeof(cusorAutoDischargeSetp);
				nCursorPosX = cusorAutoDischargeSetp[cusorOAutoDischargeSetpIndex];
			}
			else if (key == KEY_SW_UP || key == KEY_SW_DOWN) {
				int *pVolt = &setV1_cursor;
				uint8_t bUpDn;
				
				if(cusorOAutoDischargeSetpIndex>=2) {
					pVolt = &setV2_cursor;
				}
				
				// decide Up/Down direction
				if (key == KEY_SW_UP) {
					bUpDn = VALUE_UP;
				}
				else {
					bUpDn = VALUE_DOWN;
				}
					
				if(cusorOAutoDischargeSetpIndex==0 || cusorOAutoDischargeSetpIndex==2) {
					ValueChangeDischarge(bUpDn, pVolt, 1000, VALUE_MODE_VOLT);				
				}
				else {
					ValueChangeDischarge(bUpDn, pVolt, 100, VALUE_MODE_VOLT);				
				}				
			}		
			else if(key==KEY_SW_SELECT) { // save & exit
				switch(state) {
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR:
					pInfo->stat.autoDischargeVoltStep1 = VoltToByte(setV1_cursor);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1;
				break;
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR:
					pInfo->stat.autoDischargeVoltStep2 = VoltToByte(setV1_cursor);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2;
				break;
				case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR:					
					pInfo->stat.autoDischargeVoltStep3 = VoltToByte(setV1_cursor);
					nextState = CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3;
				break;
				}				
				
				bCursorOn = 0;
			}		
		}			
			break;
		
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP_C:		
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.rampUpTime = cursorByteValue;
				nextState = CLCD_STATE_SETUP_2_SUB_RAMP_UP;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN_C:		
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.rampDownTime = cursorByteValue;
				nextState = CLCD_STATE_SETUP_2_SUB_RAMP_DOWN;
				bCursorOn = 0;
			}
			break;
				
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:			
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				switch(state) {
					case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
						pInfo->stat.ocpAlarmControl = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_3_SUB_OCP_CONTROL;
					break;
					case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
						pInfo->stat.ocpAlarmEvent = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_3_SUB_OCP_EVENT;
					break;
					case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
						pInfo->stat.arcAlarmControl = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_4_SUB_ARC_CONTROL;
					break;
					case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
						pInfo->stat.arcAlarmEvent = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_4_SUB_ARC_EVENT;
					break;
					case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
						pInfo->stat.battAlarmControl = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_5_SUB_BATT_CONTROL;						
					break;
					case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:					
						pInfo->stat.battEventControl = cursorOnOffIndex;
						nextState = CLCD_STATE_SETUP_5_SUB_BATT_EVENT;
					break;
					}
				
				bCursorOn = 0;
			}
			break;
			
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT_C:
			if (key == KEY_SW_UP) {
				cursorByteValue+=5;
				//if(cursorByteValue>250) cursorByteValue = 250;
				if(cursorByteValue>N_MAX_HV_CURR/10) cursorByteValue = N_MAX_HV_CURR/10;
			}
			else if(key == KEY_SW_DOWN) {
				cursorByteValue-=5;
				if(cursorByteValue<10) cursorByteValue = 10;
			}
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.ocpAlarmLimit = cursorByteValue;
				nextState = CLCD_STATE_SETUP_3_SUB_OCP_LIMIT;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT_C:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				if(cursorOnOffIndex) {					
					dbg_ioTask(5, "Got YES\r\n");
					pInfo->stat.ocpAlarmCount = 0;
					pInfo->measure.ocpAlarmStatus = 0;					
					UpdateAlarmLED(pInfo, 1);
				}
				nextState = CLCD_STATE_SETUP_3_SUB_OCP_COUNT;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT_C:	
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				if(cursorOnOffIndex) {					
					pInfo->stat.arcAlarmCount = 0;
					pInfo->measure.arcAlarmStatus = 0;
					UpdateAlarmLED(pInfo, 1);
				}
				nextState = CLCD_STATE_SETUP_4_SUB_ARC_COUNT;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.arcAlarmLimit = cursorByteValue;
				nextState = CLCD_STATE_SETUP_4_SUB_ARC_LIMIT;
				bCursorOn = 0;
			}
			break;
			
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->stat.battAlarmLimit = cursorByteValue;
				nextState = CLCD_STATE_SETUP_5_SUB_BATT_LIMIT;
				bCursorOn = 0;
			}
			break;
			
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT_C:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT;
				bCursorOn = 0;
				if(cursorOnOffIndex) {
					dbg_ioTask(1, "Load default\r\n");
					DefaultMode(pInfo);
				}
				else {
					dbg_ioTask(1, "Do NOT Load default\r\n");
				}
			}
			break;	
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN;				
				bCursorOn = 1;
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
				//LoadUserMode(pInfo);
			}
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD;
				pInfo->stat.userMode = cursorByteValue;
				bCursorOn = 0;
				if(cursorOnOffIndex) {
					dbg_ioTask(1, "Load User mode %d\r\n", pInfo->stat.userMode);
					LoadUserMode(pInfo);
				}
				else {
					dbg_ioTask(1, "Do NOT Load\r\n");
				}				
			}
			break;				
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit				
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN;
				bCursorOn = 1;
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
				dbg_ioTask(1, "Wave load? %d\r\n", pInfo->stat.waveMode);
				//LoadWaveMode(pInfo);
			}
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD;
				pInfo->stat.waveMode = cursorByteValue;
				bCursorOn = 0;
				if(cursorOnOffIndex) {
					dbg_ioTask(1, "Wave load %d\r\n", pInfo->stat.waveMode);
					LoadWaveMode(pInfo);
				}
				else {
					dbg_ioTask(1, "Do NOT Load\r\n");
				}				
			}
			
			break;						
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit				
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN;
				bCursorOn = 1;
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
				//SaveUserMode(pInfo);
			}
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {				
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) { // save & exit
				nextState = CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE;
				pInfo->stat.userMode = cursorByteValue;
				bCursorOn = 0;
				if(cursorOnOffIndex) {
					dbg_ioTask(1, "Save User mode %d\r\n", pInfo->stat.userMode);
					SaveUserMode(pInfo);
				}
				else {
					dbg_ioTask(1, "Do NOT Save\r\n");
				}				
			}
			break;					
		case CLCD_STATE_SETUP_8_SUB_INFO_DID_C:	
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.cid = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_DID;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.nPort = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_PORT;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT_C:
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.leQlight.port = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE_C:		
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.serialSpeed = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE;
				bCursorOn = 0;
				
				//BaudRateChange(pInfo->setting.serialSpeed);
				
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_IP_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_SM_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_GW_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C:			
			if (key == KEY_SW_LEFT) {
				cusorIpIndex = (cusorIpIndex -1 + sizeof(cusorIp))%sizeof(cusorIp);
				nCursorPosX = cusorIp[cusorIpIndex];
			}
			else if (key == KEY_SW_RIGHT) {
				cusorIpIndex = (cusorIpIndex +1 + sizeof(cusorIp))%sizeof(cusorIp);
				nCursorPosX = cusorIp[cusorIpIndex];
			}
			else if (key == KEY_SW_UP || key == KEY_SW_DOWN) {
				U08 *pIp = cursorIpSet+cusorIpIndex/3;
				int tmpIp = *pIp;
				int bUpDn;
				
				int d1,d2,d3;
				
				if (key == KEY_SW_UP) {
					bUpDn = 1;
				}
				else {
					bUpDn = -1;
				}
				if(tmpIp<0) {
					bUpDn = -bUpDn;
				}
					
				d1 = (tmpIp/100)%10;
				d2 = (tmpIp/10)%10;
				d3 = (tmpIp)%10;
				
				switch(cusorIpIndex%3) {
					case 0:
						d1 += bUpDn;
						if(d1>2) d1=2;
						if(d1<0) d1 = 0;
					break;
					case 1:
						d2 += bUpDn;
						if(d2>9) d2=9;
						if(d2<0)	d2=0;
					break;
					case 2:
						d3 += bUpDn;
						if(d3>9) d3=9;
						if(d3<0)	d3=0;
					break;
				}
				tmpIp = d1*100 + d2*10 +d3;
				
				
				*pIp = tmpIp;
			}		
			else if(key==KEY_SW_SELECT) { // save & exit	
				bCursorOn = 0;
				switch(state) {
					// ###### device
					case CLCD_STATE_SETUP_8_SUB_INFO_IP_C:
					memcpy(g_pWizNetInfo->ip, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_IP;
					
					break;
					case CLCD_STATE_SETUP_8_SUB_INFO_SM_C:
					memcpy(g_pWizNetInfo->sn, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_SM;
					
					break;
					case CLCD_STATE_SETUP_8_SUB_INFO_GW_C:
					memcpy(g_pWizNetInfo->gw, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_GW;
					
					break;
					
					// ###### server
					case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C:
					memcpy(pInfo->setting.serveIp, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP;					
					break;
					/*
					case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C:
					memcpy(pInfo->setting.server.sm, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM;
					
					break;
					case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C:
					memcpy(pInfo->setting.server.gw, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW;
					
					break;
					*/
					// ###### LE
					case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C:
					memcpy(pInfo->setting.leQlight.ip, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_LE_IP;
					
					break;
					case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C:
					memcpy(pInfo->setting.leQlight.sm, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_LE_SM;
					
					break;
					case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C:
					memcpy(pInfo->setting.leQlight.gw, cursorIpSet, 4);				
					nextState = CLCD_STATE_SETUP_8_SUB_INFO_LE_GW;
					
					break;
				}
			}	
			
		break;
			
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE_C:	
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.lanLora = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_MODE;
				bCursorOn = 0;
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL_C:	
			KeyCursorByte(key)
			else if(key==KEY_SW_SELECT) { // save & exit
				pInfo->setting.protocol = cursorByteValue;
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL;
				bCursorOn = 0;
			}
			break;
			
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT_C:
			if (key == KEY_SW_LEFT || key == KEY_SW_RIGHT) {						
				cursorOnOffIndex = (cursorOnOffIndex -1 + sizeof(cusorOnOff))%sizeof(cusorOnOff);
				nCursorPosX = cusorOnOff[cursorOnOffIndex];
			}
			if(key==KEY_SW_SELECT) {
				nextState = CLCD_STATE_SETUP_8_SUB_INFO_REBOOT;
				bCursorOn = 0;
				if(cursorOnOffIndex==1) {
					dbg_ioTask(1, "REBOOT!\r\n");
					Reboot();
				}
			}
			break;

		default:
			dbg_ioTask(4, "NA state\r\n");
		break;
		}			
	}
	
	
	else {
		switch(nextState) {
		case CLCD_STATE_STOP:
			dbg_ioTask(4, "state STOP\r\n");
				break;
		case CLCD_STATE_INITIAL:			
			dbg_ioTask(4, "INIT state OFF\r\n");
			SetHvOnOFF(0); // INITIAL STATE,  HV OFF				
			
			bCursorOn = 0;
			nCursorPosY=0;
			nCursorPosX=0;
		break;
		case CLCD_STATE_HV_OUTPUT:
			if(hvOutState==HV_STATE_READY) {
				SetHvOnOFF(1);
			}
			else {
				SetHvOnOFF(0);
				nextState = CLCD_STATE_INITIAL;
			}
			bCursorOn = 1;
			cusorOutputVIndex = 0;
			
			nCursorPosY=0;
			nCursorPosX=cusorOutputV[cusorOutputVIndex];
			dbg_ioTask(4, "%s %d %d\r\n",__func__, pInfo->stat.hvSetVolt1, pInfo->stat.hvSetVolt2);
		break;
		
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE_CURSOR:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex = (pInfo->stat.hvAutoToggle & AUTO_TOGGLE1)?1:0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2_CURSOR:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex = (pInfo->stat.hvAutoToggle & AUTO_TOGGLE2)?1:0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE_CURSOR:
		bCursorOn = 1;
		nCursorPosY=1;
		cusorOutputvoltageIndex = 0;
		nCursorPosX=cusorOutputvoltage[cusorOutputvoltageIndex];
		
		setV1_cursor = ByteToVolt(pInfo->stat.hvSetVolt1);
		setV2_cursor = ByteToVolt(pInfo->stat.hvSetVolt2);

			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL_CURSOR:
		bCursorOn = 1;
		nCursorPosY=1;
		nCursorPosX = 0;
		InitCursorByte(0,3, pInfo->stat.autoDischargeStepCount);		
			break;		

		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3:
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC:
			bCursorOn = 0;
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR:
			bCursorOn = 1;
			nCursorPosY=1;
			InitCursorByte(0, 250, pInfo->stat.autoDischargeKeepTimeStep1);					
		break;
			
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR:
			bCursorOn = 1;
			nCursorPosY=1;
			InitCursorByte(0, 250, pInfo->stat.autoDischargeKeepTimeStep2);					
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR:
			bCursorOn = 1;
			nCursorPosY=1;
			InitCursorByte(0, 250, pInfo->stat.autoDischargeKeepTimeStep3);					
		break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR:
		bCursorOn = 1;
		nCursorPosY=1;
		cusorOAutoDischargeSetpIndex = 0;
		nCursorPosX=cusorAutoDischargeSetp[cusorOAutoDischargeSetpIndex];		
		setV1_cursor = ByteToVolt(pInfo->stat.autoDischargeVoltStep1);				
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR:
		bCursorOn = 1;
		nCursorPosY=1;
		cusorOAutoDischargeSetpIndex = 0;
		nCursorPosX=cusorAutoDischargeSetp[cusorOAutoDischargeSetpIndex];
		
		setV1_cursor = ByteToVolt(pInfo->stat.autoDischargeVoltStep2);
		//setV2_cursor = pInfo->stat.autoDischargeVoltStep1;
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR:
		bCursorOn = 1;
		nCursorPosY=1;
		cusorOAutoDischargeSetpIndex = 0;
		nCursorPosX=cusorAutoDischargeSetp[cusorOAutoDischargeSetpIndex];
		
		setV1_cursor = ByteToVolt(pInfo->stat.autoDischargeVoltStep3);
		//setV2_cursor = pInfo->stat.autoDischargeVoltStep1;
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP:
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP_C:
		bCursorOn = 1;
		nCursorPosY=1;
		nCursorPosX = 0;
		InitCursorByte(3, 100, pInfo->stat.rampUpTime);					
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN_C:			
		bCursorOn = 1;
		nCursorPosY=1;
		nCursorPosX = 0;
		InitCursorByte(3, 100, pInfo->stat.rampDownTime);
			break;
		
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL:
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT:
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL:
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT:
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL:
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT:
		bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			switch(nextState) {
			case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
				cursorOnOffIndex=pInfo->stat.ocpAlarmControl;
			break;
			case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
				cursorOnOffIndex=pInfo->stat.ocpAlarmEvent;
			break;
			case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
				cursorOnOffIndex=pInfo->stat.arcAlarmControl;
			break;
			case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
				cursorOnOffIndex=pInfo->stat.arcAlarmEvent;
			break;
			case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
				cursorOnOffIndex=pInfo->stat.battAlarmControl;
			break;
			case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:
				cursorOnOffIndex=pInfo->stat.battEventControl;
			break;
			}
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
			
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT:
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT:
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT:
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT:
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT:
			bCursorOn = 0;
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 2;
			cursorByteValue = pInfo->stat.ocpAlarmLimit;	
		break;
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 65000, pInfo->stat.arcAlarmLimit);
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 99, pInfo->stat.battAlarmLimit);	
		break;
		
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT_C: // no / yes
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0; //pInfo->measure.ocpAlarmStatus;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT_C: // no / yes
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0; //pInfo->measure.ocpAlarmStatus;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		
			
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(MIN_WAVE_MODE, MAX_WAVE_MODE, pInfo->stat.waveMode==0?1:pInfo->stat.waveMode);			
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(MIN_WAVE_MODE, MAX_USER_MODE, pInfo->stat.userMode==0?1:pInfo->stat.userMode);	
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C:			
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(MIN_WAVE_MODE, MAX_USER_MODE, pInfo->stat.userMode==0?1:pInfo->stat.userMode);	
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
		break;
		
		
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0,7, pInfo->setting.serialSpeed);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_DID_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, N_MAX_CID, pInfo->setting.cid);
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 65535, pInfo->setting.nPort);
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 65535, pInfo->setting.leQlight.port);
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_IP_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, g_pWizNetInfo->ip, 4);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SM_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, g_pWizNetInfo->sn, 4);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_GW_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, g_pWizNetInfo->gw, 4);
		break;

		// server
		
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.serveIp, 4);
		break;
		/*
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.server.sm, 4);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.server.gw, 4);
		break;
		*/
		// le		
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.leQlight.ip, 4);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.leQlight.sm, 4);
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			cusorIpIndex = 0;
			nCursorPosX = cusorIp[cusorIpIndex];
			memcpy(cursorIpSet, pInfo->setting.leQlight.gw, 4);
		break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 2, pInfo->setting.lanLora);
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL_C:
			bCursorOn = 1;
			nCursorPosY=1;
			nCursorPosX = 0;
			InitCursorByte(0, 1, pInfo->setting.protocol);
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT_C:
			bCursorOn = 1;
			nCursorPosY=1;
			cursorOnOffIndex=0;
			nCursorPosX = cusorOnOff[cursorOnOffIndex];
			break;
		
		default:
			bCursorOn = 0;
		break;
		}
	}
	
	
	
	return nextState;
}

void OnRefreshCLcdCursor(void) 
{	
#define CURSOR_SET_ONCE	0
	if(bCursorOn!=bCursorOnPrev) {
		//dbg_ioTask(1, "Cursor changed %d\r\n", bCursorOn);						
		bCursorOnPrev = bCursorOn;

		#if CURSOR_SET_ONCE
		if(bCursorOn)
		{
			clcdSetCursor(nCursorPosY, nCursorPosX);
			clcdCursor(1);							
		}
		else {
			clcdCursor(0);
		}
		#else
		clcdCursor(0);
		#endif
	}
	#if CURSOR_SET_ONCE==0
	if(bCursorOn) {								
		nCursorPosXPrev = nCursorPosX;
		nCursorPosYPrev = nCursorPosY;
		clcdSetCursor(nCursorPosY, nCursorPosX);
		bCursorBlink ^=1;
		clcdCursor(bCursorBlink);						
	}
	#endif
	
	if(updateCLcdNow) {
		updateCLcdNow = 0;
	}	
}				
	
uint8_t OnRefreshCLcd(uint8_t state, StructEscInfo *pInfo)
{
	uint8_t networkMode = 'N';	
	
	// Fix Cursor blink on Refresh..
	clcdCursor(0);
	
	if(g_APMode) {
		networkMode = 'A';
	}
	else {
		if(pInfo->setting.lanLora == COMM_MODE_LORA) {
			networkMode = 'W';
		}
		else if(pInfo->setting.lanLora == COMM_MODE_LAN) {
			networkMode = 'N';
		}
		else {
			networkMode = ' ';
		}
	}
	
	if(IsHvOn) {
		if(pInfo->measure.battLowAlarmStatus) {
			//dbg_ioTask(1, "BATT low Alarm\r\n");
		}
	}
	
//	clcdCursor(0);
	
	// Proc line 1...
	switch(state) {
		case CLCD_STATE_STOP:
			bCursorOn = 0;
			/*
		sprintf(lcdLineBuf0, "Processing-STOP");
		if(pInfo->measure.battLowAlarmStatus || g_lastAlarm == ALM_BATT) {
			sprintf(lcdLineBuf1, "BATT=>     %d [%%  ]", pInfo->measure.lastBattPercent);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		else if(pInfo->measure.ocpAlarmStatus || g_lastAlarm == ALM_OCP) {
			sprintf(lcdLineBuf1, "OCP =>     %d [uA ]", pInfo->measure.lastOcp);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		else if(pInfo->measure.arcAlarmStatus || g_lastAlarm == ALM_ARC) {
			sprintf(lcdLineBuf1, "ARC =>     %d [cnt]", pInfo->measure.lastArc);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		*/
		break;

		case CLCD_STATE_TG: // NA
			break;
		
		case CLCD_STATE_SETUP_0:
		case CLCD_STATE_SETUP_1:
		case CLCD_STATE_SETUP_2:
		case CLCD_STATE_SETUP_3:
		case CLCD_STATE_SETUP_4:
		case CLCD_STATE_SETUP_5:
		case CLCD_STATE_SETUP_6:
		case CLCD_STATE_SETUP_7:
		case CLCD_STATE_SETUP_8:				
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_SETUP]);
			sprintf(lcdLineBuf1,"%s", strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			
			break;
		
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE:
			if(pInfo->stat.hvAutoToggle & AUTO_TOGGLE1) {
				sprintf(lcdLineBuf1,"%s",strOnControl);
			}
			else {
				sprintf(lcdLineBuf1,"%s",strOffControl);
			}
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE_CURSOR:
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl);
			}
			break;
			
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2:
			if(pInfo->stat.hvAutoToggle & AUTO_TOGGLE2) {
				sprintf(lcdLineBuf1,"%s",strOnControl);
			}
			else {
				sprintf(lcdLineBuf1,"%s",strOffControl);
			}
			
			break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2_CURSOR:
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl);
			}			
			
			break;
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_VOLT]);			
			sprintf(lcdLineBuf1, "%+1.2fkV  %+1.2fkV",
					ByteToVolt(pInfo->stat.hvSetVolt1)/1000.0, ByteToVolt(pInfo->stat.hvSetVolt2)/1000.0);

			
			break;
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_VOLT]);			
			sprintf(lcdLineBuf1, "%+1.2fkV  %+1.2fkV",
					setV1_cursor/1000.0, setV2_cursor/1000.0);

			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_CONTROL]);
			sprintf(lcdLineBuf1,"%d         Step", pInfo->stat.autoDischargeStepCount);
			
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_CONTROL]);
			sprintf(lcdLineBuf1,"%d         Step", cursorByteValue);
						
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP1]);			
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)ByteToVolt(pInfo->stat.autoDischargeVoltStep1)/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
					
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP1]);
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)(setV1_cursor)/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
					
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1]);			
			sprintf(lcdLineBuf1,"%3.3d", (pInfo->stat.autoDischargeKeepTimeStep1));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");			
					
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1]);
			sprintf(lcdLineBuf1,"%3.3d", (cursorByteValue));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");					
					
			break;
		
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP2]);
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)ByteToVolt(pInfo->stat.autoDischargeVoltStep2)/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
						
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP2]);
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)setV1_cursor/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
							
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2]);			
			sprintf(lcdLineBuf1,"%3.3d", (pInfo->stat.autoDischargeKeepTimeStep2));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");			
					
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2]);
			sprintf(lcdLineBuf1,"%3.3d", (cursorByteValue));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");					
					
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP3]);
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)ByteToVolt(pInfo->stat.autoDischargeVoltStep3)/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
											
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_DISCHARGE_STEP3]);
			sprintf(lcdLineBuf1,"% 1.2fkV", (float)setV1_cursor/1000);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16,"VOLT");			
			break;
		
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3]);			
			sprintf(lcdLineBuf1,"%3.3d", (pInfo->stat.autoDischargeKeepTimeStep3));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");			
					
			break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3]);
			sprintf(lcdLineBuf1,"%3.3d", (cursorByteValue));
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+16," SEC");					
					
			break;
		
		
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_RAMPUP]);
			sprintf(lcdLineBuf1,"%3.3d00mSec", (pInfo->stat.rampUpTime));
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_RAMPUP]);
			sprintf(lcdLineBuf1,"%3.3d00mSec", (cursorByteValue));
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_RAMPDOWN]);
			sprintf(lcdLineBuf1,"%3.3d00mSec", (pInfo->stat.rampDownTime));
			break;
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_RAMPDOWN]);
			sprintf(lcdLineBuf1,"%3.3d00mSec", (cursorByteValue));
										
			break;
		
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL: 
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_CONTROL]);
			sprintf(lcdLineBuf1, pInfo->stat.ocpAlarmControl?strOnControl:strOffControl);
			 
										
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_CONTROL]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			 
										
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_EVENT]);
			sprintf(lcdLineBuf1, pInfo->stat.ocpAlarmEvent?strOnControl:strOffControl);
			 
										
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_EVENT]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			 
										
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_LIMIT]);
			//sprintf(lcdLineBuf1,"%1.1fuA %1.1fuA", (float)pInfo->stat.ocpAlarmLimit, (float)pInfo->stat.ocpAlarmLimit);
			sprintf(lcdLineBuf1,"%1.2fmA", ((float)pInfo->stat.ocpAlarmLimit)/(float)100.0); // *10 uA
			//sprintf(lcdLineBuf1,"%duA", ((float)pInfo->stat.ocpAlarmLimit)*10);
			 
														
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_LIMIT]);
			//sprintf(lcdLineBuf1,"%1.1fuA  %1.1fuA", (float)cursorByteValue, (float)cursorByteValue);
			sprintf(lcdLineBuf1,"%1.2fmA", (float)cursorByteValue/(float)100.0);
			//sprintf(lcdLineBuf1,"%duA", (float)cursorByteValue*10);
			 
														
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_COUNT]);
			sprintf(lcdLineBuf1,"%d", pInfo->stat.ocpAlarmCount);
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT_C:			
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_COUNT]);
			sprintf(lcdLineBuf1,"%s", cursorOnOffIndex?strYes:strNo);
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+14,"Clear?");
			break;
		case CLCD_STATE_SETUP_3_SUB_OCP_STATUS:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_OCP_STATUS]);
			sprintf(lcdLineBuf1,pInfo->measure.ocpAlarmStatus?"Occur":"Normal");
			break;			

		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_CONTROL]);
			sprintf(lcdLineBuf1, pInfo->stat.arcAlarmControl?strOnControl:strOffControl);
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
			sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_CONTROL]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_EVENT]);
			sprintf(lcdLineBuf1, pInfo->stat.arcAlarmEvent?strOnControl:strOffControl);
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_EVENT]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_LIMIT]);
			sprintf(lcdLineBuf1,"%5.5d count", pInfo->stat.arcAlarmLimit);
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_LIMIT]);
			sprintf(lcdLineBuf1,"%5.5d count",cursorByteValue);			
			break;

		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_COUNT]);
			sprintf(lcdLineBuf1,"%d", pInfo->stat.arcAlarmCount);
			 
			 
			break;
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_COUNT]);
			//sprintf(lcdLineBuf1,"%s", cursorOnOffIndex?strOnControl:strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			sprintf(lcdLineBuf1,"%s", cursorOnOffIndex?strYes:strNo);	
			memset(lcdLineBuf1+strlen(lcdLineBuf1), ' ', sizeof(lcdLineBuf1)-1-strlen(lcdLineBuf1)); 
			strcpy(lcdLineBuf1+14,"Clear?");		
			break;
		
		case CLCD_STATE_SETUP_4_SUB_ARC_STATUS:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_ARC_STATUS]);
			sprintf(lcdLineBuf1,pInfo->measure.arcAlarmStatus?"Occur":"Normal");
			break;
		
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_CONTROL]);
			sprintf(lcdLineBuf1, pInfo->stat.battAlarmControl?strOnControl:strOffControl);			
																		
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_CONTROL]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT:			
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_EVENT]);
			sprintf(lcdLineBuf1, pInfo->stat.battEventControl?strOnControl:strOffControl);			
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_EVENT]);
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strOnControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strOffControl); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_LIMIT]);
			sprintf(lcdLineBuf1,"%2.2d%%", pInfo->stat.battAlarmLimit);				
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_LIMIT]);
			sprintf(lcdLineBuf1,"%2.2d%%", cursorByteValue);				
			break;
		
		case CLCD_STATE_SETUP_5_SUB_BATT_STATUS:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_ALARM_STATUS]);
			if(DET_BATTERY) {				
				sprintf(lcdLineBuf1,"%2.2d%%", pInfo->measure.battPercent);
			}
			else {
				sprintf(lcdLineBuf1,"N/A");
			}
				
			break;

		case CLCD_STATE_SETUP_5_SUB_BATT_ALARM_STATUS:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_ALARM_STATUS]);
			if(pInfo->measure.battLowAlarmStatus) {				
				sprintf(lcdLineBuf1,"Battery Low");
			}
			else {
				sprintf(lcdLineBuf1,"Normal");
			}
					 
				
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_VOLTAGE:
			if(DET_BATTERY) {				
				sprintf(lcdLineBuf1,"%2.1fV", (float)pInfo->measure.battStatusVolt/10);	
			}
			else {
				sprintf(lcdLineBuf1,"N/A");
			} 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_VOLT]);
			 
				
			break;
		case CLCD_STATE_SETUP_5_SUB_BATT_CURRENT:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_BATT_CURRENT]);
			sprintf(lcdLineBuf1,"%3.1fmA", (float)10.0*(pInfo->measure.battCurr));
			 
			break;
		
		case CLCD_STATE_SETUP_6_SUB_TEMP:	
		{	
			//pInfo->measure.temp1 = -1013;			pInfo->measure.temp2 = -1015; // TEST Temp1/2
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_SENSOR_TEMP]);
			U08 t1NA = 0;
			U08 t2NA = 0;
			
			if( pInfo->measure.temp1-pInfo->setting.temp1Tune==TEMP_NA 
				|| (pInfo->measure.temp1-pInfo->setting.temp1Tune)/10>TEMP_MAX
				|| (pInfo->measure.temp1-pInfo->setting.temp1Tune)/10<TEMP_MIN
			) {
				t1NA = 1;
			}
			
			if( pInfo->measure.temp2-pInfo->setting.temp2Tune==TEMP_NA 
				|| (pInfo->measure.temp2-pInfo->setting.temp2Tune)/10>TEMP_MAX
				|| (pInfo->measure.temp2-pInfo->setting.temp2Tune)/10<TEMP_MIN
			) {
				t2NA = 1;
			}
			if( t1NA && t2NA ) {
				sprintf(lcdLineBuf1,"N/A     N/A");
			}
			else if( t1NA ) {
				sprintf(lcdLineBuf1,"N/A   %2.1f%cC",(pInfo->measure.temp2)/10.0, CHAR_CODE_DEGREE);
			}
			else if( t2NA ) {
				sprintf(lcdLineBuf1,"%2.1f%cC   N/A", (pInfo->measure.temp1)/10.0, CHAR_CODE_DEGREE);
			}
			else {
				sprintf(lcdLineBuf1,"%2.1f%cC   %2.1f%cC", (pInfo->measure.temp1)/10.0, CHAR_CODE_DEGREE, (pInfo->measure.temp2)/10.0, CHAR_CODE_DEGREE);
			}
			//sprintf(lcdLineBuf1,"%d'C     %d'C", (pInfo->measure.temp1), (pInfo->measure.temp2));
			 
		}
			break;
		case CLCD_STATE_SETUP_6_SUB_PRESSURE:
			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_SENSOR_PRESS]);
			if(pInfo->measure.press <= PRESSURE_NA) {
				sprintf(lcdLineBuf1,"N/A");
			}
			else {
				//sprintf(lcdLineBuf1,"Normal  760mmHg"); //
				sprintf(lcdLineBuf1,"Normal  %dmmHg", pInfo->measure.press); //
			}
			 
				
			break;
		
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT:
			sprintf(lcdLineBuf1,"%s", strNo);
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT_C:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_DEFAULT]);
			if(cursorOnOffIndex==1) {
				sprintf(lcdLineBuf1,"%s", strYes); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strNo); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;

		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD:			 
			memset(lcdLineBuf1, ' ', sizeof(lcdLineBuf1)-1); 
			
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_WAVE_LOAD]);
			sprintf(lcdLineBuf1,"%2.2d", cursorByteValue);	
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_DEFAULT]);
			if(cursorOnOffIndex==1) {
				sprintf(lcdLineBuf1,"%s", strYes); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strNo); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;		
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD:			 
			memset(lcdLineBuf1, ' ', sizeof(lcdLineBuf1)-1); 
								
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C:		 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_USER_LOAD]);
			sprintf(lcdLineBuf1,"%d", cursorByteValue);	
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_DEFAULT]);
			if(cursorOnOffIndex==1) {
				sprintf(lcdLineBuf1,"%s", strYes); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strNo); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;		
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE:	
			memset(lcdLineBuf1, ' ', sizeof(lcdLineBuf1)-1); 
				
			break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_USER_SAVE]);
			sprintf(lcdLineBuf1,"%d", cursorByteValue);	
			break;		
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_MODE_DEFAULT]);
			if(cursorOnOffIndex==1) {
				sprintf(lcdLineBuf1,"%s", strYes); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strNo); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;		
		
		case CLCD_STATE_SETUP_8_SUB_INFO_SERIAL:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERIAL]);
			sprintf(lcdLineBuf1,"%s", (char*)pInfo->setting.serialNumber); 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_HW:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_VER_HW]);
			sprintf(lcdLineBuf1,"%d", pInfo->setting.hwVer);			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_FW:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_VER_FW]);
			sprintf(lcdLineBuf1,"%d", pInfo->setting.fwVer);			 
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_DID:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_DID]);
			sprintf(lcdLineBuf1,"%4.4d", pInfo->setting.cid);			 
			break;		
		case CLCD_STATE_SETUP_8_SUB_INFO_DID_C:
			sprintf(lcdLineBuf1,"%4.4d", (cursorByteValue));
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_BAUDRATE]);
			sprintf(lcdLineBuf1,"%d: %sbps", pInfo->setting.serialSpeed, strSerialSpeed[pInfo->setting.serialSpeed%sizeof(strSerialSpeed)]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE_C:
//			clcdCursor(1);
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_BAUDRATE]);
			sprintf(lcdLineBuf1,"%d: %sbps", cursorByteValue, strSerialSpeed[cursorByteValue%sizeof(strSerialSpeed)]);
			 
			break;
		
		/// ############ Device
		case CLCD_STATE_SETUP_8_SUB_INFO_IP:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_IP]);			
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", g_pWizNetInfo->ip[0], g_pWizNetInfo->ip[1]
				,g_pWizNetInfo->ip[2], g_pWizNetInfo->ip[3]);			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_IP_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_IP]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_SM:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SM]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", g_pWizNetInfo->sn[0], g_pWizNetInfo->sn[1]
				,g_pWizNetInfo->sn[2], g_pWizNetInfo->sn[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SM_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SM]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_GW:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", g_pWizNetInfo->gw[0], g_pWizNetInfo->gw[1]
				,g_pWizNetInfo->gw[2], g_pWizNetInfo->gw[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_GW_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT:
			sprintf(lcdLineBuf1, "%5.5d", pInfo->setting.nPort);
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT_C:
			sprintf(lcdLineBuf1, "%5.5d", cursorByteValue);
		break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT:
			sprintf(lcdLineBuf1, "%5.5d", pInfo->setting.leQlight.port);
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT_C:
			sprintf(lcdLineBuf1, "%5.5d", cursorByteValue);
		break;
		

/// ############ SERVER
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_IP]);			
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.serveIp[0], pInfo->setting.serveIp[1]
				,pInfo->setting.serveIp[2], pInfo->setting.serveIp[3]);			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_IP]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		/*		
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_SM]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.server.sm[0], pInfo->setting.server.sm[1]
				,pInfo->setting.server.sm[2], pInfo->setting.server.sm[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_SM]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.server.gw[0], pInfo->setting.server.gw[1]
				,pInfo->setting.server.gw[2], pInfo->setting.server.gw[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_SERVER_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
			*/

/// ############ LE
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_IP]);			
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.leQlight.ip[0], pInfo->setting.leQlight.ip[1]
				,pInfo->setting.leQlight.ip[2], pInfo->setting.leQlight.ip[3]);			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_IP]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM:			 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_SM]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.leQlight.sm[0], pInfo->setting.leQlight.sm[1]
				,pInfo->setting.leQlight.sm[2], pInfo->setting.leQlight.sm[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_SM]);			
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", pInfo->setting.leQlight.gw[0], pInfo->setting.leQlight.gw[1]
				,pInfo->setting.leQlight.gw[2], pInfo->setting.leQlight.gw[3]);
			 
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C:
//			sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_LE_GW]);
			sprintf(lcdLineBuf1,"%3.3d.%3.3d.%3.3d.%3.3d", cursorIpSet[0], cursorIpSet[1]
				,cursorIpSet[2], cursorIpSet[3]);	
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE:				 
			sprintf(lcdLineBuf1,"%s", strModeLanLora[pInfo->setting.lanLora%sizeof(strSerialSpeed)]);		
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE_C:				 
			sprintf(lcdLineBuf1,"%s", strModeLanLora[cursorByteValue%sizeof(strSerialSpeed)]);		
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL:				 
			sprintf(lcdLineBuf1,"%s", strPorotocol[pInfo->setting.protocol%sizeof(strSerialSpeed)]);		
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL_C:
			sprintf(lcdLineBuf1,"%s", strPorotocol[cursorByteValue%sizeof(strSerialSpeed)]);		
			break;

		
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT_C:
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_REBOOT]);		
			if(cursorOnOffIndex) {
				sprintf(lcdLineBuf1,"%s", strYes); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			else {
				sprintf(lcdLineBuf1,"%s", strNo); //strLcdShowLine1Setup[state-CLCD_STATE_SETUP_0]);
			}
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT:				 
			//sprintf(lcdLineBuf0, "%s", strLcdShowLine0[MENU_TITLE_INFO_REBOOT]);
			memset(lcdLineBuf1, ' ', sizeof(lcdLineBuf1)-1);		
			break;
		case CLCD_STATE_INITIAL:
			break;
		default:
			//sprintf(lcdLineBuf0, "ERROR!");
			//sprintf(lcdLineBuf1,"state? %d", state);
			dbg_ioTask(9, "%s ERROR state %d\r\n", __FUNCTION__, state);
			 
							
			break;
		
	}
	
	
// Edit by jwaani @2020-0210: Select menu title at Once -> Special cases....
	if(state == CLCD_STATE_INITIAL) {
		//case CLCD_STATE_INITIAL:	
		if(hvOutState!=HV_STATE_READY && hvOutState<HV_STATE_RAMPDOWN_DONE) {
			bCursorOn = 1;
			ShowHvVolt(pInfo);
		}		
		else {
			bCursorOn = 0;
			sprintf(lcdLineBuf0, "A:   STAND BY     %c%c"
								,pInfo->stat.autoQueryControl?'Q':' ', IS_TOGGLE?'T':' ');				
			sprintf(lcdLineBuf1, "B:   STAND BY      %c", networkMode);				
		}
	}
	else if(state == CLCD_STATE_HV_OUTPUT) {
		//case CLCD_STATE_HV_OUTPUT:		
		//SetHvOnOFF(1); // disabled by jwaani @2020-0526
		if(IsHvOn) {				
			ShowHvVolt(pInfo);
		}
	}
	else if(state == CLCD_STATE_AUTO_DISCHARGING) {
		//case CLCD_STATE_AUTO_DISCHARGING: 			
		if(hvOutState!=HV_STATE_READY) {
			sprintf(lcdLineBuf0, "Processing");
			sprintf(lcdLineBuf1, "Auto-Discharge...");				
		}
		else {
			OnRemoteCLcd(CLCD_STATE_INITIAL, pInfo);				
		}		
	}	
	else if(state == CLCD_STATE_BOOT) {
		UpdateLcdData(STR_GREET_0, 0);	
		UpdateLcdData(STR_GREET_1, 1);			
	}
	else if(state == CLCD_STATE_STOP) {
		sprintf(lcdLineBuf0, "Processing-STOP");
		if(pInfo->measure.battLowAlarmStatus || g_lastAlarm == ALM_BATT) {
			sprintf(lcdLineBuf1, "BATT=>     %d [%%  ]", pInfo->measure.lastBattPercent);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		else if(pInfo->measure.ocpAlarmStatus || g_lastAlarm == ALM_OCP) {
			sprintf(lcdLineBuf1, "OCP =>     %d [uA ]", pInfo->measure.lastOcp);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		else if(pInfo->measure.arcAlarmStatus || g_lastAlarm == ALM_ARC) {
			sprintf(lcdLineBuf1, "ARC =>     %d [cnt]", pInfo->measure.lastArc);
			dbg_ioTask(4, "last alarm %d\r\n", g_lastAlarm);
		}
		else {
			sprintf(lcdLineBuf1, "ALM case? %d",g_lastAlarm);
		}
		//dbg_ioTask(1, "HVOUT STOP %x %s\r\n", state, __FUNCTION__);
	}
	else {
		sprintf(lcdLineBuf0, "%s", strLcdShowLine0[StateToMenu(state)]);
	}
	
	

#if CLCD_NO_AUTO_REFRESH	
	// refresh only string changed...
	if(strcmp(lcdLineBuf0Prv, lcdLineBuf0)!=0) {
		UpdateLcdData(lcdLineBuf0, 0);
		strcpy(lcdLineBuf0Prv, lcdLineBuf0);
		dbg_ioTask(10, "%s %s %d \r\n", __func__, strLcdShowLine0[StateToMenu(state)], state);	
	}
	if(strcmp(lcdLineBuf1Prv, lcdLineBuf1)!=0) {
		UpdateLcdData(lcdLineBuf1, 1);	
		strcpy(lcdLineBuf1Prv, lcdLineBuf1);
		dbg_ioTask(10, "%s %s \r\n", __func__, lcdLineBuf1);	
	}
			

#else
		UpdateLcdData(lcdLineBuf0, 0);	
		UpdateLcdData(lcdLineBuf1, 1);	
#endif	
	return state;
}


void swap_sign(U08* a, U08*b) 
{
	*a = (*a) ^ 0x80;
	*b = (*b) ^ 0x80;
	#if 0
	U08 tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
	#endif
	//dbg_ioTask(11, "SWAP <-> %d d\r\n", *a, *b);	
}
void swap(U08* a, U08*b) 
{
	U08 tmp;
	tmp = *a;
	*a = *b;
	*b = tmp;
	//dbg_ioTask(11, "SWAP <-> %d d\r\n", *a, *b);	
}
void ShowHvVolt(StructEscInfo *pInfo)
{	
	uint8_t networkMode = 'N';	
	
	if(g_APMode) {
		networkMode = 'A';
	}
	else {
		if(pInfo->setting.lanLora == COMM_MODE_LORA) {
			networkMode = 'W';
		}
		else if(pInfo->setting.lanLora == COMM_MODE_LAN) {
			networkMode = 'N';
		}
		else {
			networkMode = ' ';
		}
	}
	
	chA_V = (pInfo->measure.voltA)&0x7f;
	chB_V = (pInfo->measure.voltB)&0x7f;
	chA_uA = pInfo->measure.currA;
	chB_uA = pInfo->measure.currB;
	
	U08 c1='+';
	U08 c2='-';

	if(IS_TOGGLE)
	{
		swap(&c1, &c2);
	}
		
	U08 d1,d2, d3, d4, d5;
	
	d1 = (chA_V/10)%10;
	d2 = chA_V%10;
	d3 = (chA_uA/100)%10;
	d4 = (chA_uA/10)%10;
	d5 = chA_uA%10;
	
	sprintf(lcdLineBuf0, "A:%c%d.%d0kV %d.%d%dmA  %c%c", c1, d1, d2, d3,d4,d5
							,pInfo->stat.autoQueryControl?'Q':' ', IS_TOGGLE?'T':' ');
	
	
	d1 = (chB_V/10)%10;
	d2 = chB_V%10;
	
	d3 = (chB_uA/100)%10;
	d4 = (chB_uA/10)%10;
	d5 = chB_uA%10;

	sprintf(lcdLineBuf1, "B:%c%d.%d0kV %d.%d%dmA   %c", c2, d1, d2, d3,d4,d5,
		networkMode);
	
	//dbg_ioTask(1, "I = %d %d \r\n", pInfo->measure.currA, pInfo->measure.currB);
}			

#if CLCD_USE_MACRO	==0
void	LcdPuts(char *x) 
{ 
	uint8_t __index_;  
	for(__index_=0;__index_<strlen(x);__index_++) {
		clcdWriteChar(x[__index_]); 
	}
}

void UpdateLcdData(char *str,uint8_t line)
{
	uint8_t ret = 0;
	char i;	
	uint8_t nSpace = 0;	 
	clcdSetCursor(line,0); 
	LcdPuts(str);	
	nSpace = sizeof(str) - strlen(str);
	if(nSpace!=0) {	
		ret += nSpace;	
		for(i=0;i<nSpace;i++)LcdPuts(" ");
	} 
}
#endif
	
uint8_t OnRemoteCLcd(uint8_t state, StructEscInfo *pInfo)
{
	static U08 stateSave=0;
	//U08 key = KEY_SW_HV_PWR;
	uint8_t nextState;
	
	dbg_ioTask(2, "Remote CLCD control %d %d\r\n", state, IS_HV_ON);
	
	if(state==CLCD_STATE_AUTO_DISCHARGING) {
		stateSave = CLCD_STATE_INITIAL;
		nextState = state;
	}	
	else if(state==CLCD_STATE_STOP) {
		stateSave = CLCD_STATE_INITIAL;
		nextState = state;
	}
	else {			
		if(IsHvOn) {
			stateSave = state;
			state = CLCD_STATE_HV_OUTPUT;
			nextState = CLCD_STATE_HV_OUTPUT;
		}
		else {
			state = stateSave;	
			nextState = CLCD_STATE_INITIAL;		
		}	
	}
	clcdState = nextState;
	
	switch(nextState) {
		case CLCD_STATE_INITIAL:
			bCursorOn = 0;
			nCursorPosY=0;
			nCursorPosX=0;
		break;
		case CLCD_STATE_HV_OUTPUT:
			bCursorOn = 1;
			cusorOutputVIndex = 0;
			
			nCursorPosY=0;
			nCursorPosX=cusorOutputV[cusorOutputVIndex];
		break;
		default:
			bCursorOn = 0;
			nCursorPosY=0;
			nCursorPosX=0;
			
			break;
	}
	updateCLcdNow = 1;
	
	
	return state;
}

uint8_t StateToMenu(uint8_t state)
{
	uint8_t menu = 0;
	
	switch(state)
	{	
		case CLCD_STATE_SETUP_1:
		case CLCD_STATE_SETUP_2:
		case CLCD_STATE_SETUP_3:
		case CLCD_STATE_SETUP_4:
		case CLCD_STATE_SETUP_5:
		case CLCD_STATE_SETUP_6:
		case CLCD_STATE_SETUP_7:
		case CLCD_STATE_SETUP_8:
			menu = 	MENU_TITLE_SETUP;
		break;
							
	// Sub menu states...
	
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE:
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE_CURSOR:
			menu = 	MENU_TITLE_AT;
		break;
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2:
		case CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2_CURSOR:
			menu = 	MENU_TITLE_AT2;
		break;
	
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE:
		case CLCD_STATE_SETUP_0_SUB_VOLTAGE_CURSOR:
			menu = 	MENU_TITLE_VOLT;
		break;
	
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_CONTROL;
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP1;
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP1SEC;
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP2;
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP2SEC;
		break;
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP3;
		break;
			
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC:
		case CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR:
			menu = 	MENU_TITLE_DISCHARGE_STEP3SEC;
		break;

	
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP:
		case CLCD_STATE_SETUP_2_SUB_RAMP_UP_C:
			menu = 	MENU_TITLE_RAMPUP;
		break;
		
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN:
		case CLCD_STATE_SETUP_2_SUB_RAMP_DOWN_C:
			menu = 	MENU_TITLE_RAMPDOWN;
		break;
		
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL:
		case CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C:
			menu = 	MENU_TITLE_OCP_CONTROL;
		break;

		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT:
		case CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C:
			menu = 	MENU_TITLE_OCP_EVENT;
		break;
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT:
		case CLCD_STATE_SETUP_3_SUB_OCP_LIMIT_C:
			menu = 	MENU_TITLE_OCP_LIMIT;
		break;
			
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT:
		case CLCD_STATE_SETUP_3_SUB_OCP_COUNT_C:
			menu = 	MENU_TITLE_OCP_COUNT;
		break;

		case CLCD_STATE_SETUP_3_SUB_OCP_STATUS:
		case CLCD_STATE_SETUP_3_SUB_OCP_STATUS_C:
			menu = 	MENU_TITLE_OCP_STATUS;
		break;
			
		
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL:
		case CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C:
			menu = 	MENU_TITLE_ARC_CONTROL;
		break;
			
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT:
		case CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C:
			menu = 	MENU_TITLE_ARC_EVENT;
		break;
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT:
		case CLCD_STATE_SETUP_4_SUB_ARC_LIMIT_C:
			menu = 	MENU_TITLE_ARC_LIMIT;
		break;
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT:
		case CLCD_STATE_SETUP_4_SUB_ARC_COUNT_C:
			menu = 	MENU_TITLE_ARC_COUNT;
		break;
		case CLCD_STATE_SETUP_4_SUB_ARC_STATUS:
		case CLCD_STATE_SETUP_4_SUB_ARC_STATUS_C:
			menu = 	MENU_TITLE_ARC_STATUS;
		break;
		
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL:
		case CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C:
			menu = 	MENU_TITLE_BATT_CONTROL;
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT:
		case CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C:
			menu = 	MENU_TITLE_BATT_EVENT;
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT:
		case CLCD_STATE_SETUP_5_SUB_BATT_LIMIT_C:
			menu = 	MENU_TITLE_BATT_LIMIT;
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_STATUS:
		case CLCD_STATE_SETUP_5_SUB_BATT_ALARM_STATUS:
			menu = 	MENU_TITLE_BATT_STATUS;
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_VOLTAGE:
			menu = 	MENU_TITLE_BATT_VOLT;
		break;
		case CLCD_STATE_SETUP_5_SUB_BATT_CURRENT:
			menu = 	MENU_TITLE_BATT_CURRENT;
		break;
		

		
		case CLCD_STATE_SETUP_6_SUB_TEMP:
			menu = 	MENU_TITLE_SENSOR_TEMP;
		break;
		case CLCD_STATE_SETUP_6_SUB_PRESSURE:
			menu = 	MENU_TITLE_SENSOR_PRESS;
		break;
		
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT:
		case CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT_C:
			menu = 	MENU_TITLE_MODE_DEFAULT;
		break;
		
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD:
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C:
		case CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN:
			menu = 	MENU_TITLE_MODE_WAVE_LOAD;
		break;
		
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD:
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C:
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN:
			menu = 	MENU_TITLE_MODE_USER_LOAD;
		break;
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE:
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C:
		case CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN:
			menu = 	MENU_TITLE_MODE_USER_SAVE;
		break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_SERIAL:
			menu = 	MENU_TITLE_INFO_SERIAL;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_HW:
			menu = 	MENU_TITLE_INFO_VER_HW;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_FW:
			menu = 	MENU_TITLE_INFO_VER_FW;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_DID:
		case CLCD_STATE_SETUP_8_SUB_INFO_DID_C:
			menu = 	MENU_TITLE_INFO_DID;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE:
		case CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE_C:
			menu = 	MENU_TITLE_INFO_BAUDRATE;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_IP:
		case CLCD_STATE_SETUP_8_SUB_INFO_IP_C:
			menu = 	MENU_TITLE_INFO_IP;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SM:
		case CLCD_STATE_SETUP_8_SUB_INFO_SM_C:
			menu = 	MENU_TITLE_INFO_SM;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_GW:
		case CLCD_STATE_SETUP_8_SUB_INFO_GW_C:
			menu = 	MENU_TITLE_INFO_GW;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT:
		case CLCD_STATE_SETUP_8_SUB_INFO_PORT_C:
			menu = 	MENU_TITLE_INFO_PORT;
			break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C:
			menu = 	MENU_TITLE_INFO_SERVER_IP;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C:
			menu = 	MENU_TITLE_INFO_SERVER_SM;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW:
		case CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C:
			menu = 	MENU_TITLE_INFO_SERVER_GW;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C:
			menu = 	MENU_TITLE_INFO_LE_IP;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C:
			menu = 	MENU_TITLE_INFO_LE_SM;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C:
			menu = 	MENU_TITLE_INFO_LE_GW;
		break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT:
		case CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT_C:
			menu = 	MENU_TITLE_INFO_LE_PORT;
			break;
		
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE:
		case CLCD_STATE_SETUP_8_SUB_INFO_MODE_C:
			menu = 	MENU_TITLE_INFO_MODE;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL:
		case CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL_C:
			menu = 	MENU_TITLE_INFO_PROTOCOL;
		break;
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT:
		case CLCD_STATE_SETUP_8_SUB_INFO_REBOOT_C:
			menu = 	MENU_TITLE_INFO_REBOOT;
		break;
	
		case CLCD_STATE_NA: // = 0xff // Not Available		
		default:
			menu = 0;
		break;
	}
	
	return menu;
}

void ClearAlarm(StructEscInfo *pInfo)
{
// Clear all Alarms on Cmd
	InitRising();
	bOcpPrev = 0;
	prevTickOcp = HAL_GetTick();
	if(pInfo->measure.ocpAlarmStatus) {
		pInfo->measure.ocpAlarmStatus = 0;
		//pInfo->stat.ocpAlarmCount=0;
		g_lastAlarm = 0;
	}
	prevTickArc = HAL_GetTick();
	if(pInfo->measure.arcAlarmStatus) {
		pInfo->measure.arcAlarmStatus = 0;
		//pInfo->stat.arcAlarmCount=0;
		g_lastAlarm = 0;
	}
	if(pInfo->measure.battLowAlarmStatus) {
		pInfo->measure.battLowAlarmStatus = 0;
		g_lastAlarm = 0;
	}
	
}

#define MPL3115_ID	0xC0

uint8_t MPL3115A_Init(void)
{
	uint8_t stat = 0;

	stat = MPL3115A_Write_One(0x26, 0x38); //OSR=128, ALT(#7bit)=0
	if(stat!=0) {
		dbg_ioTask(1, "MPL3115 W ret %x\r\n", stat);
		return stat;
	}
	osDelay(10);

	stat = MPL3115A_Write_One(0x13, 0x07); // PT Data Cfg
	if(stat!=0) {
		dbg_ioTask(1, "MPL3115 W ret %x\r\n", stat);
		return stat;
	}
	osDelay(10);

	stat = MPL3115A_Write_One(0x26, 0x39); // Set Active, ALT(#7bit)=0
	if(stat!=0) {
		dbg_ioTask(1, "MPL3115 W ret %x\r\n", stat);
		return stat;
	}
	osDelay(10);
	
//	stat = MPL3115A_Read_Byte(0x00);
	g_press = MPL3115A_Read_Press();
	
	return stat;
}

float MPL3115A_Read_Press(void)
{
	uint8_t stat = 0;
	float pressure;	
	uint8_t msb, csb, lsb;
	
	stat = MPL3115A_Read_Byte(0x00);
	
	if((stat & 0x08) == 0) {
		pressure = 0;
		g_pressError = 1;
		return pressure;
	}
	else {
		msb = MPL3115A_Read_Byte(0x01);
		csb = MPL3115A_Read_Byte(0x02);
		lsb = MPL3115A_Read_Byte(0x03);
		
		// Pressure comes back as a left shifted 20 bit number
		long pressure_whole = (long)msb<<16 | (long)csb<<8 | (long)lsb;
		pressure_whole >>= 6; //Pressure is an 18 bit number with 2 bits of decimal. Get rid of decimal portion.

		lsb &= 0x30; //Bits 5/4 represent the fractional component
		lsb >>= 4; //Get it right aligned
		float pressure_decimal = (float)lsb/4.0; //Turn it into fraction

		pressure = (float)pressure_whole + pressure_decimal; // [Pa]
		pressure *= 0.00750062; // to [mmHg]
		
		g_pressError = 0;
	}
	
	return pressure;
}

float MPL3115A_Read_Temp(void)
{
	uint8_t stat = 0;
	float temperature = 0;
	uint8_t msb, csb, lsb;	
	
	stat = MPL3115A_Read_Byte(0x00);
	
	if((stat & 0x08) == 0) {
		temperature = 0;
		return temperature;
	}
	else {
		msb = MPL3115A_Read_Byte(0x04);
		lsb = MPL3115A_Read_Byte(0x05);

		//Negative temperature fix by D.D.G.
		uint16_t foo = 0;
		uint8_t negSign = 0;

		//Check for 2s compliment
		if(msb > 0x7F)
		{
			foo = ~((msb << 8) + lsb) + 1;  //2?s complement
			msb = foo >> 8;
			lsb = foo & 0x00F0; 
			negSign = 1;
		}

		float templsb = (lsb>>4)/16.0; //temp, fraction of a degree

		temperature = (float)(msb + templsb);

		if (negSign) temperature = 0 - temperature;		
	}
	
	return temperature;
}

uint8_t MPL3115A_Read_Byte (uint16_t eep_address)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint8_t RByte=0;

  //status = HAL_I2C_Mem_Read_IT(&hi2c2, (MPL3115_ID), (uint16_t)(eep_address & 0xFF), I2C_MEMADD_SIZE_8BIT, &RByte, 1);
	// Use Falling instead of IT: To Prevent CONFLICT with EEPROM
  status = HAL_I2C_Mem_Read(&hi2c2, (MPL3115_ID), (uint16_t)(eep_address & 0xFF), I2C_MEMADD_SIZE_8BIT, &RByte, 1, 32);
  HAL_Delay(10); //org = 5

  if (status == HAL_OK)
  {
    return (RByte);
  }
  else
  {
    return (HAL_ERROR);
  }
}

HAL_StatusTypeDef MPL3115A_Write_One (uint16_t eep_address, uint8_t byte_data)
{
  /* Transmit setup */
  HAL_StatusTypeDef status = HAL_OK;
  uint16_t i;

    status = HAL_I2C_Mem_Write_IT(&hi2c2, (MPL3115_ID), (uint16_t)(eep_address & 0xFF), I2C_MEMADD_SIZE_8BIT, &byte_data, 1);
    // Use Falling instead of IT: To Prevent CONFLICT with EEPROM
	status = HAL_I2C_Mem_Write(&hi2c2, (MPL3115_ID), (uint16_t)(eep_address & 0xFF), I2C_MEMADD_SIZE_8BIT, &byte_data, 1, 32);
	HAL_Delay(5); 
    return(status);
  
}

