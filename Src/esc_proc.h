#ifndef _ESC_PROC_H
#define _ESC_PROC_H

  /* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "apollo.h"


#include "queue.h"

#include "dataformat.h"
#include "esc_info.h"


/****************************************/
// Defines

/****************************************/

/// Macro

#define SetOutput(cmd, param)	do {struct OutputMessage outputMsg;  outputMsg.commandID = cmd; outputMsg.value = param; osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL); }while(0);
#define SetOutput2(cmd, param, param2)	do {struct OutputMessage outputMsg;  outputMsg.commandID = cmd; outputMsg.value = param;outputMsg.value2 = param2; osMessageQueuePut(outputQueueHandle, &outputMsg, NULL, NULL); }while(0);

/****************************************/

void TaskComm(void *argument);

void DoCustomProc(StructApolloPacket* packet);
void Ack(void);
void Nack(U08 result);
void SendCustom(StructApolloPacket* packet);

void ResponsCmdByte(U08 cmd, U08 mid, U08 data);
void SendStatusAutoProc(StructApolloPacket* packet);

void RespEventReport(void);

/****************************************/

/****************************************/
// Extern vars...
extern UART_HandleTypeDef huart5;
extern UART_HandleTypeDef huart6;


extern osMessageQueueId_t outputQueueHandle;
extern osMessageQueueId_t inputQueueHandle;

#define RXLEN	256
#define TXLEN	128
extern uint8_t RxData;
extern uint8_t EndOfTrans;
extern uint16_t RxIdx;
extern uint8_t AT_Data_buf[RXLEN];


/****************************************/
// Extern Fns...
extern void clcdSetCursor(uint8_t line, uint8_t pos);



#endif // _ESC_PROC_H

