#ifndef _ESC_INFO_H
#define _ESC_INFO_H

#include <stdarg.h>

#include "dataformat.h"
#include "apollo.h"

#ifdef __cplusplus
extern "C"
{
#endif


/****************************************/
// Defines


#define KEY_SW_UP  			(1<<KEY_SW_UP_BIT)
#define KEY_SW_DOWN 		(1<<KEY_SW_DOWN_BIT)
#define KEY_SW_LEFT 		(1<<KEY_SW_LEFT_BIT)
#define KEY_SW_RIGHT 		(1<<KEY_SW_RIGHT_BIT)
#define KEY_SW_HV_PWR 		(1<<KEY_SW_HV_PWR_BIT)
#define KEY_SW_TOGGLE 		(1<<KEY_SW_TOGGLE_BIT)
#define KEY_SW_SELECT		(1<<KEY_SW_SELECT_BIT)
#define KEY_SW_ESC_CANCEL	(1<<KEY_SW_ESC_CANCEL_BIT)


/*
	* Read Key
	1. SW_UP: PF6
	2. SW_DOWN: PF7
	3. SW_LEFT: PF8
	4. SW_RIGHT: PF9
	5. SW_HV_ON_OFF: PF10
	6. SW_TOGGLE: PF11
	7. SW_SEL_SET: PF15
	8. SW_ESC_CANCEL: PC5
	*/

#define		KEY_SW_UP_BIT 			0	//0x01
#define		KEY_SW_DOWN_BIT			1	//0x02
#define		KEY_SW_LEFT_BIT			2	//0x04
#define		KEY_SW_RIGHT_BIT		3	//0x08
#define		KEY_SW_TOGGLE_BIT		4	//0x10
#define		KEY_SW_ESC_CANCEL_BIT	5	//0x20
#define		KEY_SW_HV_PWR_BIT		6	//0x40
#define		KEY_SW_SELECT_BIT		7	//0x80

#define	ALLOWED_KEY_ALL		(0xff)
	
#define	ALLOWED_KEY_ARROWS_CURSOR	(KEY_SW_LEFT | KEY_SW_RIGHT)
#define	ALLOWED_KEY_ARROWS_VALUE	(KEY_SW_UP | KEY_SW_DOWN)
#define	ALLOWED_KEY_ARROWS			(ALLOWED_KEY_ARROWS_VALUE | ALLOWED_KEY_ARROWS_CURSOR)
	
#define	ALLOWED_KEY_MENU	(KEY_SW_HV_PWR | KEY_SW_TOGGLE | KEY_SW_SELECT)
	
#define ALLOWED_KEY_MID_INIT	(KEY_SW_SELECT | KEY_SW_ESC_CANCEL | KEY_SW_RIGHT)
#define	ALLOWED_KEY_MID			(KEY_SW_SELECT | KEY_SW_ESC_CANCEL | ALLOWED_KEY_ARROWS_CURSOR)
#define ALLOWED_KEY_MID_END		(KEY_SW_SELECT | KEY_SW_ESC_CANCEL | KEY_SW_LEFT)
	
#define	ALLOWED_KEY_DETAIL	(KEY_SW_SELECT | KEY_SW_ESC_CANCEL | ALLOWED_KEY_ARROWS)


#define u32	U32
#define u8	U08

/* Base address of the Flash sectors */
#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

#define FLASH_START_ADDR        ((u32)0x08000000)
#define FLASH_END_ADDR          ((u32)0x080FFFFF)
#define ENV_FLASH_START_ADDR    ((u32)0x080E0000)
#define ENV_FLASH_END_ADDR      ((u32)0x080FFFFF)
#define ENV_FLASH_SECTOR        FLASH_Sector_11

#define ENV_MAX_OPTION_SIZE     (16)
#define ENV_MAX_SERIAL          (16)

#define ENV_SN_ADDR             (0)
#define ENV_PROMPT_ADDR         (1)
#define ENV_DFTP_ADDR           (2)
#define ENV_LOGIN_NAME_ADDR     (3)
#define ENV_LOGIN_PASS_ADDR     (4)
#define ENV_MAC_ADDR            (5)
#define ENV_IP_ADDR             (6)
#define ENV_GW_ADDR             (7)
#define ENV_SM_ADDR             (8)

#define ENV_UNIQUE_ID_ADDR      (0x1FFF7A10)

#define ENV_MAX_SIZE        (sizeof(env_item_t))
	
#define APP_PGM_MAX_SIZE        (0x20000 * 3)
#define BOOT_START_ADDR         (0x8000000)
#define APP_START_ADDR          (0x8020000)

#define APP_FLASH_START_ADDR    ((U32)0x08020000)
#define APP_FLASH_END_ADDR      ((U32)(APP_FLASH_START_ADDR + APP_PGM_MAX_SIZE))

#define APP_BACKUP_START_ADDR   ((U32)APP_FLASH_END_ADDR)
#define APP_BACKUP_END_ADDR     ((U32)(APP_BACKUP_START_ADDR + APP_PGM_MAX_SIZE))
	
/****************************************/	
/// Macro


/****************************************/	
/// type defines
typedef struct
{
    U08                      buf[ENV_MAX_OPTION_SIZE];
} env_option_t;             

typedef struct              
{                           
    U08                      mac[6];
    U08                      ip[4];
    U08                      gw[4];
    U08                      sm[4];
} ether_set_t;              

typedef struct              
{                          
    U08                      ip[4];
} ether_dest_t;             

typedef struct              
{                           
    U08                      mac[6];
} ether_br_set_t;           

typedef struct              
{                           
    env_option_t            serialNumber;
    env_option_t            prompt;
    env_option_t            appName;
    ether_set_t             etherSet;
} env_item_t;


/// Monitor
typedef struct
{
    U08      cmd;
    U08      sub;
    U16     len;
    U32     addr;
    
    U08      extra[24]; 
    
} net_mon_p_t;

#define MAX_ETHER_BUF_SIZE          (1600)
typedef struct
{
    u8  rxValid;
    u8  txValid;
    U16 rxLen;
    U16 txLen;
    u8  rxBuf[MAX_ETHER_BUF_SIZE];
    u8  txBuf[MAX_ETHER_BUF_SIZE];
} ethBuf_t;


typedef enum
{
    NET_TCP_CMD_NONE = 0,
    NET_TCP_CMD_STR,
    NET_TCP_CMD_STR_ACK,
    NET_TCP_CMD_DOWNLOAD,
    NET_TCP_CMD_DOWNLOAD_ACK,
    NET_TCP_CMD_KEEP_ALIVE,
    NET_TCP_CMD_REQ_TIME = 50,
    NET_TCP_CMD_RES_TIME,
    NET_TCP_CMD_END
} net_tcp_cmd_t;

typedef enum
{
    NET_DL_TYPE_NONE = 0,
    NET_DL_TYPE_APP,
    NET_DL_TYPE_FONT,
    NET_DL_TYPE_END
} net_dl_type_t;

 
typedef enum
{
    NET_DL_CMD_NONE = 0,
    NET_DL_CMD_START,
    NET_DL_CMD_ERASE,
    NET_DL_CMD_DATA,
    NET_DL_CMD_DATA_END,
    NET_DL_CMD_RESET,
    NET_DL_CMD_TIMEOUT,
    NET_DL_CMD_ERROR,
    NET_DL_CMD_SEND,
    NET_DL_CMD_END
} net_dl_cmd_t;

 enum {
	 ESC_MODE_LOCAL = 0,
	 ESC_MODE_REMOTE,
	 ESC_MODE_LAN
};

typedef struct __CLCD_SETS {
	U08 state;
	U08 prevState;
	U08 validKeys; 
	U08 subMenus[8]; // UP, Down, Left, Right,  HV_PWR, Toggle, Select, Esc
} StructCLcdSets; 


/// 메뉴트리 순서 상태: __MENU_TITLE_ORDER, strLcdShowLine0, CLCD_MENU_TREES, __CLCD_STATE_ENUM 와 같이 수정 필요!
enum __CLCD_STATE_ENUM {
	CLCD_STATE_INITIAL = 0,
	CLCD_STATE_HV_OUTPUT,
	
	CLCD_STATE_TG,	//2
	CLCD_STATE_SETUP_0, /*	<SETUP / status>
							OUTPUT
	*/
	
	CLCD_STATE_SETUP_1, /*	<SETUP / status>
							AUTO-DISCHARGE
	*/
	CLCD_STATE_SETUP_2, /*	<SETUP / status>
							RAMP-TIME
	*/
	CLCD_STATE_SETUP_3, /*	<SETUP / status>
							OCP
	*/
	CLCD_STATE_SETUP_4, /*	<SETUP / status>
							ARC
	*/
	CLCD_STATE_SETUP_5, /*	<SETUP / status>
							BATTERY
	*/
	CLCD_STATE_SETUP_6, /*	<SETUP / status>
							sensor
	*/
	CLCD_STATE_SETUP_7, /*	<SETUP / status>
							MODE
	*/
	CLCD_STATE_SETUP_8, /*	<SETUP / status>
							INFORMATION
	*/
	
	// Sub menu states...
	
	CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE, // Auto toggle 1
	CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE_CURSOR, // Auto toggle
	
	CLCD_STATE_SETUP_0_SUB_VOLTAGE,	// voltage
	CLCD_STATE_SETUP_0_SUB_VOLTAGE_CURSOR,	// voltage //15

	CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2, // Auto toggle 2
	CLCD_STATE_SETUP_0_SUB_AUTO_TOGGLE2_CURSOR, // Auto toggle 2
	
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_CONTROL_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_1_SEC_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_2_SEC_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_CURSOR, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC, 
	CLCD_STATE_SETUP_1_SUB_DISCHAGE_STEP_3_SEC_CURSOR, 
	

	CLCD_STATE_SETUP_2_SUB_RAMP_UP,	
	CLCD_STATE_SETUP_2_SUB_RAMP_UP_C,	
	CLCD_STATE_SETUP_2_SUB_RAMP_DOWN,
	CLCD_STATE_SETUP_2_SUB_RAMP_DOWN_C,
	
	CLCD_STATE_SETUP_3_SUB_OCP_CONTROL,
	CLCD_STATE_SETUP_3_SUB_OCP_CONTROL_C,
	CLCD_STATE_SETUP_3_SUB_OCP_EVENT,
	CLCD_STATE_SETUP_3_SUB_OCP_EVENT_C,
	CLCD_STATE_SETUP_3_SUB_OCP_LIMIT,
	CLCD_STATE_SETUP_3_SUB_OCP_LIMIT_C,
	CLCD_STATE_SETUP_3_SUB_OCP_COUNT,
	CLCD_STATE_SETUP_3_SUB_OCP_COUNT_C,
	CLCD_STATE_SETUP_3_SUB_OCP_STATUS,
	CLCD_STATE_SETUP_3_SUB_OCP_STATUS_C,
	
	CLCD_STATE_SETUP_4_SUB_ARC_CONTROL,
	CLCD_STATE_SETUP_4_SUB_ARC_CONTROL_C,
	CLCD_STATE_SETUP_4_SUB_ARC_EVENT,
	CLCD_STATE_SETUP_4_SUB_ARC_EVENT_C,
	CLCD_STATE_SETUP_4_SUB_ARC_LIMIT,
	CLCD_STATE_SETUP_4_SUB_ARC_LIMIT_C,
	CLCD_STATE_SETUP_4_SUB_ARC_COUNT,
	CLCD_STATE_SETUP_4_SUB_ARC_COUNT_C,
	CLCD_STATE_SETUP_4_SUB_ARC_STATUS,
	CLCD_STATE_SETUP_4_SUB_ARC_STATUS_C,
	
	CLCD_STATE_SETUP_5_SUB_BATT_CONTROL,
	CLCD_STATE_SETUP_5_SUB_BATT_CONTROL_C,
	CLCD_STATE_SETUP_5_SUB_BATT_EVENT,
	CLCD_STATE_SETUP_5_SUB_BATT_EVENT_C,
	CLCD_STATE_SETUP_5_SUB_BATT_LIMIT,
	CLCD_STATE_SETUP_5_SUB_BATT_LIMIT_C,
	CLCD_STATE_SETUP_5_SUB_BATT_STATUS,
	CLCD_STATE_SETUP_5_SUB_BATT_ALARM_STATUS,
	CLCD_STATE_SETUP_5_SUB_BATT_VOLTAGE,
	CLCD_STATE_SETUP_5_SUB_BATT_CURRENT,
	
	//TODO: batt alarm status, batt voltage, batt current
	
	
	CLCD_STATE_SETUP_6_SUB_TEMP,
	CLCD_STATE_SETUP_6_SUB_PRESSURE,
	
	CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT,
	CLCD_STATE_SETUP_7_SUB_MODE_DEFAULT_C,
	
	CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD,
	CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C,
	CLCD_STATE_SETUP_7_SUB_MODE_WAVE_LOAD_C_YN,
	
	CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD,
	CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C,
	CLCD_STATE_SETUP_7_SUB_MODE_USER_LOAD_C_YN,
	CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE,
	CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C,
	CLCD_STATE_SETUP_7_SUB_MODE_USER_SAVE_C_YN,
	
	CLCD_STATE_SETUP_8_SUB_INFO_SERIAL,
	CLCD_STATE_SETUP_8_SUB_INFO_HW,
	CLCD_STATE_SETUP_8_SUB_INFO_FW,
	CLCD_STATE_SETUP_8_SUB_INFO_DID,
	CLCD_STATE_SETUP_8_SUB_INFO_DID_C,
	CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE,
	CLCD_STATE_SETUP_8_SUB_INFO_BAUDRATE_C,
	CLCD_STATE_SETUP_8_SUB_INFO_IP,
	CLCD_STATE_SETUP_8_SUB_INFO_IP_C,
	CLCD_STATE_SETUP_8_SUB_INFO_SM,
	CLCD_STATE_SETUP_8_SUB_INFO_SM_C,
	CLCD_STATE_SETUP_8_SUB_INFO_GW,
	CLCD_STATE_SETUP_8_SUB_INFO_GW_C,

	CLCD_STATE_SETUP_8_SUB_INFO_PORT,
	CLCD_STATE_SETUP_8_SUB_INFO_PORT_C,

	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP,
	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_IP_C,
	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM,
	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_SM_C,
	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW,
	CLCD_STATE_SETUP_8_SUB_INFO_SERVER_GW_C,

	CLCD_STATE_SETUP_8_SUB_INFO_LE_IP,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_IP_C,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_SM,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_SM_C,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_GW,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_GW_C,

	CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT,
	CLCD_STATE_SETUP_8_SUB_INFO_LE_PORT_C,
	
	CLCD_STATE_SETUP_8_SUB_INFO_MODE,
	CLCD_STATE_SETUP_8_SUB_INFO_MODE_C,
	
	CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL,
	CLCD_STATE_SETUP_8_SUB_INFO_PROTOCOL_C,

	CLCD_STATE_SETUP_8_SUB_INFO_REBOOT,
	CLCD_STATE_SETUP_8_SUB_INFO_REBOOT_C,
	
	CLCD_STATE_AUTO_DISCHARGING = 0xF0, // Auto Discharge
	CLCD_STATE_STOP = 0xF1, // Stop processing... -> Alarm
	
	CLCD_STATE_NA = 0xFE, // Not Available
	CLCD_STATE_BOOT = 0xFF
	
};

extern const StructCLcdSets CLCD_MENU_TREES[];
					
/****************************************/
/// Constants...

// Menu title constants: __MENU_TITLE_ORDER, strLcdShowLine0, CLCD_MENU_TREES, __CLCD_STATE_ENUM 와 같이 수정 필요!
enum __MENU_TITLE_ORDER{
	MENU_TITLE_SETUP = 0,

	MENU_TITLE_AT,
	MENU_TITLE_VOLT,
	MENU_TITLE_AT2,
	
	MENU_TITLE_DISCHARGE_CONTROL, //3
	MENU_TITLE_DISCHARGE_STEP1,
	MENU_TITLE_DISCHARGE_STEP1SEC,
	MENU_TITLE_DISCHARGE_STEP2,
	MENU_TITLE_DISCHARGE_STEP2SEC,
	MENU_TITLE_DISCHARGE_STEP3,
	MENU_TITLE_DISCHARGE_STEP3SEC,

	MENU_TITLE_RAMPUP,	//7 //10
	MENU_TITLE_RAMPDOWN,

	MENU_TITLE_OCP_CONTROL, //9   //12
	MENU_TITLE_OCP_EVENT,
	MENU_TITLE_OCP_LIMIT,
	MENU_TITLE_OCP_COUNT,
	MENU_TITLE_OCP_STATUS,
	

	MENU_TITLE_ARC_CONTROL,
	MENU_TITLE_ARC_EVENT,
	MENU_TITLE_ARC_LIMIT,
	MENU_TITLE_ARC_COUNT,
	MENU_TITLE_ARC_STATUS,	
	
	MENU_TITLE_BATT_CONTROL,
	MENU_TITLE_BATT_EVENT,
	MENU_TITLE_BATT_LIMIT,
	MENU_TITLE_BATT_STATUS,	
	MENU_TITLE_BATT_ALARM_STATUS,	
	MENU_TITLE_BATT_VOLT,	
	MENU_TITLE_BATT_CURRENT,	

	MENU_TITLE_SENSOR_TEMP,
	MENU_TITLE_SENSOR_PRESS,
	
	MENU_TITLE_MODE_DEFAULT,
	MENU_TITLE_MODE_WAVE_LOAD,
	MENU_TITLE_MODE_USER_LOAD,
	MENU_TITLE_MODE_USER_SAVE,

	MENU_TITLE_INFO_SERIAL,
	MENU_TITLE_INFO_VER_HW,
	MENU_TITLE_INFO_VER_FW,
	MENU_TITLE_INFO_DID,
	MENU_TITLE_INFO_BAUDRATE,
	
	MENU_TITLE_INFO_IP,
	MENU_TITLE_INFO_SM,
	MENU_TITLE_INFO_GW,
	
	MENU_TITLE_INFO_PORT,
	
	MENU_TITLE_INFO_SERVER_IP,
	MENU_TITLE_INFO_SERVER_SM,
	MENU_TITLE_INFO_SERVER_GW,

	MENU_TITLE_INFO_LE_IP,
	MENU_TITLE_INFO_LE_SM,
	MENU_TITLE_INFO_LE_GW,
	MENU_TITLE_INFO_LE_PORT,
	
	MENU_TITLE_INFO_MODE,
	MENU_TITLE_INFO_PROTOCOL,

	MENU_TITLE_INFO_REBOOT,
} ;



/****************************************/
/// extern vars...
extern const char strLcdShowLine0[][21];
extern const char strLcdShowLine1Setup[][21];


/****************************************/
/// FN declaration

StructEscInfo* GetEscInfo(void);

void InitEscInfo(void);
int SaveEscInfo(void);
int LoadEscInfo(void);
void ResetEscInfo(void);
int SaveEscSetting(void);


char* GetSTAT(void);
char *GetEscMeasVolt(void);
char *GetEscMeasCurr(void);

wiz_NetInfo* GetNetInfo(void);


void debug_console(U08 level, const char*format, ...);

#ifdef __cplusplus
}
#endif

#endif // _ESC_INFO_H

