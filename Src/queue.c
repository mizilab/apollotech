/**
  ******************************************************************************
  * @file    queue.c
  * @author  jw Lee
  * @version 0.0.0.1
  * @date    01.27.2017
  * @brief   Queue
  ******************************************************************************
  */

#include "queue.h"

#include "apollo.h"



long g_nUseQCount = 0;

#if USE_PLC_QUEUE==0
uint8_t plcRxBuffer[N_PLC_RX_BUFFER_SIZE];
 
uint32_t plcRxIndex;
uint32_t plcRxSize;
 
#else
static volatile char arQueue[MAX_QUEUE_SIZE];
//static unsigned int nQueueIndex = 0;
static unsigned int nEnQueueIndex = 0;
static unsigned int nDeQueueIndex = 0;
static int nQueueCount = 0;

#endif

void enQueue(char data)
{
#if USE_PLC_QUEUE==0
	plcRxBuffer[plcRxIndex++] = plcRxData;

	plcRxSize++;
	if(plcRxIndex > sizeof(plcRxBuffer)-1) plcRxIndex = 0;
#else	
		arQueue[nEnQueueIndex] = data;
		nEnQueueIndex = (nEnQueueIndex + 1 + MAX_QUEUE_SIZE) % MAX_QUEUE_SIZE;
		nQueueCount++;
#endif	
	g_nUseQCount++;
}
char deQueue(void)
{
		char data = 0;

#if USE_PLC_QUEUE		
		data = arQueue[nDeQueueIndex];
		nDeQueueIndex = (nDeQueueIndex + 1 + MAX_QUEUE_SIZE) % MAX_QUEUE_SIZE;
		nQueueCount--;
#else
		data = plcRxBuffer[ (plcRxIndex - plcRxSize + sizeof(plcRxBuffer))%sizeof(plcRxBuffer)];
		plcRxBuffer[ (plcRxIndex - plcRxSize + sizeof(plcRxBuffer))%sizeof(plcRxBuffer)] = 0x00;
		plcRxSize--;
#endif	

		return data;
}

int nQueueSize(void)
{
		int n = 0;
#if USE_PLC_QUEUE==0
		n = plcRxSize;
#else	
		n = (nEnQueueIndex - nDeQueueIndex + MAX_QUEUE_SIZE) % MAX_QUEUE_SIZE;
#endif

		return n;
}

char isQueueFull(void)
{
		if(nQueueSize()>=MAX_QUEUE_SIZE)
		{
				return 1;
		}
		else
		{
				return 0;
		}
}
char isQueueEmpty(void)
{
		if(nQueueSize()<=0) {
				return 1;
		}
		else {
				return 0;
		}
}
	



